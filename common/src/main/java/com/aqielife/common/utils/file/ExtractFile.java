package com.aqielife.common.utils.file;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author aqie
 * @Date 2022/4/22 11:22
 * @desc
 */
@Slf4j
public class ExtractFile {
    private static String SOUTH_PATH = "E:\\BaiduYunDownload\\bilibili\\nginx";
    private static String TARGET_PATH = "E:\\BaiduYunDownload\\bilibili\\tmp";
    private static int num = 0;
    public static void main(String[] args) throws IOException {
        Map<String,String> fileNameMap = new LinkedHashMap<String,String>();
        listfile(new File(SOUTH_PATH),fileNameMap);//File既可以代表一个文件也可以代表一个目录
        display(fileNameMap);
    }

    /**
     * 循环遍历文件
     * @param file
     * @param map
     */
    public static void listfile(File file,Map<String,String> map){
        //如果file代表的不是一个文件，而是一个目录
        if(!file.isFile()){
            //列出该目录下的所有文件和目录
            File[] files = file.listFiles();
            //遍历files[]数组
            assert files != null;
            for(File f : files){
                //递归
                listfile(f,map);
            }
        }else{
            String realName = file.getName();
            int lastIndexOf = realName.lastIndexOf(".");
            String suffix = realName.substring(lastIndexOf);
            if(".blv".equals(suffix)){
                log.info("a: {}", realName);
                map.put(file.toString(), realName);
            }

        }
    }

    /**
     * 展示文件
     * @param map key：原始文件路径 value: 原始文件名
     * @throws IOException
     */
    public static void display(Map<String,String> map) throws IOException{

        for (Object key : map.keySet()) {
            final String value = map.get(key);
            File f = new File(key.toString());
            if (!f.isDirectory()) {

                String prefix = value.substring(0, value.lastIndexOf("."));
                String suffix = value.substring(value.lastIndexOf("."));
                prefix = String.valueOf((Integer.parseInt(prefix) + ++num));
                log.info("{} b: {} {}", num, value, prefix + suffix);

                // 新文件
                String s = TARGET_PATH + File.separator + prefix + suffix;
                // 复制文件
                writeBytes(key.toString(), new FileOutputStream(s));
                // boolean b = new File(key.toString()).renameTo(new File(TARGET_PATH + File.separator + prefix + ".mp4"));
            }
        }
    }

    /**
     *
     * @param filePath 源文件
     * @param os       目标文件
     * @throws IOException
     */
    public static void writeBytes(String filePath, OutputStream os) throws IOException {
        FileInputStream fis = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new FileNotFoundException(filePath);
            }
            fis = new FileInputStream(file);
            byte[] b = new byte[1024];
            int length;
            while ((length = fis.read(b)) > 0) {
                os.write(b, 0, length);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
