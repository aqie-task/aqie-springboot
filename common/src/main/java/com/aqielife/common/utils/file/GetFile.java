package com.aqielife.common.utils.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/4/11 9:51
 */
public class GetFile {

    List<File> Original_List=new ArrayList<>();  //save files in Original_path
    List<File> target_List=new ArrayList<>();   //save files will to target_path

    public void getFile(String Original_path,String target_path,String format1,String format2) {
        // FileOutputStream fos=null;
        File fosFile = null;
        try {
            File dir = new File(Original_path);
            System.out.println("Read File.....");
            getAllFile(dir,Original_List);
            for (int i = 0; i < Original_List.size(); i++) {
                if (Original_List.get(i).getName().endsWith(format1)||Original_List.get(i).getName().endsWith(format2)) {
                    target_List.add(Original_List.get(i));
                }
            }

            for (File file : target_List) {
                try {
                    //fos = new FileOutputStream(target_path+file.getName());
                    fosFile = new File(target_path+file.getName());
                    // Files.copy(Paths.get(file.toURI()),fos);
                    Files.move(Paths.get(file.toURI()),Paths.get(fosFile.toURI()), StandardCopyOption.REPLACE_EXISTING);

                } catch (IOException e) {
                    System.err.println("copy Error");
                    e.printStackTrace();
                }finally {
                    // fos.close();
                    System.out.println(file.getName()+" Succeed Copy!");
                }
            }
        }catch(Exception k) {
            System.out.println("Original_path ERROR");
            k.printStackTrace();
        }

    }
    private static void getAllFile(File f,List<File> list) {
        File[] fList = f.listFiles();
        for (File file : fList) {
            list.add(file);
            if (file.isDirectory()) {
                getAllFile(file,list);
            }
        }
    }

    public static void main(String[] args) {
        GetFile g =new GetFile();
        String originPath = ReadConfig.Original_path;
        String targetPath = ReadConfig.Target_path;
        String format1;
        String format2;
        format1 = format2 = ".pdf";
        g.getFile(originPath,targetPath, format1,format2);
    }
}
