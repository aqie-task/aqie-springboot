package com.aqielife.common.utils.generic;

/**
 * @author aqie
 * @date 2020-10-20 8:58
 * @function
 */
public interface Base<T> {

    /**
     * @param variable :  变量名称
     * @description 根据泛型类和变量名称获取变量值
     * @author jia hao
     * @Date 2019/8/6 16:51
     */
    String getVariable(T t, String variable);
}
