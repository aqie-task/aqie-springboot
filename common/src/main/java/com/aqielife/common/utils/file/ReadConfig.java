package com.aqielife.common.utils.file;

import java.io.*;
import java.util.Properties;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/4/11 10:13
 */
public class ReadConfig {
    public static String Original_path;
    public static String Target_path;
    public static String format1;
    public static String format2;
    static {
        Properties properties = new Properties();
        File file = new File("common/src/main/java/com/aqielife/common/utils/file/config.properties");
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error("读取配置文件失败！");
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Original_path = properties.getProperty("Original_path");
        Target_path= properties.getProperty("Target_path");
        format1= properties.getProperty("format1");
        format2= properties.getProperty("format2");
    }

    public static void showConfig() {
        System.out.println("============================================");
        System.out.println("Original_path: " + Original_path);
        System.out.println("Target_path: " + Target_path);
        System.out.println("format1: " + format1);
        System.out.println("format2: " + Original_path);
        System.out.println("============================================");
    }
    public static void saveAsUTF8(String inputFileUrl, String outputFileUrl) throws IOException {
        /*String inputFileEncode = EncodingDetect.getJavaEncode(inputFileUrl);
        System.out.println("inputFileEncode===" + inputFileEncode);
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(new FileInputStream(inputFileUrl), inputFileEncode));
        BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(outputFileUrl), "UTF-8"));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            bufferedWriter.write(line + "\r\n");
        }
        bufferedWriter.close();
        bufferedReader.close();
        String outputFileEncode = EncodingDetect.getJavaEncode(outputFileUrl);
        System.out.println("outputFileEncode===" + outputFileEncode);
        System.out.println("txt文件格式转换完成");*/
    }

    public static void main(String[] args) {
        showConfig();
    }

}
