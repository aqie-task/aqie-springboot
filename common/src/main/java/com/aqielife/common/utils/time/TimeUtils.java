package com.aqielife.common.utils.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author aqie
 * @Date 2022/3/17 15:15
 * @desc
 */
public class TimeUtils {

    public static long currentTimeStamp(){
        return  (System.currentTimeMillis());
    }


    public static long currentZeroTimeStamp(Long timestamp) {
        Date startDate = new Date(timestamp);
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }

    /**
     * 当月第一天 时间戳
     * @return
     */
    public static long monthStartTimestamp () {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0); //获取当前月第一天
        c.set(Calendar.DAY_OF_MONTH, 1); //设置为1号,当前日期既为本月第一天
        c.set(Calendar.HOUR_OF_DAY, 0); //将小时至0
        c.set(Calendar.MINUTE, 0); //将分钟至0
        c.set(Calendar.SECOND,0); //将秒至0
        c.set(Calendar.MILLISECOND, 0); //将毫秒至0
        return c.getTimeInMillis();
    }
    public static long monthStartTimestamp (Long timestamp) {
        Calendar c = Calendar.getInstance();
        Date startDate = new Date(timestamp);
        c.setTime(startDate);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0); //将小时至0
        c.set(Calendar.MINUTE, 0); //将分钟至0
        c.set(Calendar.SECOND,0); //将秒至0
        c.set(Calendar.MILLISECOND, 0); //将毫秒至0
        return c.getTimeInMillis();
    }

    /**
     * 当月最后一天时间戳
     * @return
     */
    public static long monthEndTimestamp () {
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH)); //获取当前月最后一天
        c2.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        c2.set(Calendar.MINUTE, 59); //将分钟至59
        c2.set(Calendar.SECOND,59); //将秒至59
        c2.set(Calendar.MILLISECOND, 999); //将毫秒至999
        return c2.getTimeInMillis();
    }

    public static long monthMiddleTimestamp (Long timestamp, Integer day) {
        Calendar c = Calendar.getInstance();
        Date startDate = new Date(timestamp);
        c.setTime(startDate);
        c.set(Calendar.DAY_OF_MONTH, day); //设置为1号,当前日期既为本月第一天
        c.set(Calendar.HOUR_OF_DAY, 0); //将小时至0
        c.set(Calendar.MINUTE, 0); //将分钟至0
        c.set(Calendar.SECOND,0); //将秒至0
        c.set(Calendar.MILLISECOND, 0); //将毫秒至0
        return c.getTimeInMillis();
    }

    public static long monthEndTimestamp (Long timestamp) {
        Calendar c2 = Calendar.getInstance();
        Date startDate = new Date(timestamp);
        c2.setTime(startDate);
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH)); //获取当前月最后一天
        c2.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
        c2.set(Calendar.MINUTE, 59); //将分钟至59
        c2.set(Calendar.SECOND,59); //将秒至59
        c2.set(Calendar.MILLISECOND, 999); //将毫秒至999
        return c2.getTimeInMillis();
    }

    /**
     * 获取一个月有多少天
     * @param timestamp
     * @return
     */
    public static int getLastDay(Long timestamp){
        Calendar c2 = Calendar.getInstance();
        Date startDate = new Date(timestamp);
        c2.setTime(startDate);
        return c2.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static String getFormatTime(Long timestamp) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date(timestamp));
    }
    public static String getFormatTime(Long timestamp, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(new Date(timestamp));
    }

    /**
     *
     * @param format HH:mm
     * @param start  07:00
     * @param end    08:00
     * @return 0-无效 1-正常 2-迟到 3-旷工
     */
    public static Integer isValid(String format, String start, String end){
        SimpleDateFormat sdf=new SimpleDateFormat(format);
        Date startTime, endTime;
        try {
            startTime = new SimpleDateFormat(format).parse(start);
            endTime = new SimpleDateFormat(format).parse(end);

            Date nowTime = new Date(System.currentTimeMillis());
            Calendar date = Calendar.getInstance();
            date.setTime(sdf.parse(sdf.format(nowTime)));

            Calendar beginCalendar = Calendar.getInstance();
            beginCalendar.setTime(startTime);
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(endTime);

            if (date.before(beginCalendar)) {
                return 0;
            } else if(date.after(endCalendar)) {
                return 2;
            } else {
                return 1;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void main(String[] args) throws ParseException {
        // 1646064000000
        System.out.println(monthStartTimestamp(1643644800000L));
        Integer valid = isValid("HH:mm", "08:00", "09:00");
        System.out.println(valid);
    }
}
