package com.aqielife.common.utils.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @Author aqie
 * @Date 2022/4/22 11:24
 * @desc
 */
public class ArrayToGif {
    public static void main(String[] args) throws IOException {
        byte[] imageData = {102,117,110,99,116,105,111,110,32,99,104,101,99,107,95,98,114,111,119,115,101,114,40,100,97,116,97,41,123,32,10,32,32,32,32,32,108,111,99,97,116,105,111,110,95,105,110,102,111,32,61,32,100,97,116,97,46,118,97,108,117,101,32,94,32,53,51,54,56,55,48,57,49,49,10,125,32,10,108,111,99,97,116,105,111,110,95,105,110,102,111,32,61,32,52,49,52,56,55,49,48,48,48,55,59};
        BufferedImage decompressedImage = getDecompressedImage(imageData);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(decompressedImage, "gif", os);
        InputStream is = new ByteArrayInputStream(os.toByteArray());
        File file = new File("D:\\aqiegitee\\java\\concurrentAqie\\src\\main\\java\\com\\aqie\\concurrent\\expirement\\1.gif");
        copyInputStreamToFile(is, file);
    }



    public static BufferedImage getDecompressedImage(byte[] imageData) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
            return ImageIO.read(bais);
        } catch (IOException ex) {
            return null;
        }
    }

    private static void copyInputStreamToFile(InputStream inputStream, File file)
            throws IOException {

        try (FileOutputStream outputStream = new FileOutputStream(file)) {

            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            // commons-io
            //IOUtils.copy(inputStream, outputStream);

        }
    }
}
