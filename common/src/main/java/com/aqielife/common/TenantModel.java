package com.aqielife.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author aqie
 * @Date 2022/3/5 12:06
 * @desc
 */
@Data
public class TenantModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private String tenantId;
}
