package com.aqielife.common;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/4/5 11:45
 */
public class CodeMsg {
    private int code;
    private String msg;


    private CodeMsg() {}

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


    public CodeMsg(String msg) {
        this.msg = msg;
    }

    public CodeMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public CodeMsg fillArgs(Object... args) {
        int code = this.code;
        String message = String.format(this.msg, args);
        return new CodeMsg(code, message);
    }


    //通用
    public static CodeMsg NO_DATA = new CodeMsg(110000, "无数据");


    // 用户 1xxxxx
    public static CodeMsg USER_NOT_EXIST_ERROR = new CodeMsg(100001, "指定用户不存在");
    public static CodeMsg TOKEN_INVALID_ERROR = new CodeMsg(100002, "令牌不存在或者已经失效");
    public static CodeMsg USER_NAME_OR_PASSWORD_ERROR = new CodeMsg(100003, "用户名或密码错误");
    public static CodeMsg USER_NOT_EXIT_ERROR = new CodeMsg(100004, "用户不存在");
    public static CodeMsg PASSWORD_ERROR = new CodeMsg(100005, "密码错误");
    public static CodeMsg ALREADY_SEND_SMS_ERROR = new CodeMsg(100006, "您已发送过短信");
    public static CodeMsg USER_PHONE_NOT_EXIT_ERROR = new CodeMsg(100007, "该手机号用户不存在");
    public static CodeMsg ENUM_VALUE_NOT_EXIT_ERROR = new CodeMsg(100008, "指定枚举类不存在");
    public static CodeMsg ADDRESS_ID_NOT_EXIT_ERROR = new CodeMsg(100009, "用户收货地址不合法");
    public static CodeMsg USER_EXIT_ERROR = new CodeMsg(100010, "用户已存在: %s");
    public static CodeMsg USER_ROLE_ERROR = new CodeMsg(100011, "用户角色不存在");
    public static CodeMsg USER_BIND_ERROR = new CodeMsg(100012, "用户已绑定微信");
    public static CodeMsg SMS_CODE_ERROR = new CodeMsg(100013, "验证码不正确!");
    public static CodeMsg OPENID_NOT_EXIT_ERROR = new CodeMsg(100014, "openId不存在");
    public static CodeMsg OPENID_NOT_BIND_ERROR = new CodeMsg(100015, "openId未绑定");
    public static CodeMsg WX_TOKEN_EXPIRE_ERROR = new CodeMsg(100016, "微信accessToken过期");
	public static CodeMsg BIND_USER_NOT_EXIT_ERROR = new CodeMsg(100017, "绑定学生/老师不存在: %s");

	// ========== 定时任务 1001001000 ==========
	public static CodeMsg JOB_NOT_EXISTS = new CodeMsg(1001001000, "定时任务不存在");
	public static CodeMsg JOB_HANDLER_EXISTS = new CodeMsg(1001001001, "定时任务的处理器已经存在");
	public static CodeMsg JOB_CHANGE_STATUS_INVALID = new CodeMsg(1001001002, "只允许修改为开启或者关闭状态");
	public static CodeMsg JOB_CHANGE_STATUS_EQUALS = new CodeMsg(1001001003, "定时任务已经处于该状态，无需修改");
	public static CodeMsg JOB_UPDATE_ONLY_NORMAL_STATUS = new CodeMsg(1001001004, "只有开启状态的任务，才可以修改");
	public static CodeMsg JOB_CRON_EXPRESSION_VALID = new CodeMsg(1001001005, "CRON 表达式不正确");

    // 学生 2xxxx
	public static CodeMsg MULTI_STUDENT_NAME_ERROR = new CodeMsg(200000, "同名学生:%s %s班 %s");
	public static CodeMsg STUDENT_NOT_EXIT_ERROR = new CodeMsg(200001, "学生不存在:%s %s班 %s");
	public static CodeMsg STUDENT_NOT_EXIT2_ERROR = new CodeMsg(200002, "学生不存在");
	public static CodeMsg MULTI_STUDENT_NAME2_ERROR = new CodeMsg(200003, "同名学生,请联系客服");
	public static CodeMsg CODE_NOT_EXIT_ERROR = new CodeMsg(200004, "唯一号不存在");
	public static CodeMsg STUDENT_NOT_BIND_ERROR = new CodeMsg(200005, "学生无绑定用户");

    // 教师 3xxxx
	public static CodeMsg TEACHER_DEPT_NOT_EXIT = new CodeMsg(300000, "教师部门不存在");
	public static CodeMsg TEACHER_NOT_EXIT = new CodeMsg(300001, "教师不存在");

	public static CodeMsg NOTICE_TEACHER_GROUP_NOT_EXIT = new CodeMsg(300002, "教师通知分组不存在");
	public static CodeMsg TEACHER_MULTI_BIND_ERROR = new CodeMsg(300003, "教师重复绑定");
	public static CodeMsg CHARGE_TEACHER_NOT_EXIT = new CodeMsg(300004, "班主任不存在");
	public static CodeMsg DEAN_TEACHER_NOT_EXIT = new CodeMsg(300005, "年级主任不存在");
	public static CodeMsg CHARGE_TEACHER_NOT_BIND_EXIT = new CodeMsg(300006, "教师无绑定用户");
	public static CodeMsg CHARGE_TEACHER_REPEAT = new CodeMsg(300007, "班主任重复");

    // 学校 4xxxx
	public static CodeMsg NO_ACCOUNT_ERROR = new CodeMsg(400000, "未绑定公众号");
	public static CodeMsg NO_MSG_METHOD_ERROR = new CodeMsg(400001, "消息发送方式不存在");
	public static CodeMsg GRADE_FORMAT_ERROR = new CodeMsg(400002, "班级年级格式错误");
	public static CodeMsg GRADE_NOT_EXIT_ERROR = new CodeMsg(400003, "年级不存在");


    // 服务器 5xxxxx
    public static CodeMsg SERVER_ERROR = new CodeMsg(500000, "服务端异常11");
    public static CodeMsg PARAMETER_ERROR = new CodeMsg(500101, "参数校验异常：%s");
    public static CodeMsg REQUEST_ILLEGAL = new CodeMsg(500102, "请求非法");
    public static CodeMsg ACCESS_LIMIT_REACHED = new CodeMsg(500103, "访问太频繁！");
    public static CodeMsg SMS_SERVER_ERROR = new CodeMsg(500104, "短信服务繁忙");
    public static CodeMsg MEDIA_TYPE_NOT_ACCEPT_ERROR = new CodeMsg(500105, "媒体类型不支持");
    public static CodeMsg BEAN_COPY_ERROR = new CodeMsg(500106, "实体类拷贝失败");
    public static CodeMsg NO_DATA_ERROR = new CodeMsg(500107, "暂无数据");
    public static CodeMsg NO_REQUEST_BODY_ERROR = new CodeMsg(500108, "Required request body is missing");
    public static CodeMsg NO_REQUEST_PARAM_ERROR = new CodeMsg(500109, "Required request param is missing");
    public static CodeMsg UPLOAD_FILE_ERROR = new CodeMsg(500110, "图片上传失败");
    public static CodeMsg NULL_POINTER_ERROR = new CodeMsg(500111, "空指针异常");

    // 数据库
    public static CodeMsg DATABASE_ERROR  = new CodeMsg(500400, "数据库执行语句异常");
    public static CodeMsg DATABASE_INTEGRITY_VIOLATION_ERROR  = new CodeMsg(500401, "数据完整性的异常 不符合约束条件");
    public static CodeMsg ACCOUNT_REGISTER_ERROR = new CodeMsg(500411, "账户注册失败");
    public static CodeMsg BIND_PUSH_ID_ERROR = new CodeMsg(500412, "绑定推送ID失败");
    public static CodeMsg USER_UPDATE_ERROR = new CodeMsg(500413, "用户更新失败");

    public static CodeMsg REDIS_CONNECT_ERROR  = new CodeMsg(5007601, "reids 连接异常");
    public static CodeMsg MYSQL_CONNECT_ERROR  = new CodeMsg(5007602, "mysql 连接异常");
    public static CodeMsg MYSQL_DATA_ACCESS_RESOURCE_FAILURE_ERROR  = new CodeMsg(5007603, "客户端未检测到服务器释放连接池连接");

    // other
	public static CodeMsg FILE_LARGE_ERROR = new CodeMsg(600001, "%s图片超过: %s");
	public static CodeMsg EXCEL_IMPORT_ERROR = new CodeMsg(600002, "%s");

	// ========== 动态表单模块 1-009-010-000 ==========
	public static CodeMsg  FORM_NOT_EXISTS = new CodeMsg(1009010000, "动态表单不存在");
	public static CodeMsg  FORM_FIELD_REPEAT = new CodeMsg(1009010000, "表单项({}) 和 ({}) 使用了相同的字段名({})");

	// ========== 用户组模块 1-009-011-000 ==========
	public static CodeMsg  USER_GROUP_NOT_EXISTS = new CodeMsg(1009011000, "用户组不存在");
	public static CodeMsg  USER_GROUP_IS_DISABLE = new CodeMsg(1009011001, "名字为【{}】的用户组已被禁用");

	// ========== OA 流程模块 1-009-001-000 ==========
	public static CodeMsg OA_LEAVE_NOT_EXISTS = new CodeMsg(1009001001, "请假申请不存在");
	public static CodeMsg OA_PM_POST_NOT_EXISTS = new CodeMsg(1009001002, "项目经理岗位未设置");
	public static CodeMsg OA_DEPART_PM_POST_NOT_EXISTS = new CodeMsg(1009001009, "部门的项目经理不存在");
	public static CodeMsg OA_BM_POST_NOT_EXISTS = new CodeMsg(1009001004, "部门经理岗位未设置");
	public static CodeMsg OA_DEPART_BM_POST_NOT_EXISTS = new CodeMsg(1009001005, "部门的部门经理不存在");
	public static CodeMsg OA_HR_POST_NOT_EXISTS = new CodeMsg(1009001006, "HR岗位未设置");
	public static CodeMsg OA_DAY_LEAVE_ERROR = new CodeMsg(1009001007, "请假天数必须>=1");

	// ========== 流程模型 1-009-002-000 ==========
	public static CodeMsg MODEL_KEY_EXISTS = new CodeMsg(1009002000, "已经存在流程标识为【{}】的流程");
	public static CodeMsg MODEL_NOT_EXISTS = new CodeMsg(1009002001, "流程模型不存在");
	public static CodeMsg MODEL_KEY_VALID = new CodeMsg(1009002002, "流程标识格式不正确，需要以字母或下划线开头，后接任意字母、数字、中划线、下划线、句点！");
	public static CodeMsg MODEL_DEPLOY_FAIL_FORM_NOT_CONFIG = new CodeMsg(1009002003, "部署流程失败，原因：流程表单未配置，请点击【修改流程】按钮进行配置");
	public static CodeMsg MODEL_DEPLOY_FAIL_TASK_ASSIGN_RULE_NOT_CONFIG = new CodeMsg(1009002004, "部署流程失败，" +
		"原因：用户任务({})未配置分配规则，请点击【修改流程】按钮进行配置");
	public static CodeMsg MODEL_DEPLOY_FAIL_TASK_INFO_EQUALS = new CodeMsg(1009003005, "流程定义部署失败，原因：信息未发生变化");


	// ========== 流程定义 1-009-003-000 ==========
	public static CodeMsg PROCESS_DEFINITION_KEY_NOT_MATCH = new CodeMsg(1009003000, "流程定义的标识期望是({})，当前是({})，请修改 BPMN 流程图");
	public static CodeMsg PROCESS_DEFINITION_NAME_NOT_MATCH = new CodeMsg(1009003001, "流程定义的名字期望是({})，当前是({})，请修改 BPMN 流程图");
	public static CodeMsg PROCESS_DEFINITION_NOT_EXISTS = new CodeMsg(1009003002, "流程定义不存在");
	public static CodeMsg PROCESS_DEFINITION_IS_SUSPENDED = new CodeMsg(1009003003, "流程定义处于挂起状态");
	public static CodeMsg PROCESS_DEFINITION_BPMN_MODEL_NOT_EXISTS = new CodeMsg(1009003004, "流程定义的模型不存在");

	// ========== 流程实例 1-009-004-000 ==========
	public static CodeMsg PROCESS_INSTANCE_NOT_EXISTS = new CodeMsg(1009004000, "流程实例不存在");
	public static CodeMsg PROCESS_INSTANCE_CANCEL_FAIL_NOT_EXISTS = new CodeMsg(1009004001, "流程取消失败，流程不处于运行中");
	public static CodeMsg PROCESS_INSTANCE_CANCEL_FAIL_NOT_SELF = new CodeMsg(1009004002, "流程取消失败，该流程不是你发起的");

	// ========== 流程任务 1-009-005-000 ==========
	public static CodeMsg TASK_COMPLETE_FAIL_NOT_EXISTS = new CodeMsg(1009004000, "审批任务失败，原因：该任务不处于未审批");
	public static CodeMsg TASK_COMPLETE_FAIL_ASSIGN_NOT_SELF = new CodeMsg(1009004001, "审批任务失败，原因：该任务的审批人不是你");

	// ========== 流程任务分配规则 1-009-006-000 ==========
	public static CodeMsg TASK_ASSIGN_RULE_EXISTS = new CodeMsg(1009006000, "流程({}) 的任务({}) 已经存在分配规则");
	public static CodeMsg TASK_ASSIGN_RULE_NOT_EXISTS = new CodeMsg(1009006001, "流程任务分配规则不存在");
	public static CodeMsg TASK_UPDATE_FAIL_NOT_MODEL = new CodeMsg(1009006002, "只有流程模型的任务分配规则，才允许被修改");
	public static CodeMsg TASK_CREATE_FAIL_NO_CANDIDATE_USER = new CodeMsg(1009006003, "操作失败，原因：找不到任务的审批人！");
	public static CodeMsg TASK_ASSIGN_SCRIPT_NOT_EXISTS = new CodeMsg(1009006004, "操作失败，原因：任务分配脚本({}) 不存在");
}
