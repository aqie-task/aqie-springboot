package com.aqielife;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@ImportResource(locations = {"classpath:applicationContext-dubbo-consumer.xml"})
@SpringBootApplication(scanBasePackages = {"org.springblade", "com.aqielife","auth"}, exclude = {DruidDataSourceAutoConfigure.class})
public class ServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ServerApplication.class, args);
  }

}
