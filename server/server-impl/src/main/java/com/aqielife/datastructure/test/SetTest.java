package com.aqielife.datastructure.test;

import com.aqielife.datastructure.Interface.Set;
import com.aqielife.datastructure.balancedBinaryTree.AVLSet;
import com.aqielife.datastructure.entity.set.BSTSet;
import com.aqielife.datastructure.entity.set.LinkedListSet;
import com.aqielife.datastructure.utils.FileOperation;

import java.util.ArrayList;


public class SetTest {
    public static void main(String[] args) {
        String fileName = "server/server-impl/src/main/resources/static/aqie.txt";
        BSTSet<String> bstSet = new BSTSet<>();
        System.out.println(countWords(bstSet,fileName));

        LinkedListSet<String> linkedListSet = new LinkedListSet<>();
        System.out.println(countWords(linkedListSet,fileName));

        AVLSet<String> set = new AVLSet<>();
        System.out.println(countWords(set,fileName));
    }

    // binarySearch
    public static double countWords(Set<String> set, String fileName){
        long startTime = System.nanoTime();
        ArrayList<String> words = new ArrayList<>();
        if(FileOperation.readFile(fileName,words)) {
            System.out.println("Total words: " + words.size());
            for (String word : words)
                set.add(word);
            System.out.println("Total Different Words :" + set.getSize());
        }
        long endTime = System.nanoTime();

        return (endTime - startTime) / 1000000000.0;
    }


}
