package com.aqielife.datastructure.solution;

import com.aqielife.datastructure.entity.map.LinkedListMap;

import java.util.ArrayList;
import java.util.Arrays;

// 350 ： 数组求交集(使用映射)
public class Intersect {
    public static int[] intersect(int[] nums1, int[] nums2) {

        LinkedListMap<Integer, Integer> map = new LinkedListMap<>();
        for(int num: nums1){
            if(!map.contains(num))
                map.add(num, 1);
            else
                map.set(num, map.get(num) + 1);
        }

        ArrayList<Integer> res = new ArrayList<>();
        for(int num: nums2){
            if(map.contains(num)){
                res.add(num);
                map.set(num, map.get(num) - 1);
                if(map.get(num) == 0)
                    map.remove(num);
            }
        }

        int[] ret = new int[res.size()];
        for(int i = 0 ; i < res.size() ; i ++)
            ret[i] = res.get(i);

        return ret;
    }

    public static void main(String[] args) {
        int[] nums1 = {1,2,2,1,3};
        int[] nums2 = {2,2,3};
        // 数组转字符串
        int[] res = intersect(nums1,nums2);
        StringBuilder sb = new StringBuilder();
        for (int num : res){
            sb.append(num);
        }
        System.out.println(sb.toString());

        // 字符串转数组
        String str = "0,1,2,3,4,5";
        String[] arr = str.split(","); // 用,分割
        System.out.println(Arrays.toString(arr)); // [0, 1, 2, 3, 4, 5]
    }
}
