package com.aqielife.datastructure.segmentTree;

public interface Merger<E> {
    E merge(E a, E b);
}
