package com.aqielife.datastructure.redBlack;

import com.aqielife.datastructure.balancedBinaryTree.AVLTree;
import com.aqielife.datastructure.balancedBinaryTree.BST;
import com.aqielife.datastructure.balancedBinaryTree.IBinaryTree;

import java.util.ArrayList;
import java.util.Random;

public class RBTreeTest {
    public static void main(String[] args) {
        // 随机数据
        /*BST<Integer, Integer> map = new BST<>();
        addTest(map);*/  // 150

        AVLTree<Integer, Integer> map2 = new AVLTree<>();
        // addTest(map2); // 229

        RBTree<Integer, Integer> map3 = new RBTree<>();
        // addTest(map3); // 208

        // 顺序数据
        // addOrderedTest(map2); // 20.8
        // addOrderedTest(map3); // 19.89

    }

    private static void addTest(IBinaryTree<Integer, Integer> map) {
        int n = 20000000;

        Random random = new Random(n);
        ArrayList<Integer> testData = new ArrayList<>(n);
        for(int i = 0 ; i < n ; i ++)
            testData.add(random.nextInt(Integer.MAX_VALUE));

        // Test BST
        long startTime = System.nanoTime();

        for (Integer x: testData)
            map.add(x, null);

        long endTime = System.nanoTime();

        double time = (endTime - startTime) / 1000000000.0;
        System.out.println("BST: " + time + " s");
    }

    private static void addOrderedTest(IBinaryTree<Integer, Integer> map){
        int n = 20000000;

        ArrayList<Integer> testData = new ArrayList<>(n);
        for(int i = 0 ; i < n ; i ++)
            testData.add(i);

        // Test AVL
        long startTime = System.nanoTime();

        for (Integer x: testData)
            map.add(x, null);

        long endTime = System.nanoTime();

        double time = (endTime - startTime) / 1000000000.0;
        System.out.println("AVL: " + time + " s");
    }
}
