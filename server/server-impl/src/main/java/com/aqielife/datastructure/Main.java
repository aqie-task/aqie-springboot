package com.aqielife.datastructure;

import com.aqielife.datastructure.entity.Array;

public class Main {

    public static void main(String[] args) {
        // array();
        testArray();
    }

    private static void array(){
        int[] arr = new int[10];
        for (int i = 0; i < arr.length;++i){
            arr[i] = i;
        }

        int[] scores = new int[]{100,99,66};
        for (int anArr : scores) System.out.print(anArr);
    }

    private static void testArray(){
        Array<Integer> arr = new Array<>(20);
        for(int i = 0; i < 10;++i){
            arr.addLast(i);
        }
        arr.add(5,6);
        System.out.println(arr.toString());
        /*System.out.println(arr.get(5));
        System.out.println(arr.contains(10));
        System.out.println(arr.find(6));*/
        System.out.println(arr.find(6));
        System.out.println(arr.remove(5));
        System.out.println(arr.toString());

        arr.removeElement(9);
        arr.add(1,1);
        arr.add(1,1);
        arr.add(1,1);
        arr.remove(1);
        arr.remove(1);
        arr.remove(1);
        System.out.println(arr.toString());
    }
}
