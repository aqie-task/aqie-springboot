package com.aqielife.datastructure.entity.set;

import com.aqielife.datastructure.Interface.Set;
import com.aqielife.datastructure.entity.LinkedList;

/**
 * 线性数据结构中数据不需要具备可比性
 * 链表实现集合
 */
public class LinkedListSet<E> implements Set<E> {
    private LinkedList<E> list;
    public LinkedListSet(){
        list = new LinkedList<>();
    }

    @Override
    public void add(E e) {
        if(!list.contains(e))
            list.addFirst(e);
    }

    @Override
    public boolean contains(E e) {
        return list.contains(e);
    }

    @Override
    public void remove(E e) {
        list.removeElement(e);
    }

    @Override
    public int getSize() {
        return list.getSize();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }
}
