package com.aqielife.datastructure.unionFind;

// 使用一个数组构建一棵指向父节点的树 O(log*n)
// 基于rank优化
// 在后续的代码中, 我们并不会维护rank的语意, 也就是rank的值在路径压缩的过程中, 有可能不在是树的层数值
public class QuickUnion5 implements UF{
    private int[] parent;   // parent[i]表示第一个元素所指向的父节点

    private int[] rank;  // rank[i]表示以i为根的集合所表示的树的层数

    public QuickUnion5(int size){
        parent = new int[size];
        rank = new int[size];

        for (int i = 0; i < size; i++) {
            rank[i] = 1;
            parent[i] = i;
        }

    }

    @Override
    public int getSize() {
        return parent.length;
    }

    // O(h)复杂度, h为树的高度
    @Override
    public boolean isConnected(int p, int q) {
        return find(p) == find(q);
    }

    // O(h)复杂度, h为树的高度
    @Override
    public void unionElements(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(p);
        if(pRoot == qRoot){
            return;
        }
        if(rank[pRoot] < rank[qRoot]){
            parent[pRoot] = qRoot;  // p 的根节点指向q  , qRoot 子树高度不会变高
        }else if(rank[qRoot] < rank[pRoot]){
            parent[qRoot] = pRoot;
        }else{
            parent[pRoot] = qRoot;
            rank[qRoot] += 1;
        }

    }

    // 查找过程, 查找元素p所对应的根节点
    private int find(int p){
        if(p < 0 || p >= parent.length)
            throw new IllegalArgumentException("p is out of bound.");

        // 根节点的特点: parent[p] == p  根节点父节点是其本身
        if (p != parent[p]){
            parent[p] = find(parent[p]);
        }
        return parent[p];
    }
}
