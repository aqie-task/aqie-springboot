package com.aqielife.datastructure.test;

import com.aqielife.datastructure.Interface.Set;
import com.aqielife.datastructure.entity.set.BSTSet;
import com.aqielife.datastructure.trie.Trie;
import com.aqielife.datastructure.utils.FileOperation;

import java.util.ArrayList;

public class TrieTest {
    public static void main(String[] args) {
        /*ArrayList<String> words = new ArrayList<>();
        BSTSet<String> set = new BSTSet<>();
        BSTSet(words, set);

        words = new ArrayList<>();
        Trie trie = new Trie();
        Trie(words, trie);*/

        test208();
    }

    private static void test208() {
        Trie trie = new Trie();
        System.out.println(trie.search("Trie"));    // false
        trie.insert("apple");
        System.out.println(trie.search("apple"));   // true
        System.out.println(trie.search("app"));     // false
        System.out.println(trie.startsWith("app"));         // true
        trie.insert("app");
        System.out.println(trie.search("app"));     // true
    }

    private static void BSTSet(ArrayList<String> words, Set<String> set) {
        if(FileOperation.readFile("static/pride-and-prejudice.txt", words)){
            long startTime = System.nanoTime();

            for(String word: words)
                set.add(word);

            for(String word: words)
                set.contains(word);

            long endTime = System.nanoTime();
            double time = (endTime - startTime) / 1000000000.0;

            System.out.println("Total different words: " + set.getSize());
            System.out.println("BSTSet: " + time + " s");
        }
    }

    private static void Trie(ArrayList<String> words,Trie trie){
        if(FileOperation.readFile("static/pride-and-prejudice.txt", words)) {
            long startTime = System.nanoTime();

            for (String word : words)
                trie.insert(word);

            for (String word : words)
                trie.search(word);

            long endTime = System.nanoTime();

            double time = (endTime - startTime) / 1000000000.0;

            System.out.println("Total different words: " + trie.getSize());
            System.out.println("Trie: " + time + " s");
        }
    }
}
