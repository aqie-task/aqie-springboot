package com.aqielife.datastructure.hashTable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class UniqueCharacter {
    public static void main(String[] args) {
        test();
    }

    // 字符串词频统计 并打印输出
    private static void test() {
        String s = "loveleetcode";
        // 字符串转字符串数组
        char[] characters = s.toCharArray();

        TreeMap<Character, Integer> map = new TreeMap<>();
        for (Character character : characters){
            if(map.containsKey(character)){
                map.put(character,map.get(character) + 1);
            }else{
                map.put(character, 1);
            }
        }

        /* // 遍历map中的键
        for (Character key : map.keySet()) {
            System.out.println("Key = " + key);
        }
        //遍历map中的值
        for (Integer value : map.values()) {
            System.out.println("Value = " + value);
        } */

        // 使用迭代器
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        }
    }


    /**
     * 创建26个字母 数组，每个记录各个字母分别出现频次
     * 每一个字符都和一个索引对应 O(1)
     *
     * @param s
     * @return
     */
    private static int firstUniqChar(String s){
        int[] freq = new int[26];
        for(int i = 0; i < s.length(); i++)
            freq[s.charAt(i) - 'a']++;

        for(int i = 0 ; i < s.length() ; i ++)
            if(freq[s.charAt(i) - 'a'] == 1)
                return i;

        return -1;
    }
}
