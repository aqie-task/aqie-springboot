package com.aqielife.datastructure.solution.tire;

import java.util.TreeMap;

// 211
public class WordDictionary {
    private class Node{

        public boolean isWord;
        public TreeMap<Character, Node> next;

        public Node(boolean isWord){
            this.isWord = isWord;
            next = new TreeMap<>();
        }

        public Node(){
            this(false);
        }
    }

    private Node root;
    public WordDictionary() {
        root = new Node();
    }

    public void addWord(String word) {

        Node cur = root;
        for(int i = 0 ; i < word.length() ; i ++){
            char c = word.charAt(i);
            if(cur.next.get(c) == null)
                cur.next.put(c, new Node());
            cur = cur.next.get(c);
        }
        cur.isWord = true;
    }

    public boolean search(String word) {
        return match(root, word, 0);
    }

    /**
     *
     * @param node 为根开始搜索
     * @param word 基于字符串模式
     * @param index 基于index
     * @return
     */
    private boolean match(Node node, String word, int index){
        // index 等于字符长度 判断是否是单词
        if(index == word.length())
            return node.isWord;

        char c = word.charAt(index);

        if(c != '.'){
            if(node.next.get(c) == null)
                return false;
            return match(node.next.get(c), word, index + 1);
        }
        else{
            for(char nextChar: node.next.keySet())  // node.next 可以匹配的所有字符取出尝试匹配c 所在的.
                if(match(node.next.get(nextChar), word, index + 1))
                    return true;
            return false;
        }
    }
}
