package com.aqielife.datastructure.test;

import com.aqielife.datastructure.heap.MaxHeap;

import java.util.Random;

public class MaxHeapTest {
    public static void main(String[] args) {
        // HeapSortTest();
        int n = 1000000;
        Random random = new Random();
        Integer[] testData = new Integer[n];
        for(int i = 0 ; i < n ; i ++)
            testData[i] = random.nextInt(Integer.MAX_VALUE);

        double time1 = testHeap(testData, false);
        System.out.println("Without heapify: " + time1 + " s");

        double time2 = testHeap(testData, true);
        System.out.println("With heapify: " + time2 + " s");
    }

    // 测试堆排序 (可以优化原地排序)
    private static void HeapSortTest() {
        int n = 1000000;
        MaxHeap<Integer> heap = new MaxHeap<>();
        Random random = new Random();
        // 随机生成的[0, Integer.MAX_VALUE) 中值放入堆中
        for(int i = 0; i < n; ++i){
            heap.add(random.nextInt(Integer.MAX_VALUE));
        }

        isSortedArray(n, heap);
    }

    // 测试堆中数据是否有序
    private static void isSortedArray(int n, MaxHeap<Integer> heap) {
        int[] arr = new int[n];
        for(int i = 0; i < n; ++i)      // 此时数组中元素从大到小
            arr[i] = heap.extractMax();
        for(int i = 1; i < n; i++)
            if(arr[i - 1] < arr[i])
                throw new IllegalArgumentException("Error");
        System.out.println("Test MaxHeap complete!");
    }

    /**
     * 测试堆排序两种时间
     * heapify 和直接将空数组存进堆中
     */
    private static double testHeap(Integer[] testData, boolean isHeapify){
        long startTime = System.nanoTime();
        MaxHeap<Integer> maxHeap;
        if(isHeapify){
            maxHeap = new MaxHeap<>(testData);
        }else{
            maxHeap = new MaxHeap<>();
            for (int num : testData)
                maxHeap.add(num);
        }

        // isSortedArray(testData.length, maxHeap);
        long endTime = System.nanoTime();

        return (endTime - startTime) / 1000000000.0;
    }
}
