package com.aqielife.datastructure.trie;

import java.util.TreeMap;

/**
 * 字典树
 * 借助 TreeMap(红黑树)
 * 当前实现是字符串集合
 */
public class Trie {
    private class Node{
        // 当前节点是否是个单词
        public boolean isWord;
        public TreeMap<Character, Node> next;

        public Node(boolean isWord){
            this.isWord = isWord;
            next = new TreeMap<>();
        }

        public Node(){
            this(false);
        }
    }

    private Node root;
    private int size;

    public Trie(){
        root = new Node();
        size = 0;
    }

    // 获得Trie中存储的单词数量
    public int getSize(){
        return size;
    }

    /**
     * 向Trie中添加一个新的单词word
     * 非递归方法
     * @param word
     */
    public void insert(String word){
        Node cur = root;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            if(cur.next.get(c) == null){
                cur.next.put(c, new Node());
            }
            cur = cur.next.get(c);
        }
        if(!cur.isWord){
            cur.isWord = true;
            size++;
        }
    }

    // 查询单词word是否在Trie中
    public boolean search(String word){
        Node cur = root;
        for(int i = 0 ; i < word.length() ; i ++){
            char c = word.charAt(i);
            if(cur.next.get(c) == null)
                return false;
            cur = cur.next.get(c);
        }
        return cur.isWord;
    }

    // 查询是否在Trie中有单词以prefix为前缀
    public boolean startsWith(String prefix){
        Node cur = root;
        return !isExist(prefix, cur);
    }

    private boolean isExist(String prefix, Node cur) {
        for(int i = 0; i < prefix.length(); i++){
            char c = prefix.charAt(i);
            if(cur.next.get(c) == null){
                return true;
            }
            cur = cur.next.get(c);
        }
        return false;
    }

}
