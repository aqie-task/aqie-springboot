package com.aqielife.datastructure.solution;

import java.util.TreeSet;

public class UniqueMorseCode {
    public static int uniqueMorseRepresentations(String[] words) {
        String[] codes = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---",
                        "-.-",".-..","--","-.","---",".--.","--.-",".-.",
                        "...","-","..-","...-",".--","-..-","-.--","--.."};
        TreeSet<String> set = new TreeSet<>();
        for (String word : words){
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < word.length(); ++i){                 // 字符串数组中每个字符串长度
                sb.append(codes[word.charAt(i) - 'a']);             // 当前字母对应的 morse code 添加进StringBuilder
            }
            // 摩斯码字符串存进set
            set.add(sb.toString());
        }
        return set.size();
    }

    public static void main(String[] args) {
        String[] words = {"gin", "zen", "gig", "msg"};
        System.out.println(UniqueMorseCode.uniqueMorseRepresentations(words));
    }
}
