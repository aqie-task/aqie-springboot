package com.aqielife.datastructure.test;

import com.aqielife.datastructure.segmentTree.Merger;
import com.aqielife.datastructure.segmentTree.SegmentTree;

public class SegmentTreeTest {
    public static void main(String[] args) {
        Integer[] nums = {-2, 0, 3, -5, 2, -1};
        /*SegmentTree<Integer> segmentTree = new SegmentTree<>(nums, new Merger<Integer>() {
            @Override
            public Integer merge(Integer a, Integer b) {
                return a + b;
            }
        });*/
        SegmentTree<Integer> segmentTree = new SegmentTree<>(nums, (a, b) -> a + b);
        System.out.println(segmentTree);

        // 0-3 区间元素和
        System.out.println(segmentTree.query(0,3));
    }
}
