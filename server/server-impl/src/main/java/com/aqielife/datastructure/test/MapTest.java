package com.aqielife.datastructure.test;

import com.aqielife.datastructure.Interface.Map;
import com.aqielife.datastructure.balancedBinaryTree.AVLMap;
import com.aqielife.datastructure.entity.map.BSTMap;
import com.aqielife.datastructure.entity.map.LinkedListMap;
import com.aqielife.datastructure.utils.FileOperation;

import java.util.ArrayList;

// 测试映射, 统计每个词出现词频
public class MapTest {
    public static void main(String[] args) {
        System.out.println("Pride and Prejudice");
        LinkedListMap<String, Integer> map = new LinkedListMap<>();
        // mapTest(map);

        BSTMap<String, Integer> map2 = new BSTMap<>();
        System.out.println(mapTest(map2));

        AVLMap<String, Integer> map3 = new AVLMap<>();
        System.out.println(mapTest(map3));
    }

    private static double mapTest(Map<String, Integer> map) {
        ArrayList<String> words = new ArrayList<>();
        if(FileOperation.readFile("static/pride-and-prejudice.txt", words)){
            System.out.println("Total words: " + words.size());
            long startTime = System.nanoTime();
            for(String word : words){
                if(map.contains(word)){
                    map.set(word, map.get(word) + 1);
                }else{
                    map.add(word, 1);
                }
            }
            long endTime = System.nanoTime();
            /*System.out.println("Total different words: " + map.getSize());
            System.out.println("Frequency of PRIDE: " + map.get("pride"));
            System.out.println("Frequency of PREJUDICE: " + map.get("prejudice"));*/
            return (endTime - startTime) / 1000000000.0;
        }

        return 0;
    }
}
