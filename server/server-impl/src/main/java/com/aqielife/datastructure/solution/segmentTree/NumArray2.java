package com.aqielife.datastructure.solution.segmentTree;

// 303 比线段树更高效
public class NumArray2 {
    /**
     * sum[i]存储前i个元素和, sum[0] = 0
     * sum[i]存储nums[0...i-1]的和, sum[0] = 0 所以会产生偏移，数组长度+1
     * sum(i, j) = sum[j + 1] - sum[i]
     */
    private int[] sum;
    public NumArray2(int[] nums){           // O(n)
        sum = new int[nums.length + 1];
        sum[0] = 0;
        for(int i = 1; i < sum.length; i++){
            sum[i] = sum[ i - 1] + nums[i - 1];
        }
    }
    public int sumRange(int i, int j){   // O(1)
        return sum[j + 1] - sum[i];
    }
}
