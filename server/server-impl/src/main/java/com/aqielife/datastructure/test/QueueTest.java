package com.aqielife.datastructure.test;

import com.aqielife.datastructure.entity.ArrayQueue;


public class QueueTest {
    public static void main(String[] args) {
        test();
    }


    public static void test(){
        ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
        for (int i = 0; i < 10; ++i){
            arrayQueue.enqueue(i);
            System.out.println(arrayQueue);

            if(i % 3 == 2){
                arrayQueue.dequeue();
                System.out.println(arrayQueue);
            }
        }
    }
}
