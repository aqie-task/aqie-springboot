package com.aqielife.datastructure.entity;

import com.aqielife.datastructure.Interface.Queue;

public class LoopQueue<E> implements Queue<E> {
    private E[] data;
    private int front;
    private int tail;       // 队列最后一个元素下一个位置,下一个元素入队存放位置
    private int size;       // 队列中元素个数

    public LoopQueue(int capacity){
        data = (E[])new Object[capacity + 1];        // 会浪费一个数组,+1才能存放用户数据
        front = 0;
        tail = 0;
        size = 0;
    }

    public LoopQueue(){
        this(10);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return front == tail;
    }

    // 入队
    public void enqueue(E e){
        // 判断队列是否满
        if((tail + 1) % data.length == front){
            resize(getCapacity() * 2);  // 扩容的是真实容量
        }
        data[tail] = e;                              // tail 标识入队元素位置
        tail = (tail + 1) % data.length;             // (4 + 1) % 5
        size++;
    }

    // 出队, 会触发缩容
    @Override
    public E dequeue() {
        if(isEmpty())
            throw new IllegalArgumentException("Cannot dequeue from an empty queue.");
        E ret = data[front];
        data[front] = null;
        front = (front + 1) % data.length;
        size--;
        if(size == getCapacity() / 4 && getCapacity() / 2 != 0)
            resize(getCapacity() / 2);
        return ret;
    }

    // 获取队首元素
    @Override
    public E getFront() {
        if(isEmpty())
            throw new IllegalArgumentException("Queue is empty.");
        return data[front];
    }

    // 获取队列容量, 会浪费一个元素,-1才是真实容量
    public int getCapacity(){
        return data.length - 1;
    }

    /**
     * length = 5, size = capacity = 4
     * front = 2, tail = 0
     * n[3] = d([3 + 2] % 5)
     * @param newCapacity
     */
    private void resize(int newCapacity){
        // 开辟新空间
        E[] newData = (E[])new Object[newCapacity +1];
        // 将旧队列元素放进新队列, 旧元素相对新元素会有front偏移, 要对length 取余
        for(int i = 0; i < size; ++i){
            newData[i] = data[(i + front) % data.length];   // 防止越界
        }
        data = newData;         // data 变化 , 容积也变化
        front = 0;
        tail = size;
    }

    // 遍历循环双向队列
    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append(String.format("Queue: size = %d , capacity = %d\n", size, getCapacity()));
        res.append("front [");
        for(int i = front; i != tail ; i = (i + 1) % data.length){
            res.append(data[i]);
            if((i + 1) % data.length != tail)       // 不是最后一个元素
                res.append(", ");
        }
        res.append("] tail");
        return res.toString();
    }
}
