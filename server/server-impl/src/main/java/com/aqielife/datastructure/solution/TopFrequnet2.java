package com.aqielife.datastructure.solution;

import java.util.*;

/**
 * 347
 * java 内置的优先队列(最小堆)
 */
public class TopFrequnet2 {
    private class Freq { // implements Comparable<Freq>
        public int e;      // 元素是谁
        public int freq;   // 元素对应的频次

        public Freq(int e, int freq){
            this.e = e;
            this.freq = freq;
        }

        // 频次越低，优先级越高,最先出队
        /*@Override
        public int compareTo(Freq o) {
            // 当前元素比传入元素大 返回1
            return Integer.compare(this.freq, o.freq);
        }*/
    }

    // 可以使用比较器 代替 上面继承的 Comparable 接口
    // 比较器可以定义自己专属比较规则
    private class FreqComparator implements Comparator<Freq>{
        @Override
        public int compare(Freq o1, Freq o2) {  // o1 > o2 返回 1
            return o1.freq - o2.freq;
        }
    }

    public List<Integer> topKFrequent(int[] nums, int k) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int num : nums){
            if(map.containsKey(num)){
                map.put(num, map.get(num) + 1);
            }else {
                map.put(num, 1);
            }
        }

        PriorityQueue<Freq> pq = new PriorityQueue<>(new FreqComparator());
        for (int key : map.keySet()){
            if(pq.size() < k){
                pq.add(new Freq(key, map.get(key)));
            }else if(map.get(key) > pq.peek().freq){        // peek队首元素
                pq.remove();
                pq.add(new Freq(key, map.get(key)));
            }
        }

        LinkedList<Integer> res = new LinkedList<>();
        while(!pq.isEmpty()){
            res.add(pq.remove().e);
        }
        return res;
    }
}
