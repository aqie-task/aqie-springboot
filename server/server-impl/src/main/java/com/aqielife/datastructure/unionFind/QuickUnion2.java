package com.aqielife.datastructure.unionFind;

// 使用一个数组构建一棵指向父节点的树
// 维护根节点的元素个数
public class QuickUnion2 implements UF{
    // parent[i]表示第一个元素所指向的父节点
    private int[] parent;

    private int[] sz;     // sz[i]表示以i为根的集合中元素个数

    public QuickUnion2(int size){
        parent = new int[size];
        sz = new int[size];

        for (int i = 0; i < size; i++) {
            sz[i] = 1;
            parent[i] = i;
        }

    }

    @Override
    public int getSize() {
        return parent.length;
    }

    // O(h)复杂度, h为树的高度
    @Override
    public boolean isConnected(int p, int q) {
        return find(p) == find(q);
    }

    // O(h)复杂度, h为树的高度
    @Override
    public void unionElements(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(p);
        if(pRoot == qRoot){
            return;
        }
        if(sz[pRoot] < sz[qRoot]){
            parent[pRoot] = qRoot;  // p 的根节点指向q
            sz[qRoot] += sz[pRoot];     // 维护q根节点元素个数
        }else{
            parent[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        }

    }

    // 查找过程, 查找元素p所对应的根节点
    private int find(int p){
        if(p < 0 || p >= parent.length)
            throw new IllegalArgumentException("p is out of bound.");

        // 根节点的特点: parent[p] == p  根节点父节点是其本身
        while (p != parent[p]){
            p = parent[p];      // 一步一步向上查
        }
        return p;
    }
}
