package com.aqielife.datastructure.bst;

/**
 * 二分搜索树
 */
public class BST<E extends Comparable<E>> {
    private class Node{
        public E e;     // 存储元素
        public Node left,right;
        public Node(E e){
            this.e = e;
            left = null;
            right = null;
        }
    }

    private Node root;  // 根节点
    private int size;   // 二分搜索树元素个数
    public BST(){
        root = null;
        size = 0;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    // 向二分搜索树中添加新的元素
    public void add(E e){
        // 根节点本身为空
        if(root == null){
            root = new Node(e);
            size ++;
        }else{
            add(root, e);
        }
    }


    /**
     * 向以node 为根的二分搜索树插入元素E,递归
     * 小于根节点添加左边,大于添加到右边
     * @param node
     * @param e
     */
    private void add(Node node,E e){
        if(e.equals(node.e)){
            return;
        }else if(e.compareTo(node.e) < 0 && node.left == null){
            node.left = new Node(e);
            size ++;
            return;
        }else if(e.compareTo(node.e) > 0 && node.right == null){
            node.right = new Node(e);
            size++;
            return;
        }

        if(e.compareTo(node.e) < 0){
            add(node.left,e);
        }else{
            add(node.right,e);
        }
    }
}
