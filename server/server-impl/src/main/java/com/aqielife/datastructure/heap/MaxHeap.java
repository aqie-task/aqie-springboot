package com.aqielife.datastructure.heap;

import com.aqielife.datastructure.entity.Array;

// 0 1 2 3 4
public class MaxHeap<E extends Comparable> {
    private Array<E> data;

    // 用户知道堆中元素个数
    public MaxHeap(int capacity){
        data = new Array<>(capacity);
    }

    public MaxHeap(){
        data = new Array<>();
    }


    /**
     * heapify
     * 通过数组构建最大堆
     * @param arr
     */
    public MaxHeap(E[] arr){
        // 将数组拷贝一份到动态数组
        data = new Array<>(arr);
        for(int i = parent(arr.length - 1); i >= 0;i --){
            shiftDown(i);
        }
    }

    public int size(){
        return data.getSize();
    }

    public boolean isEmpty(){
        return data.isEmpty();
    }

    private int parent(int index){
        if(index == 0)
            throw new IllegalArgumentException("index-0 doesn't have parent.");
        return (index - 1) / 2;
    }

    private int leftChild(int index){
        return index * 2 + 1;
    }

    private int rightChild(int index){
        return index * 2 + 2;
    }

    // 向数组末尾添加元素,根据数组索引完成最大堆shift up
    public void add(E e){
        data.addLast(e);
        shiftUp(data.getSize() - 1);   
    }

    // 如果子节点比其父节点大， 则进行交换
    private void shiftUp(int i) {
        while(i > 0 && data.get(parent(i)).compareTo(data.get(i)) < 0 ){
            data.swap(i, parent(i));
            i = parent(i);              // 继续和其父节点比较
        }
    }

    // 查看堆中最大元素
    public E findMax(){
        if(data.getSize() == 0)
            throw new IllegalArgumentException("Can not findMax when heap is empty.");
        return data.get(0);
    }

    /**
     * 移除堆中最大元素
     * 将树顶元素和最后一个元素交换
     * 移除最后一个元素
     * 再将当前队首元素下移  保证堆性质
     * @return 移除的最大元素
     */
    public E extractMax(){
        E ret = findMax();
        data.swap(0, data.getSize() - 1);
        data.removeLast();
        shiftDown(0);
        return ret;
    }

    /**
     * 每次下沉 选择两个孩子中较大元素
     * @param i 下沉操作对应索引
     */
    private void shiftDown(int i) {
        // 循环结束 ： 1.没有左孩子,左孩子索引越界
        while(leftChild(i) < data.getSize()){
            // 比较 k 左右子节点, 判断是否比其小
            int j = leftChild(i);       // 肯定有左孩子
            // 有右孩子，并且比左孩子大，j++ 保证data[j] 是最大的元素
            if(j + 1 < data.getSize() && data.get(j + 1).compareTo(data.get(j)) > 0){
                j++;
            }

            // 如果最大的孩子比父节点大，交换位置, 否则终止循环
            if(data.get(i).compareTo(data.get(j)) >= 0){
                break;
            }
            data.swap(i,j);
            i = j;          // 保证循环继续逻辑正确
        }
    }

    /**
     * 取出最大元素
     * 添加新元素
     * 完成下沉操作
     * @param e
     * @return
     */
    public E replace(E e){
        E ret = findMax();
        data.set(0, e);
        shiftDown(0);
        return ret;
    }
}
