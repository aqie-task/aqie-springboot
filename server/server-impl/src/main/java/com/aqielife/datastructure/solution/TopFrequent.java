package com.aqielife.datastructure.solution;

import com.aqielife.datastructure.entity.LinkedList;
import com.aqielife.datastructure.heap.PriorityQueue;

import java.util.List;
import java.util.TreeMap;

/**
 *  347(https://leetcode.com/problems/top-k-frequent-elements/description/)
 *  使用自己实现的最大堆
 */
public class TopFrequent {
    private class Freq implements Comparable<Freq>{
        public int e;      // 元素是谁
        public int freq;   // 元素对应的频次

        public Freq(int e, int freq){
            this.e = e;
            this.freq = freq;
        }

        // 频次越低，优先级越高,最先出队
        @Override
        public int compareTo(Freq o) {
            // 当前元素比传入元素频次低, 认为其优先级高，也就是下面返回1
            // 当前元素比传入元素大1,小返回-1，等返回0
            return Integer.compare(o.freq, this.freq);
        }
    }

    public LinkedList<Integer> topKFrequent(int[] nums, int k) {
        // 映射map 统计数组中元素频次
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int num : nums){
            if(map.containsKey(num)){
                map.put(num, map.get(num) + 1);
            }else {
                map.put(num, 1);
            }
        }
        // 依据元素出现频率决定优先级 队列通过一个类来存储映射中信息
        PriorityQueue<Freq> priorityQueue = new PriorityQueue<>();
        for (int key : map.keySet()){       // 遍历映射中所有key
            if(priorityQueue.getSize() < k){    // 还没有存够k个元素,则入队新元素
                priorityQueue.enqueue(new Freq(key, map.get(key)));
            }else if( map.get(key) > priorityQueue.getFront().freq){     // 新遍历到的元素 频次 比队列中优先级最高元素(频次最低元素-队首元素)高
                priorityQueue.dequeue();            // 队首出队
                priorityQueue.enqueue(new Freq(key, map.get(key)));  // 入队新元素
            }
        }

        // 将队列中符合的元素存入链表
        LinkedList<Integer> res = new LinkedList<>();
        while(!priorityQueue.isEmpty()){
            res.addLast(priorityQueue.dequeue().e);
        }
        return res;
    }

    public static void main(String[] args) {
        TopFrequent frequent = new TopFrequent();
        int[] nums = {1,1,1,2,2,3};
        LinkedList list = frequent.topKFrequent(nums,2);
        System.out.println(list);
    }
}
