package com.aqielife.datastructure.test;
import com.aqielife.datastructure.bst.BST2;


import java.util.ArrayList;
import java.util.Random;

public class BST2Test {

    public static void main(String[] args) {
        test();
    }

    public static void test(){
        BST2<Integer> bst = new BST2<>();
        int[] nums = {5, 3, 6, 8, 4, 2};
        for(int num: nums)
            bst.add(num);

        /////////////////
        //      5      //
        //    /   \    //
        //   3    6    //
        //  / \    \   //
        // 2  4     8  //
        /////////////////
        bst.preOrder();         // 前序遍历
        System.out.println();
        bst.preOrderNR();       // 前序遍历非递归
        System.out.println();

        bst.inOrder();          // 中序遍历
        System.out.println();

        bst.postOrder();        // 后序遍历
        System.out.println();

        bst.levelOrder();       // 层序遍历
        System.out.println();
        //System.out.println(bst);

        System.out.println(bst.minimum());
        System.out.println(bst.maximum());
    }


    public void testRemove(){
        BST2<Integer> bst2 = new BST2<>();
        Random random = new Random();
        int n = 1000;
        for(int i = 0; i < n; ++ i){
            bst2.add(random.nextInt(10000));        // 添加 0-10000
        }
        ArrayList<Integer> nums = new ArrayList<>();        // 动态数组
        while (!bst2.isEmpty()){
            nums.add(bst2.removeMin());
        }

        System.out.println(nums);       // 从小到大排序

        for(int i = 1; i < nums.size(); ++i){
            if(nums.get(i - 1) > nums.get(i)){
                throw new IllegalArgumentException("Error");
            }
        }
        System.out.println(" removeMin test completed!");
    }
}
