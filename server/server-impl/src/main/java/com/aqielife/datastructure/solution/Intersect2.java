package com.aqielife.datastructure.solution;

import java.util.ArrayList;
import java.util.TreeSet;

public class Intersect2 {
    public static int[] intersection(int[] nums1, int[] nums2) {

        TreeSet<Integer> set = new TreeSet<>();
        for(int num: nums1)
            set.add(num);

        // 公有的元素放在动态数组
        ArrayList<Integer> list = new ArrayList<>();
        for(int num: nums2){
            if(set.contains(num)){
                list.add(num);
                set.remove(num);        // set中删除该元素,下一次遇到相同元素,set中不存在则 不会加入list
            }
        }

        int[] res = new int[list.size()];
        for(int i = 0 ; i < list.size() ; i ++)
            res[i] = list.get(i);
        return res;
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 2, 1, 3};
        int[] nums2 = {2, 2, 3};
        // 数组转字符串
        int[] res = intersection(nums1, nums2);
        StringBuilder sb = new StringBuilder();
        for (int num : res) {
            sb.append(num);
        }
        System.out.println(sb.toString());
    }
}
