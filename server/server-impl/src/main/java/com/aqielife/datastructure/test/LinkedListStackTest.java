package com.aqielife.datastructure.test;

import com.aqielife.datastructure.entity.LinkedListStack;


public class LinkedListStackTest {

    public static void main(String[] args) {
        test();
    }

    public static void test(){
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        for(int i = 0;i < 5; ++i){
            stack.push(i);
            System.out.println(stack);
        }
        stack.pop();
        System.out.println(stack);
    }
}
