package com.aqielife.datastructure.entity;

// 链表 真正的动态数据结构
public class LinkedListPrim<E> {

    private class Node{
        public E e;
        public Node next;

        public Node(E e,Node next){
            this.e = e;
            this.next = next;
        }

        public Node(E e){
            this(e,null);
        }

        public Node(){
            this(null,null);
        }

        @Override
        public String toString() {
            return e.toString();
        }
    }

    private Node head;  // 头节点
    private int size;   // 链表中元素

    public LinkedListPrim(){
        head = null;
        size = 0;
    }

    // 获取链表中元素个数
    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    // 链表头添加新元素   node.next = head; head = node;  向head添加节点
    public void addFirst(E e){
        head = new Node(e,head);      // 新建node 直接指向head ,然后将其赋值给head
        size ++;
        /*Node node = new Node(e);
        node.next = head;
        head = node;*/
        size++;
    }

    public void addLast(E e){
        add(size, e);
    }

    // 链表中间添加元素 (练习用)
    public void add(int index,E e){
        if(index < 0 || index > size) {
            throw new IllegalArgumentException("Add failed. Illegal index.");
        }
        if(index == 0){
            addFirst(e);
        } else{
            Node prev = head;
            for(int i = 0;i < index - 1;++i){
                prev = prev.next;
            }
            /*Node node = new Node(e);
            node.next = prev.next;
            prev.next = node;*/
            prev.next = new Node(e, prev.next);
            size ++;
        }
    }
}
