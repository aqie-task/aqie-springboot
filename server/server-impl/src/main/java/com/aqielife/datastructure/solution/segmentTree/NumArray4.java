package com.aqielife.datastructure.solution.segmentTree;

import java.util.Arrays;

public class NumArray4 {
    private SegmentTree<Integer> segmentTree;

    public NumArray4(int[] nums) {
        if(nums.length > 0){
            Integer[] data = new Integer[nums.length];
            for (int i = 0; i < nums.length; ++i)
                data[i] = nums[i];
            segmentTree = new SegmentTree<>(data,(a, b) -> a + b);
        }
    }

    public int sumRange(int i, int j) {
        if(segmentTree == null)
            throw new IllegalArgumentException("Segment Tree is null");
        return segmentTree.query(i, j);
    }

    public void update(int i, int value){
        if(segmentTree == null)
            throw new IllegalArgumentException("Error");
        segmentTree.update(i, value);
    }

    private class SegmentTree<E> {
        // 存储线段树数据
        private E[] tree;
        // 用户传进来数据
        private E[] data;

        private Merger<E> merger;

        /**
         * 1.开辟数组空间和线段树空间
         * 2.
         * @param arr
         */
        public SegmentTree(E[] arr, Merger<E> merger){
            this.merger = merger;

            data = (E[])new Object[arr.length];
            for (int i = 0; i < arr.length;++i)
                data[i] = arr[i];
            tree = (E[])new Object[4 * arr.length];
            buildSegmentTree(0, 0, arr.length - 1);
        }

        /**
         * 在treeIndex的位置创建表示区间[l...r]的线段树
         * @param treeIndex 要创建的线段树对应的根节点
         * @param l 当前节点对应的左右区间
         * @param r
         */
        private void buildSegmentTree(int treeIndex, int l, int r) {
            // 递归到底 treeIndex区间只有一个元素
            if(l == r) {
                tree[treeIndex] = data[l];
                return;
            }
            // treeIndex 表示一个区间的左右节点索引
            int leftTreeIndex = leftChild(treeIndex);
            int rightTreeIndex = rightChild(treeIndex);
            // 区间范围  l ,mid   mid+1, r
            int mid = l + (r-l)/2;
            // 递归创建左右子树
            buildSegmentTree(leftTreeIndex,l,mid);
            buildSegmentTree(rightTreeIndex,mid+1,r);

            tree[treeIndex] = merger.merge((tree[leftTreeIndex]), tree[rightTreeIndex]);
        }

        public int getSize(){
            return data.length;
        }

        public E get(int index){
            if(index < 0 || index >= data.length){
                throw new IllegalArgumentException("Index is illegal.");
            }
            return data[index];
        }

        // 返回完全二叉树的数组表示中，一个索引所表示的元素的左孩子节点的索引
        private int leftChild(int index){
            return 2*index + 1;
        }

        // 返回完全二叉树的数组表示中，一个索引所表示的元素的右孩子节点的索引
        private int rightChild(int index){
            return 2*index + 2;
        }

        // 返回区间[queryL, queryR]的值
        public E query(int queryL, int queryR){
            if(queryL < 0 || queryL >= data.length ||
                    queryR < 0 || queryR >= data.length || queryL > queryR){
                throw new IllegalArgumentException("Index is illegal.");
            }
            return query(0, 0, data.length - 1, queryL, queryR);
        }

        /**
         * 在以treeIndex为根的线段树中[l...r]的范围里，搜索区间[queryL...queryR]的值
         * @param treeIndex 线段树根节点索引
         * @param l
         * @param r
         * @param queryL
         * @param queryR
         * @return
         */
        private E query(int treeIndex, int l, int r, int queryL, int queryR){
            if(l == queryL && r == queryR){
                return tree[treeIndex];
            }
            int mid = l + ( r - l) / 2;

            int leftTreeIndex = leftChild(treeIndex);
            int rightTreeIndex = rightChild(treeIndex);

            if(queryL >= mid + 1){  // 右子树查找
                return query(rightTreeIndex, mid + 1, r, queryL, queryR);
            }else if(queryR <= mid){
                return query(leftTreeIndex, l, mid, queryL, queryR);
            }
            E leftResult = query(leftTreeIndex, l, mid, queryL, mid);
            E rightResult = query(rightTreeIndex, mid + 1, r, mid + 1, queryR);
            return merger.merge(leftResult, rightResult);
        }

        /**
         * 更新操作
         * @param index
         * @param e
         */
        public void update(int index, E e){
            if(index < 0 || index >= data.length)
                throw new IllegalArgumentException("Index is illegal");
            data[index] = e;
            update(0, 0, data.length - 1, index, e);
        }

        /**
         * 以treeIndex为根的线段树中更新index的值为e
         * @param treeIndex
         * @param l
         * @param r
         * @param index
         * @param e
         */
        private void update(int treeIndex, int l, int r, int index, E e) {
            if(l == r){
                tree[treeIndex] = e;
                return;
            }

            int mid = l + (r - l) / 2;
            // treeIndex的节点分为[l...mid]和[mid+1...r]两部分

            int leftTreeIndex = leftChild(treeIndex);
            int rightTreeIndex = rightChild(treeIndex);
            if(index >= mid + 1)
                update(rightTreeIndex, mid + 1, r, index, e);
            else // index <= mid
                update(leftTreeIndex, l, mid, index, e);

            tree[treeIndex] = merger.merge(tree[leftTreeIndex], tree[rightTreeIndex]);
        }

        @Override
        public String toString() {
            return "SegmentTree{" +
                    "tree=" + (tree == null ? null : Arrays.asList(tree)) +
                    ", data=" + (data == null ? null : Arrays.asList(data)) +
                    ", merger=" + merger +
                    '}';
        }
    }

    public interface Merger<E> {
        E merge(E a, E b);
    }
}
