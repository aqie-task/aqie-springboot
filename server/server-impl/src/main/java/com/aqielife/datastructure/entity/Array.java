package com.aqielife.datastructure.entity;

/**
 * 动态数组，支持泛型
 * @param <E>
 */
public class Array<E> {
    private E[] data;

    private int size;   // 数组实际长度

    public Array(int capacity){
        data = (E[])new Object[capacity];   // 静态数组   先 new Object数组，再强制转换
        size = 0;
    }

    // 无参构造函数,默认长度为0
    public Array(){
        this(10);
    }

    // 根据传入静态数组生成动态数组
    public Array(E[] arr){
        data = (E[])new Object[arr.length];
        for(int i = 0; i < arr.length; ++i){
            data[i] = arr[i];
        }
        size = arr.length;
    }

    // 返回数组长度
    public int getSize(){
        return this.size;
    }

    // 返回数组容量
    public int getCapacity(){
        return data.length;
    }

    // 判断数组是否为空
    public boolean isEmpty(){
        return size == 0;
    }

    // 向所有元素后添加一个新元素
    public void addLast(E e){
        add(size,e);
    }

    // 在所有元素前添加一个新元素
    public void addFirst(E e){
        add(0,e);
    }

    // 在index索引位置插入一个新元素
    public void add(int index, E e){
        // 超出长度 扩容变动态数组
        if(size == data.length){
            resize(2 * data.length);
        }

        if(index < 0 || index > size)
            throw new IllegalArgumentException("Add failed. Require index >= 0 and index <= size.");

        // 从后往前复制
        // System.arraycopy(data, index, data, index + 1, size - index);
        for(int i = size - 1; i >= index; --i){     // -1.  >= index
            data[i + 1] = data[i];
        }
        data[index] = e;
        size++;
    }

    // 获取index索引位置的元素       O(1)
    public E get(int index){
        if(index < 0 || index >= size)
            throw new IllegalArgumentException("Get failed. Index is illegal.");
        return data[index];
    }

    // 获取第一个元素
    public E getFirst(){
        return get(0);
    }

    // 获取最后一个元素
    public E getLast(){
        return get(size-1);
    }

    // 修改index索引位置的元素为e
    public void set(int index, E e){
        if(index < 0 || index >= size)
            throw new IllegalArgumentException("Set failed. Index is illegal.");
        data[index] = e;
    }

    // 判断数组是否存在某个元素         O(1)
    public boolean contains(E e){
        for(E a : data){
            if(a.equals(e)){
                return  true;
            }
        }
        return false;
    }

    // 查找数组中元素e 所在的索引,不存在,则返回-1     O(n)
    public int find(E e){
        for(int i = 0;i < size; i++){
            if(data[i].equals(e)){
                return i;
            }
        }
        return -1;
    }

    // 删除指定元素,并传入
    public E remove(int index){
        if(index < 0 || index >= size){
            throw new IllegalArgumentException("Remove failed. Index is illegal.");
        }
        E ret = data[index];
        // 从后往前复制
        // System.arraycopy(data, index+1, data, index, size - index);
        for(int i = index + 1; i < size; i++){
            data[i - 1] = data[i];
        }
        size--;
        data[size] = null;  // 对象存着引用

        // 缩容  Lazy
        if(size == data.length / 4 && data.length / 2 != 0){
            resize(data.length / 2);
        }
        return ret;
    }

    // 删除第一个元素               O(n)
    public E removeFirst(){
        return remove(0);
    }

    // 删除最后一个元素             O(1)
    public E removeLast(){
        return remove(size-1);
    }

    // 数组删除元素                 O(n/2) = O(n)
    public void removeElement(E e){
        int index = find(e);
        if(index != -1){
            remove(index);
        }
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(String.format("Array: size = %d , capacity = %d\n", size, data.length));
        res.append("[");
        for(int i = 0; i < size; i++){
            res.append(data[i]);
            if(i != size - 1){
                res.append(", ");
            }else{
                res.append("]");
            }
        }
        return res.toString();
    }

    private void resize(int newCapacity){
        E[] newData = (E[])new Object[newCapacity];
        // System.arraycopy(data, 0, newData, 0, size);
        for(int i = 0 ; i < size ; i ++)
            newData[i] = data[i];
        data = newData;
    }

    public void swap(int i, int j){
        if(i < 0 || i >= size || j < 0 || j >= size){
            throw new IllegalArgumentException("Index is illegal.");
        }
        E e = data[i];
        data[i] = data[j];
        data[j] = e;
    }
}
