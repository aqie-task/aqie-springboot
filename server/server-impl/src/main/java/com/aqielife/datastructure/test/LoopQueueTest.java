package com.aqielife.datastructure.test;

import com.aqielife.datastructure.Interface.Queue;
import com.aqielife.datastructure.entity.ArrayQueue;
import com.aqielife.datastructure.entity.LoopQueue;


import java.util.Random;

public class LoopQueueTest {

    // 统计程序运行时间，运行 opCount个 enqueue 所需时间
    private static double countTime(ArrayQueue<Integer> queue,int opCount){
        long startTime = System.nanoTime();
        Random random = new Random();
        for(int i = 0 ; i < opCount ; i ++)
            queue.enqueue(random.nextInt(Integer.MAX_VALUE));
        for(int i = 0; i < opCount; ++i){
            queue.dequeue();
        }
        long endTime = System.nanoTime();
        return (endTime - startTime) / 1000000000.0;
    }
    private static double countTime(LoopQueue<Integer> queue,int opCount){
        long startTime = System.nanoTime();
        Random random = new Random();
        for(int i = 0 ; i < opCount ; i ++) {
            queue.enqueue(random.nextInt(Integer.MAX_VALUE));
        }
        for(int i = 0; i < opCount; ++i){
            queue.dequeue();
        }
        long endTime = System.nanoTime();
        return (endTime - startTime) / 1000000000.0;
    }


    public void aqie(){
        int opCount = 100000;
       // testLoopQueue();
        ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
        double time1 = countTime(arrayQueue, opCount);
        System.out.println("ArrayQueue, time: " + time1 + " s");

        LoopQueue<Integer> loopQueue = new LoopQueue<>();
        double time2 = countTime(loopQueue, opCount);
        System.out.println("LoopQueue, time: " + time2 + " s");
    }

    public void testLoopQueue(){
        LoopQueue<Integer> queue = new LoopQueue<>(5);
        for(int i = 0 ; i < 10 ; i ++){
            queue.enqueue(i);
            System.out.println(queue);
            if(i % 3 == 2){
                queue.dequeue();
                System.out.println(queue);
            }
        }
    }


}
