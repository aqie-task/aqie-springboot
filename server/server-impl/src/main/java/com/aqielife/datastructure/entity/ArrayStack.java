package com.aqielife.datastructure.entity;

import com.aqielife.datastructure.Interface.Stack;

public class ArrayStack<E> implements Stack{

    private Array<E> array;

    // 传入指定长度
    public ArrayStack(int capacity){
        array = new Array<>(capacity);
    }

    public ArrayStack(){
        array = new Array<>();
    }

    // 查看栈的元素个数
    @Override
    public int getSize() {
        return array.getSize();
    }

    // 入栈
    public void push(E e) {
        array.addLast(e);
    }

    // 出栈
    @Override
    public E pop() {
        return array.removeLast();
    }

    // 查看栈顶元素
    @Override
    public Object peek() {
        return array.getLast();
    }

    // 判断是否为空
    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    public int getCapacity(){
        return array.getCapacity();
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Stack: ");
        res.append('[');
        for(int i = 0; i < array.getSize(); i++){
            res.append(array.get(i));
            if(i != array.getSize() - 1){
                res.append(", ");
            }else{
                res.append("] top");
            }
        }
        return res.toString();
    }
}
