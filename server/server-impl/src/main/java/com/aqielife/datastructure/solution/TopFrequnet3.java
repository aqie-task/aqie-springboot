package com.aqielife.datastructure.solution;

import java.util.*;

/**
 * 347
 * java 内置的优先队列(最小堆)
 */
public class TopFrequnet3 {

    public List<Integer> topKFrequent(int[] nums, int k) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int num : nums){
            if(map.containsKey(num)){
                map.put(num, map.get(num) + 1);
            }else {
                map.put(num, 1);
            }
        }

        // 相当于重新定义了整型比较大小, 同时闭包可以拿到变量
        PriorityQueue<Integer> pq = new PriorityQueue<>(new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {  // o1 > o2 返回 1
                return map.get(o1) - map.get(o2);
            }
        });
        for (int key : map.keySet()){
            if(pq.size() < k){
                pq.add(key);
            }else if(map.get(key) > map.get(pq.peek())){        // peek队首元素
                pq.remove();
                pq.add(key);
            }
        }

        LinkedList<Integer> res = new LinkedList<>();
        while(!pq.isEmpty()){
            res.add(pq.remove());
        }
        return res;
    }
}
