package com.aqielife.datastructure.test;

import com.aqielife.datastructure.entity.LinkedListQueue;


public class LinkedListQueueTest {
    public static void main(String[] args) {
        test();
    }

    public static void test(){
        LinkedListQueue<Integer> queue = new LinkedListQueue<>();
        for(int i = 0 ; i < 10 ; i ++){
            queue.enqueue(i);
            System.out.println(queue);

            if(i % 3 == 2){
                queue.dequeue();
                System.out.println(queue);
            }
        }
    }
}
