package com.aqielife.datastructure.test;

import com.aqielife.datastructure.entity.ArrayStack;


//import java.util.Stack;


public class TestStack {

    public static void main(String[] args) {
        System.out.println(isValid("()[]{}"));
        System.out.println(isValid("([)]"));
        System.out.println(isValid("()[()]{}"));
    }


    public void test(){
        ArrayStack<Integer> stack = new ArrayStack<>();
        for (int i = 0; i < 5;++i){
            stack.push(i);
            System.out.println(stack);
        }

        stack.pop();
        System.out.println(stack);
    }

    public static boolean isValid(String s){
        // Stack<Character> stack = new Stack<>();
        ArrayStack<Character> stack = new ArrayStack<>();
        for(int i = 0;i < s.length(); i++){
            char c = s.charAt(i);
            if(c == '(' || c =='[' || c == '{'){
                stack.push(c);
            }else{
                if (stack.isEmpty()){
                    return false;
                }
                char topChar = stack.pop();
                if(c == ')' && topChar != '(')
                    return false;
                if(c == ']' && topChar != '[')
                    return false;
                if(c == '}' && topChar != '{')
                    return false;
            }
        }
        return stack.isEmpty();     // 只有栈为空才匹配成功
    }

}
