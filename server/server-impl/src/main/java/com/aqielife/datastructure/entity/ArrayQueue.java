package com.aqielife.datastructure.entity;

import com.aqielife.datastructure.Interface.Queue;

/**
 *  数组队列 ，出队操作 O(n)
 * @param <E>
 */
public class ArrayQueue<E> implements Queue<E>{
    private Array<E> array;

    public ArrayQueue(){
        array = new Array<>();
    }

    public ArrayQueue(int capacity){
        array = new Array<>(capacity);
    }

    @Override                           // O(1)
    public int getSize() {
        return array.getSize();
    }

    @Override                           // O(1)
    public boolean isEmpty() {
        return array.isEmpty();
    }

    // 队列添加元素                       // O(1) 均摊
    public void enqueue(E e) {
        array.addLast(e);
    }

    // 队列移出元素                       // O(n)
    @Override
    public E dequeue() {
        return array.removeFirst();
    }

    @Override                           // O(1)
    public E getFront() {
        return array.getFirst();
    }

    public int getCapacity(){
        return array.getCapacity();
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Queue: ");
        res.append("front [");
        for(int i = 0; i < array.getSize(); i++){
            res.append(array.get(i));
            if(i != array.getSize() - 1){
                res.append(", ");
            }else{
                res.append("] tail");
            }
        }
        return res.toString();
    }
}
