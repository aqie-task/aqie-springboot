package com.aqielife.datastructure.unionFind;

// 使用一个数组构建一棵指向父节点的树
public class QuickUnion  implements UF{
    // parent[i]表示第一个元素所指向的父节点
    private int[] parent;

    public QuickUnion(int size){
        parent = new int[size];

        for (int i = 0; i < size; ++i)
            parent[i] = i;
    }

    @Override
    public int getSize() {
        return parent.length;
    }

    // O(h)复杂度, h为树的高度
    @Override
    public boolean isConnected(int p, int q) {
        return find(p) == find(q);
    }

    // O(h)复杂度, h为树的高度
    @Override
    public void unionElements(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(p);
        if(pRoot == qRoot){
            return;
        }
        parent[pRoot] = qRoot;  // p 的根节点指向q
    }

    // 查找过程, 查找元素p所对应的根节点
    private int find(int p){
        if(p < 0 || p >= parent.length)
            throw new IllegalArgumentException("p is out of bound.");

        // 根节点的特点: parent[p] == p  根节点父节点是其本身
        while (p != parent[p]){
            p = parent[p];      // 一步一步向上查
        }
        return p;
    }
}
