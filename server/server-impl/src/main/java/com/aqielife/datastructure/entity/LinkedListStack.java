package com.aqielife.datastructure.entity;

import com.aqielife.datastructure.Interface.Stack;

/**
 *  链表实现栈
 * @param <E>
 */
public class LinkedListStack<E> implements Stack {
    private LinkedList<E> list;

    public LinkedListStack(){
        list = new LinkedList<>();
    }
    @Override
    public int getSize() {
        return list.getSize();
    }

    public void push(E e){
        list.addFirst(e);
    }

    @Override
    public E pop() {
        return list.removeFirst();
    }

    @Override
    public E peek() {
        return list.getFirst();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public String toString() {
        String res = "Stack: top " +
                list;
        return res;
    }
}
