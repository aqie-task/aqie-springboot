package com.aqielife.datastructure.balancedBinaryTree;

public interface IBinaryTree<K, V> {
    public int getSize();

    public boolean isEmpty();

    public void add(K key, V value);

    public boolean contains(K key);

    public void set(K key, V newValue);

    public V remove(K key);

    public V get(K key);

    public boolean isBST();

    public boolean isBalanced();
}
