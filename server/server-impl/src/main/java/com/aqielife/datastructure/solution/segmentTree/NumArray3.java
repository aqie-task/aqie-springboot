package com.aqielife.datastructure.solution.segmentTree;

// 303
public class NumArray3 {
    private int[] sum;

    private int[] numsCopy;     // 单独存储nums 数组
    public NumArray3(int[] nums){           // O(n)
        numsCopy = new int[nums.length];
        for (int i = 0; i < nums.length;i++)
            numsCopy[i] = nums[i];

        sum = new int[nums.length + 1];
        sum[0] = 0;
        for(int i = 1; i < sum.length; i++){
            sum[i] = sum[ i - 1] + nums[i - 1];
        }
    }
    public void update(int i, int value){
        numsCopy[i] = value;
        for(int j = i + 1; j < sum.length;j++){
            sum[j] = sum[j - 1] + numsCopy[j - 1];
        }
    }

    public int sumRange(int i, int j){   // O(1)
        return sum[j + 1] - sum[i];
    }
}
