package com.aqielife.datastructure.hashTable;

import java.util.HashMap;
import java.util.HashSet;

public class Test {
    public static void main(String[] args) {
        int a = 36;
        System.out.println(((Integer)a).hashCode());
        int b = -36;
        System.out.println(((Integer)b).hashCode());
        double c = 3.14;
        System.out.println(((Double)c).hashCode());

        String d = "aqie";
        System.out.println(d.hashCode());

        Student student = new Student(3, 2, "aqie",  "toby");
        System.out.println(student.hashCode());

        HashSet<Student> set = new HashSet<>();
        set.add(student);

        HashMap<Student, Integer> map = new HashMap();
        map.put(student, 100);
    }
}
