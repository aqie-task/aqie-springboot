package com.aqielife.datastructure.solution;

public class Sum {
    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5};
        System.out.println(sum(nums));
    }

    // 递归求解数组和
    public static int sum(int[] arr){
        return sum(arr,0);
    }

    /**
     * 计算 [l,n) 区间所有数字的和
     * @param arr       要求和数组
     * @param l         左边界的点
     * @return
     */
    private static int sum(int[] arr, int l){
        if(l == arr.length){    // 数组长度为0
            return 0;
        }
        return arr[l] + sum(arr,l + 1);
    }
}
