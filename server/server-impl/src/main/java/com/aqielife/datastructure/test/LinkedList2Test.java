package com.aqielife.datastructure.test;

import com.aqielife.datastructure.entity.LinkedList;


public class LinkedList2Test {

    public static void main(String[] args) {
        test();
    }
    public static void test(){
        LinkedList<Integer> linkedList2 = new LinkedList<>();
        for(int i =0; i < 5;++i){
            linkedList2.addFirst(i);
            System.out.println(linkedList2);
        }
        linkedList2.add(2,666);
        System.out.println(linkedList2);
        linkedList2.remove(2);
        System.out.println(linkedList2);
        linkedList2.removeFirst();
        System.out.println(linkedList2);
        linkedList2.removeLast();
        System.out.println(linkedList2);
    }
}
