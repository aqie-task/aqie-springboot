package com.aqielife.datastructure.hashTable;

import com.aqielife.datastructure.balancedBinaryTree.IBinaryTree;

import java.util.TreeMap;

/**
 * TreeMap(底层红黑树)数组
 * @param <K>
 * @param <V>
 * N/M
 * N == size
 */
public class HashTable<K, V> implements IBinaryTree<K, V> {

    // 素数表
    private final int[] capacity
            = {53, 97, 193, 389, 769, 1543, 3079, 6151, 12289, 24593,
            49157, 98317, 196613, 393241, 786433, 1572869, 3145739, 6291469,
            12582917, 25165843, 50331653, 100663319, 201326611, 402653189, 805306457, 1610612741};

    // 每个地址冲突的上界
    private static final int upperTol = 10;
    private static final int lowerTol = 2;
    // private static final int initCapacity = 7;
    private int initCapacityIndex = 0; // 上面素数表索引

    private TreeMap<K, V>[] hashtable;
    private int size;
    private int M;

    public HashTable(){
        this.M = capacity[initCapacityIndex];
        size = 0;
        hashtable = new TreeMap[M];
        for(int i = 0 ; i < M ; i ++)
            hashtable[i] = new TreeMap<>();
    }


    // K 先转换为整型 哈希函数
    private int hash(K key){
        return (key.hashCode() & 0x7fffffff) % M;
    }

    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void add(K key, V value) {
        TreeMap<K, V> map = hashtable[hash(key)];
        if(map.containsKey(key))
            map.put(key, value);    // 更新数据
        else{
            map.put(key, value);    // 添加数据
            size ++;
            
            if(size >= upperTol * M && initCapacityIndex + 1 < capacity.length){ // 防止素数数组越界
                initCapacityIndex++;
                resize(capacity[initCapacityIndex]);
                // resize(2 * M);
            }
        }
    }

    private void resize(int newM) {
        TreeMap<K, V>[] newHashTable = new TreeMap[newM];
        // TreeMap[] newHashTable = new TreeMap[newM];
        for(int i = 0 ; i < newM ; i ++)
            newHashTable[i] = new TreeMap<>();

        int oldM = M;
        this.M = newM;              // 注意hash 函数中取模对 M
        for(int i = 0 ; i < oldM ; i ++){   // 注意边界
            TreeMap<K, V> map = hashtable[i];
            for(K key: map.keySet())
                newHashTable[hash(key)].put(key, map.get(key));
        }

        this.hashtable = newHashTable;
    }

    @Override
    public boolean contains(K key) {
        return hashtable[hash(key)].containsKey(key);
    }

    @Override
    public void set(K key, V value) {
        TreeMap<K, V> map = hashtable[hash(key)];
        if(!map.containsKey(key))
            throw new IllegalArgumentException(key + " doesn't exist!");

        map.put(key, value);
    }

    @Override
    public V remove(K key) {
        V ret = null;
        TreeMap<K, V> map = hashtable[hash(key)];
        if(map.containsKey(key)){
            ret = map.remove(key);
            size --;

            if(size < lowerTol * M && initCapacityIndex - 1 >= 0) // 注意边界,缩容过程至少是53
                initCapacityIndex--;
                resize(capacity[initCapacityIndex]);
                // resize(M / 2);
        }
        return ret;
    }

    @Override
    public V get(K key) {
        return hashtable[hash(key)].get(key);
    }

    @Override
    public boolean isBST() {
        return false;
    }

    @Override
    public boolean isBalanced() {
        return false;
    }


}
