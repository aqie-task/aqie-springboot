package com.aqielife.design.pattern.behavioral.chainOfResponsibility;

/**
 * 类中包含自己同一类型的对象
 */
public abstract class Approver {
    protected Approver approver;

    public void setNextApprover(Approver approver){
        this.approver = approver;
    }

    // 成功完成要执行事件
    public abstract void deploy(Course course);
}
