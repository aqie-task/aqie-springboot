package com.aqielife.design.pattern.behavioral.strategy.factory;

import com.aqielife.design.pattern.behavioral.strategy.*;

import java.util.HashMap;
import java.util.Map;

/**
 *  促销方案工厂
 */
public  class PromotionStrategyFactory {
    private static Map<String, PromotionStrategy> PROMOTION_STRATEGY_MAP = new HashMap<>();
    static {
        PROMOTION_STRATEGY_MAP.put(PromotionKey.DISCOUNT, new DiscountPromotionStrategy());
        PROMOTION_STRATEGY_MAP.put(PromotionKey.FULL_DISCOUNT, new FullDiscountPromotionStrategy());
        PROMOTION_STRATEGY_MAP.put(PromotionKey.RETURN_CASH, new ReturnCashPromotionStrategy());
    }

    private static final PromotionStrategy NONE_PROMOTION = new EmptyPromotionStrategy();
    private PromotionStrategyFactory(){

    }
    public static PromotionStrategy getPromotionStrategy(String promotionKey){
        PromotionStrategy promotionStrategy = PROMOTION_STRATEGY_MAP.get(promotionKey);
        return promotionStrategy == null ? NONE_PROMOTION : promotionStrategy;
    }

    private interface PromotionKey{
        String DISCOUNT = "DISCOUNT";
        String FULL_DISCOUNT = "FULL_DISCOUNT";
        String RETURN_CASH = "RETURN_CASH";
    }
}
