package com.aqielife.design.pattern.behavioral.Command;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CourseVideo {
    private String courseName;

    public CourseVideo(String courseName) {
        this.courseName = courseName;
    }

    public void open(){
        log.info("{} 课程开放",courseName);
    }

    public void close(){
        log.info("{} 课程关闭",courseName);
    }
}
