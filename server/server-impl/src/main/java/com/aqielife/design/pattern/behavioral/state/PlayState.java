package com.aqielife.design.pattern.behavioral.state;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlayState extends CourseVideoState {
    @Override
    public void play() {
        log.info("Play Video");
    }

    @Override
    public void speed() {
        super.courseVideoContext.setCourseVideoState(CourseVideoContext.SPEED_STATE);
    }

    @Override
    public void pause() {
        super.courseVideoContext.setCourseVideoState(CourseVideoContext.PAUSE_STATE);
    }

    @Override
    public void stop() {
        super.courseVideoContext.setCourseVideoState(CourseVideoContext.STOP_STATE);
    }
}
