package com.aqielife.design.pattern.creational.singleton;

public class ThreadLocalInstance {
    private static final ThreadLocal<ThreadLocalInstance> threadLocalInstanceThread =
            ThreadLocal.withInitial(ThreadLocalInstance::new);

    private ThreadLocalInstance(){}

    public static ThreadLocalInstance getInstance(){
        return threadLocalInstanceThread.get();
    }

}
