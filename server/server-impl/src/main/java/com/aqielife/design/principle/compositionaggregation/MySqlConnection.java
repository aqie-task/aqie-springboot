package com.aqielife.design.principle.compositionaggregation;

public class MySqlConnection extends DBConnection {
    @Override
    public String getConection() {
        return "mysql connection";
    }
}
