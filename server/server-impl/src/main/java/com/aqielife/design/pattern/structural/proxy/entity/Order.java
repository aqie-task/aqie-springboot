package com.aqielife.design.pattern.structural.proxy.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {
    private int Id;
    private String orderInfo;
    private int userId;
}
