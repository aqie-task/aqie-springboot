package com.aqielife.design.pattern.behavioral.templatemethord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PythonCourse extends ACourse {
    @Override
    protected void packageCourse() {
        log.info("offer python code");
    }
}
