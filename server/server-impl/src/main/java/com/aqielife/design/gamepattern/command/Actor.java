package com.aqielife.design.gamepattern.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: aqie
 * @create: 2020-08-09 10:47
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Actor {
    private String name;

    public void moveTo(int x, int y){
        System.out.println(name + " moveTo " + x + "-" + y);
    }
}
