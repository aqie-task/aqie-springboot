package com.aqielife.design.pattern.structural.decorator.v1;

public class BattercakeWithEgg extends Battercake {
    @Override
    protected String getDesc() {
        return super.getDesc() + " an egg";
    }

    @Override
    protected int cost() {
        return super.cost() + 1;
    }
}
