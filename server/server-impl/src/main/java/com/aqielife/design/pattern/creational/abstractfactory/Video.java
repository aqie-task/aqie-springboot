package com.aqielife.design.pattern.creational.abstractfactory;

public  abstract class Video {
    public abstract void produce();
}
