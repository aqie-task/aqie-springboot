package com.aqielife.design.pattern.behavioral.observer.listener;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GuavaEvent {
    @Subscribe
    public void subscribe(String s){
        log.info("执行subscribe 方法,传入参数：{}",s);
    }
}
