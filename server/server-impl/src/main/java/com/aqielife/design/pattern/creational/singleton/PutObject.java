package com.aqielife.design.pattern.creational.singleton;

public class PutObject implements Runnable{
    @Override
    public void run() {
        // ContainerSingleton
        /*ContainerSingleton.putInstance("object", new Object());
        Object instance = ContainerSingleton.getInstance("object");*/

        // ThreadLocalInstance
        ThreadLocalInstance instance = ThreadLocalInstance.getInstance();
        System.out.println(Thread.currentThread().getName() + " " + instance);
    }
}
