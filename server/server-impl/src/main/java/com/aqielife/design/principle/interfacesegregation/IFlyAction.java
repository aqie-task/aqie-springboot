package com.aqielife.design.principle.interfacesegregation;

public interface IFlyAction {
    void fly();
}
