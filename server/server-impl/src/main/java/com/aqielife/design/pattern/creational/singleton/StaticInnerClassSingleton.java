package com.aqielife.design.pattern.creational.singleton;

// 基于内部类的延时初始化方案
public class StaticInnerClassSingleton {

    private StaticInnerClassSingleton(){}

    // 静态内部类
    private static class InnerClass{
        private static StaticInnerClassSingleton staticInnerClassSingleton = new StaticInnerClassSingleton();
    }

    // 开放获取内部类
    public static StaticInnerClassSingleton getInstance(){
        return InnerClass.staticInnerClassSingleton;
    }
}
