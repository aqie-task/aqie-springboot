package com.aqielife.design.pattern.structural.decorator.v2;

public interface Senbei {
    public String getDesc();

    public int cost();
}
