package com.aqielife.design.principle.liskovsubstitution;

public interface Quadrangle {
    long getWidth();
    long getLength();
}
