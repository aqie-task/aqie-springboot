package com.aqielife.design.pattern.behavioral.chainOfResponsibility;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class ArticleApprover extends Approver {
    @Override
    public void deploy(Course course) {
        if(StringUtils.isNoneEmpty(course.getArticle())){
            log.info("{} have article : access!", course.getName());
            // 可以拿到父类的 approver; 下面还有要审批流程继续执行
            if (approver != null){
                approver.deploy(course);
            }
        }else{
            log.info("{}do not have article : deny!", course.getName());
            return;
        }
    }
}
