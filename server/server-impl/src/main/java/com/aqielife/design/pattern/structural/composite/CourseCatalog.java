package com.aqielife.design.pattern.structural.composite;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程目录类
 */
@Slf4j
public class CourseCatalog extends CatalogComponent {
    // 目录组件抽象类
    private List<CatalogComponent> items = new ArrayList<>();
    // 目录名称
    private String name;
    private Integer level;

    /**
     * int 有默认值 Integer可以做空判断
     * @param name
     * @param level
     */
    public CourseCatalog(String name,Integer level) {
        this.name = name;
        this.level = level;
    }

    @Override
    public void add(CatalogComponent catalogComponment) {
        items.add(catalogComponment);
    }

    @Override
    public void remove(CatalogComponent catalogComponent) {
       items.remove(catalogComponent);
    }

    @Override
    public void print() {
        for(CatalogComponent catalogComponent : items){
            if (this.level != null){
                for (int i = 0; i < this.level; i++){
                    System.out.println(" ");
                }
            }
            catalogComponent.print();
        }
    }
}
