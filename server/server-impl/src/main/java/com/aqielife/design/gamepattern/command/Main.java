package com.aqielife.design.gamepattern.command;

import com.aqielife.design.gamepattern.command.impl.FireCommand;
import com.aqielife.design.gamepattern.command.impl.JumpCommand;
import com.aqielife.design.gamepattern.command.impl.LurchCommand;
import com.aqielife.design.gamepattern.command.impl.SwapCommand;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author: aqie
 * @create: 2020-08-09 08:56
 *  x  jump   fire
 *  y  fire   jump
 *  a  swap   lurch
 *  b  lurch  swap
 **/
@Slf4j
public class Main {
    public static void main(String[] args) {
        // 默认按键配置
        InputHandler handler = new InputHandler();
        Button button = new Button("B");
        handler.pressed(button).execute(new Actor("com/aqie"));
        handler.pressed(button).execute(new Actor("cium"));
        Map<Button, Command> relation = handler.getRelation();
        for (Map.Entry<Button,Command> entry : relation.entrySet()){
            System.out.println("button: "+entry.getKey());
            entry.getValue().execute();
        }

        // 自定义按键配置
        handler = InputHandler.builder()
                .bindA(new LurchCommand())
                .bindB(new SwapCommand(new Actor("com/aqie"),10,15))
                .bindX(new FireCommand())
                .bindY(new JumpCommand())
                .build();

        relation = handler.getRelation();
        log.info("map: {}", relation);
        for (Map.Entry<Button,Command> entry : relation.entrySet()){
            System.out.println("button: "+entry.getKey());
            entry.getValue().execute();

        }

    }
}
