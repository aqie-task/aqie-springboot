package com.aqielife.design.pattern.behavioral.observer;

import lombok.extern.slf4j.Slf4j;

import java.util.Observable;
import java.util.Observer;

/**
 *  Teacher 真正的观察者
 *  老师观察的是课程
 *  问题属于课程
 */
@Slf4j
public class Teacher implements Observer {
    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        Course course = (Course)o;
        Question question = (Question)arg;
        log.info("{} 老师的 {} ; {} 提出的问题是： {}",name, course.getCourseName(), question.getUserName(),question.getQuestionContent());
    }
}
