package com.aqielife.design.pattern.behavioral.chainOfResponsibility;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Course {
    private String name;
    private String article;
    private String video;

    public Course(String name, String article, String video) {
        this.name = name;
        this.article = article;
        this.video = video;
    }
}



