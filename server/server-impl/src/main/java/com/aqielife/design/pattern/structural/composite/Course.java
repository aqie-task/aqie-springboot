package com.aqielife.design.pattern.structural.composite;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *  课程类
 *  可以获取名字 价格 支持打印
 */
@Slf4j
public class Course extends CatalogComponent {
    @Getter
    private String name;
    @Getter
    private double price;

    public Course(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public void print() {
        log.info("course name:{} course price: {}",this.name,this.price);
    }
}
