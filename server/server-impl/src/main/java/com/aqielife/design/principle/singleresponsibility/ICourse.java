package com.aqielife.design.principle.singleresponsibility;

// 接口拆分 ICouseContent ICouseManager
public interface ICourse {
    String getCourseName();
    byte[] getCourseVideo();

    void studyCourse();
    void refundCourse();
}
