package com.aqielife.design.pattern.behavioral.Command;

/**
 * 局部关闭 课程中某一小节
 */
public interface Command {
    void execute();
}
