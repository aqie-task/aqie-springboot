package com.aqielife.design.pattern.behavioral.strategy;

import lombok.extern.slf4j.Slf4j;

// 返现
@Slf4j
public class ReturnCashPromotionStrategy implements PromotionStrategy{
    @Override
    public void doPromotion() {
      log.info("CashBack promotion");
    }
}
