package com.aqielife.design.principle.openclose;

public class JavaDiscountCourse extends JavaCourse{
    public JavaDiscountCourse(Integer id, String name, Double price) {
        super(id, name, price);
    }

    Double getDiscountPrice(){
        return super.getPrice() * 0.8;
    }
}

