package com.aqielife.design.pattern.behavioral.templatemethord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JavaCourse extends ACourse {
    private boolean needWriteArticleFlag = false;

    public JavaCourse(boolean needWriteArticleFlag) {
        this.needWriteArticleFlag = needWriteArticleFlag;
    }

    @Override
    protected void packageCourse() {
        log.info("offer java course code");
    }

    @Override
    protected boolean needWriteArticle() {
        return this.needWriteArticleFlag;
    }
}
