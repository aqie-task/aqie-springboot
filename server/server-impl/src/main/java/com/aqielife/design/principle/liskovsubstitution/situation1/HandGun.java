package com.aqielife.design.principle.liskovsubstitution.situation1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HandGun implements Gun {
    public void HandZoom(){ log.info("small, easy to carry ");}
    public void characteristic() {log.info("one shot at a time");}
    public void shoot(){log.info("start shoot");}
}
