package com.aqielife.design.pattern.behavioral.strategy;

public interface PromotionStrategy {
    void doPromotion();
}
