package com.aqielife.design.pattern.structural.proxy.service;

import com.aqielife.design.pattern.structural.proxy.entity.Order;
import com.aqielife.design.pattern.structural.proxy.mapper.IOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IOrderServiceImpl implements IOrderService {
    @Autowired
    private IOrderDao iOrderDao;


    @Override
    public int saveOrder(Order order) {
        System.out.println("Service save Order Success!");
        if(iOrderDao.insert(order) == 1){
            return order.getId();
        }
        return 0;
    }
}
