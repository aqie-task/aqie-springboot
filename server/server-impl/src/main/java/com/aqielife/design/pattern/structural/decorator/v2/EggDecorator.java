package com.aqielife.design.pattern.structural.decorator.v2;

public class EggDecorator extends Decorator {
    public EggDecorator(Senbei senbei) {
        super(senbei);
    }

    @Override
    public String getDesc() {
        return super.getDesc() + " an egg";
    }

    @Override
    public int cost() {
        return super.cost() + 1;
    }

    @Override
    protected void doSomething() {

    }
}
