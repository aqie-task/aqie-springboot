package com.aqielife.design.principle.liskovsubstitution.situation1;

public interface Gun {
    public void HandZoom();
    public void characteristic();
    public void shoot();
}
