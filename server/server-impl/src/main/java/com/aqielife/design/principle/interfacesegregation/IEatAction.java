package com.aqielife.design.principle.interfacesegregation;

public interface IEatAction {
    void eat();
}
