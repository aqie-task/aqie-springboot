package com.aqielife.design.pattern.behavioral.interpreter;

public interface Interpreter {
    int interpret();
}
