package com.aqielife.design.pattern.behavioral.state;

import lombok.Setter;

public abstract class CourseVideoState {
    @Setter
    protected CourseVideoContext courseVideoContext;

    public abstract void play();
    public abstract void speed() throws Exception;
    public abstract void pause() throws Exception;
    public abstract void stop();
}
