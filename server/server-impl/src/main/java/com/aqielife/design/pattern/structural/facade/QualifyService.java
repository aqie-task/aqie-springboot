package com.aqielife.design.pattern.structural.facade;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QualifyService {
    public boolean isAvailable(PointsGift pointsGift){
      log.info("check " + pointsGift.getName() + "integral qualification");
      return true;
    }
}
