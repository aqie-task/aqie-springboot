package com.aqielife.design.principle.singleresponsibility;

public class FlyBird {
    public void WalkMode(String name){
        System.out.println(name + " bird fly with wings");
    }
}
