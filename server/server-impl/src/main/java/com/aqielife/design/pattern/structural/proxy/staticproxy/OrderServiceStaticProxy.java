package com.aqielife.design.pattern.structural.proxy.staticproxy;

import com.aqielife.design.pattern.structural.proxy.db.DataSourceContextHolder;
import com.aqielife.design.pattern.structural.proxy.entity.Order;
import com.aqielife.design.pattern.structural.proxy.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// 静态代理类 实现数据库分库
@Service
public class OrderServiceStaticProxy {
    // 注入目标对象
    @Autowired
    private IOrderService iOrderService;

    public int saveOrder(Order order){
        beforeMethod(order);
        int order_id = iOrderService.saveOrder(order);
        afterMethod(order);
        return order_id;
    }

    private void afterMethod(Order order) {
        System.out.println("静态代理 after code");
    }

    private void beforeMethod(Order order) {
        int orderId = order.getId();
        int dbRouter = orderId % 2;
        System.out.println("静态代理分配到【db"+dbRouter+"】处理数据");

        //todo 设置dataSource;
        DataSourceContextHolder.setDBType("db"+String.valueOf(dbRouter));
        System.out.println("静态代理 before code");
    }
}
