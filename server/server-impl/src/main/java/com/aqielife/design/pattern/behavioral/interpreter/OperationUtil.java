package com.aqielife.design.pattern.behavioral.interpreter;

public class OperationUtil {
    public static boolean isOperator(String symbol){
        return (symbol.equals("+") || symbol.equals("*"));
    }

    public static Interpreter getExpressionObject(Interpreter firstInterpreter, Interpreter secondInterpreter, String symbol){
        if (symbol.equals("+")){
            return new AddInterpreter(firstInterpreter, secondInterpreter);
        }else if (symbol.equals("*")){
            return new MultiInterpreter(firstInterpreter, secondInterpreter);
        }
        return null;
    }
}
