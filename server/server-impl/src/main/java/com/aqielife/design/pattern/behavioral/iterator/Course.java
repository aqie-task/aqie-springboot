package com.aqielife.design.pattern.behavioral.iterator;

import lombok.Getter;

public class Course {
    @Getter
    private String name;

    public Course(String name) {
        this.name = name;
    }
}
