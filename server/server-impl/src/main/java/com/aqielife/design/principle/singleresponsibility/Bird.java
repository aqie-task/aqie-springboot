package com.aqielife.design.principle.singleresponsibility;

// 拆分为两个类
public class Bird {
    public void MoveMode(String name){
        if(name.equals("ostrich")){
            System.out.println(name + " bird fly with legs");
        }else{
            System.out.println(name + " bird fly with wings");
        }
    }
}
