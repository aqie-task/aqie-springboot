package com.aqielife.design.pattern.behavioral.state;

import lombok.Getter;

public class CourseVideoContext {
    @Getter
    private CourseVideoState courseVideoState;
    public final static PlayState PLAY_STATE = new PlayState();
    public final static SpeedState SPEED_STATE = new SpeedState();
    public final static PauseState PAUSE_STATE = new PauseState();
    public final static StopState STOP_STATE = new StopState();

    public void setCourseVideoState(CourseVideoState courseVideoState) {
        this.courseVideoState = courseVideoState;
        this.courseVideoState.setCourseVideoContext(this);
    }

    public void play(){
        this.courseVideoState.play();
    }

    public void speed() throws Exception {
        this.courseVideoState.speed();
    }

    public void pause() throws Exception {
        this.courseVideoState.pause();
    }

    public void stop(){
        this.courseVideoState.stop();
    }
}
