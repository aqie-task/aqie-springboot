package com.aqielife.design.pattern.structural.facade;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PointsPaymentService {
    public boolean pay(PointsGift pointsGift){
      log.info("payment" + pointsGift.getName() + "integral success");
      return true;
    }
}
