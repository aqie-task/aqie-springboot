package com.aqielife.design.principle.liskovsubstitution.situation1;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RifleGun implements Gun{
    @Override
    public void HandZoom() {
        log.info("rifle is long ");
    }

    @Override
    public void characteristic() {
        log.info("rifle can continuous fire");
    }

    @Override
    public void shoot() {
        log.info("shoot with rifle");
    }
}
