package com.aqielife.design.principle.compositionaggregation;

public abstract class DBConnection {
    public abstract String getConection();
}
