package com.aqielife.design.pattern.creational.prototype;

import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;

@Slf4j
public class MailUtil {
    public static void sendMail(Mail mail){
        String outputContent = "向{0}同学,邮件地址:{1},邮件内容:{2}发送邮件成功";
        log.info(MessageFormat.format(outputContent,mail.getName(),mail.getEmailAddress(),mail.getContent()));
    }
    public static void saveOriginMailRecord(Mail mail){
        log.info("存储originMail记录,originMail:"+mail.getContent());
    }
}
