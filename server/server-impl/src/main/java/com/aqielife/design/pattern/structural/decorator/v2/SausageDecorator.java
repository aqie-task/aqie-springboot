package com.aqielife.design.pattern.structural.decorator.v2;

public class SausageDecorator extends Decorator {
    public SausageDecorator(Senbei senbei) {
        super(senbei);
    }


    @Override
    public String getDesc() {
        return super.getDesc() + " an sausage";
    }

    @Override
    public int cost() {
        return super.cost() + 2;
    }

    @Override
    protected void doSomething() {

    }
}
