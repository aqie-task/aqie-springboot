package com.aqielife.design.pattern.behavioral.Intermediary;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class StudyGroup {
    public static void showMessage(User user, String message){
      log.info("{},[{}:{}]" ,new Date().toString(),user.getUserName(),message);
    }
}
