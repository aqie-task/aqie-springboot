package com.aqielife.design.pattern.behavioral.observer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Question {
    private String userName;
    private String questionContent;
}
