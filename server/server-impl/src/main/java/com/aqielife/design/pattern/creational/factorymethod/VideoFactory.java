package com.aqielife.design.pattern.creational.factorymethod;


public abstract class VideoFactory {
    public abstract Video getVideo();
}
