package com.aqielife.design.pattern.creational.factorymethod;

public  abstract class Video {
    public abstract void produce();
}
