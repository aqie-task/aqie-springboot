package com.aqielife.design.pattern.structural.facade;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GiftExchangeService {
    private QualifyService qualifyService = new QualifyService();
    private PointsPaymentService pointsPaymentService = new PointsPaymentService();
    private ShippingService shippingService = new ShippingService();

    public void giftExchange(PointsGift pointsGift){
        if(qualifyService.isAvailable(pointsGift)){
            if (pointsPaymentService.pay(pointsGift)){
                String shippingNo = shippingService.shipGift(pointsGift);
                log.info("logistics system place an order success!");
            }
        }
    }
}
