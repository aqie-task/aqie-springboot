package com.aqielife.design.pattern.structural.decorator.v1;

public class BattercakeWithEggSausage extends BattercakeWithEgg {
    @Override
    protected String getDesc() {
        return super.getDesc() + " with an sausage";
    }

    @Override
    protected int cost() {
        return super.cost() + 1;
    }
}
