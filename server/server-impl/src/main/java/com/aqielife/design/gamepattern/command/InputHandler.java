package com.aqielife.design.gamepattern.command;

import com.aqielife.design.gamepattern.command.impl.FireCommand;
import com.aqielife.design.gamepattern.command.impl.JumpCommand;
import com.aqielife.design.gamepattern.command.impl.LurchCommand;
import com.aqielife.design.gamepattern.command.impl.SwapCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * 通过命令模式 构造者模式
 * 实现按键默认配置 及 自定义绑定快捷键
 * @author: aqie
 * @create: 2020-08-09 08:56
 *  x  jump   fire
 *  y  fire   jump
 *  a  swap   lurch
 *  b  lurch  swap
 **/
public class InputHandler {
    private static Map<Button, Command> relation = new HashMap<>();
    private Button A;
    private Button B;
    private Button X;
    private Button Y;

    private Command jump;
    private Command fire;
    private Command swap;
    private Command lurch;
    public InputHandler() {
        relation.put(new Button("A"), new SwapCommand());
        relation.put(new Button("B"), new LurchCommand());
        relation.put(new Button("X"), new JumpCommand());
        relation.put(new Button("Y"), new FireCommand());
    }

    public Command pressed(Button button){
        Command command = relation.get(button);
        return command;
    }

    public Map<Button, Command> getRelation(){
        return relation;
    }

    // 建造者模式
    public static RelationBuilder builder(){
        return new RelationBuilder();
    }
    public InputHandler(RelationBuilder builder){
        A = builder.A;
        B = builder.B;
        X = builder.X;
        Y = builder.Y;
    }
    public static class RelationBuilder{
        private Button A = new Button("A");
        private Button B = new Button("B");
        private Button X = new Button("X");
        private Button Y = new Button("Y");

        public RelationBuilder bindA(Command command){
            relation.put(A, command);
            return this;
        }
        public RelationBuilder bindB(Command command){
            relation.put(B, command);
            return this;
        }
        public RelationBuilder bindX(Command command){
            relation.put(X, command);
            return this;
        }
        public RelationBuilder bindY(Command command){
            relation.put(Y, command);
            return this;
        }

        public InputHandler build(){
            return new InputHandler(this);
        }
    }
}
