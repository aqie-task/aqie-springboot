package com.aqielife.design.principle.singleresponsibility;

public interface ICourseContent {
    String getCourseName();
    byte[] getCourseVideo();
}