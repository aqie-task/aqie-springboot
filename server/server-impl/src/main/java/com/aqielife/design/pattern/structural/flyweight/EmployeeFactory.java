package com.aqielife.design.pattern.structural.flyweight;


import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class EmployeeFactory {
    private static final Map<String, Employee> EMPLOYEE_MAP = new HashMap<>();

    public static Employee getManager(String department){
        Manager manager = (Manager)EMPLOYEE_MAP.get(department);

        if(manager == null){
            manager = new Manager(department);
            log.info("create department manager {}", department);
            String reportContent = department + "department report: the main content is about weather!";
            manager.setReportContent(reportContent);
            log.info("create report {}", reportContent);
            EMPLOYEE_MAP.put(department, manager);
        }
        return  manager;
    }
}
