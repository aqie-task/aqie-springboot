package com.aqielife.design.pattern.structural.proxy.dynamicproxy;

import com.aqielife.design.pattern.structural.proxy.db.DataSourceContextHolder;
import com.aqielife.design.pattern.structural.proxy.entity.Order;
import com.aqielife.design.pattern.structural.proxy.service.IOrderService;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Component
public class OrderServiceDynamicProxy implements InvocationHandler {
    // 通过构造器 传入目标对象
    private IOrderService iOrderService;

    public OrderServiceDynamicProxy(IOrderService iOrderService){
        this.iOrderService = iOrderService;
    }

    public Object bind(){
        // 目标类class
        Class cls = iOrderService.getClass();
        return Proxy.newProxyInstance(cls.getClassLoader(), cls.getInterfaces(), this);
    }

    /**
     *
     * @param proxy
     * @param method 要被增强的method
     * @param args   method 参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object argObject = args[0];
        beforeMethod(argObject);
        Object object = method.invoke(iOrderService, args);
        afterMethod(argObject);
        return object;
    }

    private void afterMethod(Object argObject) {
        System.out.println("动态代理 after code");
    }

    private void beforeMethod(Object argObject) {
        int orderId = 0;
        System.out.println("动态代理 before code");
        if(argObject instanceof Order){
            Order order = (Order)argObject;
            orderId = order.getId();
        }
        int dbRouter = orderId % 2;
        System.out.println("动态代理分配到【db"+dbRouter+"】处理数据");

        //todo 设置dataSource;
        DataSourceContextHolder.setDBType("db"+String.valueOf(dbRouter));
    }
}
