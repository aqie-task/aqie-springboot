package com.aqielife.design.pattern.creational.singleton;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public enum EnumInstance {
    INSTANCE{
        protected void test(){
            log.info("print test!");
        }
    };

    // 外部才能调用test方法
    protected abstract void test();
    @Setter
    private Object data;

    public static EnumInstance getInstance(){
        return INSTANCE;
    }
}
