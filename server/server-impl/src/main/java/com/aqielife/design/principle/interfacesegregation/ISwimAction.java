package com.aqielife.design.principle.interfacesegregation;

public interface ISwimAction {
    void swim();
}
