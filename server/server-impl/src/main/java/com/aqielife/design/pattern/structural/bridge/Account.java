package com.aqielife.design.pattern.structural.bridge;

public interface Account {
    // 账户所属银行
    Account openAccount();

    // 账户所属类型
    void showAccountType();
}
