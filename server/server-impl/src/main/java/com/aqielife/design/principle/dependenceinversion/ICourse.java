package com.aqielife.design.principle.dependenceinversion;

public interface ICourse {
    void studyCourse();
}
