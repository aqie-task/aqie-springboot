package com.aqielife.design.pattern.structural.proxy.service;

import com.aqielife.design.pattern.structural.proxy.entity.Order;

public interface IOrderService {
    int saveOrder(Order order);
}
