package com.aqielife.design.pattern.structural.proxy.mapper;

import com.aqielife.design.pattern.structural.proxy.entity.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IOrderDao {
    @Insert("insert into `order`(order_info,user_id) values(#{orderInfo},#{userId})")
    int insert(Order order);
}
