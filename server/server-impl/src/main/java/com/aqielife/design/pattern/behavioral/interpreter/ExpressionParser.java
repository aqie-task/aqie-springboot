package com.aqielife.design.pattern.behavioral.interpreter;

import lombok.extern.slf4j.Slf4j;

import java.util.Stack;

@Slf4j
public class ExpressionParser {
    private Stack<Interpreter> stack = new Stack<>();

    // todo 声明空解释器
    public int parse(String string){
        String[] strItemArray = string.split(" ");
        for(String symbol : strItemArray){
            // 是数字
            if(!OperationUtil.isOperator(symbol)){
                Interpreter numberExpression = new NumberInterpreter(symbol);
                stack.push(numberExpression);
                log.info("入栈 ： {}", numberExpression.interpret());
            }else {
                // 是运算符号
                Interpreter firstExpression = stack.pop();
                Interpreter secondExpression = stack.pop();
                log.info("出栈 {} and {}",firstExpression,secondExpression);

                Interpreter operator = OperationUtil.getExpressionObject(firstExpression, secondExpression, symbol);
                log.info("应用运算符 {}", operator);
                int result = operator.interpret();
                NumberInterpreter resultExpression = new NumberInterpreter(result);
                stack.push(resultExpression);
                log.info("阶段结果入栈: {}",resultExpression.interpret());
            }
        }
        int result = stack.pop().interpret();
        return result;
    }
}
