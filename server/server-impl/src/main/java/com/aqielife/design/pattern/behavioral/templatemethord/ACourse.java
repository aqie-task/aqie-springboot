package com.aqielife.design.pattern.behavioral.templatemethord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ACourse {
    protected final void makeCourse(){
        this.makePPT();
        this.makeVideo();
        if(needWriteArticle()){
            this.writeArticle();
        }
        this.packageCourse();
    }

    protected abstract void packageCourse();

    private void writeArticle() {
        log.info("write article");
    }

    // 钩子方法
    protected boolean needWriteArticle() {
        return false;
    }

    private void makeVideo() {
        log.info("make video");
    }

    private void makePPT() {
        log.info("make ppt");
    }
}
