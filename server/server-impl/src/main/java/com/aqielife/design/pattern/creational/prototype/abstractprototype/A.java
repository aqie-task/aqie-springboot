package com.aqielife.design.pattern.creational.prototype.abstractprototype;

// 抽象类实现原型模式
public abstract class A implements Cloneable{
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
