package com.aqielife.design.principle.liskovsubstitution.situation1;

public class Solider {
    private Gun gun;

    public void setGun(Gun gun) {
        this.gun = gun;
    }
    public void startShoot(){
        this.gun.HandZoom();
        this.gun.characteristic();
        this.gun.shoot();
    }
}
