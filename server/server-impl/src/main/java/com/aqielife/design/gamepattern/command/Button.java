package com.aqielife.design.gamepattern.command;

import lombok.Data;

/**
 * @author: aqie
 * @create: 2020-08-09 09:04
 **/
@Data
public class Button {
    public Button(String name) {
        this.name = name;
    }

    private String name;
}
