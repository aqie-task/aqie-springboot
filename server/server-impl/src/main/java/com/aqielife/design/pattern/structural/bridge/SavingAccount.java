package com.aqielife.design.pattern.structural.bridge;

public class SavingAccount implements Account {
    @Override
    public Account openAccount() {
        System.out.println("open current account");
        return new SavingAccount();
    }

    @Override
    public void showAccountType() {
        System.out.println("this is current account");
    }
}
