package com.aqielife.design.principle.dependenceinversion;

public class GoCourse implements ICourse {
    @Override
    public void studyCourse() {
        System.out.println("learn go");
    }
}
