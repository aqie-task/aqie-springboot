package com.aqielife.design.pattern.behavioral.Memo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Article {
    private String title;
    private String content;
    private String imgs;

    public Article(String title, String content, String imgs) {
        this.title = title;
        this.content = content;
        this.imgs = imgs;
    }

    public ArticleMemo saveToMemo(){
        ArticleMemo articleMemo = new ArticleMemo(title, content, imgs);
        return articleMemo;
    }

    public void undoFromMemo(ArticleMemo articleMemo){
        this.title = articleMemo.getTitle();
        this.content = articleMemo.getContent();
        this.imgs = articleMemo.getImgs();
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", imgs='" + imgs + '\'' +
                '}';
    }
}
