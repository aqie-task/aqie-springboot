package com.aqielife.design.pattern.structural.flyweight;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Manager implements Employee{
    private String title = "部门经理";
    private String department;
    @Setter
    private String reportContent;

    public Manager(String department){
        this.department = department;
    }

    @Override
    public void report() {
      log.info(reportContent);
    }
}
