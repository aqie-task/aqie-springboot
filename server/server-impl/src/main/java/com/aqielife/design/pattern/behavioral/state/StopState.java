package com.aqielife.design.pattern.behavioral.state;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StopState extends CourseVideoState {
    @Override
    public void play() {
        super.courseVideoContext.setCourseVideoState(CourseVideoContext.PLAY_STATE);
    }

    @Override
    public void speed() throws Exception {
        throw new Exception("stop state cant not speed!");
    }

    @Override
    public void pause() throws Exception {
        throw new Exception("stop state cant snot pause!");
    }

    @Override
    public void stop() {
        log.info("stop video");
    }
}
