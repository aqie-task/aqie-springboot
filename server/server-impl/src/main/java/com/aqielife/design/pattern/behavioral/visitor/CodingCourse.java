package com.aqielife.design.pattern.behavioral.visitor;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CodingCourse extends Course
{
    private String price;

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
