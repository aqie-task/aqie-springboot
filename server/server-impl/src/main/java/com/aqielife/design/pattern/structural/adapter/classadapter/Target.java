package com.aqielife.design.pattern.structural.adapter.classadapter;

public interface Target {
    void request();
}
