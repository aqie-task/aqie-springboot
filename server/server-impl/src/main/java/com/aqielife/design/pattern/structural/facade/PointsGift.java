package com.aqielife.design.pattern.structural.facade;

import lombok.Getter;

public class PointsGift {
    @Getter
    private String name;

    public PointsGift(String name){
        this.name = name;
    }

}
