package com.aqielife.design.pattern.creational.singleton;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

// 通过map容器实现单例(程序初始化将多个对象放入 map)  HashTable 线程安全
public class ContainerSingleton {
    private ContainerSingleton(){}

    private static Map<String, Object> stringObjectMap = new HashMap<>();
    public static void putInstance(String key, Object instance){
        if(StringUtils.isNotBlank(key) && instance != null){
            if(!stringObjectMap.containsKey(key)){
                stringObjectMap.put(key, instance);
            }
        }
    }

    public static Object getInstance(String key){
        return stringObjectMap.get(key);
    }
}
