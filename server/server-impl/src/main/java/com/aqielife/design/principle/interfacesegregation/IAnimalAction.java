package com.aqielife.design.principle.interfacesegregation;

public interface IAnimalAction {
    void eat();
    void fly();
    void swim();
}
