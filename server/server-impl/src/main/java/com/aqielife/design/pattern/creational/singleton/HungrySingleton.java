package com.aqielife.design.pattern.creational.singleton;

import java.io.Serializable;

// 线程安全  类加载就把对象初始化，所以反射会报错
public class HungrySingleton  implements Serializable, Cloneable {
    private final static HungrySingleton hungrySingleton;

    // final 在类加载时候完成赋值
    static {
        hungrySingleton = new HungrySingleton();
    }

    // 禁止反射创建类
    private HungrySingleton(){
        if(hungrySingleton != null){
            throw new RuntimeException("单例构造器禁止反射调用");
        }
    }

    public static HungrySingleton getInstance(){
        return hungrySingleton;
    }

    // 解决序列化 返回不同对象
    private Object readResolve(){
        return hungrySingleton;
    }

    // 重写clone 方法; 防止克隆破坏单例
    @Override
    protected Object clone() throws CloneNotSupportedException{
        //return super.clone();
         return getInstance();
    }
}
