package com.aqielife.design.pattern.behavioral.Memo;

import lombok.Getter;

@Getter
public class ArticleMemo {
    private String title;
    private String content;
    private String imgs;

    public ArticleMemo(String title, String content, String imgs) {
        this.title = title;
        this.content = content;
        this.imgs = imgs;
    }

    @Override
    public String toString() {
        return "ArticleMemo{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", imgs='" + imgs + '\'' +
                '}';
    }
}
