package com.aqielife.design.pattern.structural.composite;

// 父类将方法实现 子类选择性重写
public abstract class CatalogComponent {
    public void add(CatalogComponent catalogComponment){
        throw new UnsupportedOperationException("unsupport add operation");
    }
    public void remove(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持删除操作");
    }


    public String getName(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持获取名称操作");
    }


    public double getPrice(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持获取价格操作");
    }


    public void print(){
        throw new UnsupportedOperationException("不支持打印操作");
    }
}
