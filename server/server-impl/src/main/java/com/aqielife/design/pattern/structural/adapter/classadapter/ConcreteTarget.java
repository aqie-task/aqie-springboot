package com.aqielife.design.pattern.structural.adapter.classadapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConcreteTarget implements Target {
    @Override
    public void request() {
        log.info("concreteTarget 目标方法");
    }
}
