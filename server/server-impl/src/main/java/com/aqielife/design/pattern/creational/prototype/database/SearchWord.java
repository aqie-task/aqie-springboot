package com.aqielife.design.pattern.creational.prototype.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author aqie
 * @date 2020-10-21 17:40
 * @function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchWord {
    private String keyword;
    private long lastUpdateTime;
    private long count;
}
