package com.aqielife.design.gamepattern.command;

/**
 * x  jump
 * y  fire
 * a  swap
 * b  lurch
 */
public interface Command {
    void execute();

    default void execute(Actor actor){

    }

    default void undo(){};
}
