package com.aqielife.design.pattern.creational.simplefactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PythonVideo extends Video {
    @Override
    public void produce() {
        log.info("produce python video");
    }
}
