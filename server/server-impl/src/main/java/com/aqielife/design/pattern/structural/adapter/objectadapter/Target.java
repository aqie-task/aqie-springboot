package com.aqielife.design.pattern.structural.adapter.objectadapter;

public interface Target {
    void request();
}
