package com.aqielife.design.pattern.creational.singleton;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class T implements Runnable {
    @Override
    public void run() {
        LazySingleton lazySingleton = LazySingleton.getInstance();
        log.info(Thread.currentThread().getName() + lazySingleton);
    }
}
