package com.aqielife.design.gamepattern.command.impl;

import com.aqielife.design.gamepattern.command.Actor;
import com.aqielife.design.gamepattern.command.Command;

/**
 * @author: aqie
 * @create: 2020-08-09 08:54
 **/
public class LurchCommand implements Command {
    private String command = "lurch";
    @Override
    public void execute() {
        System.out.println(command);
    }

    @Override
    public void execute(Actor actor) {
        System.out.println(actor.getName() + " :" + command);
    }
}
