package com.aqielife.design.pattern.structural.flyweight;

public interface Employee {
    void report();
}
