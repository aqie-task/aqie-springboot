package com.aqielife.design.pattern.behavioral.visitor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Course {
    private String name;

    public abstract void accept(IVisitor visitor);
}
