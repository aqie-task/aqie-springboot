package com.aqielife.design.pattern.structural.adapter.classadapter;

// 继承被装饰类 实现目标接口
public class Adapter extends Adaptee implements Target {

    @Override
    public void request() {
        super.adapterRequest();
    }
}
