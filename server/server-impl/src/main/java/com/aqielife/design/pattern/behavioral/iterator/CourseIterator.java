package com.aqielife.design.pattern.behavioral.iterator;

/**
 *  获取下一个课程
 *  判断是否是最后课程
 */
public interface CourseIterator {
    Course nextCourse();
    boolean isLastCourse();
}
