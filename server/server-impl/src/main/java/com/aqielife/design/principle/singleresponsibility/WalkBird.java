package com.aqielife.design.principle.singleresponsibility;

public class WalkBird {
    public void WalkMode(String name){
        System.out.println(name + " bird fly with legs");
    }
}
