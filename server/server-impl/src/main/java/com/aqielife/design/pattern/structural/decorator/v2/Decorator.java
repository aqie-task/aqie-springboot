package com.aqielife.design.pattern.structural.decorator.v2;

public abstract class Decorator implements Senbei {
    private Senbei senbei;

    public Decorator(Senbei senbei){
        this.senbei = senbei;
    }
    @Override
    public String getDesc() {
        return this.senbei.getDesc();
    }

    @Override
    public int cost() {
        return this.senbei.cost();
    }

    protected abstract void doSomething();
}
