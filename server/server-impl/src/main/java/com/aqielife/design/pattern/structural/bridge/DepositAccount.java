package com.aqielife.design.pattern.structural.bridge;

public class DepositAccount implements Account{
    @Override
    public Account openAccount() {
        System.out.println("open depositAccount!");
        return new DepositAccount();
    }

    @Override
    public void showAccountType() {
        System.out.println("this is an DepositAccount!");
    }
}
