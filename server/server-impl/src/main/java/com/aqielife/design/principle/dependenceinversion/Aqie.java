package com.aqielife.design.principle.dependenceinversion;

public class Aqie {

    private ICourse iCourse;

    public void learnCourse(){
        iCourse.studyCourse();
    }

    public void setiCourse(ICourse iCourse) {
        this.iCourse = iCourse;
    }
}
