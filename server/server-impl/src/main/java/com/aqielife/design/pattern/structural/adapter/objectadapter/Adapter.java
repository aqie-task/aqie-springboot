package com.aqielife.design.pattern.structural.adapter.objectadapter;

// 继承目标接口; 将被适配者  组合
public class Adapter implements Target {
    private Adaptee adaptee = new Adaptee();
    @Override
    public void request() {
        // 前后可以添加其他功能
        adaptee.adapterRequest();
    }
}
