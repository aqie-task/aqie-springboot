package com.aqielife.design.pattern.behavioral.observer;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Observable;

// 被观察对象
@Slf4j
public class Course extends Observable {
    @Getter
    private String courseName;

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public void produceQuestion(Question question, Course course){
        log.info(" {} 在 {} 提交问题",question.getUserName(),course.getCourseName());
        setChanged();
        notifyObservers(question);
    }
}
