package com.aqielife.design.pattern.behavioral.visitor;

import lombok.extern.slf4j.Slf4j;

/**
 * 根据相同visitor 对不同数据产生不同操作
 */
@Slf4j
public class Visitor implements IVisitor {
    @Override
    public void visit(FreeCourse freeCourse) {
        log.info("Course name: {}",freeCourse.getName());
    }

    @Override
    public void visit(CodingCourse codingCourse) {
        log.info("Course name: {}，price： {}",codingCourse.getName(),codingCourse.getPrice());
    }
}
