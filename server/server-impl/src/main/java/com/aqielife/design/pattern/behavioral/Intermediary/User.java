package com.aqielife.design.pattern.behavioral.Intermediary;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private String userName;

    public User(String userName) {
        this.userName = userName;
    }

    public void sendMessage(String message){
        StudyGroup.showMessage(this,message);
    }
}
