package com.aqielife.design.pattern.creational.singleton;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

// 懒汉在初始化时候未创建; 延迟加载使用时才会初始化
@Slf4j
public class LazySingleton {
    private static LazySingleton lazySingleton = null;
    // true 表示可以实例化
    private static boolean flag = true;
    private LazySingleton(){
        if(flag){
            flag = false;
        }else{
            throw new RuntimeException("单例构造器禁止反射调用");
        }
    }
    public synchronized static LazySingleton getInstance(){
        if(lazySingleton == null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Class objectClass = LazySingleton.class;
        Constructor c = objectClass.getDeclaredConstructor();
        c.setAccessible(true);

        // 获取对象
        LazySingleton o1 = LazySingleton.getInstance();
        // 反射攻击修改flag
        Field flag = o1.getClass().getDeclaredField("flag");
        flag.setAccessible(true);
        flag.set(o1, true);
        LazySingleton o2 = (LazySingleton) c.newInstance();

        log.info("{}", o1);
        log.info("{}", o2);
        log.info("o1 == o2: {}", o1 == o2);
    }
}
