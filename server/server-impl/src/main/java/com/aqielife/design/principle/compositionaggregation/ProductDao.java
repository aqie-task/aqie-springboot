package com.aqielife.design.principle.compositionaggregation;

public class ProductDao {
    private DBConnection dbConnection;

    public void setDbConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }
    public void addProduct(){
        String conn = dbConnection.getConection();
        System.out.println(conn);
    }
}
