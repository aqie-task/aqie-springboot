package com.aqielife.design.gamepattern.command.impl;

import com.aqielife.design.gamepattern.command.Actor;
import com.aqielife.design.gamepattern.command.MoveUnitCommand;

/**
 * @author: aqie
 * @create: 2020-08-09 08:53
 **/
public class SwapCommand extends MoveUnitCommand {
    private String command = "swap";

    public SwapCommand(){}

    public SwapCommand(Actor actor, int x, int y) {
        super(actor, x, y);
    }

    // 子类调用父类同名方法
    @Override
    public void execute() {
        super.execute();
        System.out.println(command);
    }

    @Override
    public void execute(Actor actor) {
        System.out.println(actor.getName() + " :" + command);
    }
}
