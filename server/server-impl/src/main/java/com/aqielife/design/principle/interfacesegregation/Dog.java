package com.aqielife.design.principle.interfacesegregation;

public class Dog implements IEatAction, ISwimAction {
    @Override
    public void eat() {

    }

    @Override
    public void swim() {

    }
}
