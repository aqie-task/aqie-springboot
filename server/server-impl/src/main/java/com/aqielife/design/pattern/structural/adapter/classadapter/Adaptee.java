package com.aqielife.design.pattern.structural.adapter.classadapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Adaptee {
    public void adapterRequest(){
      log.info("被适配的方法");
    }
}
