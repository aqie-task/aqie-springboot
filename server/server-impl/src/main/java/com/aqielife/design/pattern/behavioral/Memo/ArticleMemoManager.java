package com.aqielife.design.pattern.behavioral.Memo;

import java.util.Stack;

public class ArticleMemoManager {
    private final Stack<ArticleMemo> ARTICLE_MEMO_STACK = new Stack<>();

    public ArticleMemo getMemo(){
        ArticleMemo articleMemo = ARTICLE_MEMO_STACK.pop();
        return articleMemo;
    }

    public void addMemo(ArticleMemo articleMemo){
        ARTICLE_MEMO_STACK.push(articleMemo);
    }
}
