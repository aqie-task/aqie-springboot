package com.aqielife.design.pattern.creational.prototype;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

// 实现克隆接口 和 clone 方法
@Getter
@Setter
@ToString
@Slf4j
public class Mail implements Cloneable {
    private String name;
    private String emailAddress;
    private String content;
    public Mail(){
        log.info("Mail Class Constructor");
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        log.info("clone mail object");
        return super.clone();
    }
}
