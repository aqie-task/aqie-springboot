package com.aqielife.design.pattern.creational.abstractfactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JavaVideo extends Video {
    @Override
    public void produce() {
      log.info("play java video");
    }
}
