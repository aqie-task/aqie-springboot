package com.aqielife.design.pattern.structural.bridge;

/**
 * account 引入到bank
 *
 */
public abstract class Bank {
    // 只有子类能拿到account
    protected Account account;
    public Bank(Account account){
        this.account = account;
    }
    // 抽象方法和接口声明抽象方法一致
    abstract Account openAccount();
}
