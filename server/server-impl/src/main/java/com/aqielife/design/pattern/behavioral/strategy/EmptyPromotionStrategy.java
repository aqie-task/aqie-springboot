package com.aqielife.design.pattern.behavioral.strategy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmptyPromotionStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        log.info("NO Promotion");
    }
}
