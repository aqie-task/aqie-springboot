package com.aqielife.design.gamepattern.command;

/**
 * @author: aqie
 * @create: 2020-08-09 11:38
 **/
public class MoveUnitCommand implements Command{
    private Actor actor;
    private int xBefore, yBefore;
    private int x, y;

    public MoveUnitCommand(){}

    public MoveUnitCommand(Actor actor, int x, int y){
        this.actor = actor;
        this.x = x;
        this.y = y;
    }

    @Override
    public void execute() {
        if(actor != null){
            actor.moveTo(x,y);
        }
    }
}
