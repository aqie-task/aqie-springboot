package com.aqielife.design.pattern.creational.factorymethod;

// 具体产生视频 由 VideoFactory 子类决定
public class PythonVideoFactory extends VideoFactory {

    @Override
    public Video getVideo() {
        return new PythonVideo();
    }
}
