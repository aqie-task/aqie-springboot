package com.aqielife.design.pattern.structural.facade;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShippingService {
    public String shipGift(PointsGift pointsGift){
        // logistics system
        log.info(pointsGift.getName() + " enter logistics system");
        String shipOrderNo = "123";
        return shipOrderNo;
    }
}
