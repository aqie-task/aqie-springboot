package com.aqielife.design.pattern.creational.abstractfactory;

public abstract class Article {
    public abstract void produce();
}
