package com.aqielife.design.pattern.behavioral.strategy;

import lombok.extern.slf4j.Slf4j;

// 立减
@Slf4j
public class DiscountPromotionStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
      log.info("Discount Promotion");
    }
}
