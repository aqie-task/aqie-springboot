package com.aqielife.design.pattern.behavioral.strategy;

import lombok.extern.slf4j.Slf4j;

// 满减
@Slf4j
public class FullDiscountPromotionStrategy implements PromotionStrategy{
    @Override
    public void doPromotion() {
      log.info("Full Discount");
    }
}
