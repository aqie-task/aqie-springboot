package com.aqielife.design.pattern.creational.simplefactory;

import java.lang.reflect.InvocationTargetException;

// 简单工厂
public class VideoFactory {
    // public abstract Video getVideo();
    public Video getVideo(Class c){
        Video video = null;
        try{
            video = (Video)Class.forName(c.getName()).getDeclaredConstructor().newInstance();
        }catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return video;
    }
}
