package com.aqielife.design.pattern.behavioral.Command;

import java.util.ArrayList;
import java.util.List;

public class Staff {
    private List<Command> commandList = new ArrayList<>();

    public void addCommand(Command command){
        commandList.add(command);
    }

    // 执行所有命令
    public void executeCommands(){
        for (Command command : commandList){
            command.execute();
        }
        commandList.clear();
    }
}
