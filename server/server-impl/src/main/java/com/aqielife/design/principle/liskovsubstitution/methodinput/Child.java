package com.aqielife.design.principle.liskovsubstitution.methodinput;

import java.util.Map;

public class Child  extends Base{

    public void method(Map map) {
        System.out.println(" child method");
    }
}
