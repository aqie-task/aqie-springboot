package com.aqielife.designmode.structural.facade;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:31
 * @Func 数据加密类
 */
@Slf4j
public class CipherMachine {
	public String Encrypt(String plainText)
	{
		log.info("数据加密，将明文转换为密文：");
		StringBuilder es = new StringBuilder();
		char[] chars = plainText.toCharArray();
		for(char ch : chars)
		{
			String c = String.valueOf(ch % 7);
			es.append(c);
		}
		log.info(es.toString());
		return es.toString();
	}

}
