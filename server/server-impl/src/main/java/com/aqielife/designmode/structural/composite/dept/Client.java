package com.aqielife.designmode.structural.composite.dept;

public class Client {
    public static void main(String[] args) {
        AbstractUnit c1,c2,d1,d2;
        c1 = new Company("阿里");
        c2 = new Company("淘宝");
        d1 = new DevelopDept("开发部");
        d2 = new FinanceDept("财务部");
        c2.add(d1);
        c2.add(d2);
        c1.add(c2);
        c1.add(d1);
        c1.send();
    }
}
