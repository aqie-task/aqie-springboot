package com.aqielife.designmode.structural.flyweight;

import java.util.Hashtable;

/**
 * @Auther aqie
 * @Date 2021/10/23 15:32
 * @Desc 享元工厂类,使用单例模式进行设计
 */
public class IgoChessmanFactory {
	private static IgoChessmanFactory instance = new IgoChessmanFactory();
	//使用Hashtable来存储享元对象，充当享元池
	private static Hashtable<String, IgoChessman> ht = new Hashtable<>();
	private IgoChessmanFactory() {
	}
	//返回享元工厂类的唯一实例
	public static IgoChessmanFactory getInstance() {
		return instance;
	}
	//通过key来获取存储在Hashtable中的享元对象
	public static IgoChessman getIgoChessman(String color) {
		if(ht.containsKey(color)){
			return (IgoChessman)ht.get(color);
		} else if ("b".equals(color)){
			IgoChessman black = new BlackIgoChessman();
			ht.put("b", black);
			return black;
		} else if("w".equals(color)){
			IgoChessman white = new WhiteIgoChessman();
			ht.put("w", white);
			return white;
		} else {
			return null;
		}

	}
}
