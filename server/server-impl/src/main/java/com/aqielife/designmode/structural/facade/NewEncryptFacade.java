package com.aqielife.designmode.structural.facade;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:57
 */
public class NewEncryptFacade extends AbstractEncryptFacade{
	private FileReader reader;
	private NewCipherMachine cipher;
	private FileWriter writer;

	public NewEncryptFacade() {
		reader = new FileReader();
		cipher = new NewCipherMachine();
		writer = new FileWriter();
	}

	@Override
	public void fileEncrypt(String fileNameSrc, String fileNameDes)
	{
		String plainStr = reader.Read(fileNameSrc);
		String encryptStr = cipher.Encrypt(plainStr);
		writer.Write(encryptStr, fileNameDes);
	}
}
