package com.aqielife.designmode.behavioral.template;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CurrentAccount  extends Account{
    @Override
    public void CalculateInterest() {
        log.info("按活期利率计算利息!");
    }
}
