package com.aqielife.designmode.structural.decorator;

/**
 * @Auther aqie
 * @Date 2021/10/23 13:44
 * 抽象界面构件类
 */
public abstract class Component
{
	public abstract void display();
}
