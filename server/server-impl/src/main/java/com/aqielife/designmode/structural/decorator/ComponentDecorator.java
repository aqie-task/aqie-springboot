package com.aqielife.designmode.structural.decorator;

/**
 * @Auther aqie
 * @Date 2021/10/23 13:47
 * 抽象装饰类 : 与被装饰类继承同一个父类
 */
public abstract class ComponentDecorator extends Component{
	private Component component; //维持对抽象构件类型对象的引用
	public ComponentDecorator(Component component) //注入抽象构件类型的对象
	{
		this.component = component;
	}
	@Override
	public void display()
	{
		component.display();
	}
}
