package com.aqielife.designmode.structural.bridge.demo;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:16
 */
public interface Color {
	void paint();
}
