package com.aqielife.designmode.behavioral.template;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SavingAccount extends Account {
    @Override
    public void CalculateInterest() {
        log.info("按定期利率计算利息!");
    }

    @Override
    public Boolean IsVip() {
        return false;
    }
}
