package com.aqielife.designmode.structural.decorator;

/**
 * @Auther aqie
 * @Date 2021/10/23 13:52
 */
public class Client {
	public static void main(String[] args) {
		// 在透明装饰模式中，要求客户端完全针对抽象编程
		Component component,componentBB; //使用抽象构件定义
		component = new Window(); //定义具体构件
		componentBB = new BlackBorderDecorator(component); //定义装饰后的构件
		componentBB.display();
	}
}
