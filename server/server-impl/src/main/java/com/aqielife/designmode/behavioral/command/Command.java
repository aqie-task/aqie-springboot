package com.aqielife.designmode.behavioral.command;

public abstract class Command {
    public abstract void execute();
}
