package com.aqielife.designmode.structural.bridge;

import com.itextpdf.text.pdf.parser.Matrix;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:23
 * 接口实现类
 */
public interface ImageImp {
	public void doPaint(Matrix m); //显示像素矩阵m
}
