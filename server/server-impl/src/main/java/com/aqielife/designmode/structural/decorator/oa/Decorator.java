package com.aqielife.designmode.structural.decorator.oa;

//抽象装饰类
public abstract class Decorator extends Document{
    private Document document;
    public Decorator(Document document)
    {
        this. document = document;
    }
    public void display() {
        document.display();
    }
}
