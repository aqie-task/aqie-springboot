package com.aqielife.designmode.structural.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 13:46
 */
@Slf4j
public class TextBox extends Component{
	@Override
	public void display() {
		log.info("show textBox");
	}
}
