package com.aqielife.designmode.structural.bridge.demo;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:16
 */
@Slf4j
public class RedColorImpl implements Color{
	@Override
	public void paint() {
		log.info("red");
	}
}
