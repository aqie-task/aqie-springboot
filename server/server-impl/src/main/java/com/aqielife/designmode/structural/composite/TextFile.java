package com.aqielife.designmode.structural.composite;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 11:25
 */
@Slf4j
public class TextFile  extends LeafAbstractFile{
	private String name;

	public TextFile(String name) {
		this.name = name;
	}


	@Override
	public void killVirus() {
		log.info("Text killVirus: " + name);
	}
}
