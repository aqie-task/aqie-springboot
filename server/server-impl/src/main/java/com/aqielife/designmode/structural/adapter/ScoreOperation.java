package com.aqielife.designmode.structural.adapter;

/**
 * @Auther aqie
 * @Date 2021/10/23 9:15
 * 目标抽象类
 */
public interface ScoreOperation {
	//成绩排序
	public int[] sort(int array[]);
	//成绩查找
	public int search(int array[],int key);
}
