package com.aqielife.designmode.structural.bridge.demo;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:15
 */
public abstract class AbstractInkbrush {
	protected Color color;

	public AbstractInkbrush(Color color) {
		this.color = color;
	}

	public abstract void write();
}
