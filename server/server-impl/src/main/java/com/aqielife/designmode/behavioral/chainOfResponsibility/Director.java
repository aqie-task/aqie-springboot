package com.aqielife.designmode.behavioral.chainOfResponsibility;

//主任类:具体处理者
class Director extends Approver {
    public Director(String name) {
        super(name);
    }
    //具体请求处理方法
    public void processRequest(PurchaseRequest request) {
        if (request.getAmount() < 100) {
            System.out.println("主任" + this.name + "审批采购单:" + request.getNumber());
        }
        else {
            this.successor.processRequest(request); //转发请求
        }
    }
}
