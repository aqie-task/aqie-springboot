package com.aqielife.designmode.structural.bridge;

import com.itextpdf.text.pdf.parser.Matrix;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:25
 */
public class LinuxImp implements ImageImp {
	public void doPaint(Matrix m) {
		//调用Linux系统的绘制函数绘制像素矩阵
		System.out.print("在Linux操作系统中显示图像：");
	}
}
