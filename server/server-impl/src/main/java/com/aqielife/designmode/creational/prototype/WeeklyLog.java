package com.aqielife.designmode.creational.prototype;

import lombok.Getter;
import lombok.Setter;

import java.io.*;

@Getter
@Setter
class WeeklyLog implements Cloneable
{
    private String name;
    private String date;
    private String content;
    private Attachment attachment;

    //克隆方法clone(),此处使用Java语言提供的克隆机制
    public WeeklyLog clone()
    {
        Object obj = null;
        try
        {
            obj = super.clone();
            return (WeeklyLog)obj;
        }
        catch(CloneNotSupportedException e)
        {
            System.out.println("不支持复制!");
            return null;
        }
    }

    //使用序列化技术实现深克隆
    public WeeklyLog deepClone() throws IOException, ClassNotFoundException
    {
//将对象写入流中
        ByteArrayOutputStream bao=new ByteArrayOutputStream();
        ObjectOutputStream oos=new ObjectOutputStream(bao);
        oos.writeObject(this);
//将对象从流中取出
        ByteArrayInputStream bis=new ByteArrayInputStream(bao.toByteArray());
                ObjectInputStream ois=new ObjectInputStream(bis);
        return (WeeklyLog)ois.readObject();
    }
}

