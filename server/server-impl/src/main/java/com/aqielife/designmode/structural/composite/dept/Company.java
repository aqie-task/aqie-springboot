package com.aqielife.designmode.structural.composite.dept;

import com.aqielife.designmode.structural.composite.AbstractFile;
import lombok.Data;

import java.util.ArrayList;

@Data
public class Company extends AbstractUnit{
    private ArrayList<AbstractUnit> deptList=new ArrayList<AbstractUnit>();
    private String name;

    public Company(String name) {
        this.name = name;
    }

    @Override
    public void add(AbstractUnit unit) {
        deptList.add(unit);
    }

    @Override
    public void remove(AbstractUnit unit) {
        deptList.remove(unit);
    }

    @Override
    public AbstractUnit getChild(int i) {
        return deptList.get(i);
    }

    @Override
    public void send() {
        for(AbstractUnit obj : deptList) {
            (obj).send();
        }
    }
}
