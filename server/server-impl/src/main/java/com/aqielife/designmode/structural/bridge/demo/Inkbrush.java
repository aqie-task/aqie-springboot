package com.aqielife.designmode.structural.bridge.demo;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:18
 */
public class Inkbrush  extends AbstractInkbrush{
	public Inkbrush(Color color) {
		super(color);
	}

	public Inkbrush(RedColorImpl redColor) {
		super(redColor);
	}

	@Override
	public void write() {
		color.paint();
	}

	public static void main(String[] args) {
		RedColorImpl redColor = new RedColorImpl();
		Inkbrush inkbrush = new Inkbrush(redColor);
		inkbrush.write();
	}
}
