package com.aqielife.designmode.structural.composite;

/**
 * @Auther aqie
 * @Date 2021/10/23 11:33
 */
public abstract class LeafAbstractFile extends AbstractFile{
	@Override
	public void add(AbstractFile file) {
		throw new RuntimeException("do not support");
	}

	@Override
	public void remove(AbstractFile file) {
		throw new RuntimeException("do not support");
	}

	@Override
	public AbstractFile getChild(int i) {
		throw new RuntimeException("do not support");
	}

	public abstract void killVirus();
}
