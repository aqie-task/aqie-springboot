package com.aqielife.designmode.behavioral.command.queue;

// 可以通过引入一个命令集合或其他方式来存储每一次操作时命令的状态
// 修改简易计算器源代码,使之能够实现多次撤销(Undo)和恢复(Redo)
public class Client {
    public static void main(String[] args) {
        CalculatorForm form = new CalculatorForm();
        AbstractCommand command;
        command = new ConcreteCommand();
        form.setCommand(command); //向发送者注入命令对象
        form.compute(10);
        form.compute(5);
        form.compute(10);
        form.undo();
    }
}
