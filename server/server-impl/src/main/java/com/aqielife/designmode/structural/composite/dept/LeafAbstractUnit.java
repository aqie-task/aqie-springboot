package com.aqielife.designmode.structural.composite.dept;

public abstract class LeafAbstractUnit extends AbstractUnit{
    @Override
    public void add(AbstractUnit unit) {
        throw new RuntimeException("do not support");
    }

    @Override
    public void remove(AbstractUnit unit) {
        throw new RuntimeException("do not support");
    }

    @Override
    public AbstractUnit getChild(int i) {
        throw new RuntimeException("do not support");
    }

    @Override
    public abstract void send();
}
