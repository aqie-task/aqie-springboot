package com.aqielife.designmode.creational.prototype.official;

//抽象公文接口,也可定义为抽象类,提供clone()方法的实现
interface OfficialDocument extends Cloneable
{
    public OfficialDocument clone();
    public void display();
}
