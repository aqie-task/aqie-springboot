package com.aqielife.designmode.behavioral.state.cycle;

//抽象状态类
abstract class State {
    public abstract void display();
}
