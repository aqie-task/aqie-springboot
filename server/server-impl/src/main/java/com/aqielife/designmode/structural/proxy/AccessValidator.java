package com.aqielife.designmode.structural.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 16:30
 */
@Slf4j
public class AccessValidator {
	//模拟实现登录验证
	public Boolean Validate(String userId)
	{
		log.info("在数据库中验证用户'" + userId + "'是否是合法用户？");
		if (userId.equals("001")) {
			log.info("'{}'登录成功！",userId);
			return true;
		}
		else {
			log.info("'{}'登录失败！", userId);
			return false;
		}
	}
}
