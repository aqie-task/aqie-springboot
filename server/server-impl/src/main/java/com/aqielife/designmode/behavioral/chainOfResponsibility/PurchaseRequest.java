package com.aqielife.designmode.behavioral.chainOfResponsibility;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

//采购单:请求类
@Getter
@Setter
@AllArgsConstructor
public class PurchaseRequest {
    private double amount; //采购金额
    private int number; //采购单编号
    private String purpose; //采购目的
}
