package com.aqielife.designmode.creational.singleton;

/**
 * 内部类中定义了一个static类型的变量
 * instance,此时会首先初始化这个成员变量,由Java虚拟机来保证其线程安全性,确保该成员
 * 变量只能初始化一次
 * 既可以实现延迟加载,又可以保证线程安全
 */
class Singleton {
    private Singleton() {
    }
    private static class HolderClass {
        private final static Singleton instance = new Singleton();
    }
    public static Singleton getInstance() {
        return HolderClass.instance;
    }
    public static void main(String args[]) {
        Singleton s1, s2;
        s1 = Singleton.getInstance();
        s2 = Singleton.getInstance();
        System.out.println(s1==s2);
    }
}
