package com.aqielife.designmode.structural.proxy;

/**
 * @Auther aqie
 * @Date 2021/10/23 16:32
 */
public interface Searcher {
	String DoSearch(String userId, String keyword);
}
