package com.aqielife.designmode.behavioral.template;

import com.aqielife.designmode.XMLUtil;

public class Client {
    public static void main(String[] args) {
        Account account;
        account = (Account)XMLUtil.getBean("server/server-impl/src/main/java/com/aqielife/designmode/behavioral/template/config.xml");
        account.Handle("张无忌", "123456");
    }
}
