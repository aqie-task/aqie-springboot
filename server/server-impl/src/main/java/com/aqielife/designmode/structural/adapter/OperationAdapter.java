package com.aqielife.designmode.structural.adapter;


import com.aqielife.designmode.structural.adapter.adaptee.BinarySearch;
import com.aqielife.designmode.structural.adapter.adaptee.QuickSort;

/**
 * @Auther aqie
 * @Date 2021/10/23 9:17
 * 对象适配器模式 : 适配器与适配者 关联关系
 * 类适配器	   : 适配器与适配者 继承关系,很少使用
 * 	+ 目标抽象类必须是接口
 * 	+ Adaptee 不能为final
 * 双向适配者
 * 缺省适配器(单接口适配器)：设计抽象类实现接口,为接口提供默认方法
 */
public final class OperationAdapter  implements ScoreOperation {
	private QuickSort sortObj; //定义适配者QuickSort对象
	private BinarySearch searchObj; //定义适配者BinarySearch对象

	// 传入需要适配接口/类
	public OperationAdapter() {
		sortObj = new QuickSort();
		searchObj = new BinarySearch();
	}




	@Override
	public int[] sort(int[] array) {
		return sortObj.quickSort(array);
	}

	@Override
	public int search(int[] array, int key) {
		return searchObj.binarySearch(array, key);
	}
}
