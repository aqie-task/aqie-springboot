package com.aqielife.designmode.structural.facade;


import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:51
 */
@Slf4j
public class NewCipherMachine {
	public String Encrypt(String plainText)
	{
		log.info("数据加密，将明文转换为密文：");
		StringBuilder es = new StringBuilder();
		int key = 10;//设置密钥，移位数为10
		char[] chars = plainText.toCharArray();
		for(char ch : chars)
		{
			int temp = Character.getNumericValue(ch);
			//小写字母移位
			if (ch >= 'a' && ch <= 'z') {
				temp += key % 26;
				if (temp > 122) temp -= 26;
				if (temp < 97) temp += 26;
			}
			//大写字母移位
			if (ch >= 'A' && ch <= 'Z') {
				temp += key % 26;
				if (temp > 90) temp -= 26;
				if (temp < 65) temp += 26;
			}
			es.append((char) temp);
		}
		log.info(es.toString());
		return es.toString();
	}
}
