package com.aqielife.designmode.creational.abstractfactory;

import com.aqielife.designmode.XMLUtil;

class Client {
    public static void main(String args[]) {
//使用抽象层定义
        SkinFactory factory;
        Button bt;
        TextField tf;
        factory = (SkinFactory) XMLUtil.getBean("server/server-impl/src/main/java/com/aqielife/designmode/creational/abstractfactory/config.xml");
        bt = factory.createButton();
        tf = factory.createTextField();
        bt.display();
        tf.display();
    }
}
