package com.aqielife.designmode.structural.facade;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:44
 * @Desc 加密外观类
 */
public class EncryptFacade extends AbstractEncryptFacade{
	private FileReader reader;
	private CipherMachine cipher;
	private FileWriter writer;

	public EncryptFacade() {
		reader = new FileReader();
		cipher = new CipherMachine();
		writer = new FileWriter();
	}

	@Override
	public void fileEncrypt(String fileNameSrc, String fileNameDes)
	{
		String plainStr = reader.Read(fileNameSrc);
		String encryptStr = cipher.Encrypt(plainStr);
		writer.Write(encryptStr, fileNameDes);
	}

	public static void main(String[] args) {
		AbstractEncryptFacade encryptFacade = new NewEncryptFacade();
		encryptFacade.fileEncrypt("doc/a.txt","doc/b.txt");
	}
}
