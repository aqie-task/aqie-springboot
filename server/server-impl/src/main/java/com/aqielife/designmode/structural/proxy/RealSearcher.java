package com.aqielife.designmode.structural.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 16:32
 */
@Slf4j
public class RealSearcher implements Searcher{
	@Override
	public String DoSearch(String userId, String keyword) {
		log.info("{} {}",userId,keyword);
		return "search info";
	}
}
