package com.aqielife.designmode.behavioral.visitor;

import lombok.Getter;
import lombok.Setter;

//全职员工类:具体元素类
@Getter
@Setter
public class FulltimeEmployee implements Employee {
    private String name;
    private double weeklyWage;
    private int workTime;
    public FulltimeEmployee(String name,double weeklyWage,int workTime)
    {
        this.name = name;
        this.weeklyWage = weeklyWage;
        this.workTime = workTime;
    }
    public void accept(Department handler)
    {
        handler.visit(this); //调用访问者的访问方法
    }
}
