package com.aqielife.designmode.creational.simplefactory;

import com.aqielife.designmode.XMLUtil;

public class Client {
    public static void main(String args[]) {
        Chart chart;
        String type = XMLUtil.getChartType("server/server-impl/src/main/java/com/aqielife/designmode/creational/simplefactory/config.xml");
        chart = ChartFactory.getChart(type); //通过静态工厂方法创建产品
        chart.init();
        chart.display();
    }
}
