package com.aqielife.designmode.behavioral.chainOfResponsibility;

class VicePresident extends Approver {
    public VicePresident(String name) {
        super(name);
    }
    //具体请求处理方法
    public void processRequest(PurchaseRequest request) {
        if (request.getAmount() < 300) {
            System.out.println("副董事长" + this.name + "审批采购单:" + request.getNumber());
        }
        else {
            this.successor.processRequest(request); //转发请求
        }
    }
}
