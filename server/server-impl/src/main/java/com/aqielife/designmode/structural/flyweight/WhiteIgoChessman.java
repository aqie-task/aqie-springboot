package com.aqielife.designmode.structural.flyweight;

/**
 * @Auther aqie
 * @Date 2021/10/23 15:35
 */
public class WhiteIgoChessman extends IgoChessman{
	@Override
	public String getColor() {
		return "white";
	}
}
