package com.aqielife.designmode.behavioral.state.share;

abstract class State {
    public abstract void on(Switch s);
    public abstract void off(Switch s);
}
