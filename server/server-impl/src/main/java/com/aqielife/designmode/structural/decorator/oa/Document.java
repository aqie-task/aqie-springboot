package com.aqielife.designmode.structural.decorator.oa;

public abstract class Document {
    public abstract void display();
}
