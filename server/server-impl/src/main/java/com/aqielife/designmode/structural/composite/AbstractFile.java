package com.aqielife.designmode.structural.composite;

/**
 * @Auther aqie
 * @Date 2021/10/23 11:20
 * ● Component(抽象构件)
 */
public abstract class AbstractFile {
	public abstract void add(AbstractFile file);
	public abstract void remove(AbstractFile file);
	public abstract AbstractFile getChild(int i);
	public abstract void killVirus();
}
