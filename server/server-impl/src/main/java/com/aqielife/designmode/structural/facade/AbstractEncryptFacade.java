package com.aqielife.designmode.structural.facade;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:53
 * @Desc 不修改客户端代码的前提下使用新的外观类呢
 */
public abstract class AbstractEncryptFacade {
	public abstract void fileEncrypt(String fileNameSrc, String fileNameDes);
}
