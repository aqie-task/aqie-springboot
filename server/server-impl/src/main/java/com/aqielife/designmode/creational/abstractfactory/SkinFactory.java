package com.aqielife.designmode.creational.abstractfactory;

//界面皮肤工厂接口:抽象工厂
// 增加新的产品族很方便,但是增加新的产品等级结构很麻烦
interface SkinFactory {
    public Button createButton();
    public TextField createTextField();
}
