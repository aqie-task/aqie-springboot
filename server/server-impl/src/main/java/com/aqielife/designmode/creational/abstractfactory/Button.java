package com.aqielife.designmode.creational.abstractfactory;

//按钮接口:抽象产品
interface Button {
    public void display();
}
