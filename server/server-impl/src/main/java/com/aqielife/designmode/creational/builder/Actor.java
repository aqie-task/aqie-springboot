package com.aqielife.designmode.creational.builder;

import lombok.Data;

// Product(产品角色)
@Data
public class Actor {
    private String type; //角色类型
    private String sex; //性别
    private String face;
    private String costume; //服装
    private String hairstyle; //发型
}
