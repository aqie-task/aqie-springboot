package com.aqielife.designmode.behavioral.memento;

import lombok.Getter;
import lombok.Setter;

//象棋棋子备忘录类:备忘录
@Getter
@Setter
class ChessmanMemento {
    private String label;
    private int x;
    private int y;
    public ChessmanMemento(String label,int x,int y) {
        this.label = label;
        this.x = x;
        this.y = y;
    }

}
