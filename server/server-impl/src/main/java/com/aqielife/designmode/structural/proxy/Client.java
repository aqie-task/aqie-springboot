package com.aqielife.designmode.structural.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 17:31
 */
@Slf4j
public class Client {
	public static void main(String[] args) {
		Searcher searcher;
		searcher = new ProxySearcher();
		String result = searcher.DoSearch("001", "aqie");
		log.info("search {}", result);
	}

}
