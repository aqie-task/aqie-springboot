package com.aqielife.designmode.structural.composite;

import java.util.ArrayList;

/**
 * @Auther aqie
 * @Date 2021/10/23 11:23
 * 容器构件
 */
public class Folder  extends AbstractFile{
	//定义集合fileList，用于存储AbstractFile类型的成员
	private ArrayList<AbstractFile> fileList=new ArrayList<AbstractFile>();
	private String name;

	public Folder(String name) {
		this.name = name;
	}

	@Override
	public void add(AbstractFile file) {
		fileList.add(file);
	}

	@Override
	public void remove(AbstractFile file) {
		fileList.remove(file);
	}

	@Override
	public AbstractFile getChild(int i) {
		return (AbstractFile)fileList.get(i);
	}

	@Override
	public void killVirus() {
		System.out.println("****对文件夹'" + name + "'killVirus"); //模拟杀毒
		//递归调用成员构件的killVirus()方法
		for(AbstractFile obj : fileList) {
			((AbstractFile)obj).killVirus();
		}
	}
}
