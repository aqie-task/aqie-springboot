package com.aqielife.designmode.structural.composite.dept;

import com.aqielife.designmode.structural.composite.AbstractFile;

public abstract class AbstractUnit {
    public abstract void add(AbstractUnit unit);
    public abstract void remove(AbstractUnit unit);
    public abstract AbstractUnit getChild(int i);
    public abstract void send();
}
