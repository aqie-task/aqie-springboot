package com.aqielife.designmode.structural.bridge;

import com.aqielife.designmode.XMLUtil;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:29
 * 桥接是抽象与实现的分离
 */
public class Client {
	public static void main(String[] args) {
		Image image;
		ImageImp imp;
		image = (Image) XMLUtil.getBean2("image", "server/server-impl/src/main/java/com/aqielife/designmode/structural/bridge/config.xml");
		imp = (ImageImp)XMLUtil.getBean2("os", "server/server-impl/src/main/java/com/aqielife/designmode/structural/bridge/config.xml");
		image.setImageImp(imp);
		image.parseFile("小龙女");
	}
}
