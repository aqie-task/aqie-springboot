package com.aqielife.designmode.behavioral.template;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class Account {
    public boolean Validate(String account, String password) {
        log.info("账号:{}", account);
        log.info("密码:{}", password);
        //模拟登录
        if (account.equals("张无忌") && password.equals("123456")) {
            return true;
        } else {
            return false;
        }
    }

    //基本方法——抽象方法
    public abstract void CalculateInterest();

    //基本方法——具体方法
    public void Display() {
        log.info("显示利息!");
    }

    // 钩子函数
    public  Boolean IsVip()
    {
        return true;
    }

    //模板方法
    public void Handle(String account, String password)
    {
        if (!Validate(account, password))
        {
            log.info("账户或密码错误!");
            return;
        }
        CalculateInterest();
        if(IsVip()){
            Display();
        }
    }

}
