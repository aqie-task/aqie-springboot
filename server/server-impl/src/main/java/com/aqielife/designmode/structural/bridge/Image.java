package com.aqielife.designmode.structural.bridge;

/**
 * @Auther aqie
 * @Date 2021/10/23 10:22
 */
public abstract class Image {
	protected ImageImp imp;
	public void setImageImp(ImageImp imp) {
		this.imp = imp;
	}
	public abstract void parseFile(String fileName);
}
