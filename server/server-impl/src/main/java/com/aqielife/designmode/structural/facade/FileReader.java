package com.aqielife.designmode.structural.facade;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:23
 * @link [读取文件方法大全](https://www.cnblogs.com/lovebread/archive/2009/11/23/1609122.html)
 * 以字节为单位读取文件 : InputStream  FileInputStream(file)
 * 以字符为单位读取文件 : Reader 	   InputStreamReader(new FileInputStream(file))
 * 以行为单位读取文件  : BufferedReader(new FileReader(file))
 * 随机读取文件内容   : RandomAccessFile(fileName, "r")
 */
@Slf4j
public class FileReader {
	public String Read(String fileNameSrc)
	{
		log.info("读取文件，获取明文：");
		InputStream fs = null;
		StringBuilder sb = new StringBuilder();
		try
		{
			fs = new FileInputStream(fileNameSrc);
			int data;
			while((data = fs.read())!= -1)
			{
				sb = sb.append((char)data);
			}
			fs.close();
			log.info(sb.toString());
		}
		catch(FileNotFoundException e)
		{
			log.info("文件不存在！");
		}
		catch(IOException e)
		{
			log.info("文件操作错误！");
		}
		return sb.toString();
	}
}

