package com.aqielife.designmode.structural.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 13:45
 * 具体构件类
 */
@Slf4j
public class Window extends Component{
	@Override
	public void display() {
		log.info("show window");
	}
}
