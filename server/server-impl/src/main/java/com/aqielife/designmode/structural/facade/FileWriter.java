package com.aqielife.designmode.structural.facade;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Auther aqie
 * @Date 2021/10/23 14:33
 * 写入文件
 * FileWriter, 字符流写入字符到文件
 * 缓冲字符（BufferedWriter ）是一个字符流类来处理字符数据
 * FileOutputStream 字节流
 */
@Slf4j
public class FileWriter {
	public void Write(String encryptStr,String fileNameDes)
	{
		File file = new File(fileNameDes);
		try(FileOutputStream fop = new FileOutputStream(file)) {
			if (!file.exists()) {
				file.createNewFile();
			}
			byte[] contentInBytes = encryptStr.getBytes();

			fop.write(contentInBytes);
			fop.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
