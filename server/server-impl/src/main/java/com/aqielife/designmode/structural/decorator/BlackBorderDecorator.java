package com.aqielife.designmode.structural.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 13:51
 */
@Slf4j
public class BlackBorderDecorator extends ComponentDecorator{
	public BlackBorderDecorator(Component component)
	{
		super(component);
	}
	public void display()
	{
		this.setBlackBorder();
		super.display();
	}
	public void setBlackBorder()
	{
		log.info("为构件增加黑色边框！");
	}
}
