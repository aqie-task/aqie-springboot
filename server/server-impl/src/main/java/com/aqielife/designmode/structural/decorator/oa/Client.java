package com.aqielife.designmode.structural.decorator.oa;

public class Client {
    public static void main(String[] args) {
        Document doc; //使用抽象构件类型定义
        doc = new PurchaseRequest();
        Approver newDoc; //使用具体装饰类型定义
        newDoc = new Approver(doc);
        newDoc.display();//调用原有业务方法
        newDoc.approve();//调用新增业务方法
    }

}
