package com.aqielife.designmode.structural.composite;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 11:22
 * @Desc 叶子构件
 */
@Slf4j
public class VideoFile extends LeafAbstractFile{
	private String name;

	public VideoFile(String name) {
		this.name = name;
	}


	@Override
	public void killVirus() {
		log.info("Video killVirus: " + name);
	}
}
