package com.aqielife.designmode.structural.flyweight;

import lombok.Data;

/**
 * @Auther aqie
 * @Date 2021/10/23 15:40
 * @desc 坐标类：外部状态类
 */
@Data
public class Coordinates {
	private int x;
	private int y;
	public Coordinates(int x,int y) {
		this.x = x;
		this.y = y;
	}
}
