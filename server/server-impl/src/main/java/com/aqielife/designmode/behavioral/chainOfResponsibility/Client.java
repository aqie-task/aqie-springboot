package com.aqielife.designmode.behavioral.chainOfResponsibility;

class Client {
    public static void main(String[] args) {
        Approver wjzhang, gyang, jguo;
        wjzhang = new Director("张无忌");
        gyang = new VicePresident("杨过");
        jguo = new President("郭靖");
        //创建职责链
        wjzhang.setSuccessor(gyang);
        gyang.setSuccessor(jguo);
//创建采购单
        PurchaseRequest pr1 = new PurchaseRequest(10,10001,"购买倚天剑");
        wjzhang.processRequest(pr1);
        PurchaseRequest pr2 = new PurchaseRequest(120,10002,"购买《葵花宝典》");
                wjzhang.processRequest(pr2);
        PurchaseRequest pr3 = new PurchaseRequest(220,10003,"购买《金刚经》");
        wjzhang.processRequest(pr3);
    }

}
