package com.aqielife.designmode.behavioral.visitor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParttimeEmployee implements Employee
{
    private String name;
    private double hourWage;
    private int workTime;
    public ParttimeEmployee(String name,double hourWage,int workTime)
    {
        this.name = name;
        this.hourWage = hourWage;
        this.workTime = workTime;
    }

    public void accept(Department handler)
    {
        handler.visit(this); //调用访问者的访问方法
    }
}
