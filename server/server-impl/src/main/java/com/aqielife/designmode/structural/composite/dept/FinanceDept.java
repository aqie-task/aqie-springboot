package com.aqielife.designmode.structural.composite.dept;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class FinanceDept extends LeafAbstractUnit {
    private String name;

    public FinanceDept(String name) {
        this.name = name;
    }

    @Override
    public void send() {
        log.info("send Finance");
    }
}
