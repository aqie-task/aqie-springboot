package com.aqielife.designmode.creational.factorymethod;

import com.aqielife.designmode.XMLUtil;

public class Client {
    public static void main(String args[]) {
        LoggerFactory factory;
        Logger logger;
        factory = (LoggerFactory) XMLUtil.getBean("server/server-impl/src/main/java/com/aqielife/designmode/creational/factorymethod/config.xml"); //可引入配置文件实现
        logger = factory.createLogger("");
        logger.writeLog();
    }
}
