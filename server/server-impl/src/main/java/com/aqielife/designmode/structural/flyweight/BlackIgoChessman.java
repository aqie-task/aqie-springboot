package com.aqielife.designmode.structural.flyweight;

/**
 * @Auther aqie
 * @Date 2021/10/23 15:33
 */
public class BlackIgoChessman extends IgoChessman{
	@Override
	public String getColor() {
		return "black";
	}
}
