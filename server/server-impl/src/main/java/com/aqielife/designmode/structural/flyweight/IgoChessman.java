package com.aqielife.designmode.structural.flyweight;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 15:29
 * @Desc 抽象享元类
 */
@Slf4j
public abstract class IgoChessman {
	public abstract String getColor();
	public void display(Coordinates coord) {
		log.info("color： {} {} {}", this.getColor(), coord.getX(),  coord.getY());
	}
}
