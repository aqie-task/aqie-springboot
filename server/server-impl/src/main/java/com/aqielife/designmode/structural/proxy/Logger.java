package com.aqielife.designmode.structural.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther aqie
 * @Date 2021/10/23 16:31
 */
@Slf4j
public class Logger {
	public void Log(String userId) {
		log.info("更新数据库，用户'{}'查询次数加1！",userId);
	}
}
