package com.aqielife.designmode.creational.simplefactory;

interface Chart {
    default void init(){
        // 公共方法的实现
    }
    public void display();
}
