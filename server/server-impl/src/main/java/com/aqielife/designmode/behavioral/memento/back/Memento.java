package com.aqielife.designmode.behavioral.memento.back;

//备忘录类
public class Memento {
    private String state;
    public Memento(Originator o) {
        state = o.getState();
    }
    public void setState(String state) {
        this.state=state;
    }
    public String getState() {
        return this.state;
    }
}
