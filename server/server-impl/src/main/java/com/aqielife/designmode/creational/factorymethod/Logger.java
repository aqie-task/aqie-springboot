package com.aqielife.designmode.creational.factorymethod;

public interface Logger {
    public void writeLog();
}
