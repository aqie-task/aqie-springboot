package com.aqielife.server.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.aqielife.server.dao.GoodsRepository;
import com.aqielife.server.mapper.GoodsMapper;
import com.aqielife.server.model.Goods;
import com.aqielife.server.service.GoodsService;
import lombok.extern.slf4j.Slf4j;

import org.apache.ibatis.cursor.Cursor;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author aqie
 * @date 2022/03/14 0:38
 * @desc
 */
@Slf4j
@Service
public class GoodsServiceImpl implements GoodsService {
  @Autowired
  private GoodsRepository repository;

  @Autowired
  private RedissonClient redisson;

  @Autowired
  private GoodsMapper goodsMapper;

  @Override
  public Integer getStock(Integer id) {
    return repository.getStock(id).orElse(0);
  }

  @Override
  @Transactional(rollbackOn = Exception.class)
  public int deductInventory(Integer id, Integer num) {
    return repository.deductInventory(id, num);
  }

  @Override
  @Transactional(rollbackOn = Exception.class)
  public void buy(Integer id, Integer num) {
    ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
    RLock rLock = redisson.getLock("SPECID_"+ id);
    for(int i = 0; i < 2; i++) {
      final int ii = i;
      fixedThreadPool.execute(() -> {
        log.info("线程名称：" + Thread.currentThread().getName() + "，执行" + ii);
        try {
          rLock.lock(5, TimeUnit.SECONDS);
          Integer stock = 0;
          try {
            Thread.sleep(2000);
            stock = getStock(id);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          if (stock >= num) {
            deductInventory(id, num);
          }
        } catch (Exception e) {
          log.error(e.getMessage(), e);
          throw new RuntimeException(e.getMessage(), e);
        } finally {
          rLock.unlock();
        }
      });
    }

  }

  @Override
  public void batchInsert() {
    List<Goods> list = new ArrayList<>();
    for (int i = 0; i < 10000; i++) {
      Goods goods = new Goods().setNum(RandomUtil.randomInt(1000)).setName(RandomUtil.randomString(5));
      list.add(goods);
    }
    repository.saveAll(list);
  }

  @Override
  public Cursor<Goods> scan(int limit) {
    return goodsMapper.scan(limit);
  }
}
