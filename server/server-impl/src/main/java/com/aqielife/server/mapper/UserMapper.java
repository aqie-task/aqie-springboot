
package com.aqielife.server.mapper;


import com.aqielife.server.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Mapper 接口
 *
 * @author aqie
 */
public interface UserMapper extends BaseMapper<User> {
  User getByAccount(String account);
}
