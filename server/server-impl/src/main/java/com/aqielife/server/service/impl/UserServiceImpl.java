package com.aqielife.server.service.impl;

import auth.entity.SignInIdentity;
import auth.service.IUserService;
import com.aqielife.server.mapper.UserMapper;
import com.aqielife.server.model.User;
import com.aqielife.server.service.UserApi;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author aqie
 * @date 2022/02/20 19:55
 * @desc
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService, UserApi {
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = getByAccount(username);
    // 从数据库查询用户
    if (user == null) {
      throw new UsernameNotFoundException("用户不存在");
    }
    // 初始化登录认证对象
    SignInIdentity signInIdentity = new SignInIdentity();
    // 拷贝属性
    BeanUtils.copyProperties(user, signInIdentity);
    return signInIdentity;
  }

  @Override
  public User getByAccount(String account) {
    return baseMapper.getByAccount(account);
  }
}
