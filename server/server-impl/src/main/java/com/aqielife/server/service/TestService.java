package com.aqielife.server.service;

/**
 * @Author aqie
 * @Date 2022/4/14 15:03
 * @desc
 */
public interface TestService {
    int test(int code) throws Exception;
}
