package com.aqielife.server.dao;

import com.aqielife.server.model.Goods;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author aqie
 * @date 2022/03/14 0:38
 * @desc
 */
@Repository
public interface GoodsRepository extends CrudRepository<Goods, Integer> {
  @Query("select g.num from Goods g where g.id=?1")
  Optional<Integer> getStock(Integer id);

  @Modifying
  @Transactional
  @Query("update Goods g set g.num = g.num - ?2 where g.id = ?1")
  int deductInventory(Integer id, Integer num);
}
