package com.aqielife.server.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author aqie
 * @date 2022/03/14 0:37
 * @desc
 */
@Accessors(chain = true)
@Entity
@Table
@Data
public class Goods {
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  private Integer id;

  private Integer num;

  private String name;
}
