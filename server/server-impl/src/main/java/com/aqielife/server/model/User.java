package com.aqielife.server.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @author aqie
 * @date 2022/02/20 20:20
 * @desc
 */
@Data
@TableName("blade_user")
public class User implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * 主键id
   */
  @TableId(value = "id", type = IdType.ASSIGN_ID)
  @JsonSerialize(using = ToStringSerializer.class)
  private Long id;

  private String tenantId;

  /**
   * 编号
   */
  private String code;
  /**
   * 账号
   */
  private String account;
  /**
   * 密码
   */
  private String password;
  /**
   * 昵称
   */
  private String username;
  /**
   * 真名
   */
  private String realName;
  /**
   * 头像
   */
  private String avatar;
  /**
   * 邮箱
   */
  private String email;
  /**
   * 手机
   */
  private String phone;
  /**
   * 生日
   */
  private Date birthday;

  /**
   * 收货地址
   */
  private String address;

  /**
   * 家长身份 1-父亲 2-母亲 3-其他
   */
  private Integer parentsIdentity;

  /**
   * 性别
   */
  private Integer sex;
  /**
   * 角色id
   */
  private String roleId;
  /**
   * 部门id
   */
  private String deptId;
  /**
   * 部门id
   */
  private String postId;

  /**
   * 用户类型 1-学生 2-老师
   */
  private Integer userType;

  /**
   * 租户所属 微信公众号 appid
   */
  @TableField(exist = false)
  private String appid;

  private Integer deleted;

  private String openId;
}
