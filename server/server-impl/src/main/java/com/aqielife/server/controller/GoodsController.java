package com.aqielife.server.controller;

import com.aqielife.common.R;
import com.aqielife.server.mapper.GoodsMapper;
import com.aqielife.server.model.Goods;
import com.aqielife.server.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cursor.Cursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author aqie
 * @date 2022/03/14 0:34
 * @desc
 */
@Slf4j
@RequestMapping("goods")
@RestController
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @PostMapping
    public R<Boolean> buy(Integer id, Integer num) {
        goodsService.buy(id, num);
        return R.status(true);
    }

    /**
     * 打开数据库连接两种方式： SqlSessionFactory TransactionTemplate
     * @param limit
     * @throws Exception
     */
    @GetMapping("scan/{limit}")
    public void scanGoods(@PathVariable("limit") int limit) throws Exception {

        transactionTemplate.execute(status -> {               // 2
            try (Cursor<Goods> cursor = goodsService.scan(limit)) {
                cursor.forEach(goods -> {
                    log.info("goods {} {}", goods.getId(), goods.getName());
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Boolean.TRUE;
        });
    }
}
