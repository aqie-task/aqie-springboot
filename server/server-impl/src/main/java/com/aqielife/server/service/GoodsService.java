package com.aqielife.server.service;

import com.aqielife.server.model.Goods;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.cursor.Cursor;

/**
 * @author aqie
 * @date 2022/03/14 0:37
 * @desc
 */
public interface GoodsService {
  /**
   * 查询库存
   */
  Integer getStock(Integer id);

  /**
   * 扣减库存
   */
  int deductInventory(Integer id, Integer num);

  void buy(Integer id, Integer num);

  void batchInsert();

  Cursor<Goods> scan(int limit);
}
