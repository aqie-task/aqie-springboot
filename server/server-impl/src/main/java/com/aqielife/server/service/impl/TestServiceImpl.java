package com.aqielife.server.service.impl;

import com.aqielife.server.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * @Author aqie
 * @Date 2022/4/14 15:03
 * @desc Retry 重试
 * 发送消息失败。
 * 调用远程服务失败。
 * 争抢锁失败。
 */
@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Override
    @Retryable(value = Exception.class,maxAttempts = 3,backoff = @Backoff(delay = 2000,multiplier = 1.5))
    public int test(int code) throws Exception {
        if (code != 1) {
            throw new Exception("failed");
        }
        log.info("execute success");
        return code;
    }

    @Recover
    public int recover(Exception e, int code){
        log.info("回调方法执行！！！！{}", code);
        //记日志到数据库 或者调用其余的方法
        return 400;
    }
}
