package com.aqielife.server.controller;

import com.aqielife.common.R;
import com.aqielife.common.utils.spring.SpringUtils;
import com.aqielife.demo.dubbo.common.DubboStudentService;
import com.aqielife.demo.dubbo.common.HelloServiceAPI;
import com.aqielife.disruptor.service.DisruptorMqService;
import com.aqielife.server.service.TestService;
import com.aqielife.web.async.DeferredResultHolder;
import com.aqielife.web.async.MockQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springblade.modules.edu.dto.SimpleStudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

/**
 * @Author aqie
 * @Date 2022/3/3 15:56
 * @desc
 */
@Slf4j
@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private MockQueue mockQueue;

    @Autowired
    private DeferredResultHolder deferredResultHolder;

    @Autowired
    private DisruptorMqService disruptorMqService;

    @Autowired
    private TestService testService;

    @GetMapping("hello")
    public R<String> hello() {
        HelloServiceAPI hello = SpringUtils.getBean("producerService");

        return R.success("EveryThing is ok " + hello.sayHello("hello"));
    }

    @GetMapping("studentList")
    public R<List<SimpleStudentDTO>> studentList(Long classId) {
        DubboStudentService dubboStudentService = SpringUtils.getBean("studentService", DubboStudentService.class);
        List<SimpleStudentDTO> studentList = dubboStudentService.getStudentListByClassId(classId);
        return R.data(studentList);
    }

    @RequestMapping("/order")
    public DeferredResult<String> order() throws Exception {
        String orderNumber = RandomStringUtils.randomNumeric(8);
        mockQueue.setPlaceOrder(orderNumber);

        DeferredResult<String> result = new DeferredResult<>();
        deferredResultHolder.getMap().put(orderNumber, result);

        return result;
        /*Callable<String> result = new Callable<String>() {
          @Override
          public String call() throws Exception {
            log.info("副线程开始");
            Thread.sleep(1000);
            log.info("副线程返回");
            return "success";
          }
        };*/
    }


    /**
     * 项目内部使用Disruptor做消息队列
     * @throws Exception
     */
    @PostMapping("disruptorSend")
    public void disruptorSend() throws Exception{
        disruptorMqService.send("send，Hello Disruptor!");
    }

    @GetMapping("retry")
    public void retry(int code) throws Exception {
        testService.test(code);
    }
}
