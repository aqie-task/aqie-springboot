package com.aqielife.server.service;


import com.aqielife.server.model.User;

/**
 * @author aqie
 * @date 2022/02/20 20:26
 * @desc
 */
public interface UserApi {
  User getByAccount(String account);
}
