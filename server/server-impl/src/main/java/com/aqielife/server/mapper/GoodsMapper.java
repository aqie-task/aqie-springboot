package com.aqielife.server.mapper;

import com.aqielife.server.model.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.cursor.Cursor;

/**
 * @author aqie
 * @date 2022/03/16 7:33
 * @desc
 */
@Mapper
public interface GoodsMapper {
  @Select("select * from goods limit #{limit}")
  Cursor<Goods> scan(@Param("limit") int limit);
}
