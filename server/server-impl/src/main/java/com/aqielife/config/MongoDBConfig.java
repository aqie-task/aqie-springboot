package com.aqielife.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

/**
 * @author aqie
 * @date 2022/03/11 22:29
 * @desc
 */
@Slf4j
@Configuration
public class MongoDBConfig {

  @Value("${spring.data.mongodb.uri}")
  private String uri;

  @Bean
  public MappingMongoConverter mappingMongoConverter(MongoMappingContext mongoMappingContext) {
    DefaultDbRefResolver dbRefResolver = new DefaultDbRefResolver(dbFactory());
    MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
    converter.setTypeMapper(new DefaultMongoTypeMapper(null));
    // 去除写入mongodb时的_class字段
    return converter;
  }

  @Bean
  public MongoDatabaseFactory dbFactory() {
    return new SimpleMongoClientDatabaseFactory(uri);
  }
}

