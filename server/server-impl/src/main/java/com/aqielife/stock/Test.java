package com.aqielife.stock;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ROUND_HALF_UP;

/**
 * @Auther aqie
 * @Date 2021/8/31 14:23
 */
@Slf4j
public class Test {
	private static List<Stock> stocks;
	static {
		stocks = new ArrayList<Stock>() {{
			add(new Stock("沧州明珠", 8.47d, 7d));
			add(new Stock("隆平高科", 22.2d, 19.31d));
			add(new Stock("隆基股份", 83.4d, 68.53d));
			add(new Stock("中航沈飞", 69d, 52.82d));
			add(new Stock("小康股份", 60d, 45.38d));
			add(new Stock("爱康科技", 4.06d, 3.81d));
			add(new Stock("华菱线缆", 14d, 13.59d));
			add(new Stock("中信重工", 4.63d, 4d));
			add(new Stock("广发证券", 21.87d, 18.88d));
		}};
	}
	// base now profit
	public static void main(String[] args) {

		//getBase(18.85,2.84,1d);
		getDown(118.95,1.05, 5d);
		getDown(1.349,8.36, 10d);
		getDown(22.11,3.58, 5d);
		getDown(173.80,4.07, 10d);
		getUp(26.27,10d,5d);
		getUp(9.07,0.78d,2d);

		// 返回最接近它的整数，若有两个返回接近的整数，则取最大的那个
		log.info("{} {}",  Math.round(11.5),  Math.round(-11.5));
		log.info("{}",  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

		System.out.println("6230250191900375842".length());
		getRate();
	}

	/**
	 * 下跌根据当前价格计算 指定跌幅价格
	 * @param nowD   当前价格
	 * @param profitD 跌幅
	 */
	private static void getDown(Double nowD,Double profitD,Double profitD2) {
		BigDecimal now = BigDecimal.valueOf(nowD);
		BigDecimal profit = BigDecimal.valueOf(profitD).divide(BigDecimal.valueOf(100),4, ROUND_HALF_UP);
		BigDecimal base = now.divide(BigDecimal.valueOf(1).subtract(profit),2, ROUND_HALF_UP);
		BigDecimal newNum = base.multiply(BigDecimal.valueOf(1).subtract(BigDecimal.valueOf(profitD2).divide(BigDecimal.valueOf(100), 4, ROUND_HALF_UP)));
		log.info("base {} newNum {} profit{}", base,newNum,profitD2);
	}

	/**
	 * 下跌根据当前价格计算 指定跌幅价格
	 * @param baseD   当前价格
	 * @param profitD 跌幅
	 */
	private static void getUp(Double baseD,Double profitD,Double profitD2) {
		BigDecimal now = BigDecimal.valueOf(baseD);
		BigDecimal profit = BigDecimal.valueOf(profitD).divide(BigDecimal.valueOf(100),4, ROUND_HALF_UP);
		BigDecimal base = now.divide(BigDecimal.valueOf(1).add(profit),2, ROUND_HALF_UP);
		BigDecimal newNum = base.multiply(BigDecimal.valueOf(1).add(BigDecimal.valueOf(profitD2).divide(BigDecimal.valueOf(100), 4, ROUND_HALF_UP)));
		log.info("base {} newNum {} profit{}", base,newNum,profitD2);
	}

	private static void getRate() {
		DecimalFormat df   = new DecimalFormat("######0.00");
		List<StockResult> collect = stocks.stream().map(stock -> {
			double rate = (stock.getCur() - stock.getAvg()) / stock.getCur() * 100;
			return StockResult.builder().name(stock.getName()).cur(stock.getCur()).rate(df.format(rate)).build();
		}).sorted().collect(Collectors.toList());
		log.info("{}", collect);

	}
}
