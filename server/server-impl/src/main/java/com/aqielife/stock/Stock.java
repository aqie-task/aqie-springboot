package com.aqielife.stock;

import lombok.Data;

/**
 * @Author aqie
 * @Date 2022/2/11 9:48
 * @desc
 */
@Data
public class Stock implements Comparable<Stock>{
	private String name;
	private double avg;
	private double cur;

	public Stock(String name, double avg, double cur) {
		this.name = name;
		this.avg = avg;
		this.cur = cur;
	}

	@Override
	public int compareTo(Stock o) {
		return (int) (o.getAvg() - avg);
	}

	public static void main(String[] args) {
		int a = 10;
		a += 10.5;
		System.out.println(a);
	}
}
