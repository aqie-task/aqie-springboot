package com.aqielife.stock;

import lombok.Builder;
import lombok.Data;

/**
 * @Author aqie
 * @Date 2022/2/11 9:59
 * @desc
 */
@Builder
@Data
public class StockResult implements Comparable<StockResult>{
	private String name;
	private String rate;
	private double cur;

	@Override
	public int compareTo(StockResult stockResult) {
		return (int) (Double.parseDouble(stockResult.rate) - Double.parseDouble(rate));
	}

	@Override
	public String toString() {
		return "StockResult{" +
			"name='" + name + '\'' +
			", rate='" + rate + '\'' +
			", cur=" + cur +
			'}'  + "\n";
	}
}
