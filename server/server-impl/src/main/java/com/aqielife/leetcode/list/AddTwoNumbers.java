package com.aqielife.leetcode.list;

import com.aqielife.leetcode.structure.ListNode;
import com.aqielife.leetcode.utils.ListUtils;

/**
 * @Function: 2
 * @Author: aqie
 * @Date: 2019/4/2 8:18
 * 1. 遍历链表
 * 2.
 */
public class AddTwoNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // return l1 == null ? l2 : l2 == null ? l1 : null;
        ListNode list = new ListNode(0);
        ListNode cur = list;
        int flag = 0;
        while (l1 != null || l2 != null){
            if (l1 == null){
                l1 = new ListNode(0);
            }
            if (l2 == null){
                l2 = new ListNode(0);
            }
            cur.next = new ListNode((flag + l1.val + l2.val)%10);
            flag = (flag + l1.val + l2.val) >= 10 ? 1 : 0;
            cur = cur.next;
            l1 = l1.next;
            l2 = l2.next;
        }
        if (flag == 1){
            cur.next = new ListNode(1);
        }
        return list.next;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(2);
        head.next = new ListNode(4);
        head.next.next = new ListNode(3);

        ListNode head2 = new ListNode(5);
        head2.next = new ListNode(6);
        head2.next.next = new ListNode(4);


        ListNode newHead = new AddTwoNumbers().addTwoNumbers(head, head2);

        new ListUtils().printLinkedList(newHead);
    }
}
