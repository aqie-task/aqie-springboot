package com.aqielife.leetcode.hash;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @Function: 1
 * @Author: aqie
 * @Date: 2019/3/28 16:46
 * nums = [2, 7, 11, 15], target = 9
 * 输出 [0, 1]
 */
@Slf4j
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        int length = nums.length;
        for (int i = 0;i < length;i++){
            int anotherNum = target - nums[i];
            if (map.containsKey(anotherNum)){
                return new int[]{i,map.get(anotherNum)};
            }
            map.put(nums[i],i);
        }
        return null;
    }

    public static void main(String[] args) {
        int[] arr = {2, 7, 11, 15};
        int target = 9;
        int[] ints = new TwoSum().twoSum(arr, target);
        log.info("{}", ints);
    }
}
