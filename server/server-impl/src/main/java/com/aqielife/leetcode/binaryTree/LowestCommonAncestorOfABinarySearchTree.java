package com.aqielife.leetcode.binaryTree;

import com.aqielife.leetcode.structure.TreeNode;

/**
 * @Function: 235
 * @Author: aqie
 * @Date: 2019/3/29 8:11
 * 二叉搜索树
 */
public class LowestCommonAncestorOfABinarySearchTree {
    public TreeNode lowestCommonAncestor2(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q){
            return root;
        }
        // p q 都在左子树
        if (Math.max(p.val, q.val) < root.val){
            return lowestCommonAncestor(root.left,p,q);
        }
        // p q 都在右子树
        if (Math.min(p.val,q.val) > root.val){
            return lowestCommonAncestor(root.right,p,q);
        }
        // p q 在左右子树
        return root;
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q){
            return root;
        }
        while (root != null){
            if (Math.max(p.val, q.val) < root.val)
                root = root.left;
            else if (Math.min(p.val,q.val) > root.val)
                root = root.right;
            else
                return root;
        }
        return null;
    }
}
