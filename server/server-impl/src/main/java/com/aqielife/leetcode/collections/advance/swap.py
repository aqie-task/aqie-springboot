class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        if head and head.next:
            temp = head.val
            head.val = head.next.val
            head.next.val = temp
            self.swapPairs(head.next.next)
        return head