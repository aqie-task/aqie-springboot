package com.aqielife.leetcode.breadth;

import com.aqielife.leetcode.structure.TreeNode;
import com.aqielife.leetcode.utils.TreeUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @Function: 102. 二叉树的层次遍历
 * @Author: aqie
 * @Date: 2019/3/30 15:25
 */
@Slf4j
public class BinaryTreeLevelOrderTraversal {
    // BFS 进行层序遍历
    public List<List<Integer>> levelOrder(TreeNode root) {
        if (root == null) return new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> lists = new ArrayList<>();

        queue.add(root);
        while (!queue.isEmpty()){
            int count = queue.size();
            ArrayList<Integer> subList = new ArrayList<>();
            while (count > 0){
                // 添加二叉树元素到队列
                TreeNode node = queue.poll();
                subList.add(node.val);
                if (node.left != null){
                    queue.add(node.left);
                }
                if (node.right != null){
                    queue.add(node.right);
                }
                count--;
            }
            lists.add(subList);
        }
        return lists;
    }

    public static void main(String[] args) {
        TreeNode root = TreeUtils.generateTree();
        log.info("{}", new BinaryTreeLevelOrderTraversal().levelOrder(root));
    }
}
