package com.aqielife.leetcode.hash;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Function: 15
 * @Author: aqie
 * @Date: 2019/3/28 17:38
 *  nums = [-1, 0, 1, 2, -1, -4]
 *  [
 *   [-1, 0, 1],
 *   [-1, -1, 2]
 * ]
 * 1. java 数组排序
 */
@Slf4j
public class ThreeSum {
    /**
     * 1.先排序
     * 2.两个指针首尾移动
     * 3. a+b+c > 0 右指针左移
     * 4. a+b+c < 0 左指针右移
     */
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        int length = nums.length;
        for (int i = 0;i < length - 2;i++){
            if (i > 0 && nums[i] == nums[i-1]) continue;
            int l = i + 1,r = length - 1;
            while (l < r){
                int s = nums[i] + nums[l] + nums[r];
                if (s < 0){
                    l++;
                }else if (s > 0){
                    r--;
                }else{
                    List<Integer> mList = new ArrayList<>();
                    mList.add(nums[i]);
                    mList.add(nums[l]);
                    mList.add(nums[r]);
                    list.add(mList);
                    // 去重
                    while (l < r && nums[l] == nums[l+1]) l++;
                    while (l < r && nums[r] == nums[r-1]) r--;
                    l+=1;
                    r -=1;
                }
            }
        }
        return list;
    }

    public static void main(String[] args) {
        int[] nums = {-1, 0, 1, 2, -1, -4};
        List<List<Integer>> lists = new ThreeSum().threeSum(nums);
        log.info("{}",lists);
        /*Arrays.sort(nums);
        log.info("{}", nums);*/
    }
}
