package com.aqielife.leetcode.multiThread.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/4/1 10:36
 */
@Component
@Slf4j
public class ScheduledTask {

    @Async("schedulePoolTaskExecutor")
    @Scheduled(fixedRate = 3000)
    public void getCurrentDate(){
        log.info("Scheduled Task : {}", new Date());
    }
}
