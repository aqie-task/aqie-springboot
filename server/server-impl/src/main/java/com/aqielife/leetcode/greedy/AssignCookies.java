package com.aqielife.leetcode.greedy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Function: 455
 * @Author: aqie
 * @Date: 2019/3/30 9:14
 * [1,2,3], [1,1]  -> 1
 * [1,2], [1,2,3]  -> 2
 */
@Slf4j
public class AssignCookies {
    public int findContentChildren(int[] g, int[] s) {
        log.info("{}");
        return 0;
    }

    public static void main(String[] args) {
        int[] childs = {1,2,3};
        int[] candies = {1,1};
        new AssignCookies().findContentChildren(childs, candies);
    }
}
