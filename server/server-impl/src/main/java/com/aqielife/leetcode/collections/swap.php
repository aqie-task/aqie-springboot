<?php
class Solution {

    /**
     * @param ListNode $head
     * @return ListNode
     */
    function swapPairs($head) {
        if(!$head || !$head->next) return $head;
        $cur = new ListNode(0);
        $cur->next = $head;
        $a = $cur;
        while($a->next && $a->next->next){
            $b = $a->next->next;
            $a->next->next = $b->next;
            $b->next = $a->next;
            $a->next = $b;
            $a = $b->next;
        }
        return $cur->next;
    }
}