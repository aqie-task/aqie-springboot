struct ListNode* swapPairs(struct ListNode* head) {
    struct ListNode *pTemp;

    if(head == NULL || head->next == NULL)
        return head;

    pTemp = head;
    head = head->next;
    pTemp->next = head->next;
    head->next = pTemp;
    head->next->next = swapPairs(head->next->next);
    return head;

}