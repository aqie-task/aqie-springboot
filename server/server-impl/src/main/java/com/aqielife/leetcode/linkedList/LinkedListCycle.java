package com.aqielife.leetcode.linkedList;

import com.aqielife.leetcode.structure.ListNode;
import com.aqielife.leetcode.utils.ListUtils;

/**
 * @Function: 141
 * @Author: aqie
 * @Date: 2019/3/26 18:47
 */
public class LinkedListCycle {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6};
        ListNode head = new ListUtils().createLinkedList(arr);
        boolean b = new LinkedListCycle().hasCycle(head);
        System.out.println(b);
    }
    public boolean hasCycle(ListNode head) {
        ListNode fast,slow;
        if (head == null || head.next == null) return  false;
        fast = slow = head;
        while (slow != null && fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast){
                return true;
            }
        }
        return false;
    }

    public boolean hasCycle2(ListNode head) {
        if(head == null || head.next == null)
            return false;
        ListNode fast = head.next.next;
        ListNode slow = head;
        while(true){
            if(fast == null || fast.next == null)
                return false;
            else if(fast == slow || fast.next == slow)
                return true;
            else{
                fast = fast.next.next;
                slow = slow.next;
            }
        }
    }
}
