package com.aqielife.leetcode.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/26 10:06
 */
@Slf4j
public class CharacterUtils {
    public static void main(String[] args) {
        Character oldChar = '-';
        Character newChar = '_';
        String underline = replaceCharacter("best-time-to-buy-and-sell-stock-with-cooldown",oldChar,newChar);
        log.info(underline);

        String oldStr = "_";
        String camel = underline2Camelcase(underline,oldStr);
        log.info(camel);

        String newUnderline = camelcase2Underline(camel);
        log.info(newUnderline);
    }

    // 1. 将字符串中-替换为_
    public static String replaceCharacter(String str,Character oldChar, Character newChar){
        return str.replace(oldChar, newChar);
    }

    // 2. 下划线转驼峰
    public static String underline2Camelcase(String str, String oldStr){
        StringBuilder builder = new StringBuilder();
        Arrays.asList(str.split(oldStr)).forEach(temp -> builder.append(StringUtils.capitalize(temp)));
        return builder.toString();
    }

    // 3.驼峰转下划线
    public static String camelcase2Underline(String str){
        // 判断首字母是否大写
        if(Character.isUpperCase(str.trim().charAt(0))){
           str = String.valueOf(Character.toLowerCase(str.charAt(0))) + str.substring(1);
        }
        return String.join("_", str.replaceAll("([A-Z])", ",$1").split(",")).toLowerCase();
    }
}
