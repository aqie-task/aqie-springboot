package com.aqielife.leetcode.depth;

import com.aqielife.leetcode.structure.TreeNode;
import com.aqielife.leetcode.utils.TreeUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/30 16:28
 */
@Slf4j
public class BinaryTreeLevelOrderTraversal {
    // DFS 进行层序遍历
    public List<List<Integer>> levelOrder(TreeNode root){
        if (root == null) return new ArrayList<>();
        List<List<Integer>> res=new ArrayList<List<Integer>>();

        _dfs(root,0,res);
        return res;
    }
    private void _dfs(TreeNode node, int level,List<List<Integer>> res){
        if (node == null) return;
        List<Integer> list;
        if (res.size() > level){
            list = res.get(level);
            list.add(node.val);
        }else{
            list = new ArrayList<>();
            list.add(node.val);
            res.add(list);
        }
        _dfs(node.left, level + 1, res);
        _dfs(node.right, level + 1, res);
    }
    public static void main(String[] args) {
        TreeNode root = TreeUtils.generateTree();
        log.info("{}", new BinaryTreeLevelOrderTraversal().levelOrder(root));
    }


}
