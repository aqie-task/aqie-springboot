package com.aqielife.leetcode.backTrack;

import java.util.ArrayList;
import java.util.List;

/**
 * @Function: 22. 括号生成
 * @Author: aqie
 * @Date: 2019/3/30 17:27
 */
public class GenerateParentheses {
    public List<String> generateParenthesis(int n) {
        List<String> list = new ArrayList<>();
        //gen(0,0,n,list,"");
        gen2(n,n,list,"");
        return list;
    }

    private void gen(int left, int right,int n, List<String> list,String res){
        if (left == n && right ==n) {
            list.add(res);
            return;
        }
        if (left < n){
            gen(left + 1,right,n,list,res+"(");
        }
        if (left > right && right < n){
            gen(left, right+1,n,list,res+")");
        }
    }

    private void gen2(int left, int right, List<String> list,String res){
        if (left == 0 && right == 0) {
            list.add(res);
            return;
        }
        if (left > 0){
            gen2(left - 1,right,list,res+"(");
        }
        if (right > left){
            gen2(left, right-1,list,res+")");
        }
    }

    public static void main(String[] args) {
        System.out.println(new GenerateParentheses().generateParenthesis(3));
    }
}
