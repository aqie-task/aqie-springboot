class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        p=ListNode(0)
        p.next=head;head=p
        while p.next!=None and p.next.next!=None:
            s=p.next.next
            p.next.next=s.next
            s.next=p.next
            p.next=s
            p=s.next
        return head.next