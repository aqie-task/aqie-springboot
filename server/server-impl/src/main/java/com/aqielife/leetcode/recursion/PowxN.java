package com.aqielife.leetcode.recursion;

import lombok.extern.slf4j.Slf4j;

/**
 * @Function: 50
 * @Author: aqie
 * @Date: 2019/3/29 9:28
 * 1. double 转 int  ：
 * 2. int 转 double  ：直接转
 */
@Slf4j
public class PowxN {
    public double myPow(double x, int n) {
        if (n == 0) return 1;
        if (n == 1) return x;
        if (n == -1) return 1/x;
        if (n > 1){
            return x * myPow(x, n - 1);
        }else{
            return 1/ (x * myPow(x, -n));
        }

    }
    // 1. 快速幂
    public double myPow2(double x, int n) {
        double pow = 1;
        long temp = n > 0 ? n : -n;
        if (temp == 0) return 1;
        while (temp > 0){
            if ((temp & 1) == 1){
                pow = pow * x;
            }
            x *= x;
            temp >>= 1;
        }
        if (n > 0)
            return pow;
        else{
            return 1 / pow;
        }
    }
    // 2. 递归
    public double myPow3(double x, int n) {

        return 0;
    }

    // 3. 分治
    public double myPow4(double x, int n) {
        if (n == 0) return 1;
        if (n < 0){
            return 1 / myPow4(x , -n);
        }
        // 如果n是奇数
        if (n % 2 == 1){
            return x * myPow4(x, n -1);
        }
        return myPow4(x*x, n / 2);
    }
    public static void main(String[] args) {
        // 0x7fffffff  = 2 6843 5455 = 16^7 * 8 - 1
        log.info("{}",Integer.MAX_VALUE);       // 21 4748 3647
        log.info("{}",Integer.MIN_VALUE);
        log.info("{}",-Integer.MAX_VALUE -1);
        log.info("{}",new PowxN().myPow2(0.00001 ,2147483647));
        log.info("{}",new PowxN().myPow2(1.00000 ,-2147483648));
        log.info("{}",new PowxN().myPow2(2.00000 ,-2147483648));
        log.info("{}",new PowxN().myPow2(34.00515 ,-3));
        // log.info("{}",new PowxN().myPow(-1, 2));
        // new PowxN().test();
    }

    // byte，short，int，long，float，double 小转大 直接转
    public void test(){
        int i = 1;
        double j = 1.00;
        double sum = i + j;
        log.info("{}", sum);
    }

    public int stringToInt(String string){
        int j = 0;
        String str = string.substring(0, string.indexOf(".")) + string.substring(string.indexOf(".") + 1);
        int intgeo = Integer.parseInt(str);
        return intgeo;
    }
}
