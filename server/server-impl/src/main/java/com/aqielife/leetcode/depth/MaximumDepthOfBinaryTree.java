package com.aqielife.leetcode.depth;

import com.aqielife.leetcode.structure.TreeNode;
import com.aqielife.leetcode.utils.TreeUtils;

/**
 * @Function: 104 二叉树的最大深度
 * @Author: aqie
 * @Date: 2019/3/30 16:32
 */
public class MaximumDepthOfBinaryTree {
    // 1. BFS
    public int maxDepth(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(maxDepth(root.left),maxDepth(root.right));
    }


    public static void main(String[] args) {
        TreeNode node = TreeUtils.generateTree();
        System.out.println(new MaximumDepthOfBinaryTree().maxDepth(node));
    }
}
