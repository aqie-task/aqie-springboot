package com.aqielife.leetcode.trie;

/**
 * @Function: 208 todo
 * @Author: aqie
 * @Date: 2019/4/2 19:53
 */
public class ImplementTriePrefixTree {
    class Trie {

        /** Initialize your data structure here. */
        public Trie() {

        }

        /** Inserts a word into the trie. */
        public void insert(String word) {

        }

        /** Returns if the word is in the trie. */
        public boolean search(String word) {
            return false;
        }

        /** Returns if there is any word in the trie that starts with the given prefix. */
        public boolean startsWith(String prefix) {
            return false;
        }
    }
    public static void main(String[] args) {

    }
}


