package com.aqielife.leetcode.recursion;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Function: 169
 * @Author: aqie
 * @Date: 2019/3/29 16:33
 * 1. ArrayList 按指定大小,将集合分成多个
 * 2. byte[]数组分成 平均切分
 * 3. Arrays.copyOfRange : 左闭右开
 */
@Slf4j
public class MajorityElement {
    // 1.分治 todo
    public int majorityElement(int[] nums) {
        // 1. 左右分别求majority partition
        int length = nums.length;
        int left,right;
        if (length % 2 == 1) {
            left = majorityElement(Arrays.copyOfRange(nums, 0, nums[length/2]));
            log.info("{}", Arrays.copyOfRange(nums, 0,length/2 + 1));
            right = majorityElement(Arrays.copyOfRange(nums, nums[length/2 +1],length));
            log.info("{}", Arrays.copyOfRange(nums, length/2 +1,length));
        }else{
            left = majorityElement(Arrays.copyOfRange(nums, 0, length/2));
            log.info("{}", Arrays.copyOfRange(nums, 0, length/2));
            right = majorityElement(Arrays.copyOfRange(nums, length/2, length));
            log.info("{}", Arrays.copyOfRange(nums, length/2, length));
        }
        if (left == right) return left;
        // 2. left == right
        // 3. return max(count(left), count(right))
        // merge
        return 0;
    }

    // 2.排序 O(NlogN)
    public int majorityElement2(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

    // 3. 开始count=1，遇到相同的就加1，遇到不同的就减1，减到0就重新换个数开始计数
    public int majorityElement3(int[] nums) {
        int count = 1;
        int num = nums[0];
        int length = nums.length;
        for (int i = 1; i < length; i++){
            if (nums[i] == num){
                count++;
            }else{
                count--;
                if (count == 0){
                    num = nums[i+1];
                }
            }
        }
        log.info("{}", num);
        return num;
    }

    // 4.map
    public int majorityElement4(int[] nums) {
        Map<Integer,Integer> map = new HashMap<>();
        int length = nums.length;
        for (int i = 0; i < length;i++){
           if (map.containsKey(nums[i])){
               map.put(nums[i],map.get(nums[i]) + 1);
           }else{
               map.put(nums[i],map.getOrDefault(nums[i],1));
           }
        }
        TreeMap<Integer, Integer> map2 = new TreeMap<>();

        for (Map.Entry<Integer,Integer> entry : map.entrySet()){
            log.info("{} - {}",entry.getKey(),entry.getValue());
            map2.put(entry.getValue(), entry.getKey());
        }

        log.info("{}",map2.lastKey());
        return map2.get(map2.lastKey());
    }

    public static void main(String[] args) {
        //int[] nums = {2,2,1,1,1,2,2,3};
        // int[] nums = {1,3,3,3,3,3,2,3,4};

        // int[] nums = {1,2,3,4};
        //int[] nums = {3,3,4};
        int[] nums = {1,1,1,2,1,1};
        new MajorityElement().majorityElement(nums);

        /*int[] nums = {1,2,3,4};
        int length = nums.length;
        log.info("{}",length / 2);*/
        // log.info("{}", Arrays.copyOfRange(nums, 0, length/2 + 1));        // 左部分
        // log.info("{}", Arrays.copyOfRange(nums, length/2 + 1,length));        // 右部分

        // log.info("{}", Arrays.copyOfRange(nums, 0, length/2));
        // log.info("{}", Arrays.copyOfRange(nums, length/2, length));
    }
}
