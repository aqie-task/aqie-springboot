package com.aqielife.leetcode.math;

import java.util.Stack;

/**
 * @Function: 7     todo
 * @Author: aqie
 * @Date: 2019/4/2 16:48
 * 1. double 转 int
 * 2. int 转 string
 * 3. string 遍历
 * 4. char 拼接成string
 * 5. StringBuilder 转 string
 * 6. string 转 int
 * 7. 字符串最后一位移到队首
 * */
public class ReverseInteger {
    public int reverse(int x) {
        String s = x + "";
        int length = s.length();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < length; i++){
            stack.push(s.charAt(i));
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++){
            Character pop = stack.pop();
            sb.append(pop);
        }
        String news = sb.toString();
        if (news.endsWith("-")){
            news = "-" + news.substring(0, length-1);
        }
        try {
            return Integer.parseInt(news);
        }catch (NumberFormatException e){
            return 0;
        }
    }

    /**
     * 1.（&）、非（~）、或（|）、异或（^）
     * 2. int [-2147483648, 2147483647]
     * 3.
     */
    public static void main(String[] args) {
        int a = 123;
        int b = (int)(Math.pow(2,31) - 1);
        int c = -(int)(Math.pow(2,31));
        int d = 20010;
        int e = -123;
        System.out.println(b);

        System.out.println(new ReverseInteger().reverse(a));
        System.out.println(new ReverseInteger().reverse(d));
        System.out.println(new ReverseInteger().reverse(b));
        // System.out.println(new ReverseInteger().reverse(c));
        System.out.println(new ReverseInteger().reverse(e));
    }
}
