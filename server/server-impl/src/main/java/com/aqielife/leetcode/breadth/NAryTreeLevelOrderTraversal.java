package com.aqielife.leetcode.breadth;

import com.aqielife.leetcode.structure.Node;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @Function: 429
 * @Author: aqie
 * @Date: 2019/3/30 13:58
 * 1. List 三种遍历方式(1.for 2.增强for 3. Iterator)
 * 2. queue.pop -> removeFirst  vs queue.poll -> pollFirst
 * 3. queue.add -> addLast  vs queue.push -> addFirst vs queue.offer -> offerLast vs queue.put -> putLast
 *      队列满： offer 返回false; put 阻塞; add 抛异常
 */
@Slf4j
public class NAryTreeLevelOrderTraversal {
    public List<List<Integer>> levelOrder(Node root) {
        if (root == null) return new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();
        List<List<Integer>> lists = new ArrayList<>();

        queue.add(root);
        while (!queue.isEmpty()){
           int count = queue.size();
           ArrayList<Integer> subList = new ArrayList<>();
           while (count > 0){
               // 1.添加队列元素 到链表
               Node node = queue.poll();
               subList.add(node.val);
               // 2. 将所有子节点加入队列
               if (node.children != null && !node.children.isEmpty()) {
                   queue.addAll(node.children);
               }
               count--;
           }
           lists.add(subList);
        }
        return lists;
    }

    public static void main(String[] args) {
        List<Node> subList = new ArrayList<>();
        subList.add(new Node(5,null));
        subList.add(new Node(6, null));
        List<Node> list = new ArrayList<>();
        list.add(new Node(3,subList));
        list.add(new Node(2,null));
        list.add(new Node(4,null));
        Node root = new Node(1,list);

        List<List<Integer>> lists = new NAryTreeLevelOrderTraversal().levelOrder(root);
        log.info("{}", lists);
    }
}
