package com.aqielife.leetcode.binaryTree;

import com.aqielife.leetcode.structure.TreeNode;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @Function: 98
 * @Author: aqie
 * @Date: 2019/3/28 18:55
 *  二叉搜索树 ： 节点的左子树只包含小于当前节点的数！！！
 *          A
 *      B      C
 *  D     E   F  G
 *  前序(根节点 再左右节点) ABDECFG
 *  中序(先遍历左节点) DBEAFCG   -> 顺序
 *  后序(先遍历左右节点) DEBFGCA
 */
@Slf4j
public class ValidateBinarySearchTree {
    /**
     * 1.
     * 2. 递归
     */
    public boolean isValidBST2(TreeNode root) {
        if (root == null) return true;
        return isValid(root,null,null);
    }

    private boolean isValid(TreeNode root, Integer min, Integer max){
        if (root == null) return true;
        if (min != null && root.val <= min) return false;
        if (max != null && root.val >= max) return false;
        return isValid(root.left,min,root.val) && isValid(root.right, root.val,max);
    }

    /****************方法：中序遍历 判断是否升序排列******************/

    public boolean isValidBST(TreeNode root) {
        if (root == null) return true;
        List<Integer> list = new ArrayList<>();
        inOrder(root,list);
        int length = list.size();
        if (length == 1) return true;
        // list 转数组
        int[] nums = list.stream().mapToInt(Integer::valueOf).toArray();

        // 注意数组越界
        for (int i =0;i<length-1;i++){
            if (nums[i+1] <= nums[i]) return false;
        }
        return true;
    }

    // 中序遍历
    private void inOrder(TreeNode root,List<Integer> list){
        if (root == null) return;
        inOrder(root.left,list);
        list.add(root.val);
        inOrder(root.right,list);
    }

    // 输入数组是层序遍历结果
    public static void main(String[] args) {
        TreeNode node = new TreeNode(1);
        node.left= new TreeNode(1);
        node.right=null;
        log.info("{}",new ValidateBinarySearchTree().isValidBST(node));
    }
}
