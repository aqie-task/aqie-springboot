package com.aqielife.leetcode.list;

import com.aqielife.leetcode.structure.ListNode;
import com.aqielife.leetcode.utils.ListUtils;

/**
 * @Function: 21
 * @Author: aqie
 * @Date: 2019/4/2 17:35
 */
public class MergeTwoSortedLists {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 1. 两个链表拼接成同一个链表
        /*ListNode node = l1.next;
        // 获取链表最后一个节点
        while (node.next != null){
            node = node.next;
        }
        node.next = l2;
        return l1;*/

        // LinkedList TreeMap  TreeSet

        // 虚拟头结点
        ListNode dummyHead = new ListNode(0);
        ListNode cur = dummyHead;
        while(l1 != null && l2 != null){
            if (l1.val < l2.val){
                cur.next = l1;
                cur = cur.next;
                l1 = l1.next;
            } else {
                cur.next = l2;
                cur = cur.next;
                l2 = l2.next;
            }
        }
        // 任一为空，直接连接另一条链表
        if (l1 == null) {
            cur.next = l2;
        } else {
            cur.next = l1;
        }
        return dummyHead.next;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(4);

        ListNode l2 = new ListNode(1);
        l2.next = new ListNode(3);
        l2.next.next = new ListNode(4);

        ListUtils.printLinkedList(new MergeTwoSortedLists().mergeTwoLists(l1, l2));
    }
}
