package com.aqielife.leetcode.linkedList;

import com.aqielife.leetcode.structure.ListNode;
import com.aqielife.leetcode.utils.ListUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @Function: 206
 * @Author: aqie
 * @Date: 2019/3/26 10:46
 * 输入: 1->2->3->4->5->NULL
 * 输出: 5->4->3->2->1->NULL
 */
@Slf4j
public class ReverseLinkedList {

    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) return head;

        ListNode pre = null;
        ListNode cur = head;
        ListNode next = null;
        while(cur != null){
            // 记录当前节点下一结点 用于后移
            next = cur.next;
            // 当前节点指向其前面节点
            cur.next = pre;
            // 前指针后移
            pre = cur;
            // 当前指针后移
            cur = next;
        }
        return pre;
    }

    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        log.info("{}",list);
        Collections.reverse(list);
        log.info("{}",list);

        // 测试自定义列表反转
        int[] arr = {3,4,5};
        ListNode head = new ListUtils().createLinkedList(arr);
        head = new ReverseLinkedList().reverseList(head);
        new ListUtils().printLinkedList(head);
    }
}
