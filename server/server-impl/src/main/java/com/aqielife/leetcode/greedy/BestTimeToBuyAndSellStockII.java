package com.aqielife.leetcode.greedy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Function: 122
 * @Author: aqie
 * @Date: 2019/3/30 9:04
 * 题目：尽可能地完成更多的交易，计算你所能获取的最大利润，
 *  你必须在再次购买前出售掉之前的股票，每天买卖次数不限
 */
@Slf4j
public class BestTimeToBuyAndSellStockII {
    /**
     * 法1.DFS
     * 法2.贪心 只有明天比今天高 今天就买
     */
    public int maxProfit(int[] prices) {
        int length = prices.length;
        int profit = 0;
        for (int i = 0; i < length-1; i++){
            if (prices[i+1] > prices[i]){
                profit += prices[i+1] - prices[i];
            }
        }
        return profit;
    }

    public static void main(String[] args) {
        // int[] prices = {7,1,5,3,6,4};
        // int[] prices = {1,2,3,4,5};
        // int[] prices = {1,2,3,4,5};
       // int[] prices = {7,6,4,3,1};
        // int[] prices = {1};
        int[] prices = {};
        log.info("{}",new BestTimeToBuyAndSellStockII().maxProfit(prices));
    }
}
