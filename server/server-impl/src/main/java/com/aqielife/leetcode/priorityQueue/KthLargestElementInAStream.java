package com.aqielife.leetcode.priorityQueue;

import lombok.extern.slf4j.Slf4j;

import java.util.PriorityQueue;

/**
 * @Function: 703
 * @Author: aqie
 * @Date: 2019/3/27 15:47
 * 思路1：保存前k个元素，每次进行排序
 * 思路2：(K个元素的小底堆,最上面一定是第K大元素)
 */
@Slf4j
public class KthLargestElementInAStream {

    final PriorityQueue<Integer> q;
    final int k;
    public KthLargestElementInAStream(int k, int[] nums) {
        this.k = k;
        q = new PriorityQueue<>(k);
        for (int num : nums){
            add(num);
        }
    }

    public int add(int val) {
        // 最小堆元素小于k，将元素放入最小堆
        if (q.size() < k){
            q.add(val);
        // 比堆顶元素大 将堆顶元素打掉 重排序
        }else if (q.peek() < val){
            q.poll();
            q.add(val);
        }
        // else 比堆顶元素还小 直接扔掉
        // 返回最小堆顶元素
        return q.peek();
    }

    public static void main(String[] args) {
        int k = 3;
        int[] arr = {4,5,8,2};
        KthLargestElementInAStream kth = new KthLargestElementInAStream(3,arr);
        log.info("{}",kth.add(3));
        log.info("{}",kth.add(5));
        log.info("{}",kth.add(10));
        log.info("{}",kth.add(9));
        log.info("{}",kth.add(4));

        int a = 8;
        // 无符号右移 a/2
        log.info("{}", a>>>2);  // 8/(2^2)
    }
}
