class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode* dummy=new ListNode(-1);
        dummy->next=head;

        ListNode *preNode=dummy,*curNode=head;

        while(curNode && curNode->next){
            preNode->next=curNode->next;
            curNode->next=preNode->next->next;
            preNode->next->next=curNode;

            // go over two nodes
            preNode=curNode;
            curNode=curNode->next;
        }

        head=dummy->next;
        delete dummy;   // 删除头结点
        return head;

    }
};