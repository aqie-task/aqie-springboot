package com.aqielife.leetcode.depth;

import com.aqielife.leetcode.structure.TreeNode;
import com.aqielife.leetcode.utils.TreeUtils;

/**
 * @Function: 111. 二叉树的最小深度
 * @Author: aqie
 * @Date: 2019/3/30 16:38
 */
public class MinimumDepthOfBinaryTree {
    public int minDepth(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;
        else if (root.left == null){
            return 1 + minDepth(root.right);
        }else if (root.right == null){
            return 1 + minDepth(root.left);
        }else {
            return 1 + Math.min(minDepth(root.right), minDepth(root.left));
        }
    }

    public static void main(String[] args) {
        TreeNode root = TreeUtils.generateTree();
        System.out.println(new MinimumDepthOfBinaryTree().minDepth(root));
    }
}
