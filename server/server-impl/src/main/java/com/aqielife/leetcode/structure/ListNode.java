package com.aqielife.leetcode.structure;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/26 15:01
 */
public class ListNode {
    public int val;
    public ListNode next;
    public ListNode(int x) { val = x; }
}
