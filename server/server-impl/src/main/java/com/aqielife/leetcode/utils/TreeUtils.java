package com.aqielife.leetcode.utils;

import com.aqielife.leetcode.structure.TreeNode;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/28 19:26
 */
public class TreeUtils {
    // 构建一棵树
    public static TreeNode generateTree() {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);
        return root;
    }
}
