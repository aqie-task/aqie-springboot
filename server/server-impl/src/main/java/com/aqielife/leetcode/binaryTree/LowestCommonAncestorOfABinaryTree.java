package com.aqielife.leetcode.binaryTree;

import com.aqielife.leetcode.structure.TreeNode;

/**
 * @Function: 236
 * @Author: aqie
 * @Date: 2019/3/29 8:13
 * 二叉树
 */
public class LowestCommonAncestorOfABinaryTree {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q){
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        return left == null ? right : right == null ? left : root;
    }

    public static void main(String[] args) {
        // [3,5,1,6,2,0,8,null,null,7,4]    5 4

    }
}
