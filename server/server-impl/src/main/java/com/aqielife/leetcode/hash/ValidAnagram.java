package com.aqielife.leetcode.hash;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @Function: 242
 * @Author: aqie
 * @Date: 2019/3/28 15:20
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 * 1.字符串转数组
 * 2.集合求差集
 * 3.
 */
@Slf4j
public class ValidAnagram {
    // 法1： 快排然后比较两个字符串是否相等 O(nlogn)
    public boolean isAnagram1(String s, String t) {
        char[] sChars = s.toCharArray();
        char[] tChars = t.toCharArray();
        Arrays.sort(sChars);
        Arrays.sort(tChars);
        return String.valueOf(sChars).equals(String.valueOf(tChars));
    }

    // 法2： Map计数
    public boolean isAnagram(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        Map<Character, Integer> map2 = new HashMap<>();
        for (char ch : s.toCharArray()){
            map.put(ch, map.getOrDefault(ch,0) + 1);
        }
        for (char ch : t.toCharArray()){
            map2.put(ch, map2.getOrDefault(ch,0) + 1);
        }
        return map.equals(map2);
    }

    public boolean isAnagram3(String s, String t) {
        int[] sCounts = new int[26];
        int[] tCounts = new int[26];
        for (char ch : s.toCharArray()){
            sCounts[ch - 'a']++;                // 对应字母处值++
        }

        for (char ch : t.toCharArray()){
            tCounts[ch - 'a']++;                // 对应字母处值++
        }

        for (int i = 0; i < 26;i++){
            if (tCounts[i] != sCounts[i]) return false;
        }
        return true;
    }
    // 单纯测试集合取差集 失败
    public boolean isAnagramFail(String s, String t) {
        // 1. 字符串转char数组
        char[] chars = s.toCharArray();
        char[] chart = t.toCharArray();
        // 2.数组转list
        List lists = Arrays.asList(chars);
        List listt = Arrays.asList(chart);
        // 3. 创建集合
        ArrayList collections = new ArrayList<Integer>(lists);
        ArrayList collectiont = new ArrayList<Integer>(listt);
        // 4. 求差集
        log.info("{}", collectiont.get(0));
        log.info("{}", collectiont.removeAll(collectiont));
        //log.info("{}", collectiont.get(0));

        return true;
    }

    public static void main(String[] args) {
        ValidAnagram validAnagram = new ValidAnagram();
        log.info("{}",validAnagram.isAnagram("anagram", "nagaram"));
    }
}
