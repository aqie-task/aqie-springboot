package com.aqielife.leetcode.dequeue;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Function: 239
 * @Author: aqie
 * @Date: 2019/3/27 17:10
 * 输入: nums = [1,3,-1,-3,5,3,6,7], 和 k = 3
 * 输出: [3,3,5,5,6,7]
 * 1. java 添加元素到数组(数组固定长度)
 * 2. list 转 数组
 * 3. 数组转list
 * 4. PriorityQueue 最大堆
 */
@Slf4j
public class SlidingWindowMaximum {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums == null || nums.length == 0 || nums.length < k){
            return new int[]{};
        }
        // 双端队列
        LinkedList<Integer> deque = new LinkedList<>();
        int n = nums.length;
        int index = 0;
        // 结果数组
        int[] res = new int[n-k+1];
        for (int i = 0;i < n;i++){
            //当deque不为空，且deque的尾部索引对应的值小于等于外面来的值，将尾部的索引弹出
            while (!deque.isEmpty() && nums[deque.peekLast()] <= nums[i]) {
                deque.pollLast();
            }
            //在deque尾部添加索引
            deque.addLast(i);
            //判断deque的头部索引是否过期 k是滑动窗口的size,当deque的头部索引等于i-k时，表明索引过期弹出
            //当然存在一种情况是deque的尾部进来的新索引将老的索引挤出去的场景
            if (deque.peekFirst() == i - k) {
                deque.pollFirst();
            }
            //当i 至少为（k-1）的时候，表示最大滑动窗口开始形成，开始收集resArr
            if (i >= k - 1) {
                res[index++] = nums[deque.peekFirst()];
            }
        }
        return res;
    }

    public static void main(String[] args) {
        // int k = 3; int[] arr = {1,3,-1,-3,5,3,6,7};     // [3, 3, 5, 5, 6, 7]
        // int k = 3; int[] arr = {10,3,11,12,5,3,6,7};    // [11, 12, 12, 12, 6, 7]
        // int k = 1; int [] arr = {1,-1};                    // [1, -1]
        // int k = 2; int [] arr = {7,2,4};                    // [7, 4]
        int k = 0; int [] arr = new int[0];
        int[] ints = new SlidingWindowMaximum().maxSlidingWindow(arr, k);
        List<Integer> list = Arrays.stream(ints).boxed().collect(Collectors.toList());
        log.info("{}", list);
    }
}
