package com.aqielife.leetcode.stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @Function: 20
 * @Author: aqie
 * @Date: 2019/3/26 20:24
 * 1. 字符串遍历
 * 2. string 转字符数组
 * 3.hashmap 根据key获取value; 根据value 获取key
 * 4.判断栈是否为空
 * 5.判断数组中是否包含某个元素
 * 6.判断hashmap是否存在key
 * 7.遍历String
 */
public class ValidParentheses {
    public boolean isValid(String s) {
        // 1.左括号 入栈  | 不是右括号入栈
        // 2.右括号 peek & pop
        // 3.最终栈是否为空

        Stack<Character> stack = new Stack<>();
        char[] chars = s.toCharArray();
        Map<Character,Character> map = new HashMap<Character, Character>(){
            {
                put('(',')');
                put('[',']');
                put('{','}');
            }
        };

        for (Character character : chars){
            // 左括号
            if (map.containsKey(character)){
                stack.push(character);
            // 右括号(如果栈不为空 且 不匹配)
            }else{
                if (!stack.empty() && map.get(stack.peek()) == character){
                    stack.pop();
                }else{
                    return false;
                }
            }
        }
        return stack.empty();
    }

    public boolean isValid2(String s) {
        Stack<Character> stack = new Stack<>();
        Map<Character,Character> map = new HashMap<Character, Character>(){
            {
                put('(',')');
                put('[',']');
                put('{','}');
            }
        };

        int length = s.length();
        for (int i = 0;i < length;i++){
            char c = s.charAt(i);
            if (map.containsKey(c)){
                stack.push(c);
            }else {
                if (!stack.empty() && map.get(stack.peek()) == c){
                    stack.pop();
                }else{
                    return false;
                }
            }
        }
        return stack.empty();
    }
    public static void main(String[] args) {
        String str = "()[]{}";
        str = "([)]";
        str = "]";
        System.out.println(new ValidParentheses().isValid(str));
    }
}
