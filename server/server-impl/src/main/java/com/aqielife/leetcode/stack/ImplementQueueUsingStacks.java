package com.aqielife.leetcode.stack;

import lombok.extern.slf4j.Slf4j;

import java.util.Stack;

/**
 * @Function: 232
 * @Author: aqie
 * @Date: 2019/3/27 14:50
 * Queue : push pop peek empty
 * Stack : push pop top  empty
 * 1. 内部类实例化
 * 2.
 */
@Slf4j
public class ImplementQueueUsingStacks {
    /**
     * InputStack   : push
     * OutputStack  : 遇到pop peek,将InputStack 数据导入到 OutputStack
     */
    class MyQueue {
        private Stack<Integer> inputStack;
        private Stack<Integer> outputStack;
        /** Initialize your data structure here. */
        public MyQueue() {
            inputStack = new Stack<>();
            outputStack = new Stack<>();
        }

        /** Push element x to the back of queue. */
        public void push(int x) {
            if (outputStack.empty()){
                inputStack.push(x);
            }else{
                while (!outputStack.empty()){
                    inputStack.push(outputStack.pop());
                }
                inputStack.push(x);
            }

        }

        /** Removes the element from in front of queue and returns that element. */
        public int pop() {
            while(!inputStack.empty()){
                outputStack.push(inputStack.pop());
            }
            if (!outputStack.empty()){
                return outputStack.pop();
            }
            return -1;
        }

        /** Get the front element.
         * 优化 peek/pop 时 先看outputStack 是否有元素 有元素直接用
         *  为空才将inputStack元素移过来
         * */
        public int peek() {
            while(!inputStack.empty()){
                outputStack.push(inputStack.pop());
            }
            if (!outputStack.empty()){
                return outputStack.peek();
            }
            return -1;
        }

        /** Returns whether the queue is empty. */
        public boolean empty() {
            return inputStack.empty() && outputStack.empty();
        }
    }

    class MyQueue2 {
        Stack<Integer> in = null;
        Stack<Integer> out = null;
        public MyQueue2() {
            in = new Stack<>();
            out = new Stack<>();
        }
        public void push(int x) {
            in.push(x);
        }
        public int pop() {
            peek();
            return out.pop();
        }
        public int peek() {//不空的时候，直接查询返回即可
            if(out.isEmpty())//空的时候，再倒换stack
                while(!in.isEmpty())
                    out.push(in.pop());
            return out.peek();
        }
        public boolean empty() {
            return in.isEmpty() && out.isEmpty();
        }
    }
    public static void main(String[] args) {
        MyQueue2 queue = new ImplementQueueUsingStacks().new MyQueue2();
        queue.push(1);
        queue.push(2);
        log.info("{}", queue.peek());
        queue.push(3);
        log.info("{}",queue.pop());
        log.info("{}",queue.pop());
        log.info("{}",queue.pop());

    }
}
