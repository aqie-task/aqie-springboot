func swapPairs(head *ListNode) *ListNode {
    if head == nil || head.Next == nil {
    	return head;
    }
    dummy := ListNode{
    	0,head,
    }
    preNode := &dummy
    preNode.Next = head
    curNode := head
    for{
    	if curNode == nil || curNode.Next == nil {
    		break;
    	}
    	preNode.Next = curNode.Next
    	curNode.Next = preNode.Next.Next
    	preNode.Next.Next = curNode

    	preNode = curNode
    	curNode = curNode.Next
    }
    return dummy.Next

}