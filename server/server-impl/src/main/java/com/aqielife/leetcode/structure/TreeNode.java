package com.aqielife.leetcode.structure;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/28 19:00
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int x) { val = x; }
}

