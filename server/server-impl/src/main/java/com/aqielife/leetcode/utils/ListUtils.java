package com.aqielife.leetcode.utils;

import com.aqielife.leetcode.structure.ListNode;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/26 15:00
 */
public class ListUtils {

    public ListNode createLinkedList(int[] arr){
        int length = arr.length;
        if (length == 0) return null;
        ListNode head = new ListNode(arr[0]);
        ListNode curNode = head;
        for (int i = 1; i < length;i++){
            curNode.next = new ListNode(arr[i]);
            curNode = curNode.next;
        }
        return head;
    }

    public static void printLinkedList(ListNode head){
        ListNode curNode = head;
        while (curNode != null){
            System.out.print(curNode.val + "->");
            curNode = curNode.next;
        }
        System.out.println("null");
    }
}
