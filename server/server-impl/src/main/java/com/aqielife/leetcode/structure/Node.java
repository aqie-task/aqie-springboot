package com.aqielife.leetcode.structure;

import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/3/30 13:59
 */
public class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val,List<Node> _children) {
        val = _val;
        children = _children;
    }
};
