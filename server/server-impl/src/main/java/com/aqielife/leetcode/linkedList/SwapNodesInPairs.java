package com.aqielife.leetcode.linkedList;

import com.aqielife.leetcode.structure.ListNode;
import com.aqielife.leetcode.utils.ListUtils;

/**
 * @Function: 24
 * @Author: aqie
 * @Date: 2019/3/26 15:13
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 * Given 1->2->3, you should return the list as 2->1->3.
 */
public class SwapNodesInPairs {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) return head;
        // 虚拟头结点
        ListNode dummyNode = new ListNode(0);
        ListNode preNode = dummyNode;
        preNode.next = head;
        ListNode curNode = head;

        while (curNode != null && curNode.next !=null) {
           preNode.next = curNode.next;
           curNode.next = preNode.next.next;
           preNode.next.next = curNode;

           preNode = curNode;
           curNode = curNode.next;
        }
        return dummyNode.next;
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6};
        ListNode head = new ListUtils().createLinkedList(arr);
        new ListUtils().printLinkedList(head);
        head = new SwapNodesInPairs().swapPairs(head);
        new ListUtils().printLinkedList(head);
    }
}
