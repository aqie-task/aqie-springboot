package com.aqielife.server.service.impl;

import com.aqielife.BaseTest;
import com.aqielife.server.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author aqie
 * @date 2022/03/14 0:50
 * @desc
 */
@Slf4j
public class GoodsServiceImplTest extends BaseTest {
  @Autowired
  private GoodsService goodsService;
  @Test
  void getStock() {
    Integer stock = goodsService.getStock(1);
    log.info("stock{}", stock);
  }

  @Test
  void deductInventory() {
    int b = goodsService.deductInventory(1, 6);
    log.info("update {}", b);
  }

  @Test
  void buy() {
  }

  @Test
  void batchInsert() {
    goodsService.batchInsert();
  }
}