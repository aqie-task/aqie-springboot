package com.aqielife.design.pattern.behavioral.strategy;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.behavioral.strategy.factory.PromotionStrategyFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

@Slf4j
public class PromotionActivityTest extends BaseTest {
    @Test
    public void test() throws Exception {
        String promotionKey = "DISCOUNT";
        PromotionActivity promotionActivity1111 = new PromotionActivity(PromotionStrategyFactory.getPromotionStrategy(promotionKey));
        promotionActivity1111.executePromotionStrategy();
    }

    // Spring 自带解释器
    @Test
    public void springTest(){
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("100 * 2 + 400 * 1 + 66");
        int result = (Integer) expression.getValue();
        log.info("计算结果： {}", result);
    }
}



