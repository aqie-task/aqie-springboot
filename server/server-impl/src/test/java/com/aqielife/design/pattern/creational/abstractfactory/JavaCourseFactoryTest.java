package com.aqielife.design.pattern.creational.abstractfactory;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class JavaCourseFactoryTest extends BaseTest {
    @Test
    public void test(){
        CourseFactory courseFactory = new JavaCourseFactory();
        Video video = courseFactory.getVideo();
        Article article = courseFactory.getArticle();
        video.produce();
        article.produce();
    }

}