package com.aqielife.design.pattern.creational.builder;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class CourseActualBuilderTest extends BaseTest {

    @Test
    public void test() {
        CourseBuilder courseBuilder = new CourseActualBuilder();
        Coach coach = new Coach();
        coach.setCourseBuilder(courseBuilder);

        Course course = coach.makeCourse("java","ppt",
                "video","hand-written","question");
      log.info("{}", course);
    }
}