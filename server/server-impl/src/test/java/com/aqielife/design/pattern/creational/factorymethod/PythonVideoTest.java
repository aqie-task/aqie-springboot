package com.aqielife.design.pattern.creational.factorymethod;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class PythonVideoTest extends BaseTest {

    @Test
    public void test() {
        VideoFactory videoFactory = new PythonVideoFactory();
        Video video  =  videoFactory.getVideo();
        video.produce();

    }
}