package com.aqielife.design.pattern.creational.simplefactory;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class PythonVideoTest extends BaseTest {

    @Test
    public void test() {
        VideoFactory videoFactory = new VideoFactory();
        PythonVideo pythonVideo = (PythonVideo) videoFactory.getVideo(PythonVideo.class);
        pythonVideo.produce();
    }
}