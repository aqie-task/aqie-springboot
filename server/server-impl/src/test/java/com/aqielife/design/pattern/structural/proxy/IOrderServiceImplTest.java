package com.aqielife.design.pattern.structural.proxy;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.structural.proxy.entity.Order;
import com.aqielife.design.pattern.structural.proxy.service.IOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IOrderServiceImplTest extends BaseTest {

    @Autowired
    private IOrderService iOrderService;

    @Test
    public void test() throws Exception {
        Order order = new Order();
        order.setOrderInfo("order info");
        order.setUserId(2);
        System.out.println(iOrderService.saveOrder(order));
    }
}