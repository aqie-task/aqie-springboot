package com.aqielife.design.pattern.structural.adapter;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class PowerAdapterTest extends BaseTest {

    @Test
    public void test() {
        DC5 dc5 = new PowerAdapter();
        dc5.outputDC5V();
    }
}