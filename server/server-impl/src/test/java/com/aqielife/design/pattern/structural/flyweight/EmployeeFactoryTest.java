package com.aqielife.design.pattern.structural.flyweight;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class EmployeeFactoryTest extends BaseTest {

    private static final String departments[] = {"RD","QA","PM","BD"};

    @Test
    public void test() throws Exception {
        for(int i =0; i < 10; i++){
            String department = departments[(int)(Math.random() * departments.length)];
            Manager manager = (Manager) EmployeeFactory.getManager(department);
            manager.report();
        }
    }

    @Test
    public void testInteger(){
        Integer a = Integer.valueOf(100);
        Integer b = 100;

        Integer c = Integer.valueOf(1000);
        Integer d = 1000;

        System.out.println("a==b:"+(a==b));

        System.out.println("c==d:"+(c==d));
    }
}