package com.aqielife.design.pattern.behavioral.interpreter;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


@Slf4j
public class ExpressionParserTest extends BaseTest {

    @Test
    public void test() throws Exception {
        String string = "6 100 11 + *";
        ExpressionParser expressionParser = new ExpressionParser();
        int result = expressionParser.parse(string);
        log.info(" 解释器计算结果： {}", result);
    }
}