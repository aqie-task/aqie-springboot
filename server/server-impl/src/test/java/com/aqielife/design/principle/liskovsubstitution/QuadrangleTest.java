package com.aqielife.design.principle.liskovsubstitution;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class QuadrangleTest extends BaseTest {

    private static void resize(Rectangle rectangle){
        while (rectangle.getWidth() <= rectangle.getLength()){
            rectangle.setWidth(rectangle.getWidth()+1);
            System.out.println("width:"+rectangle.getWidth() + " length:"+rectangle.getLength());
        }
        System.out.println("resize方法结束 width:"+rectangle.getWidth() + " length:"+rectangle.getLength());
    }

    @Test
    public void testRectangle(){
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(10);
        rectangle.setLength(20);
        resize(rectangle);
    }

    @Test
    public void testSquare(){
        Square square = new Square();
        square.setSideLength(10);
        resize(null);
    }


}