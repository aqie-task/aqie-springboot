package com.aqielife.design.pattern.structural.decorator.v2;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class BattercakeWithEggSausageTest extends BaseTest {


    // 2egg 1 sausage
    @Test
    public void test() throws Exception {
        Senbei battercake = new Battercake();
        log.info("{}", battercake.getDesc());
        log.info("{}", battercake.cost());

        Senbei battercakeWith2Egg = new EggDecorator(battercake);
        battercakeWith2Egg = new EggDecorator(battercakeWith2Egg);
        log.info("{}", battercakeWith2Egg.getDesc());
        log.info("{}", battercakeWith2Egg.cost());

        Senbei battercakeWith2Egg1Sausage = new SausageDecorator(battercakeWith2Egg);
        log.info("{}", battercakeWith2Egg1Sausage.getDesc());
        log.info("{}", battercakeWith2Egg1Sausage.cost());
    }
}