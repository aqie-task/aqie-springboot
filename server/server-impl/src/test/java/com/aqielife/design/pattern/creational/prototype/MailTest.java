package com.aqielife.design.pattern.creational.prototype;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


@Slf4j
public class MailTest extends BaseTest {

    @Test
    public void test() {
        Mail mail = new Mail();
        mail.setContent("initial template " + mail);
        log.info("init mail" + mail);
        for(int i = 0; i < 10; i++){
            try {
                Mail mailTmp = (Mail) mail.clone();
                mailTmp.setName("name " + i);
                mailTmp.setEmailAddress(i + "@qq.com");
                mailTmp.setContent("information message");
                MailUtil.sendMail(mailTmp);
                log.info("clone mailTmp " + mailTmp);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        MailUtil.saveOriginMailRecord(mail);
    }
}