package com.aqielife.design.pattern.behavioral.observer.listener;

import com.aqielife.BaseTest;
import com.google.common.eventbus.EventBus;
import org.junit.jupiter.api.Test;


public class GuavaEventTest extends BaseTest {

    @Test
    public void test() throws Exception {
        EventBus eventBus = new EventBus();
        GuavaEvent guavaEvent = new GuavaEvent();
        eventBus.register(guavaEvent);
        eventBus.post("Post 的内容");
    }
}