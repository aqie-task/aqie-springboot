package com.aqielife.design.pattern.behavioral.visitor;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class CourseTest extends BaseTest {

    @Test
    public void test() throws Exception {
        List<Course> courseList = new ArrayList<>();

        FreeCourse freeCourse = new FreeCourse();
        freeCourse.setName("SpringMVC");

        CodingCourse codingCourse = new CodingCourse();
        codingCourse.setName("SpringBoot");
        codingCourse.setPrice("299");

        courseList.add(freeCourse);
        courseList.add(codingCourse);

        for (Course course : courseList){
            course.accept(new Visitor());
        }
    }
}