package com.aqielife.design.pattern.creational.prototype.clone;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.creational.singleton.HungrySingleton;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

@Slf4j
public class PigTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Date birthday = new Date(0L);
        Pig pig1 = new Pig("Paige", birthday);
        Pig pig2 = (Pig)pig1.clone();

        log.info("{}",pig1);
        log.info("{}",pig2);

        // 默认浅克隆; 两个对象的时间对象引用一致
        pig1.getBirthday().setTime(666666666666L);
        log.info("{}",pig1);
        log.info("{}",pig2);
    }

    @Test
    public void destroySingletonByClone() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        HungrySingleton hungrySingleton = HungrySingleton.getInstance();
        Method method = hungrySingleton.getClass().getDeclaredMethod("clone");
        method.setAccessible(true);
        HungrySingleton cloneHungrySingleton = (HungrySingleton) method.invoke(hungrySingleton);

        log.info("{}",hungrySingleton);
        log.info("{}", cloneHungrySingleton);
    }
}