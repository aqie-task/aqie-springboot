package com.aqielife.design.pattern.behavioral.Intermediary;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class StudyGroupTest extends BaseTest {

    @Test
    public void test() throws Exception {
       User user = new User("Aqie");
       User tom = new User("tom");

       user.sendMessage("Hi Tom");
       tom.sendMessage("Hi Aqie");
    }
}