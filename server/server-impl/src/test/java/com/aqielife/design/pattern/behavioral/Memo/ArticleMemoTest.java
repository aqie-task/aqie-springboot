package com.aqielife.design.pattern.behavioral.Memo;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


@Slf4j
public class ArticleMemoTest extends BaseTest {

    @Test
    public void test() throws Exception {
        ArticleMemoManager articleMemoManager = new ArticleMemoManager();
        Article article = new Article("大话设计模式","content","1.jpg");
        ArticleMemo articleMemo = article.saveToMemo();
        articleMemoManager.addMemo(articleMemo);

        log.info("{}", article);

        // 修改文章
        article.setTitle("java 并发编程");
        article.setContent("content2");
        article.setImgs("2.jpg");

        log.info("{}", article);
        // 保存新文章到Memo
        articleMemo = article.saveToMemo();
        articleMemoManager.addMemo(articleMemo);

        // 再次修改
        article.setTitle("Netty权威指南");
        article.setContent("content3");
        article.setImgs("3.jpg");

        // 没有存档 开始回退
        articleMemo = articleMemoManager.getMemo();
        article.undoFromMemo(articleMemo);

        articleMemo = articleMemoManager.getMemo();
        article.undoFromMemo(articleMemo);

        log.info("{}",article);
    }
}