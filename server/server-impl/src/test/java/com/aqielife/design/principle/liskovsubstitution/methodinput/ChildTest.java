package com.aqielife.design.principle.liskovsubstitution.methodinput;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class ChildTest extends BaseTest {

    @Test
    public void method() {
        Base child = new Child();
        HashMap hashMap = new HashMap();
        ((Child) child).method(hashMap);       // Base method

        Map map = new HashMap();
        (new Child()).method(hashMap);         // Base method
        (new Child()).method(map);             // child method
        ((Child) child).method(map);           // child method
    }


}