package com.aqielife.design.pattern.creational.singleton;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class LazySingletonTest extends BaseTest {
    @Test
    public void test(){
      Thread t1 = new Thread(new T());
      Thread t2 = new Thread(new T());
      t1.start();
      t2.start();
    }

}