package com.aqielife.design.pattern.creational.singleton;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@Slf4j
public class HungrySingletonTest extends BaseTest {

    @Test
    public void test() throws IOException, ClassNotFoundException {
        HungrySingleton instance = HungrySingleton.getInstance();
        // 单例对象序列化到文件再取出
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("singleton_file"));
        oos.writeObject(instance);

        // 文件中读取对象
        File file = new File("singleton_file");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("singleton_file"));

        HungrySingleton newInstance = (HungrySingleton)ois.readObject();
        System.out.println(instance);
        System.out.println(newInstance);
        System.out.println(instance == newInstance);
    }

    @Test
    public void reflectAttack() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class objectClass = HungrySingleton.class;
        Constructor constructor = objectClass.getDeclaredConstructor();
        // 修改权限
        constructor.setAccessible(true);

        HungrySingleton instance = HungrySingleton.getInstance();
        HungrySingleton newInstance = (HungrySingleton) constructor.newInstance();

        System.out.println(instance);
        System.out.println(newInstance);
        System.out.println(instance == newInstance);
    }
}