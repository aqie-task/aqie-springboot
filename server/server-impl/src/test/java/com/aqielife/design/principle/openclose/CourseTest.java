package com.aqielife.design.principle.openclose;

import com.aqielife.BaseTest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@EqualsAndHashCode(callSuper = false)
@Data
@Slf4j
public class CourseTest extends BaseTest {
    @Test
    public void courseList(){
        ICourse course = new JavaDiscountCourse(96,"java design",299d);
        log.info("course: {}",course);
        // 强转才能拿到子类方法
        log.info("discount price {}", ((JavaDiscountCourse) course).getDiscountPrice());
    }

    @Test
    public void test(){
        int i = 10;
        log.info("{} {} {}",--i, --i, i--);
    }
}
