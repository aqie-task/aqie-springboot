package com.aqielife.design.pattern.behavioral.Command;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class StaffTest extends BaseTest {

    @Test
    public void test() throws Exception {
        CourseVideo courseVideo = new CourseVideo("设计模式");
        OpenCourseVideoCommand openCourseVideoCommand = new OpenCourseVideoCommand(courseVideo);
        CloseCourseVideoCommand closeCourseVideoCommand = new CloseCourseVideoCommand(courseVideo);

        Staff staff = new Staff();
        staff.addCommand(openCourseVideoCommand);
        staff.addCommand(closeCourseVideoCommand);

        staff.executeCommands();
    }
}