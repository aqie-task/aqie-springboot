package com.aqielife.design.principle.dependenceinversion;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class CourseTest extends BaseTest {
    @Test
    public void testCourse(){
        Aqie aqie = new Aqie();
        aqie.setiCourse(new JavaCourse());
        aqie.learnCourse();

        aqie.setiCourse(new GoCourse());
        aqie.learnCourse();
    }


}
