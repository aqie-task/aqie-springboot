package com.aqielife.design.pattern.structural.facade;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class GiftExchangeServiceTest extends BaseTest {

    @Test
    public void test() throws Exception {
        PointsGift pointsGift = new PointsGift("iTouch");
        GiftExchangeService giftExchangeService = new GiftExchangeService();
        giftExchangeService.giftExchange(pointsGift);
    }
}