package com.aqielife.design.pattern.creational.singleton;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class EnumInstanceTest extends BaseTest {

    public void test(){}
    // 测试序列化
    @Test
    public void testSerialize() throws IOException, ClassNotFoundException {
        EnumInstance instance = EnumInstance.getInstance();
        instance.setData(new Object());

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("singleton_file"));
        oos.writeObject(instance);

        File file = new File("singleton_file");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("singleton_file"));
        EnumInstance newInstance = (EnumInstance)ois.readObject();
        System.out.println(instance.getData());
        System.out.println(newInstance.getData());
        System.out.println(instance.getData() == newInstance.getData());
    }

    /**
     * Enum 类只有一个有参构造器
     * @throws NoSuchMethodException
     *  Cannot reflectively create enum objects
     */
    @Test
    public void reflectAttack() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class objectClass = EnumInstance.class;
        Constructor constructor = objectClass.getDeclaredConstructor(String.class,int.class);
        // 修改权限
        constructor.setAccessible(true);

        EnumInstance instance = (EnumInstance) constructor.newInstance("Aqie", 666);

    }

}