package com.aqielife.design.pattern.structural.proxy.staticproxy;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.structural.proxy.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class OrderServiceStaticProxyTest extends BaseTest {

    @Autowired
    private OrderServiceStaticProxy orderServiceStaticProxy;
    @Test
    public void test() throws Exception {
        Order order = new Order();
        order.setOrderInfo("order info");
        order.setUserId(1043);

        System.out.print(orderServiceStaticProxy.saveOrder(order));
    }
}