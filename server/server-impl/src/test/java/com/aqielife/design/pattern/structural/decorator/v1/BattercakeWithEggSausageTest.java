package com.aqielife.design.pattern.structural.decorator.v1;

import com.aqielife.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


@Slf4j
public class BattercakeWithEggSausageTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Battercake battercake = new Battercake();
        log.info("{}",battercake.getDesc());
        log.info("{}", battercake.cost());

        battercake = new BattercakeWithEgg();
        log.info("{}",battercake.getDesc());
        log.info("{}", battercake.cost());

        battercake = new BattercakeWithEggSausage();
        log.info("{}",battercake.getDesc());
        log.info("{}", battercake.cost());
    }
}