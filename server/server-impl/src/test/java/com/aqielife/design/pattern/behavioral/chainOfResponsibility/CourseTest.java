package com.aqielife.design.pattern.behavioral.chainOfResponsibility;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class CourseTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Approver articleApprover = new ArticleApprover();
        Approver videoApprover = new VideoApprover();

        Course course = new Course("JavaDesign","article", "video");

        // 设置下一个流程
        articleApprover.setNextApprover(videoApprover);
        articleApprover.deploy(course);
    }
}