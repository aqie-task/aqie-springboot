package com.aqielife.design.pattern.creational.singleton;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class ContainerSingletonTest extends BaseTest {
    @Test
    public void test(){
        Thread t1 = new Thread(new PutObject());
        Thread t2 = new Thread(new PutObject());
        t1.start();
        t2.start();
    }

}