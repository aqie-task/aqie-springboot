package com.aqielife.design.pattern.behavioral.templatemethord;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class JavaCourseTest extends BaseTest {

    @Test
    public void test() throws Exception {
        JavaCourse javaCourse = new JavaCourse(true);
        javaCourse.makeCourse();

        ACourse pythonCourse = new PythonCourse();
        pythonCourse.makeCourse();
    }
}