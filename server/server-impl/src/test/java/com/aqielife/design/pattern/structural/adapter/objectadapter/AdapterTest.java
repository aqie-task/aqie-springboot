package com.aqielife.design.pattern.structural.adapter.objectadapter;

import com.aqielife.BaseTest;

public class AdapterTest extends BaseTest {


    public void test() throws Exception {
       Target target = new ConcreteTarget();
       target.request();

       Target adapterTarget = new Adapter();
       adapterTarget.request();
    }
}