package com.aqielife.design.principle.liskovsubstitution.situation1;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class SoliderTest extends BaseTest {

    // 士兵使用武器进行射击，包括武器的类别和特点的介绍
    @Test
    public void startShoot() {
        Solider solider = new Solider();
        // 依赖倒置
        solider.setGun(new HandGun());
        solider.startShoot();

        solider.setGun(new RifleGun());
        solider.startShoot();
    }


}