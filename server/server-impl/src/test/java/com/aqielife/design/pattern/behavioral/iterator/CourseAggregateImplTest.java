package com.aqielife.design.pattern.behavioral.iterator;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class CourseAggregateImplTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Course course1 = new Course("java course");
        Course course2 = new Course("python course");
        Course course3 = new Course("php course");
        Course course4 = new Course("go course");
        Course course5 = new Course("kotlin course");
        Course course6 = new Course("scala course");
        Course course7 = new Course("javascript course");

        CourseAggregate courseAggregate = new CourseAggregateImpl();

        courseAggregate.addCourse(course1);
        courseAggregate.addCourse(course2);
        courseAggregate.addCourse(course3);
        courseAggregate.addCourse(course4);
        courseAggregate.addCourse(course5);
        courseAggregate.addCourse(course6);
        courseAggregate.addCourse(course7);

        printCourses(courseAggregate);
    }

    private void printCourses(CourseAggregate courseAggregate) {
        CourseIterator courseIterator = courseAggregate.getCourseIterator();
        while (!courseIterator.isLastCourse()){
            Course course = courseIterator.nextCourse();
            System.out.print(course.getName());
        }
    }

}