package com.aqielife.design.pattern.behavioral.observer;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;


public class TeacherTest extends BaseTest {

    @Test
    public void test() throws Exception {
       Course course = new Course("Java DesignModel");
       Teacher teacher = new Teacher("北极小琪");
       course.addObserver(teacher);

       // 业务逻辑
        Question question = new Question();
        question.setUserName("Aqie");
        question.setQuestionContent("This is a Question");
        course.produceQuestion(question,course);
    }
}