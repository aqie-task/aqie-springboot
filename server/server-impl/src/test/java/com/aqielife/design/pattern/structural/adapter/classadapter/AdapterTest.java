package com.aqielife.design.pattern.structural.adapter.classadapter;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class AdapterTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Target target = new ConcreteTarget();
        target.request();

        Target adapterTarget = new Adapter();
        adapterTarget.request();
    }
}