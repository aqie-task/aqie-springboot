package com.aqielife.design.principle.liskovsubstitution.methodoutput;

import com.aqielife.BaseTest;
import org.junit.jupiter.api.Test;

public class ChildTest extends BaseTest {

    @Test
    public void method() {
        Base child = new Child();
        System.out.println(child.method());
    }


}