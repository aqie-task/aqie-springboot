package com.aqielife.design.pattern.behavioral.state;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.behavioral.state.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


@Slf4j
public class CourseVideoStateTest extends BaseTest {

    @Test
    public void test() throws Exception {
        CourseVideoContext courseVideoContext = new CourseVideoContext();
        courseVideoContext.setCourseVideoState(new PlayState());

        log.info("当前状态 {}",courseVideoContext.getCourseVideoState().getClass().getSimpleName());

        courseVideoContext.setCourseVideoState(new PauseState());
        log.info("当前状态 {}",courseVideoContext.getCourseVideoState().getClass().getSimpleName());

        courseVideoContext.setCourseVideoState(new SpeedState());
        log.info("当前状态 {}",courseVideoContext.getCourseVideoState().getClass().getSimpleName());

        courseVideoContext.setCourseVideoState(new StopState());
        log.info("当前状态 {}",courseVideoContext.getCourseVideoState().getClass().getSimpleName());

        // 此行会报错
        courseVideoContext.setCourseVideoState(new SpeedState());
    }
}