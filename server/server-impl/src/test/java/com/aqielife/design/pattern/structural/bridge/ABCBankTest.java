package com.aqielife.design.pattern.structural.bridge;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.structural.bridge.*;
import org.junit.jupiter.api.Test;


public class ABCBankTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Bank icbcBank = new ICBCBank(new DepositAccount());
        Account icbcAccount = icbcBank.openAccount();
        icbcAccount.showAccountType();

        Bank abcBank = new ABCBank(new SavingAccount());
        Account abcAccount = abcBank.openAccount();
        abcAccount.showAccountType();
    }
}