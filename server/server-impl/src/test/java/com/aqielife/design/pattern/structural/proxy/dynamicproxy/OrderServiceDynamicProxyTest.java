package com.aqielife.design.pattern.structural.proxy.dynamicproxy;

import com.aqielife.BaseTest;
import com.aqielife.design.pattern.structural.proxy.entity.Order;
import com.aqielife.design.pattern.structural.proxy.service.IOrderService;
import com.aqielife.design.pattern.structural.proxy.service.IOrderServiceImpl;
import org.junit.jupiter.api.Test;

public class OrderServiceDynamicProxyTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Order order = new Order();
        order.setUserId(6);

        /* 直接代理接口 */
        IOrderService orderServiceDynamicProxy = (IOrderService)new OrderServiceDynamicProxy(new IOrderServiceImpl()).bind();
        int order_id = orderServiceDynamicProxy.saveOrder(order);
        System.out.print(order_id);
    }
}