package com.aqielife.design.pattern.creational.builder.v2;

import com.aqielife.BaseTest;
import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Set;

@Slf4j
public class CourseTest extends BaseTest {
    @Test
    public void test(){
        Course course = new Course.CourseBuilder().buildCourseArticle("article").
                buildCourseName("java").buildCoursePPT("ppt").
                buildCourseQA("qa").buildCourseVideo("video").build();
        log.info("{}", course);
        Set<String> set = ImmutableSet.<String>builder().add("a").add("b").build();
        log.info("{}", set);
    }
}