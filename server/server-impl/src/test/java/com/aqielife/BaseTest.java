package com.aqielife;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ServerApplication.class)
public class BaseTest {

  @Test
  void contextLoads() {
  }

}
