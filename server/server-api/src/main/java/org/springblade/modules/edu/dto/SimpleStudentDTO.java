package org.springblade.modules.edu.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;

import java.io.Serializable;

/**
 * @Auther aqie
 * @Date 2021/8/18 14:08
 */
@Data
public class SimpleStudentDTO implements Serializable {

	@JsonSerialize(using = ToStringSerializer.class)
	private Long studentId;

	private String name;

	private String code;

	@JsonSerialize(using = ToStringSerializer.class)
	private Long classId;

	private String gradeName;

	private String className;

	private Integer section;

	private String faceImage;

	private Integer sex;
}
