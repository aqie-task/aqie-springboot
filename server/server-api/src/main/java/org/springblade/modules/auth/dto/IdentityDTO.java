package org.springblade.modules.auth.dto;


import lombok.Data;

/**
 * @Author aqie
 * @Date 2022/1/27 9:55
 * @desc
 */
@Data
public class IdentityDTO {
	// ("身份 1-学生 2-教师")
	private Integer type;
	private String name;
	private String code;
	private boolean current;
}
