package org.springblade.modules.app.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;
import org.springblade.modules.auth.dto.IdentityDTO;

import java.io.Serializable;
import java.util.List;

/**
 * @Auther aqie
 * @Date 2021/9/30 9:05
 * 当前登录用户基本信息
 */
@Data
public class AppUserDTO implements Serializable {
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;

	/**
	 * 1-学生 2-老师
	 */
	private Integer userType;

	/**
	 * 用户唯一值
	 */
	private String code;

	private String section;

	private String gradeName;

	private String className;

	private String classId;

	private String name;

	private Integer subjectId;

	private String faceImage;

	private String appid;

	private String idCard;

	/**
	 * 关联学生/老师信息
	 */
	List<IdentityDTO> identityList;

}
