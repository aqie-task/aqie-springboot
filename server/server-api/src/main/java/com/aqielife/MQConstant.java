package com.aqielife;

/**
 * @Author aqie
 * @Date 2022/2/11 14:28
 * @desc
 */
public interface MQConstant {
	String QUEUE_TEST = "queue_test";

	String EXCHANGE_HENGSHUI =  "hengshui";

	String EXCHANGE_XINGTAI = "xingtai";
}
