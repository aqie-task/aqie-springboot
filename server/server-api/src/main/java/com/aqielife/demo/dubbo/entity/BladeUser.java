/*
 *      Copyright (c) 2018-2028, aqie Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: aqie 庄骞 (smallchill@163.com)
 */
package com.aqielife.demo.dubbo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户实体
 *
 * @author aqie
 */
@Data
public class BladeUser implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 客户端id
	 */
	private String clientId;

	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 账号
	 */
	private String account;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 租户ID
	 */
	private String tenantId;
	/**
	 * 第三方认证ID
	 */
	private String oauthId;
	/**
	 * 部门id
	 */
	private String deptId;
	/**
	 * 岗位id
	 */
	private String postId;
	/**
	 * 角色id
	 */
	private String roleId;
	/**
	 * 角色名
	 */
	private String roleName;


	/**
	 * 用户类型 1-学生 2-老师
	 */
	private Integer userType;

	/**
	 * 用户唯一编号
	 */
	private String code;

	private String openId;

	private String phone;

	private Integer parentsIdentity;
}
