package com.aqielife.demo.dubbo.common;

import org.springblade.modules.edu.dto.SimpleStudentDTO;

import java.util.List;

/**
 * @Author aqie
 * @Date 2022/3/4 11:10
 * @desc
 */
public interface DubboStudentService {
	List<SimpleStudentDTO> getStudentListByClassId(Long classId);
}
