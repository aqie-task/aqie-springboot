package com.aqielife.demo.dubbo.common;

public interface HelloServiceAPI {

    /*
        传入一个Message，增加Hello的回复
     */
    String sayHello(String message);

}
