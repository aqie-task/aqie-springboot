package com.aqielife.demo.dubbo.common;

import com.aqielife.demo.dubbo.entity.BladeUser;
import org.springblade.modules.app.dto.AppUserDTO;

/**
 * @author aqie
 * @date 2022/03/08 7:17
 * @desc
 */
public interface DubboUserService {
	BladeUser bladeUser(String auth);

	AppUserDTO getUserInfo(String code, Integer userType);
}
