package com.aqielife;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author aqie
 * @date 2022/02/20 20:57
 * @desc
 */
@Configuration(
proxyBeanMethods = false
)
@MapperScan({"com.aqielife.**.mapper"})
@EnableConfigurationProperties({MybatisPlusProperties.class})
public class MybatisPlusConfiguration {
}
