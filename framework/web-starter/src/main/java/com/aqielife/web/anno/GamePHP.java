package com.aqielife.web.anno;

import java.lang.annotation.*;

/**
 * @Author aqie
 * @Date 2022/4/15 10:11
 * @desc @RequestParam 注解改为 @GamePHP 接口即可同时兼容三种 content-type。
 * 1. post json 2. form 表单 3. form data
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GamePHP {
}
