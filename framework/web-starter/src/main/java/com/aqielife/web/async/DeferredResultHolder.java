package com.aqielife.web.async;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author aqie
 * @date 2022/03/07 7:22
 * @desc
 */
@Component
public class DeferredResultHolder {

  private Map<String, DeferredResult<String>> map = new HashMap<String, DeferredResult<String>>();

  public Map<String, DeferredResult<String>> getMap() {
    return map;
  }

  public void setMap(Map<String, DeferredResult<String>> map) {
    this.map = map;
  }

}
