package com.aqielife.web.config;

import com.aqielife.web.constant.WebFilterOrderEnum;
import com.aqielife.web.filter.CacheRequestBodyFilter;
import com.aqielife.web.filter.TimeFilter;
import com.aqielife.web.interceptor.TimeInterceptor;
import com.aqielife.web.resolver.GamePHPMethodProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author aqie
 * @date 2022/03/06 18:27
 * @desc
 */
@Slf4j
@SpringBootConfiguration
public class WebConfig implements WebMvcConfigurer {

  @SuppressWarnings("unused")
  @Autowired
  private TimeInterceptor timeInterceptor;



  @PostConstruct
  public void init() {
    log.info("WebConfig init");
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(timeInterceptor);
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
    resolvers.add(new GamePHPMethodProcessor());
  }

  /**
   * 异步拦截器
   * @param configurer
   */
  @Override
  public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
    /**
     * 1. 线程池
     * 2. 超时时间
     */
  }

  @Bean
  public RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_PLAIN));
    restTemplate.getMessageConverters().add(converter);
    return restTemplate;
  }

  private static <T extends Filter> FilterRegistrationBean<T> createFilterBean(T filter, Integer order) {
    FilterRegistrationBean<T> bean = new FilterRegistrationBean<>(filter);
    bean.setOrder(order);
    List<String> urls = new ArrayList<>();
    urls.add("/*");
    bean.setUrlPatterns(urls);
    return bean;
  }

  @Bean
  public FilterRegistrationBean<TimeFilter> reqResFilter1() {
    return createFilterBean(new TimeFilter(), WebFilterOrderEnum.TIME_FILTER);
  }

  /**
   * 创建 RequestBodyCacheFilter Bean，可重复读取请求内容
   */
  @Bean
  public FilterRegistrationBean<CacheRequestBodyFilter> requestBodyCacheFilter() {
    return createFilterBean(new CacheRequestBodyFilter(), WebFilterOrderEnum.REQUEST_BODY_CACHE_FILTER);
  }


  @Bean(name = "multipartResolver")
  public MultipartResolver multipartResolver(){
    CommonsMultipartResolver resolver = new CommonsMultipartResolver();
    resolver.setDefaultEncoding("UTF-8");
    resolver.setResolveLazily(true);//resolveLazily属性启用是为了推迟文件解析，以在在UploadAction中捕获文件大小异常
    resolver.setMaxInMemorySize(40960);
    resolver.setMaxUploadSize(50*1024*1024);//上传文件大小 50M 50*1024*1024
    return resolver;
  }
}
