package com.aqielife.web.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author aqie
 * @date 2022/03/07 7:12
 * @desc
 */
@Slf4j
@Aspect
@Component
public class TimeAspect {

  @Around("execution(* com.aqielife.*.controller.TestController.*(..))")
  public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {

    // log.info("time aspect start");

    Object[] args = pjp.getArgs();
    for (Object arg : args) {
      // log.info("arg is "+arg);
    }

    long start = new Date().getTime();

    Object object = pjp.proceed();

    // log.info("time aspect 耗时:"+ (new Date().getTime() - start));

    // log.info("time aspect end");

    return object;
  }

}
