package com.aqielife.web.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * @author aqie
 * @date 2022/03/06 18:27
 * @desc
 */
@Slf4j
public class TimeFilter extends OncePerRequestFilter {

  @Override
  protected void initFilterBean() throws ServletException {
    super.initFilterBean();
    // log.info(getFilterName() + " init");
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    long start = new Date().getTime();
    filterChain.doFilter(request, response);
    // log.info("time filter 耗时: {}", (new Date().getTime() - start));
  }



  @Override
  public void destroy() {
    // log.info(getClass().getSimpleName() + " destroy");
  }
}
