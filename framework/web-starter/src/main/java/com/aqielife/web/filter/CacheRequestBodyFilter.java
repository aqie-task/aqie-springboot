package com.aqielife.web.filter;

import cn.hutool.extra.servlet.ServletUtil;
import com.aqielife.web.utils.servlet.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Request Body 缓存 Filter，实现它的可重复读取
 *
 * @author 芋道源码
 */
@Slf4j
public class CacheRequestBodyFilter extends OncePerRequestFilter {

    @Override
    protected void initFilterBean() throws ServletException {
        super.initFilterBean();
        log.info(getFilterName() + " init");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        filterChain.doFilter(new CacheRequestBodyWrapper(request), response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        // 只处理 json 请求内容
        return !ServletUtils.isJsonRequest(request);
    }

    public static class CacheRequestBodyWrapper extends HttpServletRequestWrapper {

        /**
         * 缓存的内容
         */
        private final byte[] body;

        public CacheRequestBodyWrapper(HttpServletRequest request) {
            super(request);
            body = ServletUtil.getBodyBytes(request);
        }

        @Override
        public BufferedReader getReader() throws IOException {
            return new BufferedReader(new InputStreamReader(this.getInputStream()));
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(body);
            // 返回 ServletInputStream
            return new ServletInputStream() {

                @Override
                public int read() {
                    return inputStream.read();
                }

                @Override
                public boolean isFinished() {
                    return false;
                }

                @Override
                public boolean isReady() {
                    return false;
                }

                @Override
                public void setReadListener(ReadListener readListener) {}

                @Override
                public int available() {
                    return body.length;
                }

            };
        }

    }

}
