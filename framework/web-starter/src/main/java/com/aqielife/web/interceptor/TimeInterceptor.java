package com.aqielife.web.interceptor;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author aqie
 * @date 2022/03/07 7:04
 * @desc
 */
@Slf4j
@Component
public class TimeInterceptor implements HandlerInterceptor {

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
  throws Exception {
    // log.info("preHandle interceptor");

    // log.info(((HandlerMethod)handler).getBean().getClass().getName());
    // log.info(((HandlerMethod)handler).getMethod().getName());

    request.setAttribute("startTime", new Date().getTime());
    return true;
  }

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
   */
  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) throws Exception {
    // log.info("postHandle");
    Long start = (Long) request.getAttribute("startTime");
    // log.info("time interceptor 耗时:"+ (new Date().getTime() - start));

  }

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
   */
  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
  throws Exception {
    // log.info("afterCompletion");
    Long start = (Long) request.getAttribute("startTime");
    // log.info("time interceptor 耗时:"+ (new Date().getTime() - start));
    // log.info("ex is "+ex);

  }

}
