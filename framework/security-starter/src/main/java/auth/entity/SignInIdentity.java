package auth.entity;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author aqie
 * @date 2022/02/20 18:26
 * @desc
 */
@Getter
@Setter
public class SignInIdentity implements UserDetails {

  private Long id;

  private String code;

  /**
   * 账号
   */
  private String account;
  /**
   * 密码
   */
  private String password;
  /**
   * 昵称
   */
  private String username;
  /**
   * 真名
   */
  private String realName;

  /**
   * 头像
   */
  private String avatar;
  /**
   * 邮箱
   */
  private String email;
  /**
   * 手机
   */
  private String phone;
  /**
   * 生日
   */
  private Date birthday;

  private String openId;

  private Integer deleted;

  /**
   * 角色id
   */
  private String roleId;

  // 角色集合, 不能为空
  private List<GrantedAuthority> authorities;

  // 获取角色信息
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    if (StrUtil.isNotBlank(this.roleId)) {
      // 获取数据库中的角色信息
      this.authorities = Stream.of(this.roleId.split(",")).map(role -> {
        return new SimpleGrantedAuthority(role);
      }).collect(Collectors.toList());
    } else {
      // 如果角色为空则设置为 ROLE_USER
      this.authorities = AuthorityUtils
      .commaSeparatedStringToAuthorityList("ROLE_USER");
    }
    return this.authorities;
  }

  @Override
  public String getPassword() {
    return this.password;
  }


  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return this.deleted == 0;
  }
}
