package auth.config;



import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.annotation.Resource;

/**
 * @author aqie
 * @date 2022/02/20 19:36
 * @desc 资源服务
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Resource
  private MyAuthenticationEntryPoint authenticationEntryPoint;

  @Resource
  private RedisTokenStore redisTokenStore;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    // 配置放行的资源
    http.authorizeRequests()
    .antMatchers("/test/**").permitAll()
    .antMatchers("/ws/**").permitAll()
    .antMatchers("/word/**").permitAll()
    .antMatchers("/excelTest/**").permitAll()
    .antMatchers( "/actuator/**", "/oauth/token").permitAll()
    .anyRequest()
    .authenticated()
    .and()
    .requestMatchers()
    .antMatchers("/**");
  }

  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    resources.authenticationEntryPoint(authenticationEntryPoint)
            .resourceId("appId")
            .tokenStore(redisTokenStore).stateless(true);
  }

}
