package auth.config;



import cn.hutool.core.util.StrUtil;
import com.aqielife.common.R;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author aqie
 * @date 2022/02/20 19:44
 * @desc 认证失败处理
 */
@Component
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {

  @Resource
  private ObjectMapper objectMapper;

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
                       AuthenticationException authException) throws IOException {
    // 返回 JSON
    response.setContentType("application/json;charset=utf-8");
    // 状态码 401
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    // 写出
    PrintWriter out = response.getWriter();
    String errorMessage = authException.getMessage();
    if (StrUtil.isBlank(errorMessage)) {
      errorMessage = "登录失效!";
    }
    R<String> error = R.fail(1, errorMessage);
    out.write(objectMapper.writeValueAsString(error));
    out.flush();
    out.close();
  }

}
