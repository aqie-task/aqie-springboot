package auth.config;


import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.annotation.Resource;

/**
 * @author aqie
 * @date 2022/02/20 19:37
 * @desc Security 配置类
 */
@EnableConfigurationProperties(ClientOAuth2DataConfiguration.class)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true,jsr250Enabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  // 注入 Redis 连接工厂
  @Resource
  private RedisConnectionFactory redisConnectionFactory;

  // 初始化 RedisTokenStore 用于将 token 存储至 Redis
  @Bean
  public RedisTokenStore redisTokenStore() {
    RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
    redisTokenStore.setPrefix("TOKEN:"); // 设置key的层级前缀，方便查询
    return redisTokenStore;
  }

  // 初始化密码编码器，用 MD5 加密密码
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  // 初始化认证管理对象
  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  // 放行和认证规则
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
    .authorizeRequests()
    // 放行的请求
    .antMatchers("/test/hello").permitAll()
    .antMatchers("/word/**").permitAll()
    .antMatchers( "/oauth/token").permitAll()
    .antMatchers( "/actuator/**").permitAll()
    .and()
    .authorizeRequests()
    // 其他请求必须认证才能访问
    .anyRequest().fullyAuthenticated();
  }
  /**
   * 需要忽略的静态资源
   * @param web
   * @throws Exception
   */
  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/js/**",
    "/images/**",
    "/css/**",
    "/pages/**",
    "/plugins/**",
    "/scss/**",
    "/geodata/**");
  }

  private String buildAppApi(String url) {
    return "/test" + url;
  }
}