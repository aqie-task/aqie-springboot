package auth.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * @author aqie
 * @date 2022/02/20 19:32
 * @desc
 */
@Component
@Validated
@ConfigurationProperties(prefix = "client.oauth2")
@Data
public class ClientOAuth2DataConfiguration {
  // 客户端标识 ID
  private String clientId;

  // 客户端安全码
  private String secret;

  // 授权类型
  private String[] grantTypes;

  // token有效期
  private int tokenValidityTime;

  // refresh-token有效期
  private int refreshTokenValidityTime;

  // 客户端访问范围
  private String[] scopes;

  private String uris;
}
