package auth.controller;

import com.aqielife.common.R;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author aqie
 * @date 2022/02/20 20:48
 * @desc
 */
@RestController
@RequestMapping("oauth")
public class OAuthController {
  @Resource
  private TokenEndpoint tokenEndpoint;


  @PostMapping("token")
  public R<Map<String, Object>> postAccessToken(Principal principal, @RequestParam Map<String, String> parameters)
  throws HttpRequestMethodNotSupportedException {
    OAuth2AccessToken accessToken = tokenEndpoint.postAccessToken(principal, parameters).getBody();
    return custom(accessToken);
  }

  /**
   * 自定义 Token 返回对象
   *
   * @param accessToken
   * @return
   */
  private R<Map<String, Object>> custom(OAuth2AccessToken accessToken) {
    DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) accessToken;
    Map<String, Object> data = new LinkedHashMap(token.getAdditionalInformation());
    data.put("accessToken", token.getValue());
    data.put("expireIn", token.getExpiresIn());
    data.put("scopes", token.getScope());
    if (token.getRefreshToken() != null) {
      data.put("refreshToken", token.getRefreshToken().getValue());
    }
    return R.data(data);
  }
}
