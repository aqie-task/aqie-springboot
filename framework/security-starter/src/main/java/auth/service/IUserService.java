package auth.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author aqie
 * @date 2022/02/20 19:54
 * @desc
 */
public interface IUserService extends UserDetailsService {

}
