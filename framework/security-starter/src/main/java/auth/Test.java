package auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author aqie
 * @date 2022/02/20 21:19
 * @desc
 */
@Slf4j
public class Test {
  public static void main(String[] args) {
    log.info("{}", new BCryptPasswordEncoder().encode("123456"));
  }
}
