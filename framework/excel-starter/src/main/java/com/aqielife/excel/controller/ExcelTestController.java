package com.aqielife.excel.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.aqielife.common.R;
import com.aqielife.excel.dao.AqieRepository;
import com.aqielife.excel.entity.Aqie;
import com.aqielife.excel.utils.PoiExcelUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author aqie
 * @Date 2022/3/19 14:17
 * @desc
 */
@RestController
@RequestMapping("excelTest")
public class ExcelTestController {

	@Autowired
	private AqieRepository repository;

	@GetMapping("test")
	public R test(){
		return R.data("test");
	}

	/**
	 * 导出
	 * @param response
	 * @throws IOException
	 */
	@GetMapping("export")
	public void export(HttpServletResponse response) throws IOException {
		List<Aqie> list = repository.findAll();
		PoiExcelUtils.exportExcel(list,"title","sheet", Aqie.class, "fileName", response);
	}

	/**
	 * http://127.0.0.1/dubbo/excelTest/export2
	 * 模板导出
	 * @param response
	 * @throws IOException
	 */
	@GetMapping("export2")
	public void export2(HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Aqie> list = repository.findAll();
		TemplateExportParams params = new TemplateExportParams("WEB-INF/doc/" + "aqie.xlsx", true);
		map.put("maplist", list);
		Workbook workbook = ExcelExportUtil.exportExcel(params, map);
		PoiExcelUtils.downLoadExcel("aqieData", response, workbook);
	}

	/**
	 * 导入
	 * @param file
	 * @throws IOException
	 */
	@PostMapping("importExcel")
	public void importExcel(MultipartFile file) throws IOException {
		List<Aqie> aqies = PoiExcelUtils.importExcel(file, Aqie.class);
		repository.saveAll(aqies);
	}

}
