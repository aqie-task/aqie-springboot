package com.aqielife.excel.controller;

import com.aqielife.common.R;
import com.aqielife.excel.entity.User;
import com.aqielife.excel.service.ExportServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author aqie
 * @Date 2022/4/22 8:28
 * @desc
 */
@RestController
@RequestMapping("word")
public class WordController {
    @Autowired
    private ExportServiceImpl exportService;

    @GetMapping("test")
    public R test(){
        return R.data("test");
    }

    @GetMapping("/export")
    public void exportWord(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = "b.docx"; //文件名称
            // 设置表格数据
            User user1 = User.builder().name("aqie").age("18").sex("姓名").build();
            User user2 = User.builder().name("bqie").age("20").sex("年龄").build();
            User user3 = User.builder().name("cqie").age("22").sex("性别").build();


            List<User> users = new ArrayList<>();
            users.add(user1);
            users.add(user2);
            users.add(user3);

            Map<String, Object> map = new HashMap<>();
            map.put("list", users);
            map.put("title", "标题123");
            map.put("name", "aqie");
            exportService.exportDocToClient(response, fileName, "b.docx", map);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
