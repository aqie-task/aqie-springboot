package com.aqielife.excel;

import com.aqielife.excel.entity.User;
import com.aqielife.excel.utils.FreemarkerUtil;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author aqie
 * @Date 2022/4/22 8:47
 * @desc
 */
public class Test {
    public static void main(String[] args) throws IOException {
        test();
    }

    private static void test() throws IOException{
        FreemarkerUtil.initConfig("a.ftl");
        User user1 = User.builder().name("aqie").age("18").sex("姓名").build();
        User user2 = User.builder().name("bqie").age("20").sex("年龄").build();
        User user3 = User.builder().name("cqie").age("22").sex("性别").build();


        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);

        Map<String, Object> map = new HashMap<>();
        map.put("list", users);
        map.put("title", "标题123");
        map.put("name", "aqie");
        FreemarkerUtil.createDoc("a2.docx", map);
    }
}
