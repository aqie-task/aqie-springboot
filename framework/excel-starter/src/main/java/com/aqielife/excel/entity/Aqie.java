package com.aqielife.excel.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

import javax.persistence.*;

/**
 * @Author aqie
 * @Date 2022/3/19 14:12
 * @desc
 */
@ExcelTarget("aqie")
@Data
@Table
@Entity
public class Aqie {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Excel(name="姓名")
	private String name;

	@Excel(name="年龄")
	private Integer age;
}
