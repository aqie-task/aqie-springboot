package com.aqielife.excel.dao;


import com.aqielife.excel.entity.Aqie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author aqie
 * @Date 2022/3/19 14:18
 * @desc
 */
@Repository
public interface AqieRepository extends JpaRepository<Aqie, Integer> {
}
