package com.aqielife.excel.utils;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author aqie
 * @date 2020-10-20 15:12
 * @function
 *  <#list courseList as list>
 *  ${list.courseName} ${title}
 */
public class FreemarkerUtil {
    static String ftlPath = "framework/excel-starter/src/main/resources/doc/";

    static Template temp;

    public static void initConfig(String ftlName) throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setDirectoryForTemplateLoading(new File(ftlPath));
        cfg.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_29));
        temp = cfg.getTemplate(ftlName);
    }

    public static void generator(String fileName, Map<String, Object> map) throws IOException, TemplateException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        temp.process(map, bw);
        bw.flush();
        bw.close();
    }

    public static File createDoc(String name, Map<?, ?> dataMap) {
        File f = new File(ftlPath + name);
        try {
            // 这个地方不能使用FileWriter因为需要指定编码类型否则生成的Word文档会因为有无法识别的编码而无法打开
            //Writer w = new OutputStreamWriter(new FileOutputStream(f), StandardCharsets.UTF_8);
            // BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            Writer w = new OutputStreamWriter(new FileOutputStream(f), StandardCharsets.UTF_8);
            // Writer w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), StandardCharsets.UTF_8), 10240);
            temp.process(dataMap, w);
            // bw.flush();
            w.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return f;
    }

    public static File createDoc2(String name, Map<?, ?> dataMap) {
        File f = new File(name);
        try {
            // 这个地方不能使用FileWriter因为需要指定编码类型否则生成的Word文档会因为有无法识别的编码而无法打开
            Writer w = new OutputStreamWriter(new FileOutputStream(f), StandardCharsets.UTF_8);
            // Writer w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), StandardCharsets.UTF_8), 10240);
            BufferedWriter bw = new BufferedWriter(w);
            temp.process(dataMap, bw);
            bw.flush();
            w.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return f;
    }
}
