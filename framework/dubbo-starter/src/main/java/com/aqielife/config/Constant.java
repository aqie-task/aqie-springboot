package com.aqielife.config;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/16 8:56
 */
public class Constant {
    public static String IP = "192.168.0.8";
    public static String ZKServerPort = "2181";
    public static String PATH = "/aqie";
}
