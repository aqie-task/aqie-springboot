package com.aqielife.curator;

import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.zookeeper.WatchedEvent;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/17 8:52
 */
public class MyCuratorWatcher implements CuratorWatcher {

    @Override
    public void process(WatchedEvent event) throws Exception {
        System.out.println("触发Curator watcher，节点路径为：" + event.getPath());
    }

}
