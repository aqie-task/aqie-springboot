package com.aqielife.curator;

import com.aqielife.config.Constant;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.framework.recipes.cache.PathChildrenCache.StartMode;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent.Type;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;

import java.util.List;


/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/16 17:49
 * 相关命令：
 * ls /workspace/super
 */
public class CuratorOperator {
    public CuratorFramework client = null;
    public static final String zkServerPath = Constant.IP + ":" + Constant.ZKServerPort;
    public final static String ADD_PATH = "/super/imooc/d";

    public CuratorOperator() {
        /**
         * 1.同步创建zk示例，原生api是异步的
         *
         * curator链接zookeeper的策略:ExponentialBackoffRetry
         * baseSleepTimeMs：初始sleep的时间
         * maxRetries：最大重试次数
         * maxSleepMs：最大重试时间
         */
		 RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 5);

        /**
         * 2.重试次数
         * 重试间隔
         */
         // RetryPolicy retryPolicy = new RetryNTimes(3, 5000);

        /**
         * curator链接zookeeper的策略:RetryOneTime
         * sleepMsBetweenRetry:每次重试间隔的时间
         */
		// RetryPolicy retryPolicy = new RetryOneTime(3000);
        /**
         * curator链接zookeeper的策略:RetryUntilElapsed
         * maxElapsedTimeMs:最大重试时间
         * sleepMsBetweenRetries:每次重试间隔
         * 重试时间超过maxElapsedTimeMs后，就不再重试
         */
		// RetryPolicy retryPolicy = new RetryUntilElapsed(2000, 3000);

        client = CuratorFrameworkFactory.builder()
                .connectString(zkServerPath)
                .sessionTimeoutMs(10000).retryPolicy(retryPolicy)
                .namespace("workspace").build();
        client.start();
    }

    /**
     *
     * @Description: 关闭zk客户端连接
     */
    public void closeZKClient() {
        if (client != null) {
            this.client.close();
        }
    }

    public static void main(String[] args) throws Exception {
        // 实例化
        CuratorOperator cto = new CuratorOperator();
        boolean isZkCuratorStarted = cto.client.isStarted();
        System.out.println("当前客户的状态：" + (isZkCuratorStarted ? "连接中" : "已关闭"));
        String nodePath = "/super/aqie";

        // 1.创建节点
        // createNode(nodePath, cto);

        // 2.更新节点数据
        // updateNode(cto, nodePath);

        // 3.删除节点
        // deleteNode(cto, nodePath);

        // 4. 读取节点信息
        // getNodeInfo(cto, nodePath);

        // 5.查询子节点(先创建/super/imooc/abc)
        // getChildNode(cto, nodePath);

        // 6.判断节点是否存在,如果不存在则为空
        // isExit(cto, nodePath);

        /**
         * 命令行 ：  set /workspace/super/imooc abc
         * watcher 事件  当使用usingWatcher的时候，监听只会触发一次，监听完毕后就销毁
         */
		// cto.client.getData().usingWatcher(new MyCuratorWatcher()).forPath(nodePath);
		// cto.client.getData().usingWatcher(new MyWatcher()).forPath(nodePath);

        /**
         * 注册一次 监听多次
         * 为节点添加watcher
         * NodeCache: 监听数据节点的变更，会触发事件
         */
        // addWatchtoNode(cto, nodePath);

        /**
         * 只需要监听父节点，子节点的变化都会监听到
         * 为子节点添加watcher
         * PathChildrenCache: 监听数据节点的增删改，会触发事件
         */
        addWatchToChildNode(cto, nodePath);

        Thread.sleep(1000000);

        cto.closeZKClient();
        boolean isZkCuratorStarted2 = cto.client.isStarted();
        System.out.println("当前客户的状态：" + (isZkCuratorStarted2 ? "连接中" : "已关闭"));
    }

    private static void addWatchToChildNode(CuratorOperator cto, String nodePath) throws Exception {
        String childNodePathCache =  nodePath;
        // cacheData: 设置缓存节点的数据状态
        final PathChildrenCache childrenCache = new PathChildrenCache(cto.client, childNodePathCache, true);
        /**
         * StartMode: 初始化方式
         * POST_INITIALIZED_EVENT：异步初始化，初始化之后会触发事件
         * NORMAL：异步初始化
         * BUILD_INITIAL_CACHE：同步初始化
         */
        childrenCache.start(StartMode.POST_INITIALIZED_EVENT);

        List<ChildData> childDataList = childrenCache.getCurrentData();
        System.out.println("当前数据节点的子节点数据列表：");
        for (ChildData cd : childDataList) {
            String childData = new String(cd.getData());
            System.out.println(childData);
        }

        childrenCache.getListenable().addListener(new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
                if(event.getType().equals(Type.INITIALIZED)){
                    System.out.println("子节点初始化ok...");
                }

                else if(event.getType().equals(Type.CHILD_ADDED)){
                    String path = event.getData().getPath();
                    if (path.equals(ADD_PATH)) {
                        System.out.println("添加子节点:" + event.getData().getPath());
                        System.out.println("子节点数据:" + new String(event.getData().getData()));
                    } else if (path.equals("/super/imooc/e")) {
                        System.out.println("添加不正确...");
                    }

                }else if(event.getType().equals(Type.CHILD_REMOVED)){
                    System.out.println("删除子节点:" + event.getData().getPath());
                }else if(event.getType().equals(Type.CHILD_UPDATED)){
                    System.out.println("修改子节点路径:" + event.getData().getPath());
                    System.out.println("修改子节点数据:" + new String(event.getData().getData()));
                }
            }
        });
    }

    private static void addWatchtoNode(CuratorOperator cto, String nodePath) throws Exception {
        // node 节点及 node路径节点当成缓存
        final NodeCache nodeCache = new NodeCache(cto.client, nodePath);
        // buildInitial : 初始化的时候获取node的值并且缓存
        nodeCache.start(true);
        if (nodeCache.getCurrentData() != null) {
            System.out.println("节点初始化数据为：" + new String(nodeCache.getCurrentData().getData()));
        } else {
            System.out.println("节点初始化数据为空...");
        }
        nodeCache.getListenable().addListener(new NodeCacheListener() {
            @Override
            public void nodeChanged() throws Exception {
                if (nodeCache.getCurrentData() == null) {
                    System.out.println("空");
                    return;
                }
                String data = new String(nodeCache.getCurrentData().getData());
                System.out.println("节点路径：" + nodeCache.getCurrentData().getPath() + "数据：" + data);
            }
        });
    }

    private static void isExit(CuratorOperator cto, String nodePath) throws Exception {
        Stat statExist = cto.client.checkExists().forPath(nodePath + "/abc");
        System.out.println(statExist);
    }

    private static void getChildNode(CuratorOperator cto, String nodePath) throws Exception {
        List<String> childNodes = cto.client.getChildren()
                                            .forPath(nodePath);
        System.out.println("开始打印子节点：");
        for (String s : childNodes) {
            System.out.println(s);
        }
    }

    private static void getNodeInfo(CuratorOperator cto, String nodePath) throws Exception {
        Stat stat = new Stat();
        byte[] data = cto.client.getData().storingStatIn(stat).forPath(nodePath);
        System.out.println("节点" + nodePath + "的数据为: " + new String(data));
        System.out.println("该节点的版本号为: " + stat.getVersion());
    }

    // 只会删除最后一个节点
    private static void deleteNode(CuratorOperator cto, String nodePath) throws Exception {
        cto.client.delete()
                  .guaranteed()					// 如果删除失败，那么在后端还是继续会删除，直到成功
                  .deletingChildrenIfNeeded()	// 如果有子节点，就删除
                  .withVersion(1)
                  .forPath(nodePath);
    }

    private static void updateNode(CuratorOperator cto, String nodePath) throws Exception {
        byte[] newData = "batman".getBytes();
        // 乐观锁
        cto.client.setData().withVersion(0).forPath(nodePath, newData);
    }

    private static void createNode(String nodePath,CuratorOperator cto) throws Exception {
        byte[] data = "superme".getBytes();
        // 递归创建
        cto.client.create().creatingParentsIfNeeded()
            .withMode(CreateMode.PERSISTENT)
            .withACL(Ids.OPEN_ACL_UNSAFE)
            .forPath(nodePath, data);
    }
}
