package com.aqielife.curator;


import com.aqielife.config.Constant;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.ACL;

import java.util.ArrayList;
import java.util.List;

import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.ZooDefs.Perms;
import org.apache.zookeeper.data.Stat;

import static cn.hutool.crypto.SecureUtil.md5;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/17 9:56
 */
public class CuratorAcl {
    public CuratorFramework client = null;
    public static final String zkServerPath = Constant.IP + ":" + Constant.ZKServerPort;

    public CuratorAcl() {
        RetryPolicy retryPolicy = new RetryNTimes(3, 5000);
        client = CuratorFrameworkFactory.builder().authorization("digest", "aqie1:123".getBytes())
                .connectString(zkServerPath)
                .sessionTimeoutMs(10000).retryPolicy(retryPolicy)
                .namespace("workspace").build();
        client.start();
    }

    public void closeZKClient() {
        if (client != null) {
            this.client.close();
        }
    }

    public static void main(String[] args) throws Exception {
        // 实例化
        CuratorAcl cto = new CuratorAcl();
        boolean isZkCuratorStarted = cto.client.isStarted();
        System.out.println("当前客户的状态：" + (isZkCuratorStarted ? "连接中" : "已关闭"));

        String nodePath = "/acl/father/child/sub";

        List<ACL> acls = new ArrayList<ACL>();
        Id imooc1 = new Id("digest", md5("aqie1:123"));
        Id imooc2 = new Id("digest", md5("aqie2:123"));
        acls.add(new ACL(Perms.ALL, imooc1));
        acls.add(new ACL(Perms.READ, imooc2));
        acls.add(new ACL(Perms.DELETE | Perms.CREATE, imooc2));

        // 1.创建节点
        createNode(cto, nodePath, acls);

        cto.client.setACL().withACL(acls).forPath("/curatorNode");

        // 2.更新节点数据
        updateNode(cto, nodePath);

        // 删除节点
        deleteNode(cto, nodePath);

        // 读取节点数据
        readNode(cto, nodePath);

        cto.closeZKClient();
        boolean isZkCuratorStarted2 = cto.client.isStarted();
        System.out.println("当前客户的状态：" + (isZkCuratorStarted2 ? "连接中" : "已关闭"));
    }

    private static void readNode(CuratorAcl cto, String nodePath) throws Exception {
        Stat stat = new Stat();
        byte[] data = cto.client.getData().storingStatIn(stat).forPath(nodePath);
        System.out.println("节点" + nodePath + "的数据为: " + new String(data));
        System.out.println("该节点的版本号为: " + stat.getVersion());
    }

    private static void deleteNode(CuratorAcl cto, String nodePath) throws Exception {
        cto.client.delete().guaranteed().deletingChildrenIfNeeded().withVersion(0).forPath(nodePath);
    }

    private static void updateNode(CuratorAcl cto, String nodePath) throws Exception {
        byte[] newData = "batman".getBytes();
        cto.client.setData().withVersion(0).forPath(nodePath, newData);
    }

    private static void createNode(CuratorAcl cto, String nodePath, List<ACL> acls) throws Exception {
        byte[] data = "spiderman".getBytes();
        cto.client.create().creatingParentsIfNeeded()
                .withMode(CreateMode.PERSISTENT)
                .withACL(acls, true)
                .forPath(nodePath, data);
    }
}
