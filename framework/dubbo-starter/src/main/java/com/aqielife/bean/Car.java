package com.aqielife.bean;

import lombok.Data;

/**
 * @author: aqie
 * @create: 2020-08-29 11:51
 **/
@Data
public class Car {
    private String name;
    private String price;

}
