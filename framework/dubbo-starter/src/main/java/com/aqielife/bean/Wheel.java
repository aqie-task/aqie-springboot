package com.aqielife.bean;

import lombok.Data;

/**
 * @author: aqie
 * @create: 2020-08-29 11:48
 **/
@Data
public class Wheel {
    private String name;
    private int size;
    private String price;
}
