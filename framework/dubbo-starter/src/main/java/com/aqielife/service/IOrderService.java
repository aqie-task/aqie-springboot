package com.aqielife.service;



import com.aqielife.bean.UserAddress;

import java.util.List;

public interface IOrderService {
    List<UserAddress> initOrder(String userId);
}
