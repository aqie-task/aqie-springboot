package com.aqielife.service.impl;


import com.aqielife.bean.Car;
import com.aqielife.bean.RaceCar;
import com.aqielife.bean.Wheel;
import com.aqielife.service.CarMaker;
import com.aqielife.service.WheelMaker;
import org.apache.dubbo.common.URL;

/**
 * @author: aqie
 * @create: 2020-08-29 11:52
 **/
public class RaceCarMaker implements CarMaker {
    WheelMaker wheelMaker;

    // 通过 setter 注入 AdaptiveWheelMaker
    public void setWheelMaker(WheelMaker wheelMaker) {
        this.wheelMaker = wheelMaker;
    }

    @Override
    public Car makeCar(URL url) {
        Wheel wheel = wheelMaker.makeWheel(url);
        return new RaceCar(wheel);
    }
}
