package com.aqielife.service;


import com.aqielife.bean.UserAddress;

import java.util.List;

public interface IUserService {
    public List<UserAddress> getUserAddressList(String userId);
}
