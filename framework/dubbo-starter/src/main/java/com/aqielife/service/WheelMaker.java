package com.aqielife.service;

import com.aqielife.bean.Wheel;
import org.apache.dubbo.common.URL;

/**
 * @author: aqie
 * @create: 2020-08-29 11:46
 **/
public interface WheelMaker {
    Wheel makeWheel(URL url);
}