package com.aqielife.service;


import com.aqielife.bean.Car;
import org.apache.dubbo.common.URL;

public interface CarMaker {
    Car makeCar(URL url);
}
