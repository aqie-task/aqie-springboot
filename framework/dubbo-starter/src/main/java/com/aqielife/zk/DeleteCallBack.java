package com.aqielife.zk;

import org.apache.zookeeper.AsyncCallback;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/16 8:15
 */
public class DeleteCallBack implements AsyncCallback.VoidCallback {
    @Override
    public void processResult(int rc, String path, Object ctx) {
        System.out.println("删除节点" + path);
        System.out.println((String)ctx);
    }

}
