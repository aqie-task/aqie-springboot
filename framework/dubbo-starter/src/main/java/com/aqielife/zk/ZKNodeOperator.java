package com.aqielife.zk;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.ZooDefs.Ids;
import java.io.IOException;
import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/15 16:00
 */
@Slf4j
public class ZKNodeOperator implements Watcher {
    private ZooKeeper zookeeper = null;

    public static final String zkServerPath = "127.0.0.1:2181";
    public static final Integer timeout = 5000;

    public ZKNodeOperator() {}

    public ZKNodeOperator(String connectString) {
        try {
            zookeeper = new ZooKeeper(connectString, timeout, new ZKNodeOperator());
        } catch (IOException e) {
            e.printStackTrace();
            if (zookeeper != null) {
                try {
                    zookeeper.close();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void createZKNode(String path, byte[] data, List<ACL> acls) {

        String result = "";
        try {
            /**
             * 同步或者异步创建节点，都不支持子节点的递归创建，异步有一个callback函数
             * 参数：
             * path：创建的路径
             * data：存储的数据的byte[]
             * acl：控制权限策略
             * 			Ids.OPEN_ACL_UNSAFE --> world:anyone:cdrwa
             * 			CREATOR_ALL_ACL --> auth:user:password:cdrwa
             * createMode：节点类型, 是一个枚举
             * 			PERSISTENT：持久节点
             * 			PERSISTENT_SEQUENTIAL：持久顺序节点
             * 			EPHEMERAL：临时节点
             * 			EPHEMERAL_SEQUENTIAL：临时顺序节点
             */
            // 同步创建临时节点
            // result = zookeeper.create(path, data, acls, CreateMode.EPHEMERAL);

            // 异步创建持久节点
			String ctx = "{'create':'success'}";
			zookeeper.create(path, data, acls, CreateMode.PERSISTENT, new CreateCallBack(), ctx);

            System.out.println("创建节点：\t" + result + "\t成功...");
            // 异步需要开启线程
            new Thread().sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ZooKeeper getZookeeper() {
        return zookeeper;
    }
    public void setZookeeper(ZooKeeper zookeeper) {
        this.zookeeper = zookeeper;
    }

    @Override
    public void process(WatchedEvent event) {
        log.warn("接受到watch通知：{}", event);
    }

    public static void main(String[] args) throws KeeperException, InterruptedException {
        ZKNodeOperator zkServer = new ZKNodeOperator(zkServerPath);
        // 1.创建zk节点（节点名， 节点值， 最低权限）
		// zkServer.createZKNode("/testnode2", "testnode2".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE);

		// 2.修改节点  get /testnode 查看dataVersion
        /*Stat status  = zkServer.getZookeeper().setData("/testnode", "modify".getBytes(), 1);
        System.out.println(status.getVersion());*/

        // 3.查询节点

        // 4.删除节点
        // zkServer.getZookeeper().delete("/testnode2", 0);        // 直接删除
        // 先创建 再异步删除
//        zkServer.createZKNode("/test-delete-node", "123".getBytes(), Ids.OPEN_ACL_UNSAFE);
        String ctx = "{'delete':'success'}";
        zkServer.getZookeeper().delete("/test-delete-node", 0, new DeleteCallBack(), ctx);
        Thread.sleep(2000);
    }
}
