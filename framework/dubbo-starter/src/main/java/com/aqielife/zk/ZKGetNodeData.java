package com.aqielife.zk;

import com.aqielife.config.Constant;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Function: zookeeper 获取节点数据的demo演示
 * @Author: aqie
 * @Date: 2019/6/16 8:55
 */
public class ZKGetNodeData implements Watcher {
    private ZooKeeper zookeeper = null;

    public static final String zkServerPath = Constant.IP + ":" + Constant.ZKServerPort;
    public static final Integer timeout = 5000;
    private static Stat stat = new Stat();

    public ZKGetNodeData() {}

    public ZKGetNodeData(String connectString) {
        try {
            zookeeper = new ZooKeeper(connectString, timeout, new ZKGetNodeData());
        } catch (IOException e) {
            e.printStackTrace();
            if (zookeeper != null) {
                try {
                    zookeeper.close();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    public ZooKeeper getZookeeper() {
        return zookeeper;
    }
    public void setZookeeper(ZooKeeper zookeeper) {
        this.zookeeper = zookeeper;
    }

    private static CountDownLatch countDown = new CountDownLatch(1);

    /**
     * 事件监听方法
     * @param event 事件类型
     */
    @Override
    public void process(WatchedEvent event) {
        try {
            if(event.getType() == EventType.NodeDataChanged){
                ZKGetNodeData zkServer = new ZKGetNodeData(zkServerPath);
                byte[] resByte = zkServer.getZookeeper().getData(Constant.PATH, false, stat);
                String result = new String(resByte);
                System.out.println("更改后的值:" + result);
                System.out.println("版本号变化 version：" + stat.getVersion());
                countDown.countDown();
            } else if(event.getType() == EventType.NodeCreated) {

            } else if(event.getType() == EventType.NodeChildrenChanged) {

            } else if(event.getType() == EventType.NodeDeleted) {

            }
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 此时线程挂起 ， 在客户端执行 set /aqie newData
     * 主线程才会结束
     * @param args
     * @throws KeeperException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws KeeperException, InterruptedException {
        ZKGetNodeData zkServer = new ZKGetNodeData(zkServerPath);



        /**
         * 参数：
         * path：节点路径
         * watch：true或者false，注册一个watch事件
         * stat：状态
         */
        byte[] resByte = zkServer.getZookeeper().getData(Constant.PATH, true, stat);
        String result = new String(resByte);
        System.out.println("当前值:" + result);
        // 挂起主线程
        countDown.await();
    }
}
