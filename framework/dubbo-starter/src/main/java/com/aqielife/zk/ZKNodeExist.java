package com.aqielife.zk;

import com.aqielife.config.Constant;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @Function: zookeeper 判断阶段是否存在demo
 * @Author: aqie
 * @Date: 2019/6/16 9:54
 */
public class ZKNodeExist implements Watcher {
    private ZooKeeper zookeeper = null;

    public static final String zkServerPath = Constant.IP + ":" + Constant.ZKServerPort;
    public static final Integer timeout = 5000;

    public ZKNodeExist() {}

    public ZKNodeExist(String connectString) {
        try {
            zookeeper = new ZooKeeper(connectString, timeout, new ZKNodeExist());
        } catch (IOException e) {
            e.printStackTrace();
            if (zookeeper != null) {
                try {
                    zookeeper.close();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    public ZooKeeper getZookeeper() {
        return zookeeper;
    }
    public void setZookeeper(ZooKeeper zookeeper) {
        this.zookeeper = zookeeper;
    }

    private static CountDownLatch countDown = new CountDownLatch(1);


    /**
     * set /aqie aqie123
     * set /aqie/aa aaa  监听的是 /aqie/aa, 所以上面不会触发事件
     * @param event
     */
    @Override
    public void process(WatchedEvent event) {
        if (event.getType() == EventType.NodeCreated) {
            System.out.println("节点创建");
            countDown.countDown();
        } else if (event.getType() == EventType.NodeDataChanged) {
            System.out.println("节点数据改变");
            countDown.countDown();
        } else if (event.getType() == EventType.NodeDeleted) {
            System.out.println("节点删除");
            countDown.countDown();
        }
    }

    public static void main(String[] args) throws KeeperException, InterruptedException {
        ZKNodeExist zkServer = new ZKNodeExist(zkServerPath);

        /**
         * 参数：
         * path：要查询的节点路径
         * watch：watch
         */
        Stat stat = zkServer.getZookeeper().exists("/aqie/aa", true);
        if (stat != null) {
            System.out.println("查询的节点版本为dataVersion：" + stat.getVersion());
        } else {
            System.out.println("该节点不存在...");
        }

        countDown.await();
    }
}
