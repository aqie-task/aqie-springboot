package com.aqielife.zk;

import org.apache.zookeeper.AsyncCallback;


import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/16 9:49
 */
public class ChildrenCallBack implements AsyncCallback.ChildrenCallback {

    @Override
    public void processResult(int rc, String path, Object ctx, List<String> children) {
        for (String s : children) {
            System.out.println(s);
        }
        System.out.println("ChildrenCallback:" + path);
        System.out.println((String)ctx);
    }

}
