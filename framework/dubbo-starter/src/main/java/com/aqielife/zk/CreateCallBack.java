package com.aqielife.zk;

import org.apache.zookeeper.AsyncCallback;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/15 16:40
 */
public class CreateCallBack implements AsyncCallback.StringCallback {

    @Override
    public void processResult(int rc, String path, Object ctx, String name) {
        System.out.println("创建节点: " + path);
        System.out.println((String)ctx);
    }

}
