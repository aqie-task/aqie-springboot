package com.aqielife.zk;

import com.aqielife.config.Constant;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.List;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/16 10:18
 */
@Slf4j
public class ZKNodeAcl implements Watcher {
    private ZooKeeper zookeeper = null;

    public static final String zkServerPath = Constant.IP + ":" + Constant.ZKServerPort;
    public static final Integer timeout = 5000;

    public ZKNodeAcl() {}

    public ZKNodeAcl(String connectString) {
        try {
            zookeeper = new ZooKeeper(connectString, timeout, new ZKNodeAcl());
        } catch (IOException e) {
            e.printStackTrace();
            if (zookeeper != null) {
                try {
                    zookeeper.close();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    public ZooKeeper getZookeeper() {
        return zookeeper;
    }
    public void setZookeeper(ZooKeeper zookeeper) {
        this.zookeeper = zookeeper;
    }

    @Override
    public void process(WatchedEvent event) {
        log.warn("接受到watch通知：{}", event);
    }

    public void createZKNode(String path, byte[] data, List<ACL> acls) {
        String result = "";
        try {
            /**
             * 同步或者异步创建节点，都不支持子节点的递归创建，异步有一个callback函数
             * 参数：
             * path：创建的路径
             * data：存储的数据的byte[]
             * acl：控制权限策略
             * 			Ids.OPEN_ACL_UNSAFE --> world:anyone:cdrwa
             * 			CREATOR_ALL_ACL --> auth:user:password:cdrwa
             * createMode：节点类型, 是一个枚举
             * 			PERSISTENT：持久节点
             * 			PERSISTENT_SEQUENTIAL：持久顺序节点
             * 			EPHEMERAL：临时节点
             * 			EPHEMERAL_SEQUENTIAL：临时顺序节点
             */
            result = zookeeper.create(path, data, acls, CreateMode.PERSISTENT);
            System.out.println("创建节点：\t" + result + "\t成功...");
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param args
     * 1.  get /acl-aqie    : 获取数据
     * 2. getAcl /acl-aqie  : 获取节点权限
     * 3. getAcl /aclaqie/testdigest
     * 4. addauth digest aqie1:123456 : 命令行登录
     * 5. get /aclaqie/testdigest/childtest
     * 6. get /aclaqie/testdigest     : 读取修改后数据
     * 7. 测试ip
     *
     *   get /aclaqie/iptest6
     */
    public static void main(String[] args) throws Exception {
        ZKNodeAcl zkServer = new ZKNodeAcl(zkServerPath);
        // 1. acl 任何人都可以访问
        // zkServer.createZKNode("/acl-aqie", "test".getBytes(), Ids.OPEN_ACL_UNSAFE);


        // 2. 自定义用户认证
        /*zkServer.createZKNode("/aclaqie", "test".getBytes(), Ids.OPEN_ACL_UNSAFE);
        List<ACL> acls = new ArrayList<ACL>();  // 权限列表
		Id aqie1 = new Id("digest", AclUtils.getDigestUserPwd("aqie1:123456"));
		Id aqie2 = new Id("digest", AclUtils.getDigestUserPwd("aqie2:123456"));
		acls.add(new ACL(Perms.ALL, aqie1));
		acls.add(new ACL(Perms.READ, aqie2));
		acls.add(new ACL(Perms.DELETE | Perms.CREATE, aqie2));
		zkServer.createZKNode("/aclaqie/testdigest", "testdigest".getBytes(), acls);*/

		// 3.注册过的用户必须通过addAuthInfo才能操作节点，参考命令行 addauth
        /*zkServer.getZookeeper().addAuthInfo("digest", "aqie1:123456".getBytes());
		zkServer.createZKNode("/aclaqie/testdigest/childtest", "childtest".getBytes(), Ids.CREATOR_ALL_ACL);
		Stat stat = new Stat();
		byte[] data = zkServer.getZookeeper().getData("/aclaqie/testdigest", false, stat);
		System.out.println(new String(data));
		zkServer.getZookeeper().setData("/aclaqie/testdigest", "now".getBytes(), 0);*/

        //  4.ip方式的acl
		/*List<ACL> aclsIP = new ArrayList<ACL>();
		Id ipId1 = new Id("ip", Constant.IP);
		aclsIP.add(new ACL(Perms.ALL, ipId1));
		zkServer.createZKNode("/aclaqie/iptest6", "iptest".getBytes(), aclsIP);*/

        // 5.验证ip是否有权限 (ip 不需要addAuth)
        zkServer.getZookeeper().setData("/aclaqie/iptest6", "now".getBytes(), 0);
        Stat stat = new Stat();
        byte[] data = zkServer.getZookeeper().getData("/aclaqie/iptest6", false, stat);
        System.out.println(new String(data));
        System.out.println(stat.getVersion());
    }
}
