package com.aqielife.zk;

import com.aqielife.config.Constant;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Function:  zookeeper 获取子节点数据的demo演示
 * @Author: aqie
 * @Date: 2019/6/16 9:18
 */
public class ZKGetChildrenList implements Watcher {
    private ZooKeeper zookeeper = null;

    public static final String zkServerPath = Constant.IP + ":" + Constant.ZKServerPort;
    public static final Integer timeout = 5000;

    public ZKGetChildrenList() {}

    public ZKGetChildrenList(String connectString) {
        try {
            zookeeper = new ZooKeeper(connectString, timeout, new ZKGetChildrenList());
        } catch (IOException e) {
            e.printStackTrace();
            if (zookeeper != null) {
                try {
                    zookeeper.close();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    public ZooKeeper getZookeeper() {
        return zookeeper;
    }
    public void setZookeeper(ZooKeeper zookeeper) {
        this.zookeeper = zookeeper;
    }

    private static CountDownLatch countDown = new CountDownLatch(1);

    /**
     * 打印子节点必须监听父节点
     * 客户端命令行 create /aqie/aa aa   |  delete /aqie/aa |  set /aqie/aa aaa
     * @param event 触发子节点改变事件
     */
    @Override
    public void process(WatchedEvent event) {
        System.out.println("接收到事件" + event);
        try {
            if(event.getType()==EventType.NodeChildrenChanged){
                System.out.println("NodeChildrenChanged");
                ZKGetChildrenList zkServer = new ZKGetChildrenList(zkServerPath);
                List<String> strChildList = zkServer.getZookeeper().getChildren(event.getPath(), false);
                for (String s : strChildList) {
                    System.out.println(s);
                }
                countDown.countDown();
            } else if(event.getType() == EventType.NodeCreated) {
                System.out.println("NodeCreated");
            } else if(event.getType() == EventType.NodeDataChanged) {
                System.out.println("NodeDataChanged");
            } else if(event.getType() == EventType.NodeDeleted) {
                System.out.println("NodeDeleted");
            }
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws InterruptedException, KeeperException {
        ZKGetChildrenList zkServer = new ZKGetChildrenList(zkServerPath);
        /**
         * 同步获取 参数：
         * path：父节点路径
         * watch：true或者false，注册一个watch事件
         */
		/*List<String> strChildList = zkServer.getZookeeper().getChildren(Constant.PATH, true);
		for (String s : strChildList) {
			System.out.println(s);
		}*/


        String ctx = "{'callback':'ChildrenCallback'}";
        zkServer.getZookeeper().getChildren(Constant.PATH, true, new ChildrenCall2Back(), ctx);
        countDown.await();
    }

}
