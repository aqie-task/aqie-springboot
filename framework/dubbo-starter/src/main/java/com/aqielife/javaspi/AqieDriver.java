package com.aqielife.javaspi;


import lombok.extern.slf4j.Slf4j;
import com.mysql.cj.jdbc.NonRegisteringDriver;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;


/**
 * @author: aqie
 * @create: 2020-08-29 09:30
 **/
@Slf4j
public class AqieDriver extends NonRegisteringDriver implements Driver {
    static {
        try {
            java.sql.DriverManager.registerDriver(new AqieDriver());
        } catch (SQLException E) {
            throw new RuntimeException("Can't register driver!");
        }
    }

    /**
     * Construct a new driver and register it with DriverManager
     *
     * @throws SQLException if a database error occurs.
     */
    public AqieDriver() throws SQLException {
    }

    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        info.setProperty("user", "root");
        Connection connection =  super.connect(url, info);
        log.info("数据库连接创建完成!"+connection.toString());
        return connection;
    }
}
