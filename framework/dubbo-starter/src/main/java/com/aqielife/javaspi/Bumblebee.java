package com.aqielife.javaspi;

/**
 * @author: aqie
 * @create: 2020-08-28 17:39
 **/
public class Bumblebee implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Bumblebee.");
    }
}
