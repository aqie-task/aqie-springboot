package com.aqielife.javaspi;

import org.apache.dubbo.common.extension.SPI;


/**
 * dubbo 才需要
 */
@SPI
public interface Robot {
    void sayHello();
}
