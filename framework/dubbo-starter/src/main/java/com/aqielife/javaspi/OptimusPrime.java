package com.aqielife.javaspi;

/**
 * @author: aqie
 * @create: 2020-08-28 17:39
 **/
public class OptimusPrime implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Optimus Prime.");
    }
}
