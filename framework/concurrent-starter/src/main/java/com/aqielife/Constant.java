package com.aqielife;

/**
 * @author: aqie
 * @create: 2020-07-13 09:19
 **/
public class Constant {
    public static final String ZNODE = "/aqienetty/";
    public static int PORT = 9000;
    public static int PORT2 = 9001;
    public static String HOST = "127.0.0.1";
    public static String HOST_HOME = "255.255.255.255";
    public static String LOG_DIR = "/var/logs/";
    public static final String ZOOKEEPER_SERVER = "127.0.0.1:2181";

    // message

    public static final String RABBIT_ROUTE_KEY = "tacocloud.routekey";
    public static final String RABBIT_EXCHANGE= "tacocloud.exchange";
    public static final String RABBIT_QUEUE= "tacocloud.queue";
    public static final String KAFKA_TOPIC= "test";

    public static final String INTEGRATION_CHANNEL= "textInChannel";

    // thread
    public static final String THREAD_LOG = "D:/gitee/java/technique/aqieNetty/log/log.txt";
}
