package com.aqielife.concurrency.mvolatile;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util;

@NotThreadSafe
public class VolatileTest  extends Util{
        // 总请求数
        public static int clientTotal = 5000;

        // 并发线程数
        public static int threadTotal = 200;

        // 计数值
        public static volatile int  count = 0;   // 属于工作内存
        public static void main(String[] args) throws InterruptedException {
            doCount();
        }

        public  void add(){
            count++;
            // 1. 取出count值
            // 2. +1
            // 3. 写回主存
        }

        public static void doCount() throws InterruptedException {
            for(int i = 0; i < 10; i++){
                Util util = new VolatileTest();
                util.simulationConCurrency(clientTotal,threadTotal,count);
            }
        }
}

