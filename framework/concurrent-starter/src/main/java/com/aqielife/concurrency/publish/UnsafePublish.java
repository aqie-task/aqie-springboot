package com.aqielife.concurrency.publish;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

// 不安全的发布对象
@Slf4j
@NotThreadSafe      // 无法假设其他线程是修改属性值
public class UnsafePublish {
    @Getter
    private String[] states = {"a", "b", "c"};

    public static void main(String[] args) {
        UnsafePublish unsafePublish = new UnsafePublish();
        log.info("{}", Arrays.toString(unsafePublish.getStates()));

        // 对私有属性数组修改
        unsafePublish.getStates()[0] = "d";
        log.info("{}", Arrays.toString(unsafePublish.getStates()));
    }
}
