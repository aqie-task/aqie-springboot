package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;

// 每次声明新的类(局部变量) 才不会出现并发问题
@Slf4j
@ThreadSafe
public class DateFormat2<T> extends Util2<T> {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        Util2 util = new DateFormat2();
        util.simulationConCurrency(simpleDateFormat);
    }

    @Override
    public void func(T t, int threadNum) throws ParseException {
        SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
        log.info("{},{}", threadNum,date.parse("20171213"));
    }

    @Override
    public void out(T t) {

    }
}
