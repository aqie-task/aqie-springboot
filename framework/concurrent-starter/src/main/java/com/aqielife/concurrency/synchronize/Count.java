package com.aqielife.concurrency.synchronize;

import com.aqielife.concurrency.annocations.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

@Slf4j
@ThreadSafe
public class Count {
    // 总请求数
    public static int clientTotal = 5000;

    // 并发线程数
    public static int threadTotal = 200;

    // 计数值
    public static int count = 0;
    public  void simulationConCurrency(int clientTotal,int threadTotal,int count) throws InterruptedException {
        // 定义线程池
        ExecutorService executorService = Executors.newCachedThreadPool();
        // 定义信号量 指定允许并发数
        final Semaphore semaphore = new Semaphore(threadTotal);

        // 定义计数器
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        // 请求全部放入线程池
        for(int i = 0; i < clientTotal; i++){
            executorService.execute(() -> {
                // 执行一次线程操作
                try {
                    semaphore.acquire();    // 判断当前进程是否允许被执行,达到一定并发量,add被临时阻塞
                    add();
                    semaphore.release();
                } catch (InterruptedException e) {
                    log.error("exception :" , e);
                }
                // 执行闭锁
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        // 关闭线程池
        executorService.shutdown();
        log.info("count:{}", count);            // 主内存
    }

    public  static synchronized void add(){
        count++;
    }

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Count util = new Count();
            util.simulationConCurrency(clientTotal,threadTotal,count);
        }
    }
}
