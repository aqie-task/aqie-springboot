package com.aqielife.concurrency.immutable;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
@NotThreadSafe
public class Immutable {
    private static final String a = "1";
    private static final Integer b = 1;
    private final static Map<Integer, Integer> map = Maps.newHashMap();

    static {
        map.put(1, 2);
        map.put(2, 4);
        map.put(3, 6);
    }

    public static void main(String[] args) {
        map.put(1,3);
        log.info("{}", map.values());
    }
}
