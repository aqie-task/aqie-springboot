package com.aqielife.concurrency.syncContainer;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

@Slf4j
@ThreadSafe
public class CollectionsTest3<T> extends Util2<T> {
    private static Map<Integer, Integer> map = Collections.synchronizedMap(new Hashtable<>());

    public static void main(String[] args) {
        doCount();
    }

    private static void doCount(){
        for(int i = 0; i < 10; i++){
            Util2 util = new CollectionsTest3();
            util.simulationConCurrency(map);
        }
    }
    @Override
    public void func(T t, int threadNum) throws ParseException {
        map.put(threadNum,threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", map.size());
    }
}
