package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;

@Slf4j
@ThreadSafe
public class StringTest3<T> extends Util2<T> {
    public  static StringBuffer stringBuffer = new StringBuffer();
    public static void main(String[] args) throws InterruptedException {
        // test();
        doCount();
    }

    @Override
    public void func(T t, int threadNum) throws ParseException {
        stringBuffer.append("1");
    }

    public void out(T str){
        log.info("length of StringBuilder:{}", stringBuffer.length());
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 100; i++){
            Util2 util = new StringTest3();
            util.simulationConCurrency(stringBuffer);
        }
    }
    public static void test(){
        if(System.out.append("hello") == null)
            System.out.print("hello");
        else
            System.out.print("world");
    }
}
