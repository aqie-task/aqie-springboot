package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;


@Slf4j
@ThreadSafe
public class MapTest2<T> extends Util2<T> {
    private static Map<Integer, Integer> map = Maps.newConcurrentMap();

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            Util2 util = new MapTest2();
            util.simulationConCurrency(map);
        }
    }

    @Override
    public void func(T t, int threadNum) {
        map.put(threadNum, threadNum);
    }

    @Override
    public void out(T t) {
        log.info("map size:{}", map.size());
    }
}
