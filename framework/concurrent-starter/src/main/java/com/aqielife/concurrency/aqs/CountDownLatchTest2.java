package com.aqielife.concurrency.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class CountDownLatchTest2 {
    // 给定测试线程数
    private static int threadNumTotal = 20;

    public static void main(String[] args) throws InterruptedException {
        // 定义线程池
        ExecutorService exec = Executors.newCachedThreadPool();
        // 闭锁实例
        final CountDownLatch countDownLatch = new CountDownLatch(threadNumTotal);

        for (int index = 0; index < threadNumTotal; index++) {
            final int threadNum = index;
            exec.execute(() -> {
                try {
                    func(threadNum, countDownLatch);
                } catch (Exception e) {
                    log.error("exception", e);
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await(10, TimeUnit.MICROSECONDS); // 只等待指定的时间，可以提前结束
        log.info("finish");
        exec.shutdown();        // 让已有的线程执行完, 再关闭线程池
    }

    public static void func(int threadNum, CountDownLatch countDownLatch) throws Exception {
        log.info("Thread:{}, count:{}", threadNum, countDownLatch.getCount());
        Thread.sleep(100);
    }
}
