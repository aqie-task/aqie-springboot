package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.Recommend;
import com.aqielife.concurrency.annocations.ThreadSafe;

/**
 * 枚举模式: 最安全
 */
@ThreadSafe
@Recommend
public class Singleton7 {
    private Singleton7() {
    }

    public static Singleton7 getInstance() {
        return Singleton.INSTANCE.getInstance();
    }

    private enum Singleton {

        INSTANCE;

        // 定义私有类的实例
        private Singleton7 singleton;

        //JVM会保证此方法绝对只调用一次
        Singleton() {
            singleton = new Singleton7();
        }

        public Singleton7 getInstance() {
            return singleton;
        }
    }
}
