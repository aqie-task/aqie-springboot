package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.ThreadSafe;

/**
 * 饿汉模式 : 静态块
 * 单例实例在 类装载 时创建
 * 保证线程安全
 * problem : 构造方法中存在过多处理,类加载慢，性能差资源浪费
 */
@ThreadSafe
public class Singleton6 {
    // 1. 私有构造函数
    private Singleton6(){}


    // 2.单例对象
    private static Singleton6 instance = null;

    // 注意静态代码块顺序
    static {
        instance = new Singleton6();
    }
    // 3.静态的工厂方法
    public static Singleton6 getInstance(){
        return instance;
    }

    public static void main(String[] args) {
        System.out.println(getInstance().hashCode());
        System.out.println(getInstance().hashCode());
    }
}
