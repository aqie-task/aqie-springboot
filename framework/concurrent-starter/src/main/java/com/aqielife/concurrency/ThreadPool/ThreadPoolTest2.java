package com.aqielife.concurrency.ThreadPool;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ThreadPoolTest2 {
    public static void main(String[] args) {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);
        // 指定运行时间
        scheduledThreadPool.schedule(new Runnable() {
            @Override
            public void run() {
                log.warn("Schedule run");
            }
        }, 3, TimeUnit.SECONDS);

        // 定时执行任务(延迟三秒 间隔四秒)
        scheduledThreadPool.scheduleAtFixedRate(new Runnable() {
            public void run() {
                log.info("delay 4 seconds");
            }
        }, 3,4, TimeUnit.SECONDS);

        //scheduledThreadPool.shutdown();       关闭上面程序会不执行

        // Timer(定时器)  每次间隔五秒执行
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                log.warn("Timer Schedule run");
            }
        }, new Date(),5 * 1000);
    }
}
