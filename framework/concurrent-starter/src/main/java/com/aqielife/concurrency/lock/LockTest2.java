package com.aqielife.concurrency.lock;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@ThreadSafe
public class LockTest2<T> extends Util2<T> {
    // 定义接口实例
    private final static Lock lock = new ReentrantLock();
    public static int count = 0;

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util2 util = new LockTest2();
            util.simulationConCurrency(count);
        }
    }

    @Override
    public void func(T t, int threadNum) throws ParseException {
        lock.lock();
        try {
            count++;
        }finally {
            lock.unlock();
        }
    }

    @Override
    public void out(T t) {
        log.info("count:{}", count);
    }
}
