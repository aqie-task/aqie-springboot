package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.ThreadSafe;

/**
 * 饿汉模式
 * 单例实例在 类装载 时创建
 * 保证线程安全
 * problem : 构造方法中存在过多处理,类加载慢，性能差资源浪费
 */
@ThreadSafe
public class Singleton5 {
    // 1. 私有构造函数
    private Singleton5(){}

    // 2.单例对象
    private static Singleton5 instance = new Singleton5();

    // 3.静态的工厂方法
    public static Singleton5 getInstance(){
        return instance;
    }
}
