package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@NotThreadSafe
public class ListTest<T> extends Util2<T> {
    private static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util2 util = new ListTest();
            util.simulationConCurrency(list);
        }
    }

    @Override
    public void func(T t, int threadNum) {
        list.add(threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", list.size());
    }
}
