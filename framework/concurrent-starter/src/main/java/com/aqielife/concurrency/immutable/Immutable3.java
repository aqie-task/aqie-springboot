package com.aqielife.concurrency.immutable;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@ThreadSafe
public class Immutable3 {
    private final static ImmutableList list = ImmutableList.of(1,2,3);
    private final static List<Integer> list2 = ImmutableList.of(1,2,3);

    private final static ImmutableSet set = ImmutableSet.copyOf(list);

    private final static ImmutableMap map = ImmutableMap.of("name","aqie");
    private final static ImmutableMap map2 = ImmutableMap.builder().put("name","aqie").build();
    public static void main(String[] args) {
        // list.add(1);     // 不允许修改
        // list2.add(1);       // 同样不允许修改
       //  set.add(1);      // 不允许修改
       // map.put();        // 不允许修改
       // map2.put();       // 不允许修改
    }
}
