package com.aqielife.concurrency.syncContainer;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.Collections;
import java.util.Set;

@Slf4j
@ThreadSafe
public class CollectionsTest2<T> extends Util2<T> {
    private static Set<Integer> set = Collections.synchronizedSet(Sets.newHashSet());

    public static void main(String[] args) {
        doCount();
    }

    private static void doCount(){
        for(int i = 0; i < 10; i++){
            Util2 util = new CollectionsTest2();
            util.simulationConCurrency(set);
        }
    }
    @Override
    public void func(T t, int threadNum) throws ParseException {
        set.add(threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", set.size());
    }
}
