package com.aqielife.concurrency.annocations;

import java.lang.annotation.*;

// 标记线程安全类
@Documented
@Target(ElementType.TYPE)   // 注解作用目标
@Retention(RetentionPolicy.SOURCE)  // 注解存在范围
public @interface ThreadSafe {

    String value() default "";
}
