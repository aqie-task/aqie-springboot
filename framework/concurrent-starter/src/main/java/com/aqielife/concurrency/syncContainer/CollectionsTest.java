package com.aqielife.concurrency.syncContainer;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

@Slf4j
@ThreadSafe
public class CollectionsTest<T> extends Util2<T> {
    // list 变为同步容器类
    private static List<Integer> list = Collections.synchronizedList(Lists.newArrayList());

    public static void main(String[] args) {
        doCount();
    }

    private static void doCount(){
        for(int i = 0; i < 10; i++){
            Util2 util = new CollectionsTest();
            util.simulationConCurrency(list);
        }
    }
    @Override
    public void func(T t, int threadNum) throws ParseException {
        list.add(threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", list.size());
    }
}
