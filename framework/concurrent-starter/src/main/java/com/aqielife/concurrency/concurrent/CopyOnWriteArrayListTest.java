package com.aqielife.concurrency.concurrent;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@ThreadSafe
public class CopyOnWriteArrayListTest<T> extends Util2<T> {
    private static List<Integer> list = new CopyOnWriteArrayList<>();

    public static void main(String[] args){
        doCount();
    }

    public static void doCount() {
        for(int i = 0; i < 100; i++){
            Util2 util = new CopyOnWriteArrayListTest();
            util.simulationConCurrency(list);
        }
    }

    @Override
    public void func(T t, int threadNum) {
        list.add(threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", list.size());
    }
}
