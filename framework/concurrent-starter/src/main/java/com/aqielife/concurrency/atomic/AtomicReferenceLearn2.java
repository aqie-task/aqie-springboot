package com.aqielife.concurrency.atomic;

import com.aqielife.concurrency.annocations.ThreadSafe;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicStampedReference;

// 原子性更新某个类的实例
@Slf4j
@ThreadSafe
public class AtomicReferenceLearn2 {

    // 传入要更新对象
    private static AtomicIntegerFieldUpdater<AtomicReferenceLearn2> updater =
            AtomicIntegerFieldUpdater.newUpdater(AtomicReferenceLearn2.class, "count");

    @Getter
    public volatile int count = 100;

    // 创建实例
    private static AtomicReferenceLearn2 arl2 = new AtomicReferenceLearn2();
    public static void main(String[] args) {
        if(updater.compareAndSet(arl2, 100, 120)){
            log.info("update success {}", arl2.getCount());
        }

        if(updater.compareAndSet(arl2, 100, 120)){
            log.info("update success {}", arl2.getCount());
        }else{
            log.info("update failed {}", arl2.getCount());
        }
    }

    // 变量存在版本
    public void test(){
        Integer num  = 0;
        AtomicStampedReference asr = new AtomicStampedReference(num,0);
    }
}
