package com.aqielife.concurrency.atomic;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicLong;

// 并发测试 (线程池 + 信号量 + 闭锁)
@Slf4j
@ThreadSafe
public class Count3 extends Util{
    // 总请求数
    public static int clientTotal = 5000;

    // 并发线程数
    public static int threadTotal = 200;

    // 计数值
    public static AtomicLong count = new AtomicLong(0);   // 属于工作内存
    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public  void add(){
        count.getAndIncrement();
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util util = new Count3();
            util.simulationConCurrency(clientTotal,threadTotal,count);
        }
    }
}
