package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ThreadSafe
public class StringTest2<T> extends Util<T>{
    // 总请求数
    public static int clientTotal = 500;

    // 并发线程数
    public static int threadTotal = 200;

    public  static StringBuffer stringBuffer = new StringBuffer();
    public static void main(String[] args) throws InterruptedException {
        // test();
        doCount();
    }

    public void add(){
        stringBuffer.append("1");
    }

    public void out(T str){
        log.info("length of StringBuilder:{}", stringBuffer.length());
    }
    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 100; i++){
            Util util = new StringTest2();
            util.simulationConCurrency(clientTotal,threadTotal,stringBuffer);
        }
    }
    public static void test(){
        if(System.out.append("hello") == null)
            System.out.print("hello");
        else
            System.out.print("world");
    }
}
