package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@NotThreadSafe
public class SetTest<T> extends Util2<T> {
    private static Set<Integer> set = new HashSet<>();
    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util2 util = new SetTest();
            util.simulationConCurrency(set);
        }
    }

    @Override
    public void func(T t,int threadNum) {
        Set<Integer> data = (Set<Integer>)t;
        data.add(threadNum);
    }

    @Override
    public void out(T t) {
        Set<Integer> data = (Set<Integer>)t;
        log.info("size:{}", data.size());
    }
}
