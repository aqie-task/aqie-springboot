package com.aqielife.concurrency.syncContainer;

import com.aqielife.concurrency.annocations.NotThreadSafe;

import java.util.Iterator;
import java.util.Vector;

@NotThreadSafe
public class VectorTest3 {
    private static Vector<Integer> vector = new Vector<>();

    public static void main(String[] args) {
        vector.add(1);
        vector.add(2);
        vector.add(3);

        test1(vector);
    }

    // 遍历集合 对指定元素做删除操作
    // java.util.ConcurrentModificationException
    private static void test1(Vector<Integer> v1){
        for(Integer i : v1){
            if(i.equals(3))
                v1.remove(i);
        }
    }

    // java.util.ConcurrentModificationException
    private static void test2(Vector<Integer> v2){  // 迭代器
        Iterator<Integer> iterator = v2.iterator();
        while(iterator.hasNext()){
            Integer i = iterator.next();
            if(i.equals(3))
                v2.remove(i);
        }
    }

    // success
    private static void test3(Vector<Integer> v3){  // for 循环
        for(int i = 0; i < v3.size();i++){
            if(v3.get(i) == 3){
                v3.remove(i);
            }
        }
    }
}
