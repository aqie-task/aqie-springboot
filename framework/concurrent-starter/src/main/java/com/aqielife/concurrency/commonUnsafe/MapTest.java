package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util2;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;


@Slf4j
@NotThreadSafe
public class MapTest<T> extends Util2<T> {
    private static Map<Integer, Integer> map = Maps.newHashMap();

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            Util2 util = new MapTest();
            util.simulationConCurrency(map);
        }
    }

    @Override
    public void func(T t, int threadNum) {
        Map<Integer, Integer> data = (Map<Integer, Integer>)t;
        data.put(threadNum, threadNum);
    }

    @Override
    public void out(T t) {
        Map<Integer, Integer> data = (Map<Integer, Integer>)t;
        log.info("size:{}", data.size());
    }
}
