package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.Recommend;
import com.aqielife.concurrency.annocations.ThreadSafe;

/**
 * 懒汉模式 => 双重同锁单例模式 + volatile
 * 单例实例在第一次使用时创建
 *  1、memory = allocate() 分配对象的内存空间
 *  2、ctorInstance() 初始化对象
 *  3、instance = memory  设置instance指向刚分配的内存
 *
 *  JVM和CPU优化后，指令重排：
 *  1、memory = allocate() 分配对象的内存空间
 *  3、instance = memory  设置instance指向刚分配的内存
 *  2、ctorInstance() 初始化对象
 */
@ThreadSafe  // 因为有指令重排
@Recommend
public class Singleton4 {
    // 1. 私有构造函数
    private Singleton4(){}

    // 2.单例对象 : 限制指令重排
    private volatile static Singleton4 instance = null;

    // 3.静态的工厂方法
    public static Singleton4 getInstance(){
        if(instance == null){                       //双重检测机制
            synchronized (Singleton4.class){        //同步锁
                if(instance == null)                //双重检测机制
                    instance = new Singleton4();
            }

        }
        return instance;
    }
}

