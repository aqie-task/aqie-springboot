package com.aqielife.concurrency.syncContainer;


import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.Vector;

@Slf4j
@ThreadSafe
public class VectorTest<T> extends Util2<T> {
    private static Vector<Integer> list = new Vector<>();
    
    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    private static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util2 util = new VectorTest();
            util.simulationConCurrency(list);
        }
    }

    @Override
    public void func(T t, int threadNum) throws ParseException {
        list.add(threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", list.size());
    }
}
