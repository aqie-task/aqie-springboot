package com.aqielife.concurrency.syncContainer;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;

@Slf4j
@ThreadSafe
public class HashTableTest<T> extends Util2<T>{
    private static Map<Integer, Integer> map = new Hashtable<>();

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    private static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util2 util = new HashTableTest();
            util.simulationConCurrency(map);
        }
    }

    @Override
    public void func(T t, int threadNum) throws ParseException {
        map.put(threadNum,threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", map.size());
    }
}
