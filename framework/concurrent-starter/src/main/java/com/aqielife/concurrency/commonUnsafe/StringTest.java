package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NotThreadSafe
public class StringTest<T> extends Util<T>{
    // 总请求数
    public static int clientTotal = 500;

    // 并发线程数
    public static int threadTotal = 200;

    public  static StringBuilder stringBuilder = new StringBuilder();
    public static void main(String[] args) throws InterruptedException {
        // test();
        doCount();
    }

    public void add(){
        stringBuilder.append("1");
    }

    public void out(T stringBuilder){
        StringBuilder str = (StringBuilder)stringBuilder;
        log.info("length of StringBuilder:{}", str.length());
    }
    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 100; i++){
            Util util = new StringTest();
            util.simulationConCurrency(clientTotal,threadTotal,stringBuilder);
        }
    }
    public static void test(){
        if(System.out.append("hello") == null)
            System.out.print("hello");
        else
            System.out.print("world");
    }
}
