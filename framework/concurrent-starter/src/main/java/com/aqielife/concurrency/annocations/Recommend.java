package com.aqielife.concurrency.annocations;

import java.lang.annotation.*;

// 推荐写法
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Recommend {

    String value() default "";
}
