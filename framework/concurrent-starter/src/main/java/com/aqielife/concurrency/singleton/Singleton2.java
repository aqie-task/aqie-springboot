package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.NotRecommend;
import com.aqielife.concurrency.annocations.ThreadSafe;

/**
 * 懒汉模式
 * 单例实例在第一次使用时创建
 * problem : 添加synchronized 工厂方法在一个时间内只允许一个线程访问,
 *          带来性能开销
 */
@ThreadSafe
@NotRecommend
public class Singleton2 {
    // 1. 私有构造函数
    private Singleton2(){}

    // 2.单例对象
    private static Singleton2 instance = null;

    // 3.静态的工厂方法
    public static synchronized Singleton2 getInstance(){
        if(instance == null){
            instance = new Singleton2();
        }
        return instance;
    }
}
