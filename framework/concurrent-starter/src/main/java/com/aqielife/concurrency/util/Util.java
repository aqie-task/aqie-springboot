package com.aqielife.concurrency.util;


import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

@Slf4j
public abstract class Util<T> {
    public  void simulationConCurrency(int clientTotal,int threadTotal,T count) throws InterruptedException {
        // 定义线程池
        ExecutorService executorService = Executors.newCachedThreadPool();
        // 定义信号量 指定允许并发数
        final Semaphore semaphore = new Semaphore(threadTotal);

        // 定义计数器
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        // 请求全部放入线程池
        for(int i = 0; i < clientTotal; i++){
            executorService.execute(() -> {
                // 执行一次线程操作
                try {
                    semaphore.acquire();    // 判断当前进程是否允许被执行,达到一定并发量,add被临时阻塞
                    add();
                    semaphore.release();
                } catch (InterruptedException e) {
                    log.error("exception :{}" , e);
                }
                // 执行闭锁
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        // 关闭线程池
        executorService.shutdown();
            out(count);     // 主内存
    }

    public  abstract void add();

    public void out(T count){
        log.info("count:{}", count);
    }

}
