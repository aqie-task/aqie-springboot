package com.aqielife.concurrency.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

@Slf4j
public class SemaphoreTest {
    // 给定测试线程数
    private static int threadNumTotal = 20;

    public static void main(String[] args) throws InterruptedException {
        // 定义线程池
        ExecutorService exec = Executors.newCachedThreadPool();

        final Semaphore semaphore = new Semaphore(3);      // 允许并发数
        for (int index = 0; index < threadNumTotal; index++) {
            final int threadNum = index;
            exec.execute(() -> {
                try {
                    semaphore.acquire(3);        // 拿到许可
                    func(threadNum);
                    semaphore.release(3);        // 释放许可
                } catch (Exception e) {
                    log.error("exception", e);
                }
            });
        }

        log.info("finish");
        exec.shutdown();
    }

    public static void func(int threadNum) throws Exception {
        log.info("{}",threadNum);
        Thread.sleep(1000);     // 等待一秒
    }
}
