package com.aqielife.concurrency.publish;

import com.aqielife.concurrency.annocations.NotRecommend;
import com.aqielife.concurrency.annocations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

// 对象逸出
@Slf4j
@NotThreadSafe
@NotRecommend
public class Escape {
    private int thisCanBeEscape = 0;

    private Escape(){
        new InnerClass();
    }

    private class InnerClass{
        private InnerClass(){
            // 调用Escape的变量
            log.info("{}", Escape.this.thisCanBeEscape);
        }
    }

    public static void main(String[] args) {
        new Escape();
    }
}
