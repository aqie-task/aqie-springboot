package com.aqielife.concurrency.lock;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.StampedLock;

/**
 * StampedLock控制锁有三种模式（写，读，乐观读），一个StampedLock状态是由版本和模式两个部分组成，锁获取方法返回一个数字作为票据stamp，
 * 它用相应的锁状态表示并控制访问，数字0表示没有写锁被授权访问。在读锁上分为悲观锁和乐观锁。
 *
 * 所谓的乐观读模式，也就是若读的操作很多，写的操作很少的情况下，你可以乐观地认为，写入与读取同时发生几率很少
 * 因此不悲观地使用完全的读取锁定，程序可以查看读取资料之后，是否遭到写入执行的变更
 * 再采取后续的措施（重新读取变更信息，或者抛出异常） ，这一个小小改进，可大幅度提高程序的吞吐量！！
 */
@Slf4j
@ThreadSafe
public class LockTest4<T> extends Util2<T> {

    private static long count = 0;
    private static StampedLock lock = new StampedLock();

    public static void main(String[] args) {
        doCount();
    }

    @Override
    public void func(T t, int threadNum){
        long stamp = lock.writeLock();
        try {
            count++;
        } finally {
            lock.unlock(stamp);
        }
    }

    @Override
    public void out(T t) {
        log.info("count:{}", count);
    }

    public static void doCount(){
        for(int i = 0; i < 10; i++){
            Util2 util = new LockTest4();
            util.simulationConCurrency(count);
        }
    }
}
