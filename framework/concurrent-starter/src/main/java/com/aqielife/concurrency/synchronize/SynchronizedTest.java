package com.aqielife.concurrency.synchronize;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class SynchronizedTest {

    // 修饰一个代码块，被修饰的代码块称为同步语句块，
    // 其作用的范围是大括号{}括起来的代码，作用的对象是调用这个代码块的对象
    public void test1(int j) {
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                log.info("test1 - {} - {}", j,i);
            }
        }
    }

    // 修饰一个方法，被修饰的方法称为同步方法，
    // 其作用的范围是整个方法，作用的对象是调用这个方法的对象； 
    public synchronized void test2() {
        for (int i = 0; i < 10; i++) {
            log.info("test2 - {}", i);
        }
    }

    public static void main(String[] args) {
        SynchronizedTest st1 = new SynchronizedTest();
        SynchronizedTest st2 = new SynchronizedTest();

        // 声明线程池, 实现一个对象两个进程【同时】调用代码块
        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(() -> {
            st1.test1(1);
        });
        exec.execute(() -> {
            st1.test1(2);
        });

        exec.execute(() -> {
            st2.test2();
        });

        exec.shutdown();
    }
}
