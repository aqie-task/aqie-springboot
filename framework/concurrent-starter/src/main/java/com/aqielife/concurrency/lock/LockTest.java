package com.aqielife.concurrency.lock;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ThreadSafe
public class LockTest<T> extends Util2<T> {
    public static int count = 0;
    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() {
        for(int i = 0; i < 10; i++){
            Util2 util = new LockTest();
            util.simulationConCurrency(count);
        }
    }

    @Override
    public synchronized void func(T t, int threadNum){
        count++;
    }

    @Override
    public void out(T t) {
        log.info("count:{}", count);
    }
}
