package com.aqielife.concurrency.synchronize;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class SynchronizedTest2 {
    // 修饰一个静态的方法
    // 其作用的范围是整个静态方法，作用的对象是这个类的所有对象
    public static synchronized void test1(int j) {
        for (int i = 0; i < 10; i++) {
            log.info("test1 - {} - {}", i);
        }
    }

    // 修饰一个类，
    // 其作用的范围是synchronized后面括号括起来的部分，作用主的对象是这个类的所有对象。
    public static void test2() {

        synchronized (SynchronizedTest2.class) {
            for (int i = 0; i < 10; i++) {
                log.info("test2 - {}", i);
            }
        }
    }

    public static void main(String[] args) {
        SynchronizedTest2 st1 = new SynchronizedTest2();
        SynchronizedTest2 st2 = new SynchronizedTest2();

        // 声明线程池, 实现一个对象两个进程【同时】调用代码块
        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(() -> {
            st1.test1(1);
        });
        exec.execute(() -> {
            st1.test1(2);
        });

        exec.execute(() -> {
            st2.test2();
        });

        exec.shutdown();
    }
}
