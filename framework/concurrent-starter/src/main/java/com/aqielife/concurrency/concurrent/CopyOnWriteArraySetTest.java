package com.aqielife.concurrency.concurrent;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

// HashSet
@Slf4j
@ThreadSafe
public class CopyOnWriteArraySetTest<T> extends Util2<T> {
    private static Set<Integer> set = new CopyOnWriteArraySet<>();

    public static void main(String[] args){
        doCount();
    }

    public static void doCount() {
        for(int i = 0; i < 100; i++){
            Util2 util = new CopyOnWriteArraySetTest();
            util.simulationConCurrency(set);
        }
    }

    @Override
    public void func(T t, int threadNum) {
        set.add(threadNum);
    }

    @Override
    public void out(T t) {
        log.info("size:{}", set.size());
    }
}
