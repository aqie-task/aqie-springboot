package com.aqielife.concurrency.atomic;


import com.aqielife.concurrency.util.Util;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class AtomicBooleanTest extends Util {
    private static AtomicBoolean isHappened = new AtomicBoolean(false);
    private static int threadTotal = 200;
    private static int clientTotal = 5000;

    // 只会执行一次
    public  void add() {
        if (isHappened.compareAndSet(false, true)) {
            log.info("execute");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util util = new AtomicBooleanTest();
            util.simulationConCurrency(clientTotal,threadTotal,isHappened);
        }
    }
}
