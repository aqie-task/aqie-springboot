package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


@Slf4j
@ThreadSafe
public class DateFormat3<T> extends Util2<T> {
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyyMMdd");

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        Util2 util = new DateFormat3();
        util.simulationConCurrency(dateTimeFormatter);
    }

    @Override
    public void func(T t, int threadNum) {

        log.info("{},{}", threadNum, DateTime.parse("20171213", dateTimeFormatter).toDate());
    }

    @Override
    public void out(T t) {

    }
}
