package com.aqielife.concurrency.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

@Slf4j
public abstract class Util2<T> {
    // 总请求数
    public static int clientTotal = 5000;

    // 并发线程数
    public static int threadTotal = 200;
    public  void simulationConCurrency(T t) {
        ExecutorService exec = Executors.newCachedThreadPool();
        final Semaphore semp = new Semaphore(threadTotal);

        // 定义计数器
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int index = 0; index < clientTotal; index++) {
            final int threadNum = index;
            exec.execute(() -> {
                try {
                    semp.acquire();
                    func(t,threadNum);
                    semp.release();
                } catch (Exception e) {
                    log.error("exception", e);
                }finally {
                    // 执行闭锁
                    countDownLatch.countDown();
                }

            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exec.shutdown();
        out(t);
    }

    public abstract void func(T t,int threadNum) throws ParseException;


    public abstract void out(T t);
}
