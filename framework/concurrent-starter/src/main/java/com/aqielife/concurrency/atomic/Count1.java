package com.aqielife.concurrency.atomic;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util;
import lombok.extern.slf4j.Slf4j;

// 并发测试 (线程池 + 信号量 + 闭锁)
@Slf4j
@NotThreadSafe
public class Count1 extends Util{
    // 总请求数
    public static int clientTotal = 5000;

    // 并发线程数
    public static int threadTotal = 200;

    // 计数值
    public static int count = 0;
    public static void main(String[] args) throws InterruptedException {
        doCount();
    }


    public  void add(){
        count++;
    }

    public static void doCount() throws InterruptedException {
        for(int i = 0; i < 10; i++){
            Util util = new Count1();
            util.simulationConCurrency(clientTotal,threadTotal,count);
        }
    }
}
