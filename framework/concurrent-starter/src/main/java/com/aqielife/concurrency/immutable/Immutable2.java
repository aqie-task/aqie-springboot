package com.aqielife.concurrency.immutable;

import com.aqielife.concurrency.annocations.ThreadSafe;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;

@Slf4j
@ThreadSafe
public class Immutable2 {
    private static final String a = "1";
    private static final Integer b = 1;
    private static Map<Integer, Integer> map = Maps.newHashMap();

    static {
        map.put(1, 2);
        map.put(2, 4);
        map.put(3, 6);
        map = Collections.unmodifiableMap(map);
    }

    public static void main(String[] args) {
        map.put(1,3);                       // 会抛出异常
        log.info("{}", map.values());
    }
}
