package com.aqielife.concurrency.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class ConditionTest {
    public static void main(String[] args) throws Exception {

        ReentrantLock reentrantLock = new ReentrantLock();
        Condition condition = reentrantLock.newCondition();     // 多线程间协同工作的通信类

        new Thread(() -> {
            try {
                reentrantLock.lock();           // 1.线程加入aqs等待队列
                log.info("wait signal"); // 1
                condition.await();              // 2.aqs等待队列移除,锁的释放加入condition等待队列中去
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("get signal"); // 4
            reentrantLock.unlock();           // 6.线程1释放锁整个过程执行完毕
        }).start();

        new Thread(() -> {
            reentrantLock.lock();            // 3.线程2获取锁加入到aqs等待队列中
            log.info("get lock"); // 2
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            condition.signalAll();
            log.info("send signal ~ "); // 4.condition等待队列中有线程1 的节点，被取出来加入到aqs等待队列
            reentrantLock.unlock();     // 5.释放锁，aqs中只剩下线程1,线程1被唤醒
        }).start();
    }
}
