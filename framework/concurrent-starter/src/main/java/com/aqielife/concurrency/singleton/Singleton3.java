package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.NotRecommend;
import com.aqielife.concurrency.annocations.NotThreadSafe;

/**
 * 懒汉模式 => 双重同锁单例模式
 * 单例实例在第一次使用时创建
 *  1、memory = allocate() 分配对象的内存空间
 *  2、ctorInstance() 初始化对象
 *  3、instance = memory  设置instance指向刚分配的内存
 *
 *  JVM和CPU优化后，指令重排：
 *  1、memory = allocate() 分配对象的内存空间
 *  3、instance = memory  设置instance指向刚分配的内存
 *  2、ctorInstance() 初始化对象
 */
@NotThreadSafe  // 因为有指令重排
@NotRecommend
public class Singleton3 {
    // 1. 私有构造函数
    private Singleton3(){}

    // 2.单例对象
    private static Singleton3 instance = null;

    // 3.静态的工厂方法
    public static  Singleton3 getInstance(){
        if(instance == null){                       //双重检测机制    B 发现内存有值返回空对象
            synchronized (Singleton3.class){        //同步锁
                if(instance == null)                //双重检测机制
                    instance = new Singleton3();    // A - 3
            }

        }
        return instance;
    }
}

