package com.aqielife.concurrency.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * 通过它可以实现让一组线程等待至某个状态之后再全部同时执行。叫做回环是因为当所有等待线程都被释放以后，CyclicBarrier可以被重用。
 * 我们暂且把这个状态就叫做barrier，当调用await()方法之后，线程就处于barrier了
 * <p>
 * CyclicBarrier可以用于多线程计算数据，最后合并计算结果的应用场景。
 * 比如我们用一个Excel保存了用户所有银行流水，每个Sheet保存一个帐户近一年的每笔银行流水，
 * 现在需要统计用户的日均银行流水，先用多线程处理每个sheet里的银行流水，
 * 都执行完之后，得到每个sheet的日均银行流水，
 * 最后，再用barrierAction用这些线程的计算结果，计算出整个Excel的日均银行流水
 * <p>
 * CyclicBarrier和CountDownLatch的区别
 * CountDownLatch的计数器只能使用一次。
 * 而CyclicBarrier的计数器可以使用reset() 方法重置。
 * 所以CyclicBarrier能处理更为复杂的业务场景，比如如果计算发生错误，可以重置计数器，并让线程们重新执行一次。
 * CyclicBarrier还提供其他有用的方法，比如getNumberWaiting方法可以获得CyclicBarrier阻塞的线程数量。
 * isBroken方法用来知道阻塞的线程是否被中断。
 */
@Slf4j
public class CyclicBarrierTest {
    private static CyclicBarrier barrier = new CyclicBarrier(2, () -> {     // 指定五个线程同步等待
        // 在线程到达屏障时，优先执行部分
        log.info("Callback is running");
    });

    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {      // 十个线程
            final int threadNum = i;
            Thread.sleep(1000);
            exec.execute(() -> {
                try {
                    race(threadNum);
                } catch (Exception e) {
                    log.error("exception", e);
                }
            });
        }
        exec.shutdown();
    }

    // 多线程一起做事
    public static void race(int threadNum) throws Exception {
        Thread.sleep(1000);
        log.info("{} is ready", threadNum);
        try{
            barrier.await(2000, TimeUnit.MILLISECONDS);
        }catch(BrokenBarrierException | TimeoutException e){
            log.warn("Exception");
        }

        //barrier.await();                            // 线程准备好调用await()进行等待,达到五个执行后面操作
        log.info("{} continue", threadNum);
    }
}
