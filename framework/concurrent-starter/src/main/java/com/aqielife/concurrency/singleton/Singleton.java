package com.aqielife.concurrency.singleton;

import com.aqielife.concurrency.annocations.NotThreadSafe;

/**
 * 懒汉模式
 * 单例实例在第一次使用时创建
 * problem : 两个线程同时进入判断实例为null，会实例化两次
 */
@NotThreadSafe
public class Singleton {
    // 1. 私有构造函数
    private Singleton(){}

    // 2.单例对象
    private static Singleton instance = null;

    // 3.静态的工厂方法
    public static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }
}
