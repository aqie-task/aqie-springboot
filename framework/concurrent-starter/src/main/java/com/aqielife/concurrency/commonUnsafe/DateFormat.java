package com.aqielife.concurrency.commonUnsafe;

import com.aqielife.concurrency.annocations.NotThreadSafe;
import com.aqielife.concurrency.util.Util2;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Slf4j
@NotThreadSafe
public class DateFormat<T> extends Util2<T> {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

    public static void main(String[] args) throws InterruptedException {
        doCount();
    }

    public static void doCount() throws InterruptedException {
        Util2 util = new DateFormat();
        util.simulationConCurrency(simpleDateFormat);
    }

    @Override
    public void func(T t, int threadNum) {
        SimpleDateFormat date = (SimpleDateFormat)t;
        try {
            log.info("{},{}", threadNum,date.parse("20171213"));
        } catch (ParseException e) {
            log.error("parse exception", e);
        }
    }

    @Override
    public void out(T t) {

    }
}
