package com.aqielife.concurrent.threadmanagement.threadfactory;

import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-07-27 17:44
 **/
public class Task implements Runnable {

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
