package com.aqielife.concurrent.threadexecuter.controltask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-07-29 17:14
 * 执行器内控制任务完成
 **/
public class Main {
    public static void main(String[] args) {
        // Create an executor
        ExecutorService executor= Executors.newCachedThreadPool();

        //Create five tasks
        ResultTask resultTasks[]=new ResultTask[5];
        for (int i=0; i<5; i++) {
            ExecutableTask executableTask=new ExecutableTask("Task "+i);
            resultTasks[i]=new ResultTask(executableTask);
            executor.submit(resultTasks[i]);
        }

        // Sleep the thread five seconds
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        // Cancel all the tasks. In the tasks that have finished before this moment, this
        // cancellation has no effects
        for (int i=0; i<resultTasks.length; i++) {
            resultTasks[i].cancel(true);
        }

        // Write the results of those tasks that haven't been cancelled
        for (int i=0; i<resultTasks.length; i++) {
            try {
                if (!resultTasks[i].isCancelled()){
                    System.out.printf("%s\n",resultTasks[i].get());
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        // Finish the executor.
        executor.shutdown();

    }
}
