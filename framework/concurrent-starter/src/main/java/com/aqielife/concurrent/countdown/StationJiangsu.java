package com.aqielife.concurrent.countdown;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

/**
 * @Function:
 * @Author: aqie
 * @Date: 2019/6/16 8:37
 */
@Slf4j
public class StationJiangsu extends DangerCenter {
    public StationJiangsu(CountDownLatch countDown) {
        super(countDown, "江苏");
    }

    @Override
    public void check() {
        log.info("正在检查 {} 。。。", this.getStation());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("检查 [" + this.getStation() + "] 完毕，可以发车~");
    }
}
