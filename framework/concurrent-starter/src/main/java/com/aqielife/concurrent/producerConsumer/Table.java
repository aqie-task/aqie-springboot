package com.aqielife.concurrent.producerConsumer;

/**
 * @Author aqie
 * @Date 2022/4/22 11:17
 * @desc
 */
public class Table {
    private String[] cakes; //选择使用数组来存放：1.需求上我们不需要扩充容量 2. 原生比包装的效率高
    private int takeIndex; // 取蛋糕的索引
    private int putIndex;  // 放蛋糕的索引
    private int count;// 蛋糕的数量

    public Table(int count){
        cakes = new String[count];

    }
    public synchronized void put(String cake) throws InterruptedException{
        if(count >= cakes.length){
            wait();
        }
        cakes[putIndex] = cake;
        System.out.println(cake + " puts by: " +  Thread.currentThread().getName());
        putIndex = (putIndex + 1) % cakes.length;
        count++;
        notifyAll();
    }
    public synchronized String take() throws InterruptedException {
        if(count <=0 ){
            wait();
        }
        String cake = cakes[takeIndex];
        System.out.println(cake + " takes by: " +  Thread.currentThread().getName());
        takeIndex = (takeIndex + 1) % cakes.length;
        count--;
        notifyAll();
        return cake;
    }
}

