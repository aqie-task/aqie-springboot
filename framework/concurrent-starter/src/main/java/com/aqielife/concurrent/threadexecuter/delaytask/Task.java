package com.aqielife.concurrent.threadexecuter.delaytask;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-07-29 16:58
 **/
public class Task implements Callable<String> {

    /**
     * Name of the task
     */
    private final String name;

    /**
     * Constructor of the class
     * @param name Name of the task
     */
    public Task(String name) {
        this.name=name;
    }

    /**
     * Main method of the task. Writes a message to the console with
     * the actual date and returns the 'Hello world' string
     */
    @Override
    public String call() throws Exception {
        System.out.printf("%s: Starting at : %s\n",name,new Date());
        return "Hello, world";
    }

    public static void main(String[] args) {

        // Create a ScheduledThreadPoolExecutor
        ScheduledExecutorService executor= Executors.newScheduledThreadPool(1);

        System.out.printf("Main: Starting at: %s\n",new Date());

        // Send the tasks to the executor with the specified delay
        for (int i=0; i<5; i++) {
            Task task=new Task("Task "+i);
            executor.schedule(task,i+1 , TimeUnit.SECONDS);
        }

        // Finish the executor
        executor.shutdown();

        // Waits for the finalization of the executor
        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Writes the finalization message
        System.out.printf("Core: Ends at: %s\n",new Date());
    }

}

