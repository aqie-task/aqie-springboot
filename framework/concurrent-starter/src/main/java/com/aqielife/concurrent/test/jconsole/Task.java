package com.aqielife.concurrent.test.jconsole;

import java.util.Date;

/**
 * @author: aqie
 * @create: 2020-07-30 09:59
 **/
public class Task implements Runnable {

    @Override
    public void run() {

        Date start, end;

        start = new Date();

        do {
            System.out.printf("%s: tick\n", Thread.currentThread().getName());
            end = new Date();
        } while (end.getTime() - start.getTime() < 100000);
    }

}
