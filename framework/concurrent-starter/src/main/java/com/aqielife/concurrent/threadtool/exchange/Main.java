package com.aqielife.concurrent.threadtool.exchange;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * @author: aqie
 * @create: 2020-07-29 15:50
 * 生产者和消费者如何并发执行和同步交换缓冲区的
 * 第一个调用Exchange的线程将会休眠,知道该方法由另一个线程调用
 **/
public class Main {
    public static void main(String[] args) {

        // Creates two buffers
        List<String> buffer1=new ArrayList<>();
        List<String> buffer2=new ArrayList<>();

        // Creates the exchanger
        Exchanger<List<String>> exchanger=new Exchanger<>();

        // Creates the producer
        Producer producer=new Producer(buffer1, exchanger);
        // Creates the consumer
        Consumer consumer=new Consumer(buffer2, exchanger);

        // Creates and starts the threads
        Thread threadProducer=new Thread(producer);
        Thread threadConsumer=new Thread(consumer);

        threadProducer.start();
        threadConsumer.start();

    }
}
