package com.aqielife.concurrent.threadtool.msemaphore;

/**
 * @author: aqie
 * @create: 2020-07-28 16:35
 **/
public class Main {
    public static void main (String args[]){

        // Creates the print queue
        PrintQueue printQueue=new PrintQueue();

        // Creates twelve Threads
        Thread[] threads=new Thread[12];
        for (int i=0; i < threads.length; i++){
            threads[i]=new Thread(new Job(printQueue),"Thread "+i);
        }

        // Starts the Threads
        for (int i=0; i < threads.length; i++){
            threads[i].start();
        }
    }
}
