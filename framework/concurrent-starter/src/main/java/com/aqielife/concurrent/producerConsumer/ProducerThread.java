package com.aqielife.concurrent.producerConsumer;

import java.util.Random;

/**
 * @Author aqie
 * @Date 2022/4/22 11:17
 * @desc
 */
public class ProducerThread extends Thread{
    private final TableBlock table;
    private String cake;
    private static int id=1;
    private Random random = new Random();
    //创建构造器，对象名和table对象
    public ProducerThread(String name, TableBlock table){
        super(name);
        this.table = table;
    }
    //重写run方法，实现放蛋糕
    @Override
    public void run() {
        while (true){
            String cake = "[" +  getName() +"做了第"+generateId() + "个蛋糕]";
            try {
                table.put(cake);//放入蛋糕
                Thread.sleep(random.nextInt(1000));//随机阻塞
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    //生成蛋糕顺序id值
    private static synchronized int generateId(){
        return id++;
    }

}
