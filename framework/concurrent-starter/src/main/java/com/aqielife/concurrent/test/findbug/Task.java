package com.aqielife.concurrent.test.findbug;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: aqie
 * @create: 2020-07-30 09:46
 **/
public class Task implements Runnable {

    /**
     * Lock used in the task
     */
    private ReentrantLock lock;

    /**
     * Constructor of the class
     * @param lock Lock used in the task
     */
    public Task(ReentrantLock lock) {
        this.lock=lock;
    }

    /**
     * Main method of the task.
     */
    @Override
    public void run() {
        lock.lock();

        try {
            TimeUnit.SECONDS.sleep(1);
            /*
             * There is a problem with this unlock. If the thread is interrupted
             * while it is sleeping, the lock won't be unlocked and it will cause
             * that the threads that are waiting for this block will be blocked and
             * never will get the control of the Lock.
             */
            lock.unlock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        /*
         * Create a Lock
         */
        ReentrantLock lock=new ReentrantLock();

        /*
         * Executes the threads. There is a problem with this
         * block of code. It uses the run() method instead of
         * the start() method.
         */
        for (int i=0; i<10; i++) {
            Task task=new Task(lock);
            Thread thread=new Thread(task);
            thread.run();
        }

    }

}

