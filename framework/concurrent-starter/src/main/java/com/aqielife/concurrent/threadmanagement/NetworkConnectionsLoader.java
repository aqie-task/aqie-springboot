package com.aqielife.concurrent.threadmanagement;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-07-27 16:32
 * Class that simulates an initialization operation. It sleeps during six seconds
 **/
public class NetworkConnectionsLoader implements Runnable {


    /**
     * Main method of the class
     */
    @Override
    public void run() {
        // Writes a message
        System.out.printf("Begining network connections loading: %s\n",new Date());
        // Sleep six seconds
        try {
            TimeUnit.SECONDS.sleep(6);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Writes a message
        System.out.printf("Network connections loading has finished: %s\n",new Date());
    }

    public static void main(String[] args) {

        // Creates and starts a DataSourceLoader runnable object
        DataSourcesLoader dsLoader = new DataSourcesLoader();
        Thread thread1 = new Thread(dsLoader,"DataSourceThread");

        // Creates and starts a NetworkConnectionsLoader runnable object
        NetworkConnectionsLoader ncLoader = new NetworkConnectionsLoader();
        Thread thread2 = new Thread(ncLoader,"NetworkConnectionLoader");

        // Start both threads
        thread1.start();
        thread2.start();

        // Wait for the finalization of the two threads
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Waits a message
        System.out.printf("Main: Configuration has been loaded: %s\n",new Date());
    }
}
