package com.aqielife.concurrent.threadtool.countdownlatch;

/**
 * @author: aqie
 * @create: 2020-07-28 18:52
 * 视频会议在所有人到达结束
 **/
public class Main {
    public static void main(String[] args) {

        // Creates a VideoConference with 10 participants.
        Videoconference conference=new Videoconference(10);
        // Creates a thread to run the VideoConference and start it.
        Thread threadConference=new Thread(conference);
        threadConference.start();

        // Creates ten participants, a thread for each one and starts them
        for (int i=0; i<10; i++){
            Participant p=new Participant(conference, "Participant "+i);
            Thread t=new Thread(p);
            t.start();
        }

    }
}
