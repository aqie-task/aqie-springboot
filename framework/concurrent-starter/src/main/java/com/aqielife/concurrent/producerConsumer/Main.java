package com.aqielife.concurrent.producerConsumer;

/**
 * @Author aqie
 * @Date 2022/4/22 11:18
 * @desc
 */
public class Main {
    public static void main(String[] args) {
        TableBlock tableBlock = new TableBlock(10);
        new ProducerThread("1", tableBlock).start();
        new ProducerThread("2", tableBlock).start();
        new ProducerThread("3", tableBlock).start();
        new ProducerThread("4", tableBlock).start();
        new ProducerThread("5", tableBlock).start();

        new ConsumerThread("6", tableBlock).start();
        new ConsumerThread("7", tableBlock).start();
        new ConsumerThread("8", tableBlock).start();
        new ConsumerThread("9", tableBlock).start();
        new ConsumerThread("10", tableBlock).start();
    }
}
