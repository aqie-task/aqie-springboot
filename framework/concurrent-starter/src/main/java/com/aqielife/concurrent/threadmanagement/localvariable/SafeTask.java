package com.aqielife.concurrent.threadmanagement.localvariable;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-07-27 17:24
 **/
public class SafeTask implements Runnable {

    /**
     * ThreadLocal shared between the Thread objects
     */
    private static ThreadLocal<Date> startDate = new ThreadLocal<Date>() {
        @Override
        protected Date initialValue() {
            return new Date();
        }
    };

    /**
     * Main method of the class
     */
    @Override
    public void run() {
        // Writes the start date
        System.out.printf("Starting Thread: %s : %s\n", Thread.currentThread().getId(), startDate.get());
        try {
            TimeUnit.SECONDS.sleep((int) Math.rint(Math.random() * 10));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Writes the start date
        System.out.printf("Thread Finished: %s : %s\n", Thread.currentThread().getId(), startDate.get());
    }

}
