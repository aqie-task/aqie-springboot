package com.aqielife.concurrent.forkjoin;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static java.util.function.UnaryOperator.identity;

/**
 * @Author aqie
 * @Date 2022/4/22 11:25
 * @desc
 */
@Slf4j
public class PutOrange {
    private static int THREAD_COUNT = 10;
    // 总元素数量
    private static int ITEM_COUNT = 1000;

    public static void main(String[] args) throws InterruptedException {
        correct();
    }

    private static void correct() throws InterruptedException {
        ConcurrentHashMap<String, Long> concurrentHashMap = getData(ITEM_COUNT - 100);
        // 初始元素
        log.info("init size:{}", concurrentHashMap.size());
        ForkJoinPool forkJoinPool = new ForkJoinPool(THREAD_COUNT);
        // 使用线程池并发处理逻辑
        forkJoinPool.execute(
                () ->
                        IntStream.rangeClosed(1, 10)
                                .parallel()
                                .forEach(
                                        i -> {
                                            // 下面的这段复合逻辑需要锁一下这个ConcurrentHashMap
                                            synchronized (concurrentHashMap) {
                                                // 查询还需要补充多少个元素
                                                int gap = (ITEM_COUNT - concurrentHashMap.size());
                                                log.info("gap size:{}", gap);
                                                // 补充元素
                                                concurrentHashMap.putAll(getData(gap));
                                            }
                                        }));

        // 等待所有任务完成
        forkJoinPool.shutdown();
        boolean b = forkJoinPool.awaitTermination(1, TimeUnit.HOURS);
        log.info("finish size:{}", concurrentHashMap.size());
    }

    /**
     * 用来获得一个指定元素数量模拟数据的ConcurrentHashMap
     *
     * @param count
     * @return
     */
    private static ConcurrentHashMap<String, Long> getData(int count) {
        return LongStream.rangeClosed(1, count)
                .boxed()
                .collect(
                        Collectors.toConcurrentMap(
                                i -> UUID.randomUUID().toString(),
                                identity(),
                                (o1, o2) -> o1,
                                ConcurrentHashMap::new));
    }


}
