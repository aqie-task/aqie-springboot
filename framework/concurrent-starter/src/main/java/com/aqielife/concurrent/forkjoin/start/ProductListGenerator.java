package com.aqielife.concurrent.forkjoin.start;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: aqie
 * @create: 2020-07-29 17:34
 **/
public class ProductListGenerator {

    /**
     * This method generates the list of products
     * @param size the size of the product list
     * @return the generated list of products
     */
    public List<Product> generate (int size) {
        List<Product> ret=new ArrayList<Product>();

        for (int i=0; i<size; i++){
            Product product=new Product();
            product.setName("Product "+i);
            product.setPrice(10);
            ret.add(product);
        }

        return ret;
    }

}
