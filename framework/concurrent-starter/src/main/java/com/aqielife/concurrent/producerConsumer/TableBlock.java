package com.aqielife.concurrent.producerConsumer;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @Author aqie
 * @Date 2022/4/22 11:17
 * @desc
 */
public class TableBlock extends ArrayBlockingQueue<String> {
    //创建构造器
    public TableBlock(int count) {
        super(count);
    }
    //创建put方法（放蛋糕）
    public void put(String cake) throws InterruptedException {
        super.put(cake);
        System.out.println(cake);

    }
    //创建get方法（取蛋糕）
    public String take() throws InterruptedException {
        String cake = super.take();
        System.out.println(cake + " 被" +  Thread.currentThread().getName()+"吃了");
        return cake;
    }
}
