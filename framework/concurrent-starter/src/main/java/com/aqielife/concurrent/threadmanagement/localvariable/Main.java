package com.aqielife.concurrent.threadmanagement.localvariable;

import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-07-27 17:29
 **/
public class Main {
    public static void main(String[] args) {
        // Creates the unsafe task
        UnsafeTask task = new UnsafeTask();

        // Throw ten Thread objects
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(task);
            thread.start();
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Creates a task
        SafeTask task2 = new SafeTask();

        // Creates and start three Thread objects for that Task
        for (int i = 0; i < 2 * Runtime.getRuntime().availableProcessors(); i++) {
            Thread thread = new Thread(task2);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            thread.start();
        }
    }
}
