package com.aqielife.concurrent.parallelstream.react;

/**
 * @author: aqie
 * @create: 2020-07-29 19:06
 **/
public class Item {

    private String title;
    private String content;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }
    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }



}