package com.aqielife.concurrent.semaphore;

import java.util.concurrent.Semaphore;

/**
 * @Author aqie
 * @Date 2022/4/22 11:27
 * @desc
 */
public class Service {
    private boolean isFair = true;

    private static int TOTAL_PERMIT = 10;

    private static int CURRENT_PERMIT = 1;

    private static int THREAD_NUMBER = 10;

    private static int SLEEP_TIME = 5000;


    // 最多允许几个线程执行 acquire() release() 之间代码,同一时间允许 10/2
    private Semaphore semaphore = new Semaphore(TOTAL_PERMIT, isFair);
    public void testMethod() {
        try {
            if(semaphore.tryAcquire()){
                // semaphore.acquire(CURRENT_PERMIT);
                semaphore.acquireUninterruptibly(CURRENT_PERMIT);
                System.out.println(Thread.currentThread() + " begin time " + System.currentTimeMillis());
                System.out.println("还有 " + semaphore.getQueueLength() + " 线程等待 " + semaphore.hasQueuedThreads());
                Thread.sleep(SLEEP_TIME);
                System.out.println(Thread.currentThread() + " end time " + System.currentTimeMillis());
                semaphore.release(CURRENT_PERMIT);
            } else {
                System.out.println(Thread.currentThread() + " 未成功获得许可");
            }

        } catch (InterruptedException e) {
            System.out.println("线程 " + Thread.currentThread().getName() + " catch");
            e.printStackTrace();
        }
    }

    static class ThreadA extends Thread {
        private Service service;

        public ThreadA(Service service) {
            super();
            this.service = service;
        }

        @Override
        public void run() {
            service.testMethod();
        }
    }

    static class ThreadB extends Thread {
        private Service service;

        public ThreadB(Service service) {
            super();
            this.service = service;
        }

        @Override
        public void run() {
            service.testMethod();
        }
    }

    public static void main(String[] args) {
        Service service = new Service();
        // executeAB(service);

        testLoop(service);
    }

    private static void testLoop(Service service) {
        ThreadA[] a = new ThreadA[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++) {
            a[i] = new ThreadA(service);
            a[i].start();
        }
    }

    private static void executeAB(Service service) {
        ThreadA threadA = new ThreadA(service);
        threadA.setName("A");
        threadA.start();

        ThreadB threadB = new ThreadB(service);
        threadB.setName("B");
        threadB.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        threadB.interrupt();
        System.out.println("main 中断 A");
    }
}

