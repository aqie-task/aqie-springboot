package com.aqielife.concurrent.collection.variablehandle;

public class Account {

	public double amount;
	
	public double unsafeAmount;
	
	public Account() {
		this.amount=0;
		this.unsafeAmount=0;
	}
}
