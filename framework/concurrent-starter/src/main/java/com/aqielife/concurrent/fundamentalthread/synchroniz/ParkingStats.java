package com.aqielife.concurrent.fundamentalthread.synchroniz;

/**
 * @author: aqie
 * @create: 2020-07-28 08:46
 **/
public class ParkingStats {

    /**
     * This two variables store the number of cars and motorcycles in the
     * parking
     */
    private long numberCars;
    private long numberMotorcycles;
    // 两个线程不能同时修改同一个属性
    private final Object carLock, motoLock;

    private ParkingCash cash;

    public ParkingCash getCash() {
        return cash;
    }

    /**
     * Constructor of the class
     */
    public ParkingStats(ParkingCash cash) {
        numberCars = 0;
        numberMotorcycles = 0;
        this.cash = cash;
        this.carLock = new Object();
        this.motoLock = new Object();
    }

    public void carComeIn() {
        synchronized (carLock){
            numberCars++;
        }

    }

    public void carGoOut() {
        synchronized (carLock) {
            numberCars--;
        }
        cash.vehiclePay();
    }

    public synchronized void motoComeIn() {
        synchronized (motoLock) {
            numberMotorcycles++;
        }
    }

    public synchronized void motoGoOut() {
        synchronized (motoLock) {
            numberMotorcycles--;
        }
        cash.vehiclePay();
    }

    public long getNumberCars() {
        synchronized (carLock) {
            return numberCars;
        }
    }

    public long getNumberMotorcycles() {
        synchronized (motoLock) {
            return numberMotorcycles;
        }
    }

}
