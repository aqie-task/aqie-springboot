package com.aqielife.concurrent.design.lazyinit;

/**
 * @author: aqie
 * @create: 2020-07-30 10:22
 **/
public class Task implements Runnable {

    @Override
    public void run() {

        System.out.printf("%s: Getting the connection...\n",Thread.currentThread().getName());
        DBConnectionOK connection=DBConnectionOK.getConnection();
        System.out.printf("%s: End\n",Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        for (int i=0; i<20; i++){
            Task task=new Task();
            Thread thread=new Thread(task);
            thread.start();
        }

    }

}
