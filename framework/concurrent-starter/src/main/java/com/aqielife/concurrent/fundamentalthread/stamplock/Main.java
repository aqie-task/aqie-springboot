package com.aqielife.concurrent.fundamentalthread.stamplock;

import java.util.concurrent.locks.StampedLock;

/**
 * @author: aqie
 * @create: 2020-07-28 15:55
 * writeLock 写模式获得锁
 * readLock 读模式获得锁
 * tryOptimisticRead()尝试获取锁  validate()是否尅访问变量
 * write 任务获得锁的控制权,其他两个任务optReader reader不能顺利访问受保护数据
 **/
public class Main {
    public static void main(String[] args) {

        Position position=new Position();
        StampedLock lock=new StampedLock();

        Thread threadWriter=new Thread(new Writer(position,lock));
        Thread threadReader=new Thread(new Reader(position, lock));
        Thread threadOptReader=new Thread(new OptimisticReader(position, lock));

        threadWriter.start();
        threadReader.start();
        threadOptReader.start();

        try {
            threadWriter.join();
            threadReader.join();
            threadOptReader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
