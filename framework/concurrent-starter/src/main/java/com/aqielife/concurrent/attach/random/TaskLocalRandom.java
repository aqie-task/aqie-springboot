package com.aqielife.concurrent.attach.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author: aqie
 * @create: 2020-07-30 10:10
 **/
public class TaskLocalRandom implements Runnable {

    /**
     * Main method of the class. Generate 10 random numbers and write them
     * in the console
     */
    @Override
    public void run() {
        String name=Thread.currentThread().getName();
        for (int i=0; i<10; i++){
            System.out.printf("%s: %d\n",name, ThreadLocalRandom.current().nextInt(10));
        }
    }

    public static void main(String[] args) {

        /*
         * Create an array to store the threads
         */
        Thread threads[]=new Thread[3];

        /*
         * Launch three tasks
         */
        for (int i=0; i<threads.length; i++) {
            TaskLocalRandom task=new TaskLocalRandom();
            threads[i]=new Thread(task);
            threads[i].start();
        }

    }

}
