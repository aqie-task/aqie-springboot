package com.aqielife.concurrent.fundamentalthread.synchroniz;

/**
 * @author: aqie
 * @create: 2020-07-28 08:46
 **/
public class Main {
    public static void main(String[] args) {

        ParkingCash cash = new ParkingCash();
        ParkingStats stats = new ParkingStats(cash);


        int numberSensors=2 * Runtime.getRuntime().availableProcessors();
        System.out.printf("Parking Simulator %s\n",numberSensors);
        // 12 * 2 * 3 * 10 * 2
        Thread[] threads =new Thread[numberSensors];
        for (int i = 0; i < numberSensors; i++) {
            Sensor sensor=new Sensor(stats);
            Thread thread=new Thread(sensor);
            thread.start();
            threads[i]=thread;
        }

        for (int i=0; i< numberSensors; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.printf("Number of cars: %d\n", stats.getNumberCars());
        System.out.printf("Number of motorcycles: %d\n", stats.getNumberMotorcycles());
        System.out.printf("total %d", stats.getCash().getCash());
        cash.close();
    }
}
