package com.aqielife.concurrent.forkjoin;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * @Author aqie
 * @Date 2022/4/22 11:26
 * @desc
 */
public class CountKey {
    //循环次数
    private static int LOOP_COUNT = 10000000;
    //线程数量
    private static int THREAD_COUNT = 10;
    //元素数量
    private static int ITEM_COUNT = 1000;

    public static void main(String[] args) {}

    /**
     * 使用 ConcurrentHashMap 来统计，Key 的范围是 10。
     * 使用最多 10 个并发，循环操作 1000 万次，每次操作累加随机的 Key。
     * 如果 Key不存在的话，首次设置值为 1。
     *
     * @return
     * @throws InterruptedException
     */
    private Map<String, Long> normalise() throws InterruptedException {
        ConcurrentHashMap<String, Long> freqs = new ConcurrentHashMap<>(ITEM_COUNT);
        ForkJoinPool forkJoinPool = new ForkJoinPool(THREAD_COUNT);
        forkJoinPool.execute(() -> IntStream.rangeClosed(1, LOOP_COUNT).parallel());

        forkJoinPool.shutdown();
        forkJoinPool.awaitTermination(1, TimeUnit.HOURS);
        return freqs;
    }
}
