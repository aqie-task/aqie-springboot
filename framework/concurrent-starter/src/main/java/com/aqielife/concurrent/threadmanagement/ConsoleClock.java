package com.aqielife.concurrent.threadmanagement;

import lombok.SneakyThrows;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * todo
 * @author: aqie
 * @create: 2020-07-27 16:20
 * Class that writes the actual date to a file every second
 **/
public class ConsoleClock implements Runnable {

    /**
     * Main method of the class
     */
    @SneakyThrows
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.printf("%s\n", new Date());
            try {
                // Sleep during one second
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                System.out.printf("The FileClock has been interrupted.\n");
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread(new ConsoleClock());
        thread.start();
        try {
            // Waits five seconds
            TimeUnit.SECONDS.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Interrupts the Thread
        thread.interrupt();
    }
}
