package com.aqielife.concurrent.threadmanagement.threadfactory;

/**
 * @author: aqie
 * @create: 2020-07-27 17:45
 **/
public class Main {
    public static void main(String[] args) {
        // Creates the factory
        MyThreadFactory factory = new MyThreadFactory("MyThreadFactory");
        // Creates a task
        Task task = new Task();
        Thread thread;

        // Creates and starts ten Thread objects
        System.out.printf("Starting the Threads\n");
        for (int i = 0; i < 10; i++) {
            thread = factory.newThread(task);
            thread.start();
        }
        // Prints the statistics of the ThreadFactory to the console
        System.out.printf("Factory stats:\n");
        System.out.printf("%s\n", factory.getStats());

    }
}
