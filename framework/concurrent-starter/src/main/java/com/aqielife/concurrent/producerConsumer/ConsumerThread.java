package com.aqielife.concurrent.producerConsumer;

import java.util.Random;

/**
 * @Author aqie
 * @Date 2022/4/22 11:17
 * @desc
 */
public class ConsumerThread extends Thread{
    private final TableBlock table;
    private Random random = new Random();
    //编写构造器，将name，table装进对象里
    public ConsumerThread(String name, TableBlock table){
        super(name);
        this.table = table;
    }
    @Override
    public void run() {
        while(true) {
            try {
                String cake = table.take();//拿蛋糕
                Thread.sleep(random.nextInt(1000));//阻塞
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
