package com.aqielife.concurrent.fundamentalthread.synchroniz;

/**
 * @author: aqie
 * @create: 2020-07-28 08:45
 **/
public class ParkingCash {

    // 停车收取费用
    private final int cost=2;

    private long cash;

    public long getCash() {
        return cash;
    }

    public ParkingCash() {
        cash=0;
    }

    // 车离场 收费
    public synchronized void vehiclePay() {
        cash+=cost;
    }

    public void close() {
        System.out.printf("Closing accounting\n");
        long totalAmount,carNum,motoNum;
        synchronized (this) {
            totalAmount = cash;
            cash = 0;
        }
        System.out.printf("The total amount is : %d",totalAmount);
    }
}
