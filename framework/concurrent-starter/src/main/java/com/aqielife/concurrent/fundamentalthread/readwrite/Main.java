package com.aqielife.concurrent.fundamentalthread.readwrite;

/**
 * @author: aqie
 * @create: 2020-07-28 11:25
 * 获得写锁,所有读线程无法获取数据
 **/
public class Main {
    public static void main(String[] args) {

        // Creates an object to store the prices
        PricesInfo pricesInfo=new PricesInfo();

        Reader readers[]=new Reader[5];
        Thread threadsReader[]=new Thread[5];

        // Creates five readers and threads to run them
        for (int i=0; i<5; i++){
            readers[i]=new Reader(pricesInfo);
            threadsReader[i]=new Thread(readers[i]);
        }

        // Creates a writer and a thread to run it
        Writer writer=new Writer(pricesInfo);
        Thread threadWriter=new Thread(writer);

        // Starts the threads
        for (int i=0; i<5; i++){
            threadsReader[i].start();
        }
        threadWriter.start();

    }
}
