package com.aqielife.concurrent.cyclicBarrier;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author aqie
 * @Date 2022/4/14 11:20
 * @desc https://mp.weixin.qq.com/s/lL_zIlYiJmdJ-iS3UcL5MA
 */
public class CyclicBarrierDemo {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(5, () -> {
            System.out.println("所有人都到场了，大家统一出发");
        });

        for (int i = 0; i < 10; i++) {
            final int id = i;
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName()+"，id："+id+"现前往集合地点");
                try {
                    Thread.sleep(new Random().nextInt(10000));
                    System.out.println(Thread.currentThread().getName()+"到了集合地点，开始等待其他人到达");
                    cyclicBarrier.await();
                    System.out.println(Thread.currentThread().getName()+"出发了");
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
