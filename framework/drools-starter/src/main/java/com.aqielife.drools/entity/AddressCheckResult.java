package com.aqielife.drools.entity;

import lombok.Data;

/**
 * @author: aqie
 * @create: 2020-08-24 16:29
 **/
@Data
public class AddressCheckResult {
    //true通过校验，false 未通过
    private boolean postCodeResult = false;
}
