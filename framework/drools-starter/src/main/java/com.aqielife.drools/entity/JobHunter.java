package com.aqielife.drools.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: aqie
 * @create: 2020-08-24 17:28
 **/
@Data
public class JobHunter implements Serializable {
    /**
     * 年龄
     */
    private int age;
    /**
     * 工作城市
     */
    private String workCity;
    /**
     * 申请城市
     */
    private String applyCity;
    /**
     * 居住城市
     */
    private String addressCity;
}
