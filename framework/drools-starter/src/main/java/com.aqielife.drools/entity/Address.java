package com.aqielife.drools.entity;

import lombok.Data;

/**
 * @author: aqie
 * @create: 2020-08-24 16:29
 **/
@Data
public class Address {
    private String postCode;
    private String street;
    private String state;
}
