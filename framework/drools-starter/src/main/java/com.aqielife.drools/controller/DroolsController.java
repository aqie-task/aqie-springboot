package com.aqielife.drools.controller;



import com.aqielife.common.R;
import com.aqielife.drools.entity.Address;
import com.aqielife.drools.entity.AddressCheckResult;
import com.aqielife.drools.entity.JobHunter;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: aqie
 * @create: 2020-08-24 16:28
 **/
@Slf4j
@RestController
@RequestMapping(value = "/drools")
public class DroolsController {

    @Resource
    KieSession kieSession;

    @GetMapping(value = "/address")
    public R<String> test1(){
        Address address = new Address();
        address.setPostCode("99425");

        AddressCheckResult result = new AddressCheckResult();
        kieSession.insert(address);
        kieSession.insert(result);
        int ruleFiredCount = kieSession.fireAllRules();
        log.info("触发了"+ruleFiredCount+"条规则");

        if (result.isPostCodeResult()){
            return R.data(ruleFiredCount + " 条规则校验通过");
        }
        return R.fail("校验失败");
    }

    @GetMapping("jobHunter")
    public R<Map<String,String>> jobHunter(){
        Map<String,String> result =new HashMap<>(1);
        JobHunter jobHunter=new JobHunter();
        jobHunter.setAge(80);
        kieSession.setGlobal("map",result);
        kieSession.insert(jobHunter);
        int count=kieSession.fireAllRules();
        log.info("规则执行条数：--------"+count);
        log.info("规则执行完成--------"+jobHunter.toString());
        Map<String,String> map = (Map<String,String>)kieSession.getGlobals().get("map");
        return R.data(map);
    }
}
