package com.aqielife.redis;

import lombok.Getter;

/**
 * @author aqie
 * @date 2022/03/06 9:58
 * @desc
 */
@Getter
public enum RedisKeyConstant {
  lock_key("lockby:", "分布式锁的key"),
  ;

  private final String key;
  private final String desc;

  RedisKeyConstant(String key, String desc) {
    this.key = key;
    this.desc = desc;
  }
}
