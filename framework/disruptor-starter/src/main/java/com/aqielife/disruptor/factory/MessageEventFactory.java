package com.aqielife.disruptor.factory;

import com.aqielife.disruptor.task.MessageModel;
import com.lmax.disruptor.EventFactory;

/**
 * @Author aqie
 * @Date 2022/4/13 15:53
 * @desc
 */
public class MessageEventFactory implements EventFactory<MessageModel> {
    @Override
    public MessageModel newInstance() {
        return new MessageModel();
    }
}
