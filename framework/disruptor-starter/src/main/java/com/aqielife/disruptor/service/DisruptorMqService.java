package com.aqielife.disruptor.service;

/**
 * @Author aqie
 * @Date 2022/4/13 15:56
 * @desc
 */
public interface DisruptorMqService {
    /**
     * 消息
     * @param message
     */
    void send(String message);
}
