package com.aqielife.job;

import com.aqielife.job.config.XxlJobConfig;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @Author aqie
 * @Date 2022/4/6 14:48
 * @desc
 */

@Slf4j
@SpringBootConfiguration
@EnableConfigurationProperties({XxlJobConfig.class})
public class XxlConfig {
    @Autowired
    private XxlJobConfig config;
    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        log.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(config.getAdmin().getAddresses());
        xxlJobSpringExecutor.setAppname(config.getExecutor().getAppname());
        xxlJobSpringExecutor.setAddress(config.getExecutor().getAddress());
        xxlJobSpringExecutor.setIp(config.getExecutor().getIp());
        xxlJobSpringExecutor.setPort(config.getExecutor().getPort());
        if(config.getAccessToken() != null){
            xxlJobSpringExecutor.setAccessToken(config.getAccessToken());
        }
        xxlJobSpringExecutor.setLogPath(config.getExecutor().getLogPath());
        xxlJobSpringExecutor.setLogRetentionDays(config.getExecutor().getLogRetentionDays());

        return xxlJobSpringExecutor;
    }
}
