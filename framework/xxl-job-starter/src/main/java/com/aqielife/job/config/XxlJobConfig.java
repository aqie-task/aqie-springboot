package com.aqielife.job.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @Author aqie
 * @Date 2022/4/6 11:15
 * @desc
 */
@Primary
@Data
@Slf4j
@Component
@ConfigurationProperties(prefix = "xxl.job")
public class XxlJobConfig {

    private String accessToken;

    private Admin admin;
    private Executor executor;
    @Data
    public static class Admin {
        private String addresses;
    }

    @Data
    public static class Executor {

        private String appname;

        private String address;

        private String ip;

        private int port;

        private String logPath;

        private int logRetentionDays;
    }
}
