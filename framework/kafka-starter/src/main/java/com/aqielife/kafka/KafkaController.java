package com.aqielife.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author aqie
 * @Date 2022/3/22 10:17
 * @desc
 */
@RestController
@RequestMapping("kafka")
public class KafkaController {

    @Autowired
    private KafkaProducer kafkaProducer;

    @GetMapping("send")
    public void sendMsg() {
        kafkaProducer.send("send msg");
    }
}
