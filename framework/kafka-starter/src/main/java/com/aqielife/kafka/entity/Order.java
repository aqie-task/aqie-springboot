package com.aqielife.kafka.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: aqie
 * @create: 2020-07-23 14:17
 **/
@Data
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String orderSn;
}
