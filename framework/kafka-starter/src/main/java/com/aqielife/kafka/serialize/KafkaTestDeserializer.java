package com.aqielife.kafka.serialize;


import com.aqielife.common.utils.serialize.SerializeUtil;
import com.aqielife.kafka.entity.Order;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

/**
 * @author: aqie
 * @create: 2020-07-24 11:04
 **/
public class KafkaTestDeserializer implements Deserializer<Order> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        // TODO Auto-generated method stub

    }
    @Override
    public Order deserialize(String topic, byte[] bytes) {
        return (Order) SerializeUtil.deserialize(bytes,Order.class);
    }
    @Override
    public void close() {
        // TODO Auto-generated method stub

    }
}
