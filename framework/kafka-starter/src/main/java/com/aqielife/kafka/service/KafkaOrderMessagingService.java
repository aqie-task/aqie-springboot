package com.aqielife.kafka.service;

import com.aqielife.kafka.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @author: aqie
 * @create: 2020-07-24 09:27
 **/
@Service
public class KafkaOrderMessagingService {
    @Autowired
    private KafkaTemplate<String, Order> kafkaTemplate;



    public void sendOrder(Order order) {
        kafkaTemplate.sendDefault("order",order);
    }

}
