package com.aqielife.kafka.entity;

import lombok.Data;

/**
 * @author: aqie
 * @create: 2020-07-24 14:43
 **/
@Data
public class Taco {
    private Long id;
    private String name;
    private String price;
}
