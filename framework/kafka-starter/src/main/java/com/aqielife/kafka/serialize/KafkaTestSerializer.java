package com.aqielife.kafka.serialize;


import com.aqielife.common.utils.serialize.SerializeUtil;
import com.aqielife.kafka.entity.Order;
import org.apache.kafka.common.serialization.Serializer;

/**
 * @author: aqie
 * @create: 2020-07-24 11:02
 **/
public class KafkaTestSerializer implements Serializer<Order> {
    @Override
    public byte[] serialize(String topic, Order data) {
        return SerializeUtil.serialize(data);
    }
}
