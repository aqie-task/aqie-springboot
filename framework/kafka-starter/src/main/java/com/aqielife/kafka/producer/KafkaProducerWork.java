package com.aqielife.kafka.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;


/**
 * 
* Description:生产者线程 <br/>
* @author moudaen
* @version 1.0
 */
@Slf4j
public class KafkaProducerWork implements Runnable{

	
	private  KafkaProducer<String, String> producer = null;
	
	private  ProducerRecord<String, String> record = null;
	
	public KafkaProducerWork(KafkaProducer<String, String> producer ,ProducerRecord<String, String> record) {
		this.producer = producer;
		this.record = record;
	}
	
	@Override
	public void run() {
		producer.send(record,new Callback() {

			@Override
			public void onCompletion(RecordMetadata metaData,
					Exception exception) {
				if (null != exception) {// 发送异常记录异常信息
					log.error("Send message occurs exception.",
							exception);
				}
				if (null != metaData) {
					log.info(String.format("offset:%s,partition:%s",
							metaData.offset(), metaData.partition()));
				}
			}

		});
	}

}
