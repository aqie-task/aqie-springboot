package com.aqielife.netty.demo.client;

import com.aqielife.netty.Constant;
import com.aqielife.netty.demo.initializer.ClientInitializer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: aqie
 * @create: 2020-07-15 16:00
 **/
@Slf4j
public class NettyClient {
    public static void main(String[] args) throws Exception {

        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.handler(new ClientInitializer());

            // Start the client.
            ChannelFuture f = b.connect(Constant.HOST, Constant.PORT).sync();
            f.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()){
                        future.channel().writeAndFlush("complete: hello netty server\r\n");
                        log.info("NettyClient 连接到{}：{}",Constant.HOST, Constant.PORT);
                    } else {
                        Throwable cause = future.cause();
                        cause.printStackTrace();
                    }
                }
            });

            // Wait until the connection is closed.
            f.channel().closeFuture().sync();
            log.info("key:{}",f.channel().attr(AttributeKey.valueOf("key")).get());

        } finally {
            workerGroup.shutdownGracefully();
        }
    }
}
