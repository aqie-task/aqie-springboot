package com.aqielife.netty.demo.handler;

import com.aqielife.netty.utils.StringUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: aqie
 * @create: 2020-07-15 15:56
 **/
@Slf4j
public class ServerHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("Client " + ctx.channel().remoteAddress() + " connected");
        ChannelFuture f = ctx.channel().writeAndFlush("hello netty client\r\n");
         // f.addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * 服务端接收消息
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        log.info("服务端开始读取数据");
        if (msg instanceof ByteBuf){
            String content = StringUtils.convertByteBufToString((ByteBuf) msg);
            // 向客户端回显消息
            ctx.channel().writeAndFlush(content+"\r\n");
            log.info("Server receive: {}", content);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
        // 关闭客户端
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER)
                .addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
                                Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
