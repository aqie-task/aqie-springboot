package com.aqielife.netty.demo.handler.outbound;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

import java.util.concurrent.TimeUnit;

/**
 * @author: aqie
 * @create: 2020-08-12 15:01
 **/
public class OutboundHandlerB extends ChannelOutboundHandlerAdapter {
    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        System.out.println("outboundB " + msg);
        ctx.write(msg, promise);
    }

    @Override
    public void handlerAdded(final ChannelHandlerContext ctx) throws Exception {
        // 模拟读取并处理数据 返回给客户端
        ctx.executor().schedule(() -> {
            ctx.channel().write("航空运输逆势领涨");
            // ctx.write("明天股市大涨");
        },3, TimeUnit.SECONDS);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("outboundHandlerB ExceptionCaught");
        ctx.fireExceptionCaught(cause);
    }
}
