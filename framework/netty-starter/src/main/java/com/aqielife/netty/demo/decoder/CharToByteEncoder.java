package com.aqielife.netty.demo.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author: aqie
 * @create: 2020-07-18 11:05
 * 将 Character 转换回字节。这个类扩
 * 展了 MessageToByteEncoder，因为它需要将 char 消息编码到 ByteBuf 中。这是通过直接
 * 写入 ByteBuf 做到的
 **/
public class CharToByteEncoder extends
        MessageToByteEncoder<Character> {
    @Override
    public void encode(ChannelHandlerContext ctx, Character msg,
                       ByteBuf out) throws Exception {
        out.writeChar(msg);
    }
}
