package com.aqielife.netty.demo.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author: aqie
 * @create: 2020-07-18 11:04
 * decode()方法一次将从 ByteBuf 中提取 2 字节，并将它们作为 char 写入到 List
 * 中，其将会被自动装箱为 Character 对象
 **/
public class ByteToCharDecoder extends ByteToMessageDecoder {
    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in,
                       List<Object> out) throws Exception {
        while (in.readableBytes() >= 2) {
            out.add(in.readChar());
        }
    }
}
