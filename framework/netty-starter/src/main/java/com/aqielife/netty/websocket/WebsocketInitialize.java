package com.aqielife.netty.websocket;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;


/**
 * @Author aqie
 * @Date 2022/4/22 10:02
 * @desc
 */
public class WebsocketInitialize extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        // ======================= 支持http协议 ====================
        // 编解码
        pipeline.addLast(new HttpServerCodec());
        // 对大数据流写入支持
        pipeline.addLast(new ChunkedWriteHandler());
        // http message进行聚合，聚合成fullHttpRequest或 fullHttpResponse(netty编程几乎都会用到此handler)
        pipeline.addLast(new HttpObjectAggregator(1024 * 64));  // 消息长度

        // =================== websocket====================

        /**
         *  websocket处理协议,及制定客户端访问路由
         *  会处理握手handshake(close ping pong ) ping + pong = 心跳
         *  对websocket 都是以frames传输,不同数据类型对应不同 frame
         */
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));

        // 自定义处理类,
        pipeline.addLast(new ChatHandler());
    }
}
