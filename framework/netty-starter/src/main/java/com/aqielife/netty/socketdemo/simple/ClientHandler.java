package com.aqielife.netty.socketdemo.simple;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ClientHandler {
    public static final int MAX_DATA_LEN = 1024;
    private final Socket socket;

    // 保存客户端socket
    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    // start 对每个客户端均会创建线程,
    public void start() {
        System.out.println("新客户端接入");
        new Thread(new Runnable() {
            @Override
            public void run() {
                doStart();
            }
        }).start();
    }

    private void doStart() {
        try {
            // 拿到socket输入流
            InputStream inputStream = socket.getInputStream();
            // 客户端与服务端通信过程
            while (true) {
                byte[] data = new byte[MAX_DATA_LEN];
                int len;
                while ((len = inputStream.read(data)) != -1) {
                    String message = new String(data, 0, len);
                    System.out.println("客户端传来消息: " + message);
                    // 得到客户端socket输出流
                    socket.getOutputStream().write(data);
                }

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
