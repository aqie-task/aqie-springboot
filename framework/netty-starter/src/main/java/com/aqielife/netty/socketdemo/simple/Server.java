package com.aqielife.netty.socketdemo.simple;

import com.aqielife.netty.Constant;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class Server {
    public static void main(String[] args) {
        Server server = new Server(Constant.PORT);
        server.start();
    }

    private ServerSocket serverSocket;
    Server(int port){
        try {
            this.serverSocket = new ServerSocket(port);
            System.out.println("服务端启动成功，端口:" + port);
        } catch (IOException exception) {
            System.out.println("服务端启动失败");
        }
    }

    // 不想创建server线程阻塞主线程
    void start() {
        new Thread(this::doStart).start();
    }

    private void doStart() {
        while (true) {
            try {
                // accept是阻塞方法,客户端过来才会
                Socket client = serverSocket.accept();
                new ClientHandler(client).start();
            } catch (IOException e) {
                System.out.println("服务端异常");
            }
        }
    }
}
