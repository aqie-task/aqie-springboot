package com.aqielife.netty.demo.server;

import com.aqielife.netty.Constant;
import com.aqielife.netty.demo.handler.ExceptionHandler;
import com.aqielife.netty.demo.handler.inbound.InboundHandlerA;
import com.aqielife.netty.demo.handler.inbound.InboundHandlerB;
import com.aqielife.netty.demo.handler.inbound.InboundHandlerC;
import com.aqielife.netty.demo.handler.outbound.OutboundHandlerA;
import com.aqielife.netty.demo.handler.outbound.OutboundHandlerB;
import com.aqielife.netty.demo.handler.outbound.OutboundHandlerC;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: aqie
 * @create: 2020-07-12 09:11
 * Discards any incoming data.
 **/
@Slf4j
public class NettyServer {

    public static void main(String[] args) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap(); 
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) 
                    .childHandler(new ChannelInitializer<SocketChannel>() { 
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            // ch.pipeline().addLast(new DiscardServerHandler());
                            // 向客户端发送系统时间
                            // ch.pipeline().addLast(new TimeEncoder2(), new TimeServerHandler());
                            /*ch.pipeline().addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE,
                                    Delimiters.lineDelimiter()[0]));
                            ch.pipeline().addLast(new StringEncoder());
                            ch.pipeline().addLast(new ServerHandler());
                            ch.pipeline().addLast(new StringDecoder());*/
                            // 读事件传播Demo
                            ch.pipeline().addLast(new InboundHandlerA());
                            ch.pipeline().addLast(new InboundHandlerB());
                            ch.pipeline().addLast(new InboundHandlerC());

                            // 写事件传播
                            ch.pipeline().addLast(new OutboundHandlerA());
                            ch.pipeline().addLast(new OutboundHandlerB());
                            ch.pipeline().addLast(new OutboundHandlerC());

                            ch.pipeline().addLast(new ExceptionHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)          
                    .childOption(ChannelOption.SO_KEEPALIVE, true); 

            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(Constant.HOST, Constant.PORT).sync();
            log.info("NettyServer 启动：{}:{}",Constant.HOST, Constant.PORT);

            // Wait until the server socket is closed.
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

}
