package com.aqielife.netty.demo.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author: aqie
 * @create: 2020-08-12 16:02
 * pipeline 最后异常处理器
 **/
public class ExceptionHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof RuntimeException){
            System.out.println("caugth runtime exception");
        }
    }
}
