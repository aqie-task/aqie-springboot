package com.aqielife.netty.demo.pojo;

import java.util.Date;

/**
 * @author: aqie
 * @create: 2020-07-12 15:59
 **/
public class UnixTime {

    private final long value;

    public UnixTime() {
        this(System.currentTimeMillis() / 1000L + 2208988800L);
    }

    public UnixTime(long value) {
        this.value = value;
    }

    public long value() {
        return value;
    }

    @Override
    public String toString() {
        return new Date((value() - 2208988800L) * 1000L).toString();
    }
}
