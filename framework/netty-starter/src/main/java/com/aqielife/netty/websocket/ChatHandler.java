package com.aqielife.netty.websocket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.time.LocalDateTime;

/**
 * 文本对象 处理消息的Handler
 */
public class ChatHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {
    // 记录 管理所有客户端的channel
    private static ChannelGroup clients = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        // 获取客户端发送字符串
        String content = msg.text();
        System.out.println("客户端数据： " + content);

        // 每个客户端对应一个channel,针对channel发送
        // 数据获取到刷到对应的所有客户端
        /*for(Channel channel : clients){
            // 向缓冲区中写数据,并刷到客户端
            channel.writeAndFlush(
                    new TextWebSocketFrame("[服务器接收到时间]" + LocalDateTime.now() + " - " + content));
        }*/

        // 等价上面for循环
        clients.writeAndFlush(new TextWebSocketFrame("[服务器接收数据时间]" + LocalDateTime.now() + " - " + content));

    }

    /**
     * 客户端连接服务端(打开连接, )
     * 获取客户端对应channel, 并添加到channelGroup中
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        clients.add(ctx.channel());
    }

    // 触发handlerRemoved ChannelGroup会自动移除客户端的channel
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        // clients.remove(ctx.channel());
        System.out.println("客户端断开,channel对应的长id: " + ctx.channel().id().asLongText());
        System.out.println("客户端断开,channel对应的短id: " +ctx.channel().id().asShortText());
    }
}
