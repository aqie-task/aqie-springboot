package com.aqielife.netty.demo.encodedecode;

import com.aqielife.netty.demo.decoder.ByteToCharDecoder;
import com.aqielife.netty.demo.decoder.CharToByteEncoder;
import io.netty.channel.CombinedChannelDuplexHandler;

/**
 * @author: aqie
 * @create: 2020-07-18 11:06
 **/
public class CombinedByteCharCodec extends
        CombinedChannelDuplexHandler<ByteToCharDecoder, CharToByteEncoder> {
    public CombinedByteCharCodec() {
        super(new ByteToCharDecoder(), new CharToByteEncoder());
    }
}
