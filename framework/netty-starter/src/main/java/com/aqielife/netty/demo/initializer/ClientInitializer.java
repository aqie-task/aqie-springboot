package com.aqielife.netty.demo.initializer;

import com.aqielife.netty.demo.handler.ClientHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @author: aqie
 * @create: 2020-07-18 10:30
 **/
public class ClientInitializer extends ChannelInitializer<SocketChannel>{
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline p = ch.pipeline();
        p.addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE,
                Delimiters.lineDelimiter()[0]));
        p.addLast(new StringEncoder());
        p.addLast(new ClientHandler());
        p.addLast(new StringDecoder());
    }
}
