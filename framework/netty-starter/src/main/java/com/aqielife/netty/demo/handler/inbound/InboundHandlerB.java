package com.aqielife.netty.demo.handler.inbound;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author: aqie
 * @create: 2020-08-12 10:47
 **/
public class InboundHandlerB extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("InboundHandlerB " + msg);
        ctx.fireChannelRead(msg);
        throw new RuntimeException("from inboundB");
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.channel().pipeline().fireChannelRead("黄金大跌8-12");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("InboundHandlerB ExceptionCaught");
        ctx.fireExceptionCaught(cause);
    }
}
