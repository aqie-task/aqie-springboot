package com.aqielife.mqtt.config;

import com.aqielife.mqtt.MqttPushCallback;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import static com.aqielife.mqtt.MqttConstant.*;

/**
 * @Author aqie
 * @Date 2022/4/6 15:16
 * @desc
 */
@Slf4j
@SpringBootConfiguration
@EnableConfigurationProperties({MqttConfigBean.class})
public class MqttConfig {

    @Autowired
    private  MqttConfigBean bean;

    @Bean
    public MqttClient mqttClient() {
        MqttClient client = null;
        try {
            client = new MqttClient(bean.getUrl(), bean.getClient().getId(), new MemoryPersistence());
            MqttConnectOptions option = getMqttConnectOptions();
            client.setCallback(new MqttPushCallback(client,option));
            client.connect(option);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    @Bean
    public MqttConnectOptions getMqttConnectOptions(){
        MqttConnectOptions option = new MqttConnectOptions();
        option.setCleanSession(false);
        option.setUserName(bean.getUsername());
        option.setPassword(bean.getPassword().toCharArray());
        option.setConnectionTimeout(bean.getCompletionTimeout());
        option.setKeepAliveInterval(bean.getKeepalive());
        option.setServerURIs(new String[]{bean.getUrl()});
        option.setAutomaticReconnect(true);
        return option;
    }

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(getMqttConnectOptions());
        return factory;
    }

    //接收通道
    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    //配置client,监听的topic
    @Bean
    public MessageProducer inbound() {
        MqttPahoMessageDrivenChannelAdapter adapter =
                new MqttPahoMessageDrivenChannelAdapter(bean.getClient().getId()+"_inbound", mqttClientFactory(),
                        TOPIC1, TOPIC2);
        adapter.setCompletionTimeout(bean.getCompletionTimeout());
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(1);
        adapter.setOutputChannel(mqttInputChannel());
        return adapter;
    }

    //通过通道获取数据
    @Bean
    @ServiceActivator(inputChannel = DEFAULT_CHANNEL)
    public MessageHandler handler() {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                String topic = message.getHeaders().get(MqttHeaders.TOPIC).toString();
                String type = topic.substring(topic.lastIndexOf("/")+1, topic.length());
                if(TOPIC1.equalsIgnoreCase(topic)){
                    log.info("hello1: ,"+message.getPayload().toString());
                }else if(TOPIC2.equalsIgnoreCase(topic)){
                    log.info("hello2: ,"+message.getPayload().toString());
                }
            }
        };
    }

}
