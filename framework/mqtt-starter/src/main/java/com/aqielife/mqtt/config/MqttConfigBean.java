package com.aqielife.mqtt.config;

import com.aqielife.mqtt.MqttPushCallback;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import static com.aqielife.mqtt.MqttConstant.*;

/**
 * @ClassName MqttReceiveConfig
 * @Description
 * @Author aqie
 * @Date 2020/5/20 16:10
 * @Version 1.0
 *
 @IntegrationComponentScan
 * add by aqie 2020/5/20
 **/
// @IntegrationComponentScan
@Primary
@Data
@Slf4j
@ConfigurationProperties(prefix = "spring.mqtt")
public class MqttConfigBean {

    private String username;


    private String password;


    private String url;

    private int completionTimeout;

    private int keepalive;

    private Client client;

    private Default aDefault;

    @Data
    public static class Client {
        private String id;
    }

    @Data
    public static class Default {
        private String topic;
    }













}
