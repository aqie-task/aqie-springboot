package com.aqielife.mqtt;


import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * MQTT 推送回调
 *
 * @author wuxw
 * @date 2020-05-20
 */
public class MqttPushCallback implements MqttCallback {

    private static final Logger log = LoggerFactory.getLogger(MqttPushCallback.class);
    private MqttClient client;

    private MqttConnectOptions option;

    public MqttPushCallback() {

    }

    public MqttPushCallback(MqttClient client, MqttConnectOptions option) {
        this.client = client;
        this.option = option;
    }

    @Override
    public void connectionLost(Throwable cause) {
        log.info("断开连接，建议重连" + this);
        log.error("连接断开",cause);
        while(true) {
            try {
                Thread.sleep(1000);
                // 重新连接
                client.connect(option);
                break;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        //log.info(token.isComplete() + "");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        try {
            log.info("Topic: " + topic);
            log.info("Message: " + new String(message.getPayload()));
            // todo
        } catch (Exception e) {
            log.error("处理订阅消息失败", e);
        }
    }

    /**
     * 获取协议
     *
     * @param topic
     * @param message
     * @return
     */
    private String getHmId(String topic, MqttMessage message) {

        if(topic.contains("hiot")){
            return "18";
        }

        //德安中获取
        String hmId = getHmIdByDean(topic);
        if (!StringUtils.isEmpty(hmId)) {
            return hmId;
        }

        //伊兰度中获取协议
        hmId = getHmIdByYld(topic, message);


        return hmId;

    }

    /**
     * 伊兰度中获取设备ID
     *
     * @param topic   face.{sn}.response
     * @param message
     * @return
     */
    private String getHmIdByYld(String topic, MqttMessage message) {
        // todo
        return "";
    }

    /**
     * 德安门禁获取
     *
     * @param topic mqtt/face/ID/Rec mqtt/face/ID/Snap mqtt/face/ID/QRCode mqtt/face/ID/Ack
     * @return
     */
    private String getHmIdByDean(String topic) {
        String hmId = "";
        //判断是否为德安心跳 todo

        return hmId;
    }

}