package com.aqielife.mqtt.service;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import static com.aqielife.mqtt.MqttConstant.DEFAULT_CHANNEL;

/**
 * @Author aqie
 * @Date 2022/3/22 14:32
 * @desc
 */
@Service
@MessagingGateway(defaultRequestChannel = DEFAULT_CHANNEL)
public interface MqttService {
    void sendToMqtt(String data,@Header(MqttHeaders.TOPIC) String topic);
}
