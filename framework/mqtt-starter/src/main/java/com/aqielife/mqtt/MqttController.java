package com.aqielife.mqtt;

import com.aqielife.mqtt.service.MqttService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.aqielife.mqtt.MqttConstant.TOPIC1;

/**
 * @Author aqie
 * @Date 2022/3/22 14:33
 * @desc
 */
@RequestMapping("mqtt")
@RestController
public class MqttController {
    @Autowired
    private MqttService mqttService;

    @RequestMapping("/send")
    public String sendMqtt(String msg){
        mqttService.sendToMqtt(msg, TOPIC1);
        return "OK";
    }

}
