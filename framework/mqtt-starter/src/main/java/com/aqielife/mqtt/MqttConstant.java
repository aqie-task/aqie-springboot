package com.aqielife.mqtt;

/**
 * @Author aqie
 * @Date 2022/3/22 14:26
 * @desc
 */
public interface MqttConstant {
    String DEFAULT_CHANNEL = "mqttOutboundChannel";
    String TOPIC1 = "hello1";
    String TOPIC2 = "hello2";
}
