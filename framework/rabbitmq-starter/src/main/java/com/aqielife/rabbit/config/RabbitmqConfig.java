package com.aqielife.rabbit.config;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

import static com.aqielife.MQConstant.*;


/**
 * @Author aqie
 * @Date 2022/2/11 14:27
 * @desc
 */
@SpringBootConfiguration
public class RabbitmqConfig {
	@Bean
	public MessageConverter messageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	public Queue test() {
		return new Queue(QUEUE_TEST);
	}

	@Bean
	public FanoutExchange exchangeHengshui() {
		return new FanoutExchange(EXCHANGE_HENGSHUI);
	}

	@Bean
	public FanoutExchange exchangeXingtaii() {
		return new FanoutExchange(EXCHANGE_XINGTAI);
	}

}
