package com.aqielife.rabbit.listener;


import com.aqielife.rabbit.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static com.aqielife.MQConstant.QUEUE_TEST;

/**
 * @author: aqie
 * @create: 2020-07-23 15:40
 * 消息推送模式
 **/
@Component
@Slf4j
public class OrderListener {
    @RabbitListener(queues = QUEUE_TEST)
    public void receiveOrder(Order order) {
        log.info("rabbitMQ listener: {}", order);
    }

}
