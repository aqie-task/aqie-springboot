package com.aqielife.rabbit.controller;

import com.aqielife.common.R;
import com.aqielife.rabbit.entity.Order;
import com.aqielife.rabbit.service.RabbitOrderMessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author aqie
 * @Date 2022/4/21 15:29
 * @desc
 */
@RequestMapping("rabbitmq")
@RestController
public class RabbitmqController {
    @Autowired
    private RabbitOrderMessagingService service;

    @PostMapping("send")
    public R send(@RequestBody Order order){
        service.sendOrder(order);
        return R.data("ok");
    }
}
