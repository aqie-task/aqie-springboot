package com.aqielife.rabbit.service;



import com.aqielife.rabbit.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.aqielife.MQConstant.QUEUE_TEST;

/**
 * @author: aqie
 * @create: 2020-07-23 16:44
 **/
@Service
@Slf4j
public class RabbitOrderMessagingService {
    @Autowired
    private RabbitTemplate rabbit;

    @Autowired
    private MessageConverter converter;

    public void sendOrder(Order order) {
        MessageConverter converter = rabbit.getMessageConverter();
        MessageProperties props = new MessageProperties();
        props.setHeader("X_ORDER_SOURCE", "WEB");
        Message message = converter.toMessage(order, props);
        rabbit.send(QUEUE_TEST, message);
    }

    /**
     *  amqp 转换器
     * Jackson2JsonMessageConverter
     * MarshallingMessageConverter
     * SerializerMessageConverter
     * SimpleMessageConverter
     * ContentTypeDelegatingMessageConverter
     * MessagingMessageConverter
     */

    public void sendOrder2(Order order) {
        rabbit.convertAndSend(QUEUE_TEST, order, new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                MessageProperties props = message.getMessageProperties();
                props.setHeader("X_ORDER_SOURCE", "WEB");
                return message;
            }
        });
    }

    public Order receiveOrder() {
        Message message = rabbit.receive(QUEUE_TEST);
        return message != null
                ? (Order) converter.fromMessage(message)
                : null;
    }

}
