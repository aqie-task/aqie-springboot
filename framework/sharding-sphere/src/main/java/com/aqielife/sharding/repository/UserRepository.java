package com.aqielife.sharding.repository;


import com.aqielife.sharding.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: aqie
 * @create: 2020-08-30 09:57
 **/
public interface UserRepository extends JpaRepository<Users, Integer>, JpaSpecificationExecutor<Users> {
}
