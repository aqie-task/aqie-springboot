package com.aqielife.sharding.controller;


import com.aqielife.common.R;
import com.aqielife.sharding.entity.Users;
import com.aqielife.sharding.service.IUserShardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: aqie
 * @create: 2020-08-30 09:51
 **/
@RequestMapping("users")
@RestController
public class UserController {
    @Autowired
    private IUserShardService userService;

    @PostMapping("add")
    public R<Users> add(@RequestBody Users user){
        Users res = userService.add(user);
        return R.data(res);
    }
}
