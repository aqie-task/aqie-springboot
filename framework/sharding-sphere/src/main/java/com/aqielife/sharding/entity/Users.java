package com.aqielife.sharding.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author: aqie
 * @create: 2020-08-30 09:52
 **/
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@Table
@Data
public class Users {

    @Id
    private Long id;
    private int type;
    private String name;
}
