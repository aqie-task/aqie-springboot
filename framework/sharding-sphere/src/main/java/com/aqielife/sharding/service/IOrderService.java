package com.aqielife.sharding.service;



import com.aqielife.sharding.entity.Orders;

import java.util.List;

public interface IOrderService {
    Orders add(Orders order);
    List<Orders> get(Long uid);
}
