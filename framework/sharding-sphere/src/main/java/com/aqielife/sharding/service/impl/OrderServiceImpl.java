package com.aqielife.sharding.service.impl;

import com.aqielife.sharding.entity.Orders;
import com.aqielife.sharding.repository.OrderRepository;
import com.aqielife.sharding.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: aqie
 * @create: 2020-08-30 10:56
 **/
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Override
    public Orders add(Orders order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Orders> get(Long uid) {
        return orderRepository.getByUserId(uid).orElse(null);
    }
}
