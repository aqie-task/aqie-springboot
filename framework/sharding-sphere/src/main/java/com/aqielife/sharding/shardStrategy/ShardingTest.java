package com.aqielife.sharding.shardStrategy;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Range;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author: aqie
 * @create: 2020-08-30 10:43
 **/
@Slf4j
public class ShardingTest implements PreciseShardingAlgorithm<Integer>, RangeShardingAlgorithm<String> {
    /**
     * in/= 判断标准
     *
     * @param collection
     * @param preciseShardingValue
     * @return
     */
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<Integer> preciseShardingValue) {
        log.info("精确分片算法类: {}", preciseShardingValue.getValue());
        Date time = DateUtil.date((long)preciseShardingValue.getValue() * 1000);
        // Date time = DateUtil.parse(preciseShardingValue.getValue(), "yyyy-MM-dd HH:mm:ss");
        String logicTableName = preciseShardingValue.getLogicTableName();
        int year = DateUtil.year(time);
        // 定义表名
        String tableName = logicTableName + "_" + year;
        log.info("tableName:{}", tableName);
        for (String name : collection) {
            if (tableName.equals(name)) {
                return tableName;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Collection<String> doSharding(Collection<String> collection, RangeShardingValue<String> rangeShardingValue) {
        log.info("范围分片算法类: {}", rangeShardingValue.getValueRange());
        Range<String> valueRange = rangeShardingValue.getValueRange();
        Date right = null;
        Date left = null;
        // 如果有固定范围则取
        if (valueRange.hasLowerBound()) {
            String lowerEndpoint = valueRange.upperEndpoint();
            String yearMonth = lowerEndpoint.substring(0, 7);
            left = DateUtil.parse(yearMonth, "yyyy-MM");
        }
        if (valueRange.hasUpperBound()) {
            String upperEndpoint = valueRange.upperEndpoint();
            String yearMonth = upperEndpoint.substring(0, 7);
            right = DateUtil.parse(yearMonth, "yyyy-MM");
        }
        // Assert.notNull(left, "left cannot be null");
        // Assert.notNull(right, "right cannot be null");
        Range<Date> range;
        if (left == null && right != null) {
            range = Range.upTo(right, valueRange.upperBoundType());
        } else if (right == null && left != null) {
            range = Range.downTo(left, valueRange.lowerBoundType());
        } else {
            range = Range.range(right, valueRange.lowerBoundType(), left, valueRange.upperBoundType());
        }
        ArrayList<String> list = new ArrayList<>();
        for (String tableOriginName : collection) {
            String[] userorders = tableOriginName.split("orders_");
            String tableTime = userorders[1];
            DateTime time = DateUtil.parse(tableTime, "yyyy");
            if (range.contains(time)) {
                list.add(tableOriginName);
            }
        }
        return list;
    }
}