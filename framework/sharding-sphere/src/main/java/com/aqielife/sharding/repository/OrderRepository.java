package com.aqielife.sharding.repository;



import com.aqielife.sharding.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * @author: aqie
 * @create: 2020-08-30 09:57
 **/
public interface OrderRepository extends JpaRepository<Orders, Integer>, JpaSpecificationExecutor<Orders> {
    Optional<List<Orders>> getByUserId(Long uid);
}
