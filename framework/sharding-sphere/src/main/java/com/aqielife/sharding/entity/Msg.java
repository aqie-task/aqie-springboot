package com.aqielife.sharding.entity;


import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @Author aqie
 * @Date 2022/4/20 16:59
 * @desc
 */
@Accessors(chain = true)
@Entity
@Table
@Data
public class Msg {
    @Id
    private Long id;

    private Long msgId;

    private String content;
}
