package com.aqielife.sharding.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author: aqie
 * @create: 2020-08-30 10:52
 **/
@Entity
@Table
@Data
public class Orders {
    @Id
    private Long id;
    private int time;
    private BigDecimal totalPrice;
    private String orderSn;
    private Long userId;
}
