package com.aqielife.sharding.service;


import com.aqielife.sharding.entity.Users;

/**
 * @author: aqie
 * @create: 2020-08-30 09:50
 **/
public interface IUserShardService {
    Users add(Users user);
}
