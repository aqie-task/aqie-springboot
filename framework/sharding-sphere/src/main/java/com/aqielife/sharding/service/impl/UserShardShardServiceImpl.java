package com.aqielife.sharding.service.impl;


import com.aqielife.sharding.entity.Users;
import com.aqielife.sharding.repository.UserRepository;
import com.aqielife.sharding.service.IUserShardService;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: aqie
 * @create: 2020-08-30 09:50
 **/
@Service
public class UserShardShardServiceImpl implements IUserShardService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public Users add(Users user) {
        user.setId((Long) new SnowflakeShardingKeyGenerator().generateKey());
        return userRepository.save(user);
    }
}
