package com.aqielife.stomp.config;

import com.aqielife.stomp.interceptor.AuthChannelInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @Author aqie
 * @Date 2022/4/8 8:51
 * @desc
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {
    @Autowired
    private AuthChannelInterceptor authChannelInterceptor;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry
                .addEndpoint("/ws")  //端点名称
                //.setHandshakeHandler() 握手处理，主要是连接的时候认证获取其他数据验证等
                //.addInterceptors() 拦截处理，和http拦截类似
                .setAllowedOriginPatterns("*") //跨域
                .withSockJS(); //使用sockJS
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        //这里注册两个，主要是目的是将广播和队列分开。
        registry.enableStompBrokerRelay("/exchange")
                .setRelayHost("127.0.0.1").setRelayPort(61613)
                .setClientLogin(username)
                .setClientPasscode(password)
                .setAutoStartup(true);
        // registry.enableSimpleBroker("/topic", "/queue");
        //客户端名称前缀
        registry.setApplicationDestinationPrefixes("/app");
        //用户名称前缀
        registry.setUserDestinationPrefix("/user");
    }

    /*@Override
    public void configureClientOutboundChannel(ChannelRegistration registration) {
        registration.interceptors(authChannelInterceptor);
    }*/
}
