package com.aqielife.stomp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author aqie
 * @Date 2022/4/8 15:54
 * @desc
 */
@RequestMapping("stomp")
@RestController
public class StompController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    @GetMapping("send")
    public void send(String topic, String msg){
        messagingTemplate.convertAndSend("/exchange/"+topic, msg);
    }

}
