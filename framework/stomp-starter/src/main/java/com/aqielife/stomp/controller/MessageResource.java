package com.aqielife.stomp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.aqielife.stomp.StompConstant.HENGSHUI;

/**
 * @Author aqie
 * @Date 2022/4/8 8:54
 * @desc
 */
@Slf4j
@RestController
public class MessageResource {
    /**
     * 接收前端publish命令发送的
     * /app/hello
     * @param message
     * @return
     */
    @MessageMapping("/{topic}")
    @SendTo("/exchange/{topic}")
    public String hello(@Payload String message, @DestinationVariable String topic) {
        log.info("hello {}", message);
        if(HENGSHUI.equals(topic)){
            return "1";
        } else {
            return "2";
        }
    }

    /*@MessageMapping("/hello")
    @SendTo("/exchange/hello")
    public String hello(@Payload String message) {
        log.info("hello {}", message);
        return "123";
    }*/


    /**
     * 接收前端subscribe命令发送的
     * /app/subscribe
     * @return
     */
    @SubscribeMapping("/subscribe")
    public String subscribe() {
        return "aaa";
    }
}
