#### deploy
java -jar server-impl-1.0.0.jar --spring.profiles.active=prod

#### finish
- security: 认证
- oauth   : 授权

#### 端口
9042 cassandra
5672 15672 rabbitmq
9200 es
2181  zookeeper
20800 dubbo
27017 mongodb

#### stomp 前端
- [big-screen-vue-datav](https://gitee.com/the-guests/big-screen-vue-datav)

#### book
- 设计模式
  - design-pattern-java
- 基础
  - java 核心知识点整理
- 并发编程
  - java并发编程的艺术