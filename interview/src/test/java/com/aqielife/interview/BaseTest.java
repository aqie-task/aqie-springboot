package com.aqielife.interview;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Author aqie
 * @Date 2022/4/13 17:18
 * @desc
 */
@SpringBootTest(classes = Application.class)
public class BaseTest {

}
