package com.aqielife.interview.concurrentPractice.amdahl;

import com.aqielife.interview.utils.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

// todo
public class PutTakeTest {
    private static final ExecutorService pool = Executors.newCachedThreadPool();
    private final AtomicInteger putSum = new AtomicInteger(0);
    private final AtomicInteger takeSum = new AtomicInteger(0);
    private  CyclicBarrier barrier;
    private  BounderBuffer<Integer> bb;
    private  int nTrials, nPairs;


    void putTakeTest(int capacity, int nTrials, int nPairs) {
        this.barrier = new CyclicBarrier(nPairs * 2 +1);
        this.bb = new BounderBuffer<>(capacity);
        this.nTrials = nTrials;
        this.nPairs = nPairs;
    }

    @Test
    void doExec() {
        putTakeTest(10, 10, 100000);
        test();
        pool.shutdown();
    }

    void test() {
        try{
            for (int i = 0; i < nPairs; i++) {
                pool.execute(new Producer());
                pool.execute(new Consumer());
            }
            barrier.await(); //  等待所有线程作好准备
            barrier.await(); // 等待所有线程最终完成
            Assertions.assertEquals(putSum.get(), takeSum.get());
        } catch (BrokenBarrierException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }




}
