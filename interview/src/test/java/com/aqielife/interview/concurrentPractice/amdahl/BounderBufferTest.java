package com.aqielife.interview.concurrentPractice.amdahl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

class BounderBufferTest {
    private static final long LOCKUP_DETECT_TIMOUT = 1000;

    @Test
    void testFull() throws InterruptedException {
        BounderBuffer<Integer> bb = new BounderBuffer<>(10);
        Assertions.assertTrue(bb.isEmpty());
        Assertions.assertFalse(bb.isFull());

        for (int i =0;i<10;i++) {
            bb.put(i);
        }
        Assertions.assertTrue(bb.isFull());
        Assertions.assertFalse(bb.isEmpty());
    }

    @Test
    void testBlock(){
        BounderBuffer<Integer> bb = new BounderBuffer<>(10);
        Thread taker = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    int unused = bb.take();
                    fail();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        try {
            taker.start();
            Thread.sleep(LOCKUP_DETECT_TIMOUT);
            taker.interrupt();
            taker.join(LOCKUP_DETECT_TIMOUT);
            Assertions.assertFalse(taker.isAlive());
        } catch (Exception e) {
            fail();
        }
    }
}