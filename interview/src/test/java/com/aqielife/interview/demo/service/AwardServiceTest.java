package com.aqielife.interview.demo.service;

import com.aqielife.interview.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author aqie
 * @Date 2022/4/13 17:18
 * @desc
 */
class AwardServiceTest extends BaseTest {

    @Autowired
    private AwardService awardService;

    @Test
    void getRewardResult() {
        awardService.getRewardResult("123", "wx");
    }
}