package com.aqielife.interview.concurrentArt.practice;

import com.aqielife.interview.concurrentArt.practice.queue.ArticleBlockingQueue;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;

import static com.aqielife.interview.concurrentArt.practice.MsgQueueManager.getInstance;

/**
 * 消息分发线程
 * 分发消息,负责把消息从大队列塞到小队列里
 */
@Slf4j
public class DispatchMessageTask implements Runnable {

    @Override
    public void run() {
        for (;;){
            // 如果没有数据,则阻塞在这里
            Message msg = MsgQueueFactory.getMessageQueue().take();
            // 如果为空,则表示没有Session机器连接上来,
            // 需要等待,直到有Session机器连接上来
            BlockingQueue<Message> subQueue = new ArticleBlockingQueue<>(10);
            while ((subQueue = getInstance().getSubQueue()) == null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            // 把消息放到小队列里
            try {
                subQueue.put(msg);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }


}
