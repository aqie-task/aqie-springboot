package com.aqielife.interview.concurrentArt.practice.queue;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class QuickEmailToWikiExtractor {
    private ThreadPoolExecutor threadsPool;
    private ArticleBlockingQueue<ExchangeEmailShallowDTO> emailQueue;
    public QuickEmailToWikiExtractor() {
        emailQueue= new ArticleBlockingQueue<ExchangeEmailShallowDTO>(10);
        int corePoolSize = Runtime.getRuntime().availableProcessors() * 2;
        threadsPool = new ThreadPoolExecutor(corePoolSize, corePoolSize, 10l, TimeUnit.
                SECONDS,
                new LinkedBlockingQueue<Runnable>(2000));
    }
    public void extract() throws InterruptedException {
        long start = System.currentTimeMillis();
        // 抽取所有邮件放到队列里
        new ExtractEmailTask().start();
        // 把队列里的文章插入到Wiki
        insertToWiki();
        long end = System.currentTimeMillis();
        double cost = (end - start) / 1000;

    }

    private String getExtractorName() {
        return null;
    }

    /**
     * 把队列里的文章插入到Wiki
     */
    private void insertToWiki() throws InterruptedException {
        // 登录Wiki,每间隔一段时间需要登录一次

        while (true) {
            // 2秒内取不到就退出
            ExchangeEmailShallowDTO email = emailQueue.poll(2, TimeUnit.SECONDS);
            if (email == null) {
                break;
            }
            threadsPool.submit(new insertToWikiTask(email));
        }
    }
    protected List<Article> extractEmail() {
        List<ExchangeEmailShallowDTO> allEmails = queryAllEmails();
        if (allEmails == null) {
            return null;
        }
        for (ExchangeEmailShallowDTO exchangeEmailShallowDTO : allEmails) {
            emailQueue.offer(exchangeEmailShallowDTO);
        }
        return null;
    }

    private List<ExchangeEmailShallowDTO> queryAllEmails() {
        return null;
    }


    /**
     * 抽取邮件任务
     *
     * @author tengfei.fangtf
     */
    public class ExtractEmailTask extends Thread {
        public void run() {
            extractEmail();
        }
    }
}

