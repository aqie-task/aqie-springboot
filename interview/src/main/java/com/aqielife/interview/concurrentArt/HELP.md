
##### Context
1. 减少上下文切换
   1. 无锁并发编程：将数据的ID按照Hash算法取模分段,不同的线程处理不同段的数
   2. CAS算法。Java的Atomic
   3. 使用最少线程
   4. 协程

2. 上下文切换工具
   1. vmstat 可以测量上下文切换的次数
   2. Lmbench3 可以测量上下文切换的时长
   
#### DeadLock
1. 避免死锁
> * 避免一个线程同时获取多个锁。
> * 避免一个线程在锁内同时占用多个资源,尽量保证每个锁只占用一个资源。
> * 尝试使用定时锁,使用lock.tryLock(timeout)来替代使用内部锁机制。
> * 对于数据库锁,加锁和解锁必须在一个数据库连接里,否则会出现解锁失败的情况。
 

#### 实际业务场景
1. 上传附件
   1. 用户上传附件 -> 任务队列  -> 消费者从队列取文件
2. 调用远程接口
   1. 提供申请查询接口 -> 查询任务入库 -> 服务器端用线程轮询并获取申请任务进行处理
   2. 处理完发送数据给调用方 -> 调用方再来调用另外一个接口取数据
3. 资源限制
   1. 硬件：带宽的上传/下载速度、硬盘读写速度和CPU的处理速度
   2. 软件：数据库的连接数和socket连接数
   3. 服务器复用，数据ID%机器数。 资源池复用。线程数量比数据库连接大很多会造成线程阻塞

#### java并发机制
1. 原理： 并发机制依赖于JVM的实现和CPU的指令
2. volatile实现原则: 
   1. Lock前缀指令会引起处理器缓存回写到内存
   2. 一个处理器的缓存回写到内存会导致其他处理器的缓存无效
   3. synchronized 原理：JVM基于进入和退出Monitor对象来实现方法同步和代码块同步，锁存在java对象头
      ·对于普通同步方法,锁是当前实例对象。
      ·对于静态同步方法,锁是当前类的Class对象。
      ·对于同步方法块,锁是Synchonized括号里配置的对象。
3. 锁状态：
   1. 无锁状态、
   2. 偏向锁状态 :适用只有一个线程访问同步块 
      1. 对象头和栈帧中的锁记录里存储锁偏向的线程ID
      2. 应用程序里所有的锁通常情况下处于竞争状态,可以通过JVM参数关闭偏向锁:-XX:-UseBiasedLocking=false
   3. 轻量级锁状态 ：追求响应时间
      1. CAS将对象头中的Mark Word替换为指向锁记录的指针。如果成功,当前线程获得锁，失
         败,表示其他线程竞争锁,当前线程便尝试使用自旋来获取锁
   4. 重量级锁状态 : 追求吞吐量

4. CAS 问题及场景
   1. ABA:使用版本号; AtomicStampedReference
   2. 循环时间长开销大
   3. 只能保证一个共享变量的原子操作: AtomicReference类来保证引用对象之间的原子性,就可以把多个变量放在一个对象里来进行CAS操作
   
5. 之前提到锁(如Mutex和ReentrantLock)基本都是排他锁,这些锁在同一时刻只允许一个线
   程进行访问,而读写锁在同一时刻可以允许多个读线程访问,但是在写线程访问时,所有的读
   线程和其他写线程均被阻塞。读写锁维护了一对锁,一个读锁和一个写锁,通过分离读锁和写
   锁,使得并发性相比一般的排他锁有了很大提升
6. 锁降级是指把持住(当前拥有的)写锁,再获取到读锁,随后释放(先前拥有的)写锁的过程
#### Java 内存模型
1. 实例域、静态域和数组元素都存储在堆内存