package com.aqielife.interview.concurrentArt.practice.queue;

import java.util.concurrent.ArrayBlockingQueue;

public class ArticleBlockingQueue<T> extends ArrayBlockingQueue<T> {
    public ArticleBlockingQueue(int capacity) {
        super(capacity);
    }
}
