package com.aqielife.interview.concurrentArt.practice;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;

/**
 * 消息队列管理
 */
@Slf4j
public class MsgQueueManager implements IMsgQueue {

    /**
     * 消息总队列
     */
    public final BlockingQueue<Message> messageQueue;

    private final List<BlockingQueue<Message>> subMsgQueues = new ArrayList<>();
    private MsgQueueManager() {
        messageQueue = new LinkedTransferQueue<Message>();
    }

    public static MsgQueueManager getInstance() {
        return new MsgQueueManager();
    }

    @Override
    public void put(Message msg) {
        try {
            messageQueue.put(msg);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public Message take() {
        try {
            return messageQueue.take();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return null;
    }

    /**
     * 均衡获取子队列
     * @return
     */
    public BlockingQueue<Message> getSubQueue() {
        int errorCount = 0;
        for (;;) {
            if (subMsgQueues.isEmpty()) {
                return null;
            }
            int index = (int) (System.nanoTime() % subMsgQueues.size());
            try {
                return subMsgQueues.get(index);
            } catch (Exception e) {
                // 出现错误表示,在获取队列大小之后,队列进行了一次删除操作
                log.error("获取子队列出现错误", e);
                if ((++errorCount) < 3) {
                    continue;
                }
            }
        }
    }
}
