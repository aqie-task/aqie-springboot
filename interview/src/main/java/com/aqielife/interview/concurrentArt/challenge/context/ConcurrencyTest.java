package com.aqielife.interview.concurrentArt.challenge.context;

public class ConcurrencyTest {
    long wan = 10000l;
    static long million = 1000000l; // 百万

    static long billion = 1000000000l;
    private static final long count = billion;
    public static void main(String[] args) throws InterruptedException {
        concurrency();
        serial();
    }
    private static void concurrency() throws InterruptedException {
        long start = System.currentTimeMillis();
        final int[] a = {0};
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                for (long i = 0; i < count; i++) {
                    a[0] += 5;
                }
            }
        });
        thread.start();
        int b = 0;
        for (long i = 0; i < count; i++) {
            b--;
        }
        long time = System.currentTimeMillis() - start;
        thread.join();
        System.out.println("concurrency :" + time+"ms,b="+b+",a="+ a[0]);
    }
    private static void serial() {
        long start = System.currentTimeMillis();
        int a = 0;
        for (long i = 0; i < count; i++) {
            a += 5;
        }
        int b = 0;
        for (long i = 0; i < count; i++) {
            b--;
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("serial:" + time+"ms,b="+b+",a="+a);
    }
}
