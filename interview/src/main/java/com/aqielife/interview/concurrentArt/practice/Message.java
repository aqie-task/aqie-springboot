package com.aqielife.interview.concurrentArt.practice;

import lombok.Data;

@Data
public class Message {
    private Integer id;
    private Object content;
}
