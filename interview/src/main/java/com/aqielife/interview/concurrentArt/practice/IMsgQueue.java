package com.aqielife.interview.concurrentArt.practice;

public interface IMsgQueue {
    void put(Message msg);
    Message take();
}
