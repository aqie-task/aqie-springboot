package com.aqielife.interview.concurrentPractice.amdahl;

import com.aqielife.interview.concurrentPractice.lock.Account;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;

import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
public class Interaction {

    private Integer Capacity = 100000;

    private Integer THRESHOLD = 1000;

    private  int nTrials, nPairs;

    private CyclicBarrier barrier;

    private final AtomicInteger putSum = new AtomicInteger(0);
    private final AtomicInteger takeSum = new AtomicInteger(0);

    private static final ExecutorService pool = Executors.newCachedThreadPool();
    /**
     * 1. 测试资源泄漏
     */
    class Big{
        double[] data = new double[Capacity];

        void testLeak() throws InterruptedException {
            BounderBuffer<Big> bb = new BounderBuffer<>(Capacity);
            int heapSize1 = Capacity; // heap快照
            for (int i = 0; i<Capacity;i++) {
                bb.put(new Big());
            }

            for (int i = 0; i < Capacity; i++) {
                bb.take();
            }
            int heapSize2 = Capacity;
            Assertions.assertTrue(Math.abs(heapSize2-heapSize1) < THRESHOLD);
        }
    }

    /**
     * 测试 TheadPoolExecutor 线程工厂
     */
    class TestingTheadFactory implements ThreadFactory {
        public final AtomicInteger numCreated = new AtomicInteger();
        private final ThreadFactory factory = Executors.defaultThreadFactory();

        public Thread newThread(Runnable r) {
            numCreated.incrementAndGet();
            return factory.newThread(r);
        }
    }

    /**
     * 线程池扩展的测试方法
     */
    public void testPoolExpansion() throws InvalidTypeIdException, InterruptedException {
        int MAX_SIZE = 10;
        ExecutorService exec = Executors.newFixedThreadPool(MAX_SIZE);
        for (int i = 0; i<10 * MAX_SIZE; i++) {
            exec.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(Long.MAX_VALUE);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        TestingTheadFactory threadFactory = null;
        for (int i = 0; i < 10 && threadFactory.numCreated.get() < MAX_SIZE; i++);

        for (int i = 0; i<20 && threadFactory.numCreated.get() < MAX_SIZE; i++) {
            Thread.sleep(100);
        }

        Assertions.assertEquals(threadFactory.numCreated.get(), MAX_SIZE);
        exec.shutdown();
    }


    /**
     * Thread.yield 产生更多交替操作
     */
    public synchronized void transferCredits(Account from, Account to, int amount) {
        /*from.setMoney(from.getMoney() - amount);
        if (THRESHOLD < Random.nextInt(1000)) {
            Thread.yield();
        }
        to.setMoney(to.getMoney() + amount);*/
    }

    /**
     * 关卡的计时器
     */
    public class BarrierTimer implements Runnable {
        private boolean started;
        private long startTime, endTime;

        public synchronized void run () {
            long t =  System.nanoTime();
            if (!started) {
                started = true;
                startTime = t;
            } else {
                endTime = t;
            }
        }

        public synchronized void clear() {
            started = false;
        }

        public synchronized long getTime() {
            return endTime - startTime;
        }
    }



    /**
     * 关卡计时器进行测试
     */
    public void test() throws BrokenBarrierException, InterruptedException {
        BarrierTimer timer = new BarrierTimer();
        timer.clear();
        for (int i = 0; i < nPairs; i++) {
           pool.execute(new Producer());
           pool.execute(new Consumer());
        }
        barrier.await();
        barrier.await();
        long nsPerItem = timer.getTime() / (nPairs * (long)nTrials);
        Assertions.assertEquals(putSum.get(), takeSum.get());
    }

}
