package com.aqielife.interview.concurrentPractice.lock.buff;

import javax.annotation.concurrent.Immutable;
import java.util.concurrent.atomic.AtomicReference;

/**
 * CAS 避免多元不变约束
 */
public class CasNumberRange {
    @Immutable
    private static class IntPair {
        // 不变约束
        final int lower;
        final int upper;
        public IntPair(int lower, int upper) {
            this.lower = lower;
            this.upper = upper;
        }
    }

    private final AtomicReference<IntPair> values = new AtomicReference<>(new IntPair(0, 0));

    public int getLower() {
        return values.get().lower;
    }

    public int getUpper() {
        return values.get().upper;
    }

    public void setLower(int i) {
        while (true) {
            IntPair oldv = values.get();
            if (i > oldv.upper) {
                throw new IllegalArgumentException("Cant set lower to" + i + " > upper");
            }
            IntPair newV = new IntPair(i, oldv.upper);
            if (values.compareAndSet(oldv, newV)){
                return;
            }
        }
    }
}
