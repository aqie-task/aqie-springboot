package com.aqielife.interview.concurrentPractice.futureTask.travel;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 旅行信息
 */
@AllArgsConstructor
@Data
public class TravelInfo {
    /**
     * 景点
     */
    private String name;
    private int day;

}
