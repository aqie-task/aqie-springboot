package com.aqielife.interview.concurrentPractice.futureTask.puzzle;

import java.util.Set;

/**
 * https://www.puzzleworld.org/SlidingBlockPuzzles/
 * @param <P>
 * @param <M>
 */
public interface Puzzle<P, M> {
    P initialPosition();
    boolean isGoal(P position);
    Set<M> legalMoves(P position);
    P move(P position, M move);
}
