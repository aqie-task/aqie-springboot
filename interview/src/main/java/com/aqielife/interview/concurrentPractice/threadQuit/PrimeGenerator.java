package com.aqielife.interview.concurrentPractice.threadQuit;

import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.springframework.scheduling.config.Task;

import javax.annotation.concurrent.ThreadSafe;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;


/**
 * 线程取消
 */
@ThreadSafe
public class PrimeGenerator implements Runnable{
    private static final ExecutorService exec = Executors.newSingleThreadExecutor();

    private final List<BigInteger> primes = new ArrayList<>();
    private volatile boolean cancelled;
    @Override
    public void run() {
        BigInteger p = BigInteger.ONE;
        while (!cancelled) {
            p = p.nextProbablePrime();
            synchronized (this) {
                primes.add(p);
            }
        }
    }

    public void cancel(){
        cancelled = true;
    }
    public synchronized List<BigInteger> get() {
        return new ArrayList<>(primes);
    }

    List<BigInteger> aSecondOfPrimes() throws InterruptedException {
        PrimeGenerator generator = new PrimeGenerator();
        new Thread(generator).start();
        try {
            SECONDS.sleep(1);
        }finally {
            generator.cancel();
        }
        return generator.get();
    }

    /**
     * 通过使用中断取消
     */
    class PrimeProducer extends Thread {
        private final BlockingQueue<BigInteger> queue;
        PrimeProducer(BlockingQueue<BigInteger> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            try {
                BigInteger p = BigInteger.ONE;
                while (!Thread.currentThread().isInterrupted()) {
                    queue.put(p=p.nextProbablePrime());
                }
            } catch (InterruptedException e) {
                // 允许线程退出
            }
        }
        public void  cancel() {
            interrupt();
        }
    }

    /**
     * 不可取消任务在退出前保存中断
     */
    public Task getNextTask(BlockingQueue<Task> queue){
        boolean interrupted = false;
        try {
            while (true) {
                try {
                    return queue.take();
                } catch (InterruptedException e) {
                    interrupted = true;
                    // 失败并重试
                }
            }
        } finally {
            if (interrupted)
                Thread.currentThread().interrupt();
        }
    }


    /**
     * 取消任务 1
     * 在专门线程中断任务
     */
    public static void timedRun(final  Runnable r, long timeout, TimeUnit unit)
            throws InterruptedException {
        class RethrowableTask implements Runnable {
            private volatile Throwable t;

            @Override
            public void run() {
                try {
                    r.run();
                } catch (Throwable t) {
                    this.t = t;
                }
            }
            void rethrow() {
                if (t != null) {
                    throw new RuntimeException();
                }
            }
        }
        RethrowableTask task = new RethrowableTask();
        final Thread taskThread = new Thread(task);
        taskThread.start();
        ListeningScheduledExecutorService cancelExec = MoreExecutors.listeningDecorator(Executors
                .newScheduledThreadPool(8));
        cancelExec.schedule(new Runnable(){

            @Override
            public void run() {
                taskThread.interrupt();
            }
        }, timeout, unit);
        taskThread.join(unit.toMillis(timeout));
        task.rethrow();
    }

    /**
     * 取消任务 1
     * 通过future 取消
     */
    public static void timedRun2(Runnable r, long timeout, TimeUnit unit) throws InterruptedException {
        Future<?> task = exec.submit(r);
        try {
           task.get(timeout, unit);
        } catch (TimeoutException e) {
            // 下面任务会被取消
        } catch(ExecutionException e) {
            // task 抛出异常重新抛出
        } finally {
            // 如果任务已经结束
            task.cancel(true);
        }
    }

    /**
     * Thread 通过覆写interrupt封装非标准取消
     */
    public class ReaderThread extends Thread {
        private final Socket socket;
        private final InputStream in;
        public ReaderThread(Socket socket) throws IOException {
            this.socket = socket;
            this.in = socket.getInputStream();
        }

        public void  interrupt() {
            try {
                socket.close();
            } catch (IOException ignored) {

            } finally {
                super.interrupt();
            }
        }

        @Override
        public void run() {
            try {
                byte[] buf = new byte[1024];
                while (true) {
                    int count = in.read(buf);
                    if (count < 0) {
                        break;
                    } else if (count > 0) {
                        processBuffer(buf, count);
                    }
                }
            } catch (IOException e){
                // 允许线程退出
            }
        }

        private void processBuffer(byte[] buf, int count) {
        }
    }
}
