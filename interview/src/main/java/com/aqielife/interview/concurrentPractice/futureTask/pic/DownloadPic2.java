package com.aqielife.interview.concurrentPractice.futureTask.pic;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/**
 * 每下载一个图片，就创建一个任务，并在线程池中执行
 */
@Slf4j
public class DownloadPic2 {
    private final ExecutorService executorService;

    public DownloadPic2(ExecutorService executorService) {
        this.executorService = executorService;
    }

    void download() {
        final List<String> imageUrls = new ArrayList<>(Arrays.asList("1.jpg", "2.jpg"));
        CompletionService<String> completionService =
                new ExecutorCompletionService<>(executorService);
        for (final String imgUrl : imageUrls) {
            Future<String> submit = completionService.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    Thread.sleep(4000);
                    return imgUrl;
                }
            });
        }

        try {
            for (int i = 0,n=imageUrls.size(); i <n;i++ ) {
                Future<String> f = completionService.take();
                String image = f.get();
                renderPic(image);
            }
        }catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }catch (ExecutionException e) {
            throw new RuntimeException();
        }
    }

    void renderPic(String pic){
        log.info("{}", pic);
    }

    public static void main(String[] args) {
        DownloadPic2 downloadPic2 = new DownloadPic2(Executors.newCachedThreadPool());
        downloadPic2.download();
    }
}
