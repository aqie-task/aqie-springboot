package com.aqielife.interview.concurrentPractice.futureTask.pic;



import lombok.Data;

import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

public class Parallel {
    public<T> void parallelRecursive(final Executor exec, List<Node<T>> nodes, final Collection<T> results) {
        for (final Node<T> n : nodes) {
            exec.execute(new Runnable() {
                @Override
                public void run() {
                    //results.add(n.compute());
                }
            });
            parallelRecursive(exec, n.getChildren(),results);
        }
    }

    public<T> Collection<T> getParallelResults(List<Node<T>> nodes) throws InterruptedException{
        ExecutorService exec = Executors.newCachedThreadPool();
        Queue<T> resultQueue = new ConcurrentLinkedQueue<>();
        parallelRecursive(exec, nodes, resultQueue);
        exec.shutdown();
        exec.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        return resultQueue;
    }

    @Data
    class Node<A> {
        private List<Node<A>> children;
        public Node<A> compute(){
            return null;
        }
    }
}
