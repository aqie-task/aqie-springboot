package com.aqielife.interview.concurrentPractice.amdahl;

import com.aqielife.interview.utils.Random;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer implements Runnable {
    private CyclicBarrier barrier;
    private  BounderBuffer<Integer> bb;
    private  int nTrials, nPairs;

    private final AtomicInteger putSum = new AtomicInteger(0);
    private final AtomicInteger takeSum = new AtomicInteger(0);


    @Override
    public void run() {
        int seed = (this.hashCode()) ^ (int) System.nanoTime();
        int sum = 0;
        try {
            barrier.await();
            for (int i = nTrials; i > 0; --i) {
                bb.put(seed);
                sum += seed;
                seed = Random.xOrShift(seed);
            }
            putSum.getAndAdd(sum);
            barrier.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (BrokenBarrierException e) {
            throw new RuntimeException(e);
        }
    }
}
