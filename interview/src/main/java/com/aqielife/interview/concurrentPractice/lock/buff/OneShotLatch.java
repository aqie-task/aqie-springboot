package com.aqielife.interview.concurrentPractice.lock.buff;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.locks.AbstractQueuedLongSynchronizer;

@ThreadSafe
public class OneShotLatch {
    private final Sync sync = new Sync();

    public void signal() {
        sync.releaseShared(0);
    }

    public void await() throws InterruptedException {
        sync.acquireSharedInterruptibly(0);
    }

    private class Sync extends AbstractQueuedLongSynchronizer {
        protected int tryAcquireShared(int ignored) {
            // 如果闭锁打开则成功 (state == 1)
            return (getState() == 1 ? 1 : -1);
        }

        protected boolean tryReleaseShared(int ignored) {
            setState(1);    // 闭锁现在已打开
            return true;
        }
    }

    /**
     * 非公平的 ReentrantLock 中 tryAcquire
     */
    protected boolean tryAcquire(int ignored) {
       final Thread current = Thread.currentThread();
       int c = getState();
        Thread owner = Thread.currentThread();
        if (c == 0) {
           if (compareAndSetState(0, 1)) {
               owner = current;
               return true;
           }
       } else if (current == owner) {
           setState(c + 1);
           return true;
       }
       return false;
    }

    private void setState(int i) {

    }

    private int getState() {
        return 0;
    }

    protected int tryAcquireShared(int acquires) {
        while (true) {
            int available = getState();
            int remaining = available - acquires;
            if (remaining < 0 || compareAndSetState(available, remaining)) {
                return remaining;
            }
        }
    }

    private boolean compareAndSetState(int available, int remaining) {
        return false;
    }

    protected boolean tryReleaseShared(int releases) {
        while (true) {
            int p = getState();
            if (compareAndSetState(p, p+releases)) {
                return true;
            }
        }
    }
}
