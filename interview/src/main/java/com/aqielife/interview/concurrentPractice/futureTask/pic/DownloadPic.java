package com.aqielife.interview.concurrentPractice.futureTask.pic;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/**
 * 使用Future 等待图像下载
 */
@Slf4j
public class DownloadPic {
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

    static void download(){
        final List<String> imageUrls = new ArrayList<>(Arrays.asList("1.jpg","2.jpg"));
        Callable<List<String>> task = new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                List<String> pics = new ArrayList<>();
                // download
                for (String imageUrl : imageUrls) {
                    Thread.sleep(4000);
                    pics.add(imageUrl);
                }
                return pics;
            }
        };
        Future<List<String>> future = executorService.submit(task);
        try {
            List<String> pics = future.get();
            for (String pic : pics){
                renderPic(pic);
            }
        } catch (InterruptedException e) {
            // 重新声明线程的中断状态
            Thread.currentThread().interrupt();
            // 不需要结果，取消任务
            future.cancel(true);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    static void renderPic(String pic){
        log.info("{}", pic);
    }

    public static void main(String[] args) {
        download();
    }
}
