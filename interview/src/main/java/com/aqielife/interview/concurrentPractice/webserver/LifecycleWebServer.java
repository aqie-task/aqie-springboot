package com.aqielife.interview.concurrentPractice.webserver;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.http.HttpRequest;
import java.util.concurrent.*;

@Slf4j
public class LifecycleWebServer {
    private final ExecutorService exec = Executors.newCachedThreadPool();

    public void start() throws IOException {

        ServerSocket socket = new ServerSocket(80);
        while (!exec.isShutdown()) {
            try {
                final Socket conn = socket.accept();
                exec.execute(new Runnable() {
                    @Override
                    public void run() {
                        handleRequest(conn);
                    }
                });
            } catch (RejectedExecutionException e) {
                if (!exec.isShutdown()) {
                    log.info("task submission rejected", e);
                }
            }
        }
    }

    public void stop() {
        exec.shutdown();
    }


    private void handleRequest(Socket conn) {
        HttpRequest req = readRequest(conn);
        if (isShutdownRequest(req)) {
            stop();
        } else {
            dispatchRequest(req);
        }
    }

    private void dispatchRequest(HttpRequest req) {
    }

    private boolean isShutdownRequest(HttpRequest req) {
        return false;
    }

    private HttpRequest readRequest(Socket conn) {
        return null;
    }
}
