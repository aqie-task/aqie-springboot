package com.aqielife.interview.concurrentPractice;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Visibility {
    private volatile static boolean ready;
    private static int num;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while (!ready) {
                Thread.yield();
            }
            log.info("{}", num);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new ReaderThread().start();

        for(int i=0;i < 100; i++) {
            num ++;
            if (num == 10){
                ready = true;
            }
            Thread.sleep(10);
        }

    }
}
