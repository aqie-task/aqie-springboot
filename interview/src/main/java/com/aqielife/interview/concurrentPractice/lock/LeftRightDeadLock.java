package com.aqielife.interview.concurrentPractice.lock;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@NotThreadSafe
@Slf4j
public class LeftRightDeadLock {
    private static final ExecutorService exec = Executors.newFixedThreadPool(2);
    private final Object left = new Object();
    private final Object right = new Object();
    private Integer TIME = 1000;

    public void leftRight() throws InterruptedException {
        synchronized (left) {
            Thread.sleep(TIME);
            log.info("1");
            synchronized (right) {
                log.info("leftRight");
            }
        }
    }
    public void rightLeft() throws InterruptedException {
        synchronized (right) {
            Thread.sleep(TIME);
            log.info("2");
            synchronized (left) {
                log.info("rightLeft");
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        LeftRightDeadLock lock = new LeftRightDeadLock();
        List<Runnable> list = new ArrayList<>();
        Runnable runnable = () -> {
            try {
                lock.leftRight();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
        Runnable runnable1 = () -> {
            try {
                lock.rightLeft();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
        list.add(runnable);
        list.add(runnable1);
        list.forEach(exec::submit);
        exec.shutdown();

    }
}
