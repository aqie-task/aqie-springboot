package com.aqielife.interview.concurrentPractice.futureTask.travel;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class TravelDemo {
    private static ThreadPoolExecutor exec = new ThreadPoolExecutor(5, 5,
            60 * 1000, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5),
            new MyThreadFactory());

    private class QuoteTask implements Callable<TravelQuote> {
        private final TravelCompany company;
        private final TravelInfo travelInfo;

        public QuoteTask(TravelCompany company, TravelInfo travelInfo) {
            this.company = company;
            this.travelInfo = travelInfo;
        }

        public TravelQuote call() throws Exception {
            return company.solicitQuote(travelInfo);
        }

        public TravelQuote getFailureQuote(Throwable cause) {
            return new TravelQuote(travelInfo.getName(), travelInfo.getDay(), cause.getMessage());
        }

        public TravelQuote getTimeoutQuote(CancellationException e) {
            return new TravelQuote(travelInfo.getName(), travelInfo.getDay(), e.getMessage());
        }
    }

    public List<TravelQuote> getRankedTravelQuotes(
            TravelInfo travelInfo, Set<TravelCompany> companies,
            Comparator<TravelQuote> ranking, long time, TimeUnit unit) throws InterruptedException {
        List<QuoteTask> tasks = new ArrayList<>();
        for (TravelCompany company : companies) {
            tasks.add(new QuoteTask(company, travelInfo));
        }

        List<Future<TravelQuote>> futures = exec.invokeAll(tasks, time, unit);
        List<TravelQuote> quotes = new ArrayList<>(tasks.size());
        Iterator<QuoteTask> taskIter = tasks.iterator();
        for(Future<TravelQuote> f: futures) {
            QuoteTask task = taskIter.next();
            try {
                quotes.add(f.get());
            } catch(ExecutionException e) {
                quotes.add(task.getFailureQuote(e.getCause()));
            } catch(CancellationException e) {
                quotes.add(task.getTimeoutQuote(e));
            }
        }
        quotes.sort(ranking);
        return quotes;
    }

    // 给工作线程自定义名字，方便观察提交的任务被哪个线程执行
    static class MyThreadFactory implements ThreadFactory {
        private AtomicInteger threadNum = new AtomicInteger(1);
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r, String.valueOf(threadNum.getAndIncrement()));
            if (thread.getPriority() != Thread.NORM_PRIORITY) {
                thread.setPriority(Thread.NORM_PRIORITY);
            }
            return thread;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TravelInfo travelInfo = new TravelInfo("泰山", 5);
        Set<TravelCompany> companies = new HashSet<>(Arrays.asList(new TravelCompany("祥云", 100),
                new TravelCompany("哈哈", 200)));

        TravelDemo demo = new TravelDemo();
        List<TravelQuote> quotes = demo.getRankedTravelQuotes(travelInfo, companies, new PriceComparator(), 1, TimeUnit.SECONDS);
        log.info("{}", quotes);
        exec.shutdown();
    }

}
