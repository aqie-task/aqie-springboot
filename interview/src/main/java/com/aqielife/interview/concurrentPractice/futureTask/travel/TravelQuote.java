package com.aqielife.interview.concurrentPractice.futureTask.travel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

/**
 * 旅行路线
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TravelQuote {
    private String companyName;
    private String name;
    private int price;
    private int day;

    private String info;

    public TravelQuote(String name,int price, int day) {
        this.name = name;
        this.price = price;
        this.day = day;
    }

    public TravelQuote(String name, int day, String info) {
        this.name = name;
        this.day = day;
        this.info = info;
    }

    public TravelQuote(String companyName, String name, int price, int day) {
        this.companyName = companyName;
        this.name = name;
        this.price = price;
        this.day = day;
    }
}
