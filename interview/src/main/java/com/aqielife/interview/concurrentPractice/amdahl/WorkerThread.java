package com.aqielife.interview.concurrentPractice.amdahl;

import java.util.concurrent.BlockingQueue;

/**
 * 串行访问任务队列
 */
public class WorkerThread extends Thread{
    private final BlockingQueue<Runnable> queue;
    public WorkerThread(BlockingQueue<Runnable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            try {
                Runnable task = queue.take();
                task.run();
            } catch (InterruptedException e) {
                // 允许线程退出
                break;
            }
        }

    }
}
