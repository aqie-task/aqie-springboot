package com.aqielife.interview.concurrentPractice.amdahl;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class Consumer implements Runnable {

    private CyclicBarrier barrier;
    private  BounderBuffer<Integer> bb;
    private  int nTrials, nPairs;
    private final AtomicInteger takeSum = new AtomicInteger(0);

    @Override
    public void run() {
        try {
            barrier.await();
            int sum = 0;
            for (int i = nTrials; i > 0; --i) {
                sum += bb.take();
            }
            takeSum.getAndAdd(sum);
            barrier.await();
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
