package com.aqielife.interview.concurrentPractice.lock.dispatchTaxi;



import javax.annotation.concurrent.NotThreadSafe;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

@NotThreadSafe
public class Taxi {
    private Point location, destination;
    private final Dispatcher dispatcher;

    public Taxi(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }
    public synchronized Point getLocation() {
        return location;
    }

    public synchronized void setLocation(Point point) {
        this.location = location;
        if (location.equals(destination)) {
            dispatcher.notifyAvailable(this);
        }
    }
}

class Dispatcher {
    private final Set<Taxi> taxis;
    private final Set<Taxi> availableTaixs;

    public Dispatcher() {
        this.taxis = new HashSet<>();
        this.availableTaixs = new HashSet<>();
    }

    public synchronized void notifyAvailable(Taxi taxi){
        availableTaixs.add(taxi);
    }

    public synchronized Image getImage() {
        Image image = new Image();
        for (Taxi taxi:taxis){
            image.drawMarket(taxi.getLocation());
        }
        return image;
    }


    class Image{
        public void drawMarket(Point location) {

        }
    }
}
