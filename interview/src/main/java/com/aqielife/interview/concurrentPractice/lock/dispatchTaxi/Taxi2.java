package com.aqielife.interview.concurrentPractice.lock.dispatchTaxi;



import com.itextpdf.awt.geom.Point;

import javax.annotation.concurrent.ThreadSafe;
import java.util.HashSet;
import java.util.Set;

/**
 * 使用开放式调用避免协作对象间死锁
 */
@ThreadSafe
public class Taxi2 {
    private Point location, destination;
    private final Dispatcher2 dispatcher;

    public Taxi2(Dispatcher2 dispatcher) {
        this.dispatcher = dispatcher;
    }
    public synchronized Point getLocation() {
        return location;
    }

    public synchronized void setLocation(Point point) {
        boolean reachedDestination;
        synchronized (this) {
            this.location = location;
            reachedDestination = location.equals(destination);
        }

        if (reachedDestination) {
            dispatcher.notifyAvailable(this);
        }
    }
}

class Dispatcher2 {
    private final Set<Taxi2> taxis;
    private final Set<Taxi2> availableTaixs;

    public Dispatcher2() {
        this.taxis = new HashSet<>();
        this.availableTaixs = new HashSet<>();
    }

    public synchronized void notifyAvailable(Taxi2 taxi){
        availableTaixs.add(taxi);
    }

    public synchronized Image getImage() {
        Set<Taxi2> copy;
        synchronized (this) {
            copy = new HashSet<Taxi2>(taxis);
        }
        Image image = new Image();
        for (Taxi2 taxi:copy){
            image.drawMarket(taxi.getLocation());
        }
        return image;
    }


    class Image{
        public void drawMarket(Point location) {

        }
    }
}
