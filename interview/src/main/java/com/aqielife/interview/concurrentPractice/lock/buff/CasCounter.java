package com.aqielife.interview.concurrentPractice.lock.buff;

import javax.annotation.concurrent.ThreadSafe;

/**
 * 使用CAS 实现非阻塞计数器
 */
@ThreadSafe
public class CasCounter {
    private SimulatedCAS value;

    public int getValue() {
        return value.get();
    }

    public int increment() {
        int v;
        do {
            v = value.get();
        }
        while (v != value.compareAndSwap(v , v + 1));
        return v + 1;
    }
}
