package com.aqielife.interview.concurrentPractice.futureTask.travel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TravelCompany {
    private String name;
    private int price;

    public TravelCompany(int price) {
        this.price = price;
    }

    public TravelQuote solicitQuote(TravelInfo travelInfo) {
        return new TravelQuote(name, travelInfo.getName(), price, travelInfo.getDay());
    }
}
