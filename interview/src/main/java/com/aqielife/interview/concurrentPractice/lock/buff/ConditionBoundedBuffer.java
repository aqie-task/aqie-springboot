package com.aqielife.interview.concurrentPractice.lock.buff;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.springframework.util.FileCopyUtils.BUFFER_SIZE;

/**
 * 有限缓存使用显示的条件变量
 * @param <T>
 */
@ThreadSafe
public class ConditionBoundedBuffer<T> {
    protected final Lock lock = new ReentrantLock();

    private final Condition notFull = lock.newCondition();

    private final Condition notyEmpty = lock.newCondition();

    private final T[] items = (T[])new Object[BUFFER_SIZE];

    private int tail, head, count;

    // 阻塞 直到not full
    public void put(T x) throws InterruptedException {
        lock.lock();
        try{
            while (count == items.length) {
                notFull.await();
            }
            items[tail] = x;
            if (++tail == items.length){
                tail = 0;
            }
            ++count;
            notyEmpty.signal();
        } finally {
            lock.unlock();
        }
    }

    public T take() throws InterruptedException {
        lock.lock();
        try {
            while (count == 0) {
                notyEmpty.await();
            }
            T x = items[head];
            items[head] = null;
            if (++head == items.length) {
                head = 0;
            }
            --count;
            notFull.signal();
            return x;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {

    }
}
