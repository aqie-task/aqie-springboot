package com.aqielife.interview.concurrentPractice.futureTask.travel;

import java.util.Comparator;

public class PriceComparator implements Comparator<TravelQuote> {

    @Override
    public int compare(TravelQuote o1, TravelQuote o2) {
        return o1.getPrice() - o2.getPrice();
    }
}
