package com.aqielife.interview.concurrentPractice.threadpool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * 线程饥饿死锁
 * jstack -l
 * jconsole
 */
@Slf4j
public class ThreadStarvationDeadLock {
    ExecutorService exec = Executors.newSingleThreadExecutor();
    class RenderTask implements Callable<String> {
        @Override
        public String call() throws Exception {
            Future<String> header, footer;
            header = exec.submit(new LoadFileTask("header.html"));
            footer = exec.submit(new LoadFileTask("footer.html"));
            return header.get() + footer.get();
        }
    }
    static class LoadFileTask implements Callable<String> {
        private String content;
        public LoadFileTask(String s) {this.content = s;}

        @Override
        public String call() throws Exception { return this.content;}
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int N_CPU = Runtime.getRuntime().availableProcessors();
        log.info("{}", N_CPU);
        ThreadStarvationDeadLock lock = new ThreadStarvationDeadLock();
        Future<String> submit = lock.exec.submit(new ThreadStarvationDeadLock().new RenderTask());
        submit.get();
        lock.exec.shutdown();
    }
}
