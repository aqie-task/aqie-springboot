package com.aqielife.interview.concurrentPractice.lock.buff;

import javax.annotation.concurrent.ThreadSafe;

/**
 * 有限缓存使用条件队列
 * @param <V>
 */
@ThreadSafe
public class BoundedBuffer<V> extends BaseBoundedBuffer<V> {
    public BoundedBuffer(int capacity) {
        super(capacity);
    }

    // 条件谓词 not-full
    public synchronized void put(V v) throws InterruptedException{
        while (isFull()) {
            wait();
        }
        doPut(v);
        notifyAll();
    }

    public synchronized V take() throws InterruptedException {
        while (isEmpty()) {
            wait();
        }
        V v = doTake();
        notifyAll();
        return v;
    }
}
