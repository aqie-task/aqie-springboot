package com.aqielife.interview.concurrentPractice.atomic;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 实现随机数字生成器
 */
@ThreadSafe
public class ReentrantLockRandom {
    private final Lock lock = new ReentrantLock(false);
    private int seed;

    public ReentrantLockRandom(int seed) {
        this.seed = seed;
    }

    public int netInt(int n){
        lock.lock();
        try {
            int s = seed;
            seed = calculateNext(s);
            int remainder = s % n;
            return remainder > 0 ? remainder : remainder + n;
        } finally {
            lock.unlock();
        }
    }

    private int calculateNext(int s) {
        return 0;
    }
}
