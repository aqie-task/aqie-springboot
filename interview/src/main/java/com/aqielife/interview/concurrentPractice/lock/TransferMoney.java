package com.aqielife.interview.concurrentPractice.lock;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.concurrent.ThreadSafe;
import javax.naming.InsufficientResourcesException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * hashCode 制定锁执行顺序
 */
@Slf4j
@ThreadSafe
public class TransferMoney {
    private static final ExecutorService exec = Executors.newFixedThreadPool(2);
    private static final Object tieLock = new Object();
    private Integer TIME = 1000;

    public boolean transferMoney(final Account from, final Account to, final Integer amount)
            throws InterruptedException {
        class Helper {
            public boolean transfer() {
                if (from.getMoney() < amount) {
                    log.info("fail {} {}", from.getMoney(), amount);
                    return false;
                } else {
                    from.decrease(amount);
                    to.increase(amount);
                    // log.info("success");
                    return true;
                }
            }
        }
        int fromHash = System.identityHashCode(from);
        int toHash = System.identityHashCode(to);
        if (fromHash < toHash) {
            synchronized (from) {
                synchronized (to) {
                    return new Helper().transfer();
                }
            }
        } else if (toHash < fromHash) {
            synchronized (to) {
                synchronized (from) {
                    return new Helper().transfer();
                }
            }
        } else {
            synchronized (tieLock) {
                synchronized (from) {
                    synchronized (to) {
                       return new Helper().transfer();
                    }
                }
            }
        }
    }

    public boolean transferMoney2(final Account from, final Account to, final Integer amount,
                                  long timeout, TimeUnit unit) throws InterruptedException {
        final Random rnd = new Random();
        long fixedDelay = getFixedDelayComponentNanos(timeout, unit);
        long randMod = getRandomDelayModulusNanos(timeout, unit);
        long stopTime = System.nanoTime() + unit.toNanos(timeout);

        while (true) {
            if (from.lock.tryLock()) {
                try {
                    if (to.lock.tryLock()) {
                        try {
                            if (from.getMoney().compareTo(amount) < 0) {
                                throw new RuntimeException("not enough");
                            } else {
                                from.decrease(amount);
                                to.increase(amount);
                                return true;
                            }
                        } finally {
                            to.lock.unlock();
                        }
                    }
                } finally {
                    from.lock.unlock();
                }
            }
            if (System.nanoTime() < stopTime) {
                return false;
            }
            NANOSECONDS.sleep(fixedDelay + rnd.nextLong() % randMod);
        }
    }

    private long getFixedDelayComponentNanos(long timeout, TimeUnit unit) {
        return 0;
    }

    private long getRandomDelayModulusNanos(long timeout, TimeUnit unit) {
        return 0;
    }

    private static final int NUM_THREADS = 20;
    private static final int NUM_ACCOUNTS = 5;
    private static final int NUM_ITERATIONS = 1000000;
    public static void main(String[] args) throws InterruptedException {
        TransferMoney demo = new TransferMoney();
        final Random rnd = new Random();
        final Account[] accounts = new Account[NUM_ACCOUNTS];
        for (int i=0;i<accounts.length; i++){
            accounts[i] = new Account(String.valueOf(i), 1000000);
        }
        log.info(" accounts {}", Arrays.asList(accounts));

        class TransferThread extends Thread {
            @Override
            public void run() {
                super.run();
                for (int i =0;i<NUM_ITERATIONS;i++) {
                    int from = rnd.nextInt(NUM_ACCOUNTS);
                    int to = rnd.nextInt(NUM_ACCOUNTS);
                    Integer amount = rnd.nextInt(20);
                    try {
                        // log.info("{} {} {}", accounts[from].getMoney(), accounts[to].getMoney(), amount);
                        demo.transferMoney(accounts[from], accounts[to], amount);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }

        for (int i = 0; i < NUM_THREADS; i++) {
            new TransferThread().start();
        }
    }

    private static void test() throws InterruptedException {
        TransferMoney demo = new TransferMoney();

        Account a = new Account("a", 100), b = new Account("b", 200);

        List<Callable<Boolean>> list = new ArrayList<>();
        list.add(() -> demo.transferMoney(a, b, 20));
        list.add(() -> demo.transferMoney(b, a, 40));
        List<Future<Boolean>> futures = exec.invokeAll(list);
        futures.forEach(booleanFuture -> {
            try {
                System.out.println("result " + booleanFuture.get());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        });
        exec.shutdown();
        log.info("from {}", a);
        log.info("from {}", b);
    }
}
