package com.aqielife.interview.concurrentPractice.lock;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Data
@NoArgsConstructor
public class Account {
    private String name;
    private Integer money;

    public Lock lock;

    public Account(String name, Integer money) {
        this.name = name;
        this.money = money;
        this.lock = new ReentrantLock();
    }
    public void increase(int amount){
        money  += amount;
    }

    public void decrease(int amount){
        money -= amount;
    }
}
