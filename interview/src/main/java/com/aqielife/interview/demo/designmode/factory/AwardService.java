package com.aqielife.interview.demo.designmode.factory;

import org.springframework.stereotype.Service;

/**
 * @Author aqie
 * @Date 2022/4/14 11:46
 * @desc 工厂模式+抽象类
 */
@Service("abstractAwardService")
public class AwardService {
    public void reward(String userId, String source) {
        AwardAbstract instance = AwardFactory.getAwardInstance(source);
        if (null != instance) {
            instance.award(userId);
        }
    }
}
