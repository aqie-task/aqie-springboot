package com.aqielife.interview.demo.designmode.factory;

import lombok.extern.slf4j.Slf4j;

// 微信渠道发放奖励业务
@Slf4j
public class WeChatAwardService extends AwardAbstract {
    @Override
    public Boolean award(String userId) {
        log.info("微信渠道用户{}奖励100元红包!", userId);
        return Boolean.TRUE;
    }
}
