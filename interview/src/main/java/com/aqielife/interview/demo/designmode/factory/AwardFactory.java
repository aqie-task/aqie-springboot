package com.aqielife.interview.demo.designmode.factory;

/**
 * @Author aqie
 * @Date 2022/4/14 11:46
 * @desc
 */
public class AwardFactory {
    public static AwardAbstract getAwardInstance(String source) {
        if ("toutiao".equals(source)) {
            return new TouTiaoAwardService();
        } else if ("wx".equals(source)) {
            return new WeChatAwardService();
        }
        return null;
    }
}
