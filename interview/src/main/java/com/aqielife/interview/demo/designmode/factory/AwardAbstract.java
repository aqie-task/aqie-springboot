package com.aqielife.interview.demo.designmode.factory;

/**
 * @Author aqie
 * @Date 2022/4/14 11:44
 * @desc
 */
public abstract class AwardAbstract {
    public abstract Boolean award(String userId);
}
