package com.aqielife.interview.demo.designmode.strategy.award;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WeChatAwardStrategyService extends BaseAwardTemplate implements AwardStrategy {
    /**
     * 奖励发放接口
     */
    @Override
    public Boolean awardStrategy(String userId) {
        return super.awardTemplate(userId);
    }

    @Override
    public String getSource() {
        return "wx";
    }

    /***
     * 具体的业务奖励发放实现
     */
    @Override
    protected Boolean awardRecord(String userId) {
        log.info("微信渠道用户{}奖励100元红包!", userId);
        return Boolean.TRUE;
    }
}