package com.aqielife.interview.demo.designmode.strategy.order.handler;

import com.aqielife.interview.demo.designmode.strategy.order.AOrderTypeHandler;
import com.aqielife.interview.demo.designmode.strategy.order.OrderTypeEnum;
import com.aqielife.interview.demo.designmode.strategy.order.OrderTypeHandlerAnno;
import com.aqielife.interview.demo.designmode.strategy.order.dto.OrderDTO;
import org.springframework.stereotype.Component;

/**
 * 促销订单处理
 */
@Component
@OrderTypeHandlerAnno(OrderTypeEnum.Promotion)
public class PromotionOrderHandler extends AOrderTypeHandler {

    @Override
    public String handler(OrderDTO dto) {
        return "处理促销订单";
    }

}