package com.aqielife.interview.demo.designmode.strategy.order.service.impl;

import com.aqielife.interview.demo.designmode.strategy.order.AOrderTypeHandler;
import com.aqielife.interview.demo.designmode.strategy.order.HandlerContext;
import com.aqielife.interview.demo.designmode.strategy.order.dto.OrderDTO;
import com.aqielife.interview.demo.designmode.strategy.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author aqie
 * @Date 2022/4/14 11:57
 * @desc
 */
@Component
public class OrderServiceImpl implements IOrderService {
    //使用策略模式实现
    @Autowired
    private HandlerContext handlerContext;

    @Override
    public String orderHandler(OrderDTO dto) {
        /*
         * 1：使用if..else实现
         * 2：使用策略模式实现
         */
        AOrderTypeHandler instance = handlerContext.getInstance(dto.getType());
        return instance.handler(dto);
    }
}
