package com.aqielife.interview.demo.designmode.strategy.award;

/** 策略业务接口 */
public interface AwardStrategy {
    /**
     * 奖励发放接口
     */
    Boolean awardStrategy(String userId);

    /**
     * 获取策略标识,即不同渠道的来源标识
     */
    String getSource();
}
