package com.aqielife.interview.demo.designmode.strategy.award;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author aqie
 * @Date 2022/4/14 11:48
 * @desc 定义奖励发放模板流程
 */
@Slf4j
public abstract class BaseAwardTemplate {

    //奖励发放模板方法
    public Boolean awardTemplate(String userId) {
        this.authentication(userId);
        this.risk(userId);
        return this.awardRecord(userId);
    }

    //身份验证
    protected void authentication(String userId) {
        log.info("{} 执行身份验证!", userId);
    }

    //风控
    protected void risk(String userId) {
        log.info("{} 执行风控校验!", userId);
    }

    //执行奖励发放
    protected abstract Boolean awardRecord(String userId);
}
