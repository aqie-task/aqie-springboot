package com.aqielife.interview.demo.designmode.strategy.order;

import lombok.Getter;

/**
 * @Author aqie
 * @Date 2022/4/14 11:59
 * @desc
 */
@Getter
public enum OrderTypeEnum {
    Normal("1", "普通"),
    Group("2", "团队"),
    Promotion("3", "促销");

    private String code;    //代码
    private String name;    //名称，描述

    OrderTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
