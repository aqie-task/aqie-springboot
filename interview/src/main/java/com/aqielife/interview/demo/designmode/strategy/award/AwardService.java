package com.aqielife.interview.demo.designmode.strategy.award;

import org.springframework.stereotype.Service;

/**
 * @Author aqie
 * @Date 2022/4/14 11:50
 * @desc 策略模式+模板方法+工厂模式+单例模式
 */
@Service("strategyAwardService")
public class AwardService {
    public void reward(String userId, String source) {
        AwardStrategyFactory.getInstance().getAwardResult(userId, source);
    }
}
