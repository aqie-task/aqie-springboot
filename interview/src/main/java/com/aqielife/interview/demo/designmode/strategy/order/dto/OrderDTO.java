package com.aqielife.interview.demo.designmode.strategy.order.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author aqie
 * @Date 2022/4/14 11:56
 * @desc
 */
@Data
public class OrderDTO {
    private String code;
    private BigDecimal price;

    /*
     * 订单类型：
     * 1：普通订单
     * 2：团购订单
     * 3：促销订单
     */
    private String type;
}
