package com.aqielife.interview.demo.designmode.strategy.order;

import com.aqielife.interview.demo.designmode.strategy.order.dto.OrderDTO;

/**
 * 订单类型处理定义
 * 使用抽象类，那么子类就只有一个继承了
 */
public abstract class AOrderTypeHandler {

    /**
     * 一个订单类型做一件事
     *
     * @param dto 订单实体
     * @return 为了简单，返回字符串
     */
    abstract public String handler(OrderDTO dto);

}
