package com.aqielife.interview.demo.designmode.factory;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author aqie
 * @Date 2022/4/14 11:45
 * @desc
 */
@Slf4j
public class TouTiaoAwardService extends AwardAbstract {
    @Override
    public Boolean award(String userId) {
        log.info("头条渠道用户{}奖励50元红包!", userId);
        return Boolean.TRUE;
    }
}


