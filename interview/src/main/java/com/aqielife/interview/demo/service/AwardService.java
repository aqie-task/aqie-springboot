package com.aqielife.interview.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * @Author aqie
 * @Date 2022/4/13 16:19
 * @desc 函数式接口
 */
@Slf4j
@Service("functionalAwardService")
public class AwardService {
    private Map<String, BiFunction<String, String, Boolean>> sourceMap = new HashMap<>();

    @PostConstruct
    private void dispatcher() {
        sourceMap.put("wx", (userId, source) -> this.wxReward(userId));
        sourceMap.put("tt", (userId, source) -> this.toutiaoReward(userId));
    }

    public Boolean getRewardResult(String userId, String source) {
        BiFunction<String, String, Boolean> result = sourceMap.get(source);
        if (null != result) {
            return result.apply(userId, source);
        }
        return Boolean.FALSE;
    }

    private Boolean toutiaoReward(String userId) {
        log.info("头条渠道用户{}奖励50元红包!", userId);
        return Boolean.TRUE;
    }

    private Boolean wxReward(String userId) {
        log.info("微信渠道用户{}奖励100元红包!", userId);
        return Boolean.TRUE;
    }
}
