package com.aqielife.interview.demo.designmode.strategy.order.service;

import com.aqielife.interview.demo.designmode.strategy.order.dto.OrderDTO;

/**
 * @Author aqie
 * @Date 2022/4/14 11:56
 * @desc
 */
public interface IOrderService {
    /**
     * 根据订单的不同类型做出不同的处理
     *
     * @param dto 订单实体
     * @return 为了简单，返回字符串
     */
    String orderHandler(OrderDTO dto);
}
