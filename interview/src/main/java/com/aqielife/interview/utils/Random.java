package com.aqielife.interview.utils;

public class Random {
    public static int xOrShift(int y){
        y ^= (y << 6);
        y ^= (y >>> 21);
        y ^= (y << 7);
        return y;
    }

    public static void main(String[] args) {
        System.out.println(xOrShift(1));
    }
}
