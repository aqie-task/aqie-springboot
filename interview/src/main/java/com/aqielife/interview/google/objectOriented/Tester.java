package com.aqielife.interview.google.objectOriented;

public class Tester {
    public static void main(String[] args) {
        LinkedList2<Integer> list = LinkedList2.newEmptyList();
        for (int i = 0; i < 10; ++i){
            list.add(i);
        }
        for (Integer value : list){
            System.out.print(value);
        }
    }
}
