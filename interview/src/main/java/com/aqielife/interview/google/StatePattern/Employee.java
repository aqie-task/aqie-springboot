package com.aqielife.interview.google.StatePattern;

import java.util.List;
import java.util.Objects;

public class Employee {
    private final String name;
    private final int salary;
    static List<Employee> allEmployees;
    public int age;
    private Role role;

    public Employee(String name, int salary, Role role) {
        this.name = name;
        this.salary = salary;
        this.role = role;
    }

    public Employee(String name) {
        this(name, 0,null);
    }

    public void doWork(){
        role.doWork();
    }

    public void getPaid(BankEndPoint bank){
        bank.payment(name, salary);
    }

    /**
     * Package private for login in the package to control
     */
    static void loadAllEmployees(){
        // loads all employees from database to store in allEmployees
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return salary == employee.salary &&
                Objects.equals(name, employee.name)
                && Objects.equals(role, employee.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, salary,role);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", role=" + role +
                '}';
    }

    String getName() {
        return name;
    }

    int getSalary() {
        return salary;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
