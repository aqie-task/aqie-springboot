package com.aqielife.interview.google.task2;


// public class CodingTask extends LoggingTask{
public class CodingTask implements Runnable {
    private final int employeeId;
    public CodingTask(int employeeId){
        this.employeeId = employeeId;
    }

    // Runnable 是null; Callable 有返回值
    @Override
    public void run() {
        System.out.println("Employee " + employeeId + "Writing code");
        try {
            Thread.sleep(5000);             // 休眠五秒钟
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
