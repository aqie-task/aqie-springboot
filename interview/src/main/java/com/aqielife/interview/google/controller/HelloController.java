package com.aqielife.interview.google.controller;

public class HelloController {
    public static void main(String[] args) {
        System.out.println("hello world");
        testBox();
    }

    static void testBox(){
        System.out.println(new Integer(2) == 2);                            // true
        System.out.println(new Integer(2) == new Integer(2));        //  false
        System.out.println(Integer.valueOf(1000) == Integer.valueOf(1000));        // true/false
        System.out.println(Integer.valueOf(2).intValue() == 2);                    // true
        System.out.println(new Integer(2).equals(new Integer(2)));   // true,判断的值
    }
}
