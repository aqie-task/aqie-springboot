package com.aqielife.interview.google.tree;

public class TreeTravel {
    public void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.getValue());
        preOrder(root.getLeft());
        preOrder(root.getRight());
    }

    public void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.getLeft());
        System.out.print(root.getValue());
        inOrder(root.getRight());
    }

    public void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        postOrder(root.getLeft());
        postOrder(root.getRight());
        System.out.print(root.getValue());
    }

    public String postOrder(String preOrder, String inOrder) {
        if (preOrder.isEmpty()) {
            return "";
        }

        char rootValue = preOrder.charAt(0);
        int rootIndex = inOrder.indexOf(rootValue);

        return
                postOrder(
                        preOrder.substring(1, 1 + rootIndex),
                        inOrder.substring(0, rootIndex)) +
                        postOrder(
                                preOrder.substring(1 + rootIndex),
                                inOrder.substring(1 + rootIndex)) +
                        rootValue;
    }

    public static void main(String[] args) {
        TreeCreator creator = new TreeCreator();
        TreeTravel travel = new TreeTravel();
        System.out.println("Sample tree traversal");
        TreeNode sampleTree = creator.createSampleTree();
        travel.preOrder(sampleTree);
        System.out.println();
        travel.inOrder(sampleTree);
        System.out.println();
        travel.postOrder(sampleTree);
        System.out.println();

        System.out.println("Creating tree from preOrder and inOrder");
        TreeNode tree = creator.createTree("ABDEGCF", "DBGEACF");
        travel.postOrder(tree);
        System.out.println();
        travel.postOrder(creator.createTree("AB", "BA"));
        System.out.println();

        System.out.println("Generating postOrder directly");
        System.out.println("=====");
        System.out.println(
                travel.postOrder("ABDEGCF", "DBGEACF"));
        System.out.println(
                travel.postOrder("", ""));
    }
}
