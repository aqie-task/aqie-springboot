package com.aqielife.interview.google.task2;

public class Tester {
    public static void main(String[] args) {
        new TransactionalRunnable(
                new LoggingRunnable(
                        new CodingTask(1)
                )
        ).run();
    }
}
