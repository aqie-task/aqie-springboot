package com.aqielife.interview.google.StatePattern;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Manager implements Role {
    private final List<Employee> reporters;

    public Manager(List<Employee> reporters){
        this.reporters = Collections.unmodifiableList(new ArrayList<>(reporters));
    }

    @Override
    public void doWork() {
        System.out.println("dispatching work");
        Employee worker = selectReporter();
        worker.doWork();
    }

    // select a  reporter to do work
    private Employee selectReporter(){
        return reporters.get(0);
    }

    @Override
    public String toString() {
        return "Manager";
    }
}
