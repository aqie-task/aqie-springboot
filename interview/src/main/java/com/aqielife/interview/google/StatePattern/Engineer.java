package com.aqielife.interview.google.StatePattern;

public class Engineer implements Role{
    @Override
    public void doWork() {
        System.out.println("Doing engineer work.");
    }

    @Override
    public String toString() {
        return "engineer";
    }
}
