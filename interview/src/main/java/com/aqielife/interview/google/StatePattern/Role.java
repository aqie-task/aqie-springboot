package com.aqielife.interview.google.StatePattern;

public interface Role {
    void doWork();
}
