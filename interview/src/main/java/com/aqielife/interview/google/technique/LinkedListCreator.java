package com.aqielife.interview.google.technique;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LinkedListCreator {

    /**
     * create a linked list
     * 1->2->3->4->5->null
     * @param data the data to create the list
     * @return  head of the linked list,
     * The returned linked list ends with last node with getNext() == null
     */
    public Node createLinkedList(List<Integer> data){
        if(data.isEmpty()){
            return null;
        }
        Node firstNode = new Node(data.get(0));
        firstNode.setNext(createLinkedList(data.subList(1, data.size())));
        return firstNode;
    }

    public Node createLargeLinkedList(int size){
        Node prev = null;
        Node head = null;
        for (int i = 1; i<= size; ++i){
            Node node = new Node(i);
            if(prev != null){
                prev.setNext(node);
            }else{
                head = node;
            }
            prev = node;
        }
        return head;
    }

    public static void main(String[] args) {
        LinkedListCreator listCreator = new LinkedListCreator();
        Node.printList(listCreator.createLinkedList(new ArrayList<>()));
        Node.printList(listCreator.createLinkedList(Arrays.asList(1)));
        Node.printList(listCreator.createLinkedList(Arrays.asList(1, 2, 3, 4, 5)));
    }
}
