package com.aqielife.interview.google.tree;

public class InOrder {
    private TreeNode next(TreeNode node){
        if (node == null){
            return null;
        }

        if(node.getRight() != null){            // 右子树不为空,返回右子树的第一个节点
            return first(node.getRight());
        }else{                                  // 一直向上,直到是某个树的左子树
            while(node.getParent() != null
                    && node.getParent().getLeft() == node){
                node = node.getParent();
            }
            return node.getParent();
        }
    }

    /**
     * 获取从这个节点开始，最左边节点
     * @param root
     * @return
     */
    private TreeNode first(TreeNode root){
        if(root == null){
            return null;
        }
        TreeNode curNode = root;
        while(curNode.getLeft() != null){
            curNode = curNode.getLeft();
        }
        return curNode;
    }

    // 非递归完成树的中序遍历
    private void traverse(TreeNode root){
        for(TreeNode node = first(root);
            node != null;
            node = next(node)){
            System.out.print(node.getValue());
        }
        System.out.println();
    }

    public static void main(String[] args) {
        TreeCreator creator = new TreeCreator();
        InOrder inOrder = new InOrder();

        TreeNode sampleTree = creator.createSampleTree();
        inOrder.traverse(sampleTree);

        inOrder.traverse(creator.createTree("", ""));
        inOrder.traverse(creator.createTree("A", "A"));
        inOrder.traverse(creator.createTree("AB", "BA"));
        inOrder.traverse(creator.createTree("ABCD", "DCBA"));
        inOrder.traverse(creator.createTree("ABCD", "ABCD"));
    }
}
