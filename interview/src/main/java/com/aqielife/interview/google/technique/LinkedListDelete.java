package com.aqielife.interview.google.technique;

import java.util.Arrays;

public class LinkedListDelete {

    /**
     * delete duplicate elements
     * 2,2,3,3,2,1,5,6,6 -> 15  12325 -> 135
     * 2,2,2,2 -> 2
     * 2 -> 1
     * @param head
     * @param value
     */
    public<T> Node<T> deleteIfEquals(Node<T> head, T value){
        while(head!=null && head.getValue() == value){        // 注意这里处理头结点
            head = head.getNext();
        }
        if(head == null){                   // 处理全部是重复元素
            return null;
        }
        Node prev = head;
        /**
         * Loop invariant   循环不变式
         * list nodes from head up to prev has been processed
         */
        while (prev.getNext() != null){
            // prev.getNext() == null
            if(prev.getNext().getValue() == value){
                // delete it
                prev.setNext(prev.getNext().getNext());
            }else{
                prev = prev.getNext();
            }
        }
        return head;
    }
    public static void main(String[] args) {
        LinkedListCreator listCreator = new LinkedListCreator();
        LinkedListDelete deletor = new LinkedListDelete();
        Node.printList(deletor.deleteIfEquals(
                // listCreator.createLinkedList(Arrays.asList(2)),1
                // listCreator.createLinkedList(new ArrayList<>()),1
                listCreator.createLinkedList(Arrays.asList(2,2,2,2)),2
        ));
    }
}
