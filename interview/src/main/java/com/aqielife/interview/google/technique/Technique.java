package com.aqielife.interview.google.technique;

public class Technique {
    public static void test(int n){
        if(n > 0){
            test(n - 1);
            System.out.print(n);
            test(n - 2);
        }
    }

    public static void main(String[] args) {
        test(4);
    }
}

