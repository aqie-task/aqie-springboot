package com.aqielife.interview.google.codejam;

public class BeautifulNumber {
    /**
     * 接收一个数,输出其二进制
     * @param n 要转换的十进制数
     * @param m 进制
     */
    public static StringBuilder binaryOutPut(long n, int m){
        long[] arr = new long[100];
        int j = 0;
        StringBuilder res = new StringBuilder();
        while(n > 0){
            arr[j] = n % m;
            n /= m;
            j++;
        }
        for(int i = j - 1;i >= 0; --i){
            res.append(arr[i]);
        }
        return res;
    }
}
