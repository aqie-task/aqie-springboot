package com.aqielife.interview.google.task;

public abstract class TransactionalRunnable implements Runnable {
    protected abstract void doRun();

    @Override
    public void run() {
        boolean shouldRollback = false;

        try {
            beginTransaction();
            doRun();
        } catch (Exception e) {
            shouldRollback = true;
            throw e;
        } finally {
            if (shouldRollback) {
                rollBack();
            } else {
                commit();
            }
        }
    }

    private void rollBack() {
        System.out.println("rollBack! ");
    }

    private void beginTransaction() {
        System.out.println("BeginTransaction! ");
    }

    private void commit() {
        System.out.println("commit ");
    }
}
