package com.aqielife.interview.google.technique;

import java.util.ArrayList;
import java.util.Arrays;

public class LinkedListReverse2 {
    public Node reverseLinkedList(Node head){
        /*if (head == null){
            return null;
        }*/

        Node newHead = null;
        Node curHead = head;
        /**
         * 定义循环不变式
         * newHead points to the linked list already reversed
         * curHead points to the linked list not yet reversed
         */
        while(curHead != null){                     // curHead being last node
            // 记录当前链表头指向的下一个节点
            Node next = curHead.getNext();          // next = null
            curHead.setNext(newHead);               // curHead.next reversed
            newHead = curHead;                      // newHead points to last node
            curHead = next;                         // curHead = null
        }
        return newHead;
    }

    public static void main(String[] args) {
        LinkedListCreator listCreator = new LinkedListCreator();
        LinkedListReverse2 reverse2 = new LinkedListReverse2();
        Node.printList(
                reverse2.reverseLinkedList(
                        listCreator.createLinkedList(new ArrayList<>())
                )
        );
        Node.printList(
                reverse2.reverseLinkedList(
                        listCreator.createLinkedList(Arrays.asList(1))
                )
        );
        Node.printList(
                reverse2.reverseLinkedList(
                        listCreator.createLinkedList(Arrays.asList(1, 2, 3, 4, 5))
                )
        );

        Node.printList(
            reverse2.reverseLinkedList(
                    listCreator.createLargeLinkedList(1000000)
            )
        );
    }
}
