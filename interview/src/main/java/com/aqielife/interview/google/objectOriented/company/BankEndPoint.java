package com.aqielife.interview.google.objectOriented.company;

public interface BankEndPoint {
    void payment(String name, int salary);
}
