package com.aqielife.interview.google.task2;

public class LoggingRunnable implements Runnable {
    private final Runnable innerRunnable;
    public LoggingRunnable(Runnable innerRunnable){
        this.innerRunnable = innerRunnable;
    }

    @Override
    public void run() {
        Long startTime = System.currentTimeMillis();
        System.out.println("Task started at "
                + startTime);

        innerRunnable.run();

        System.out.println("Task finished at "
                + (System.currentTimeMillis() - startTime));
    }
}
