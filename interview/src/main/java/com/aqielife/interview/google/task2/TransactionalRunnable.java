package com.aqielife.interview.google.task2;

public class TransactionalRunnable implements Runnable {
    private final Runnable innerRunnable;
    public TransactionalRunnable (Runnable innerRunnable){
        this.innerRunnable = innerRunnable;
    }

    @Override
    public void run() {
        boolean shouldRollback = false;

        try {
            beginTransaction();
            innerRunnable.run();
        } catch (Exception e) {
            shouldRollback = true;
            throw e;
        } finally {
            if (shouldRollback) {
                rollBack();
            } else {
                commit();
            }
        }
    }

    private void rollBack() {
        System.out.println("rollBack! ");
    }

    private void beginTransaction() {
        System.out.println("BeginTransaction! ");
    }

    private void commit() {
        System.out.println("commit ");
    }
}
