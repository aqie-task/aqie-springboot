package com.aqielife.interview.google.technique;

public class BinarySearch {
    /**
     * Search element k in a sorted array
     * @param arr   a sorted array
     * @param k     the element to search
     * @return      index in arr whee k is , -1 if not found
     */
    public int binarySearch(int[] arr, int k){
        int a = 0;
        /**
         * [a,b) + [b,c) = [a,c)
         * b - a = len([a, b))
         * [a,a) ==> empty range
         */
        int b = arr.length;     // k [a, b)
        /*
         * loop invariant
         * [a,b) is valid a < b
         * k maybe within range [a, b)
         */
        while( a < b){
            // if(a == b) return -1;       // 区间是空的
            int m = a + (b-2) / 2;        // 除不尽中间偏左
            /**
             * a == b; m = a and m = b,
             * b == a + 1; m = a;
             * b == a + 2; m = a + 1;
             */
            // m = b 第一条可能会越界
            if(k < arr[m]){
                b = m;      // 取不到m, 又小于m的值
            }else if(k > arr[m]){
                a = m + 1;
            }else{
                return m;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        BinarySearch search = new BinarySearch();
        System.out.println(search.binarySearch(new int[]{1,3,8,10}, 8));
        System.out.println(search.binarySearch(new int[]{1,3,8,10}, -2));
        System.out.println(search.binarySearch(new int[]{1,3,8,10}, 11));
        System.out.println(search.binarySearch(new int[]{1,3,8,10}, 4));
        System.out.println("=============");
        System.out.println(search.binarySearch(new int[]{}, 4));
        System.out.println(search.binarySearch(new int[]{4}, 4));
    }
}
