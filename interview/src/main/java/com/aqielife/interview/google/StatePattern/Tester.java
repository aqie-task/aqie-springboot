package com.aqielife.interview.google.StatePattern;

import java.util.Arrays;
import java.util.LinkedList;

public class Tester {
    public static void main(String[] args) {
        Employee employee = new Employee("john",10000, new Engineer());
        employee.age = 18;

        Employee employee2 = new Employee("marry",20000, new Engineer());
        employee2.age = 24;

        System.out.println(employee.equals(employee2));     // false

        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(employee);
        employees.add(employee2);

        // 将员工二提升为manager
        System.out.println("Test managers");
        employee2.setRole(new Manager(Arrays.asList(employee)));

        for (Employee empl : employees){
            System.out.println(empl);
        }
        System.out.println("Test doWork");
        employee.doWork();
        employee2.doWork();
    }
}
