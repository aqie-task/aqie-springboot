package com.aqielife.interview.google.advance;

// 死锁
public class DeadLock {
    /**
     * transfer(from, to, 100) 和 transfer(to, from,100) 同时发生
     * @param from
     * @param to
     * @param amount
     */
    void transfer(Account from,Account to, int amount){
        synchronized (from){        // 刚锁完别人在等我们的锁from
            synchronized (to){      // 别人抢到to的锁，我们在等
                from.setAmount(from.getAmount() - amount);
                to.setAmount(to.getAmount() + amount);
            }
        }

    }
}
