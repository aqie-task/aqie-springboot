package com.aqielife.interview.google.StatePattern;

public interface BankEndPoint {
    void payment(String name, int salary);
}
