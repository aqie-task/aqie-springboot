package com.aqielife.interview.google.beautiful;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BeautifulNumberLarge {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        long cases = in.nextLong();  // Scanner has functions to read ints, longs, strings, chars, etc.
        for(int i = 1; i <= cases; ++i) {
            long n = in.nextLong();
            System.out.println("Case #" + i + ": " + beautiful(n));
        }
    }

    // 看能转换成多少个1，最多64个  lonN * logN * logN  64*64*64
    private static long beautiful(long n){
        for(int bits = 64; bits >= 2; bits--){
            long radix = getRadix(n, bits);
            if(radix != -1){
                return radix;
            }
        }
        // should not reach hear
        // return n - 1;
        throw new IllegalStateException("Should not reach here.");
    }

    /**
     *
     * @param n  要求的参数
     * @param bits 一共多少位1
     * @return      转换成的进制 the radix, -1 if there is no such radix
     */
    private static long getRadix(long n, int bits) {
        long minRadix = 2;
        long maxRadix = n;      // 能取到的最大边界+1
        while(minRadix < maxRadix){
            long m = minRadix + (maxRadix - minRadix) / 2;
            long t = convert(m, bits);      // m 进制下有 bits位 的1，的值
            if (t == n) {
                return m;
            } else if (t < n) {          // 算出来值偏小,增加进位
                minRadix = m + 1;
            } else {
                maxRadix = m;
            }
        }
        return -1;
    }

    private static long convert(long radix, int bits) {
        long component = 1;
        long sum = 0;
        for (int i = 0; i < bits; i++) {
            if (Long.MAX_VALUE - sum < component) {
                sum = Long.MAX_VALUE;
            } else {
                sum += component;
            }

            if (Long.MAX_VALUE / component < radix) {
                component = Long.MAX_VALUE;
            } else {
                component *= radix;
            }
        }
        return sum;
    }

}
