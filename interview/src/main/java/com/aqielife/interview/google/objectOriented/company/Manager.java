package com.aqielife.interview.google.objectOriented.company;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee{
    private final List<Employee> reporters = new ArrayList<>();
    private void loadReporters(){
        reporters.clear();                  // 每次新增前要清空
        reporters.add(new Employee("john", 100000));
    }

    public Manager(String name, int salary) {
        super(name, salary);
        // reporters = new ArrayList<>();              // 构造函数在初始化时调用
    }

    @Override
    public void getPaid(BankEndPoint bank) {
        super.getPaid(bank);
        getStocks();
    }

    private void getStocks() {
    }

    @Override
    public void doWork() {
        Employee worker = selectReporter();
        worker.doWork();
    }

    private Employee selectReporter(){
        loadReporters();
        return null;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "name='" + getName() + '\'' +
                ", salary=" + getSalary() +
                '}';
    }
}
