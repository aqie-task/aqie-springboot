package com.aqielife.interview.google.technique;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Combinations {
    /**
     * Generate all combinations and output them
     * @param data
     * @param n select n  element from data
     */
    public void combination(List<Integer> selected, List<Integer> data, int n){
        // initial value for recursion
        // how to select elements
        // how to output

        if(n == 0){         // 空集中选取一个元素
            // output all selected elements
            for (Integer i : selected){
                System.out.print(i);
                System.out.print(" ");
            }
            System.out.println();
            return;
        }

        if(data.isEmpty()){
            return;
        }

        if(data.size() < n){
            return;
        }

        // select element 0
        selected.add(data.get(0));
        combination(selected, data.subList(1,data.size()), n - 1);

        // un-select element 0
        selected.remove(selected.size() - 1);       // ??
        combination(selected, data.subList(1, data.size()), n);
    }

    public static void main(String[] args) {
        Combinations combinations = new Combinations();
        combinations.combination(new ArrayList<>(), Arrays.asList(1,2,3,4),2);
        System.out.println("========");
        combinations.combination(new ArrayList<>(), new ArrayList<>(),0);
        System.out.println("========");
        combinations.combination(new ArrayList<>(), new ArrayList<>(),2);
        System.out.println("========");
        combinations.combination(new ArrayList<>(), Arrays.asList(1,2,3,4),1);
        System.out.println("========");
        combinations.combination(new ArrayList<>(), Arrays.asList(1,2,3,4),5);
    }
}
