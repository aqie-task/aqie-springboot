package com.aqielife.interview.google.objectOriented;

import com.aqielife.interview.google.technique.Node;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList implements Iterable<Integer> {
    private Node head;
    private Node tail;

    public static LinkedList newEmptyList(){
        return new LinkedList();
    }
    private LinkedList(){
        head = null;
        tail = null;
    }

    public void add(int value){     // 维持tail始终指向末尾
        Node node = new Node(value);
        if(tail == null){           // 第一个节点,head指向他
            head = node;
        }else {
            tail.setNext(node);
        }
        tail = node;
    }

    private class ListIterator implements Iterator<Integer> {
        private Node currentNode;

        public ListIterator(Node head){
            currentNode = head;
        }
        @Override
        public boolean hasNext() {
            return currentNode != null;
        }

        @Override
        public Integer next() {
            if(currentNode == null){
                throw new NoSuchElementException();
            }
            int value = (int)currentNode.getValue();
            currentNode = currentNode.getNext();
            return value;
        }
    }
    @Override
    public Iterator<Integer> iterator() {
        return new ListIterator(head);
    }
}
