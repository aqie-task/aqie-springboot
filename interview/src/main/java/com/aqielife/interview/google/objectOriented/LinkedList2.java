package com.aqielife.interview.google.objectOriented;

import com.aqielife.interview.google.technique.Node;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList2<T> implements Iterable<T> {
    private Node<T> head;
    private Node<T> tail;

    public static<T> LinkedList2<T> newEmptyList(){
        return new LinkedList2<T>();
    }
    private LinkedList2(){
        head = null;
        tail = null;
    }

    public void add(T value){     // 维持tail始终指向末尾
        Node<T> node = new Node<>(value);
        if(tail == null){           // 第一个节点,head指向他
            head = node;
        }else {
            tail.setNext(node);
        }
        tail = node;
    }

    private class ListIterator implements Iterator<T> {          // 外部类已经有T,这里可以省略
        private Node<T> currentNode;

        ListIterator(Node<T> head){
            currentNode = head;
        }
        @Override
        public boolean hasNext() {
            return currentNode != null;
        }

        @Override
        public T next() {
            if(currentNode == null){
                throw new NoSuchElementException();
            }
            T value = currentNode.getValue();
            currentNode = currentNode.getNext();
            return value;
        }
    }
    @Override
    public Iterator<T> iterator() {
        return new ListIterator(head);
    }
}
