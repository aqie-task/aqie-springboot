package com.aqielife.interview.google.task;

public abstract class LoggingTask implements Runnable {
    protected abstract void doRun();

    @Override
    public void run() {
        Long startTime = System.currentTimeMillis();
        System.out.println("Task started at "
                + startTime);

        doRun();

        System.out.println("Task finished at "
                + (System.currentTimeMillis() - startTime));
    }
}
