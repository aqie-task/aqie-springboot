package com.aqielife.interview.google.objectOriented.company;

public class Accounting {
    BankEndPoint bank;

    void payAll(){
        Employee.loadAllEmployees();        // 保险
        for(Employee employee : Employee.allEmployees){
            employee.getPaid(bank);
        }
    }
}
