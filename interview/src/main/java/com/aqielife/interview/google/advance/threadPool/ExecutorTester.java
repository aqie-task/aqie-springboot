package com.aqielife.interview.google.advance.threadPool;

import com.aqielife.interview.google.task2.CodingTask;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *  线程池创建
 *  任务派发
 *  利用Future 检查任务结果
 */
public class ExecutorTester {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // new ThreadPoolExecutor()
        ExecutorService executor = Executors.newFixedThreadPool(3);
        // 用来存储线程执行结果
        List<Future<?>> taskResults = new LinkedList<>();

        for(int i = 0; i < 10; ++i){
            Future<?> task =  executor.submit(new CodingTask(i));
            taskResults.add(task);
        }
        System.out.println("10 tasks dispatched successful ");

        for (Future<?> taskResult : taskResults){
            taskResult.get();
        }
        System.out.println("All tasks finished! ");
        executor.shutdown();        // 关闭, 回收线程
    }
}
