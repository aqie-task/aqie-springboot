package com.aqielife.interview.google.technique;

import java.util.Arrays;

public class LinkedListReverse {
    /**
     * Reverse a linked list
     * 3->2->1->null
     * @param head
     * @return
     */
    public Node reverseLinkedList(Node head){
        // size = 0 || size = 1
        if (head == null || head.getNext() == null){
            return head;
        }


        Node newHead = reverseLinkedList(head.getNext());
        head.getNext().setNext(head);
        head.setNext(null);
        return newHead;
    }

    public static void main(String[] args) {
        LinkedListReverse reverse = new LinkedListReverse();
        LinkedListCreator listCreator = new LinkedListCreator();
        Node.printList(
                reverse.reverseLinkedList(
                    listCreator.createLinkedList(Arrays.asList(1, 2, 3, 4, 5))
                )
        );
        Node.printList(
                reverse.reverseLinkedList(
                        listCreator.createLargeLinkedList(1000000)
                )
        );
    }
}
