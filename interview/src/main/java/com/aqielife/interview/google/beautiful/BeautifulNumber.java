package com.aqielife.interview.google.beautiful;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BeautifulNumber {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        long cases = in.nextLong();  // Scanner has functions to read ints, longs, strings, chars, etc.
        for(int i = 1; i <= cases; ++i) {
            long n = in.nextLong();
            System.out.println("Case #" + i + ": " + beautiful(n));
        }
    }

    // O(NlogN)     logN 对二取log  N = 10^18 , 10^18*64 , 10^8 是秒级
    private static long beautiful(long n){
        // 2进制试到n-1进制
        for(long radix = 2; radix < n; radix++){
            // if n is 11 in radix, return radix
            if(isBeautiful(n,radix)){
                return radix;
            }
        }
        // should not be reached
        return n - 1;
    }

    private static boolean isBeautiful(long n, long radix){
        while (n > 0){
            if(n % radix != 1){
                return false;
            }
            n /= radix;
        }
        return true;
    }

}
