package com.aqielife.interview.google.task;

// public class CodingTask extends LoggingTask{
public class CodingTask extends TransactionalRunnable{
    @Override
    protected void doRun() {
        System.out.println("Writing code");
        try {
            Thread.sleep(5000);             // 休眠五秒钟
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
