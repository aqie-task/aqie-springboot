package com.aqielife.interview.google.objectOriented.company;

import java.util.List;
import java.util.Objects;

public class Employee {
    private final String name;
    private final int salary;
    static List<Employee> allEmployees;
    public int age;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public Employee(String name) {
        this(name, 0);
    }

    public void doWork(){
        loadAllEmployees();
    }

    public void getPaid(BankEndPoint bank){
        bank.payment(name, salary);
    }

    /**
     * Package private for login in the package to control
     */
    static void loadAllEmployees(){
        // loads all employees from database to store in allEmployees
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return salary == employee.salary &&
                Objects.equals(name, employee.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, salary);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    String getName() {
        return name;
    }

    int getSalary() {
        return salary;
    }
}
