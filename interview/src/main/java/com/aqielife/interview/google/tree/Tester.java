package com.aqielife.interview.google.tree;

import com.aqielife.interview.google.objectOriented.company.Employee;
import com.aqielife.interview.google.objectOriented.company.Manager;

import java.util.LinkedList;

public class Tester {
    public static void main(String[] args) {
        // testEmployee();
        testLinkedList();
    }

    private static void testLinkedList() {
        com.aqielife.interview.google.objectOriented.LinkedList list = com.aqielife.interview.google.objectOriented.LinkedList.newEmptyList();
        for (int i = 0; i < 10; ++i){
            list.add(i);
        }
        for (Integer value : list){
            System.out.print(value);
        }
    }

    private static void testEmployee() {
        Employee employee = new Employee("john",10000);
        employee.age = 18;

        Employee employee2 = new Employee("marry",20000);
        employee2.age = 24;

        Employee employee3 = new Employee("john",10000);

        System.out.println(employee.equals(employee3));     // true
        System.out.println(employee.equals(employee2));     // false

        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(employee);
        employees.add(employee2);
        employees.add(employee3);


        System.out.println("Test managers");
        Employee manager = new Manager("Tony", 1000000);
        employees.add(manager);

        for (Employee empl : employees){
            System.out.println(empl);
        }
    }
}
