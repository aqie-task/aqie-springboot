package com.aqielife.interview.swordtoOffer.thread.waitsleep;

import lombok.extern.slf4j.Slf4j;

/**
 * wait 会释放锁，B线程代码会在A执行中间执行
 */
@Slf4j
public class WaitSleepDemo {
    public static void main(String[] args) {
        final Object lock = new Object();
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread A is waiting for get lock!");
                synchronized (lock){
                    try {
                        log.info("Thread A get lock");
                        Thread.sleep(20);

                        log.info("Thread A do wait");
                        lock.wait(1000);        // 传参数一秒后自动唤醒, 会释放锁

                        log.info("Thread A is done");
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        // 保证A 先执行
        try{
            Thread.sleep(10);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread B is waiting for get lock!");
                synchronized (lock){
                    try {
                        log.info("Thread B get lock");

                        log.info("Thread B is sleeping 10 ms");
                        Thread.sleep(10);

                        log.info("Thread B is done");
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
