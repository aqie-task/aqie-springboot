package com.aqielife.interview.swordtoOffer.juc;

import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 交换男女说的话
 */
public class ExchangerDemo {
    private static Exchanger<String> exchanger = new Exchanger<>();
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(() -> {
            try {
                String girl = exchanger.exchange("hello");
                System.out.println(System.currentTimeMillis() + " 女生： " + girl );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        service.execute(() -> {
            try {
                System.out.println("3 秒后:" + System.currentTimeMillis());
                TimeUnit.SECONDS.sleep(3);
                String boy = exchanger.exchange("world");
                System.out.println(System.currentTimeMillis() + " 男生： " + boy);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
