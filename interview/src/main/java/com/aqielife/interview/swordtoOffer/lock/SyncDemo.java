package com.aqielife.interview.swordtoOffer.lock;

public class SyncDemo {
    public static void main(String[] args) {
        // 必须锁同一个对象
        SynchThread synchThread = new SynchThread();
        // 异步执行
        Thread A1 = new Thread(synchThread,"A1");
        Thread A2 = new Thread(synchThread,"A2");
        // 顺序执行 代码块外代码不受影响
        Thread B1 = new Thread(synchThread,"B1");
        Thread B2 = new Thread(synchThread,"B2");
        // 顺序执行
        Thread C1 = new Thread(synchThread,"C1");
        Thread C2 = new Thread(synchThread,"C2");

        // 锁类
        Thread D1 = new Thread(synchThread,"D1");
        Thread D2 = new Thread(synchThread,"D2");

        Thread E1 = new Thread(synchThread,"E1");
        Thread E2 = new Thread(synchThread,"E2");
        A1.start();
        A2.start();
        B1.start();
        B2.start();
        C1.start();
        C2.start();

        D1.start();
        D2.start();
        E1.start();
        E2.start();
    }
}
