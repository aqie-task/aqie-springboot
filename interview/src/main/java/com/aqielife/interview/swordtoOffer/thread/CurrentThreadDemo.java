package com.aqielife.interview.swordtoOffer.thread;

/**
 * Thread 和Runnable 区别
 */
public class CurrentThreadDemo {
    public static void main(String[] args) {
        System.out.println("Current Thread: " + Thread.currentThread().getName());
        MyThread mt1 = new MyThread("Thread1");
        MyThread mt2 = new MyThread("Thread2");
        MyThread mt3 = new MyThread("Thread3");
        // mt1.start();mt2.start();mt3.start();

        MyRunnable mr1 = new MyRunnable("Runnable1");
        MyRunnable mr2 = new MyRunnable("Runnable2");
        MyRunnable mr3 = new MyRunnable("Runnable3");
        Thread t1 = new Thread(mr1);
        Thread t2 = new Thread(mr2);
        Thread t3 = new Thread(mr3);
        t1.start();t2.start();t3.start();
    }
}
