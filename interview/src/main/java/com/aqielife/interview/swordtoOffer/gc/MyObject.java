package com.aqielife.interview.swordtoOffer.gc;

import lombok.Data;

import java.util.Random;

@Data
public class MyObject {

    public MyObject childNode;
    private Integer type =  new Random().nextInt(100);
}
