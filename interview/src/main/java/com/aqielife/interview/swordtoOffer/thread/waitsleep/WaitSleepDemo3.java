package com.aqielife.interview.swordtoOffer.thread.waitsleep;

import lombok.extern.slf4j.Slf4j;

/**
 * 1. 设定时间自动唤醒
 * 2. 让线程进入等待 wait
 * 3. notify / notifyAll
 */
@Slf4j
public class WaitSleepDemo3 {
    public static void main(String[] args) {
        final Object lock = new Object();
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread A is waiting for get lock!");
                synchronized (lock){
                    try {
                        log.info("Thread A get lock");
                        Thread.sleep(20);

                        log.info("Thread A do wait");
                        lock.wait();        //  进入无线等待

                        log.info("Thread A is done");
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        // 保证A 先执行
        try{
            Thread.sleep(100);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread B is waiting for get lock!");
                synchronized (lock){
                    try {
                        log.info("Thread B get lock");

                        log.info("Thread B is sleeping 10 ms");
                        Thread.sleep(10);

                        log.info("Thread B is done");
                        // 线程B 执行完成 唤醒A
                        lock.notify();
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
