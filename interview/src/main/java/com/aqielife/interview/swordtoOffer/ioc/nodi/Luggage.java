package com.aqielife.interview.swordtoOffer.ioc.nodi;

public class Luggage {
    private Bottom bottom;
    Luggage(int size){
        this.bottom = new Bottom(size);
    }
    public void move(){
        System.out.println("move");
    }

    public static void main(String[] args) {
        (new Luggage(3)).move();
    }
}
