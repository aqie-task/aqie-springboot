package com.aqielife.interview.swordtoOffer.gc;

public class Finalization {
    public static Finalization finalization;

    // 不建议使用
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize");
        finalization = this;
    }

    public static void main(String[] args) {
        Finalization f = new Finalization();
        System.out.println("first" + f);
        f = null;
        System.gc();
        try{
            Thread.currentThread().sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("second" + f);
        System.out.println(f.finalization);
    }
}
