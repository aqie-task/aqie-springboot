package com.aqielife.interview.swordtoOffer.thread.waitsleep;

import lombok.extern.slf4j.Slf4j;

/**
 * wait 会释放锁
 * sleep 不会释放锁，B线程代码会在A执行中间执行
 * B 通知完A 后才 执行结束
 * yield 不会影响锁
 */
@Slf4j
public class WaitSleepDemo4 {
    public static void main(String[] args) {
        final Object lock = new Object();
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread A is waiting for get lock!");
                synchronized (lock) {
                    try {
                        log.info("Thread A get lock");
                        Thread.sleep(20);

                        log.info("Thread A do wait");
                        lock.wait();

                        log.info("Thread A is done");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        // 保证A 先执行
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Thread B is waiting for get lock!");
                synchronized (lock) {
                    try {
                        log.info("Thread B get lock");
                        log.info("Thread B is sleeping 10 ms");
                        Thread.sleep(10);

                        lock.notify();                 // 把A 放入锁池竞争锁, 但B 虽然Sleep 仍不会释放锁；直到B执行结束
                        Thread.yield();
                        Thread.sleep(2000);
                        log.info("Thread B is done");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
/*
    Thread A get lock
    Thread A do wait
    Thread B get lock
    Thread B is sleeping 10 ms
    Thread B is done
    Thread A is done
*/

