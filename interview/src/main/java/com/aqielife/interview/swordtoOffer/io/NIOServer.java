package com.aqielife.interview.swordtoOffer.io;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

@Slf4j
public class NIOServer {
    public void server(int port) throws IOException {
        log.info("Listen on port: " + port);
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        ServerSocket ss = serverChannel.socket();
        InetSocketAddress address = new InetSocketAddress(port);
        // 1. ServerSocket 绑定到指定的端口
        ss.bind(address);
        // 设置为非阻塞,阻塞模式 注册操作不允许
        serverChannel.configureBlocking(false);
        Selector selector = Selector.open();
        // 2.将Channel 注册到Selector; 并说明Selector 关注的点, 这里关注建立连接事件
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
        while (true){
            try {
                // 阻塞等待就绪的Channel 即没有与客户端建立连接前就一直轮询
                selector.select();
            }catch (IOException ignore){
                break;
            }
            // 3. 获取Selector 里所有就绪恶SelectedKey 实例 每将一个channel 注册到一个
            // selector 就会产生一个SelectorKey
            Set<SelectionKey> readyKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = readyKeys.iterator();
            while (iterator.hasNext()){
                SelectionKey key = (SelectionKey)iterator.next();
                // 将就绪的SelectedKey 从Selector 中移除, 马上就要处理它 防止重复执行
                iterator.remove();
                try{
                    // 如果SelectedKey 处于Acceptable 状态
                    if (key.isAcceptable()){
                        ServerSocketChannel server = (ServerSocketChannel)key.channel();
                        // 接收客户端连接
                        SocketChannel client = server.accept();
                        log.info("Accept connection from: " + client);
                        // 向selector 注册socketChannel 主要关注读写 并传入一个ByteBuffer 实例供读写缓存
                        client.register(selector, SelectionKey.OP_WRITE | SelectionKey.OP_READ,ByteBuffer.allocate(100));
                    }
                    // 若SelectedKey 处于可读状态
                    if (key.isReadable()){
                        SocketChannel client = (SocketChannel)key.channel();
                        ByteBuffer output = (ByteBuffer)key.attachment();
                        // 从channel 里读取数据存入到ByteBuffer
                        client.read(output);
                    }

                    if (key.isWritable()){
                        SocketChannel client = (SocketChannel)key.channel();
                        ByteBuffer output = (ByteBuffer)key.attachment();
                        output.flip();
                        // 从channel 里读取数据存入到ByteBuffer
                        client.write(output);
                        output.compact();
                    }
                }catch (IOException e){
                    key.cancel();
                    try{
                        key.channel().close();
                    }catch (IOException ignore){}
                }
            }
        }
    }
}
