package com.aqielife.interview.swordtoOffer.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InterruptDemo {
    public static void main(String[] args) throws InterruptedException{
        Runnable interruptTask = new Runnable() {
            @Override
            public void run() {
                int i = 0;
                try{
                    // 正常运行任务, 经常检查本线程的中断标志位,如果被设置中断标志 就自动停止线程
                    while(!Thread.currentThread().isInterrupted()){
                        Thread.sleep(100);
                        i++;
                        log.info(Thread.currentThread().getName() + " (" + Thread.currentThread() + " )" );
                    }
                }catch (InterruptedException e){
                    // 掉用 阻塞方法时, 正确处理InterruptedException
                    log.info(Thread.currentThread().getName() + " (" + Thread.currentThread() + " ) Exception");
                }
            }
        };

        Thread t1 = new Thread(interruptTask, "t1");
        log.info(t1.getName() + " (" + t1.getState() + ") is new.");

        t1.start();
        log.info(t1.getName() + " (" + t1.getState() + ") is started.");

        // 主线程休眠300ms; 主线程给t1 发送中断指令
        Thread.sleep(300);
        t1.interrupt();
        log.info(t1.getName() + " (" + t1.getState() + ") is interrupted.");

        // 主线程休眠300 秒 然后查看t1 状态
        Thread.sleep(300);
        log.info(t1.getName() + " (" + t1.getState() + ") is interrupted now.");
    }
}
