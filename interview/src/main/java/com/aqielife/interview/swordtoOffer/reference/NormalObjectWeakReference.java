package com.aqielife.interview.swordtoOffer.reference;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

public class NormalObjectWeakReference extends WeakReference<NormalObject> {
    public String name;
    public NormalObjectWeakReference(NormalObject normalObject, ReferenceQueue<NormalObject> referenceQueue) {
        super(normalObject,referenceQueue);
        this.name = normalObject.name;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalizing weak: " + name);
    }
}
