package com.aqielife.interview.swordtoOffer.gc;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class Reference {
    public void Reference(){
        // 强引用
        A a = new A();

        // 软引用
        String str = new String("abc");
        SoftReference<String> stringSoftReference = new SoftReference<>(str);

        // 弱引用
        WeakReference<String> stringWeakReference = new WeakReference<>(str);

        // 虚引用
        ReferenceQueue queue = new ReferenceQueue();
        PhantomReference reference = new PhantomReference(str, queue);
    }
}
