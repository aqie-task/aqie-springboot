package com.aqielife.interview.swordtoOffer.thread.threadreturn;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * MyCallable
 * 法1.通过FutureTask 获取线程返回值
 */
public class FutureTaskDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> task = new FutureTask<String>(new MyCallable());
        // 执行多线程
        new Thread(task).start();
        if(!task.isDone()){
            System.out.println("task has not finished, please wait!");
        }
        System.out.println("task return: " + task.get());

    }
}
