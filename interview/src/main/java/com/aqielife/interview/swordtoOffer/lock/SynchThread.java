package com.aqielife.interview.swordtoOffer.lock;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

// 获取对象锁
@Slf4j
public class SynchThread implements Runnable{
    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        if (threadName.startsWith("A")){
            async();
        }else if(threadName.startsWith("B")){
            syncObjectBlock();
        }else if (threadName.startsWith("C")){
            syncObjectMethod();
        }else if (threadName.startsWith("D")){
            syncClassBlock();
        }else if (threadName.startsWith("E")){
            syncClassMethod();
        }
        
    }

    // 修饰 非静态方法
    private synchronized void syncObjectMethod() {
        log.info(Thread.currentThread().getName() + "_SyncObjectMethod: " + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        try{
            log.info(Thread.currentThread().getName() + "_SyncObjectMethod_Start: "
                    + new SimpleDateFormat("HH:mm::ss").format(new Date()));
            Thread.sleep(1000);
            log.info(Thread.currentThread().getName() + "_SyncObjectMethod_End: "
                    + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // synchronized(this|Object) 同步代码块
    private void syncObjectBlock() {
        log.info(Thread.currentThread().getName() + "_SyncObjectBlock: " + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        synchronized (this){
            try{
                log.info(Thread.currentThread().getName() + "_SyncObjectBlock_Start: "
                        + new SimpleDateFormat("HH:mm::ss").format(new Date()));
                Thread.sleep(1000);
                log.info(Thread.currentThread().getName() + "_SyncObjectBlock_End: "
                        + new SimpleDateFormat("HH:mm::ss").format(new Date()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // 异步方法
    private void async() {
        try{
            log.info(Thread.currentThread().getName() + "_Async_Start: " + new SimpleDateFormat("HH:mm::ss").format(new Date()));
            Thread.sleep(1000);
            log.info(Thread.currentThread().getName() + "_Async_End: " + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 锁 类

    private void syncClassBlock() {
        log.info(Thread.currentThread().getName() + "_SyncClassBlock: " + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        synchronized (SynchThread.class){
            try{
                log.info(Thread.currentThread().getName() + "_SyncClassBlock_Start: "
                        + new SimpleDateFormat("HH:mm::ss").format(new Date()));
                Thread.sleep(1000);
                log.info(Thread.currentThread().getName() + "_SyncClassBlock_End: "
                        + new SimpleDateFormat("HH:mm::ss").format(new Date()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized static void syncClassMethod() {
        log.info(Thread.currentThread().getName() + "_SyncClassMethod: " + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        try{
            log.info(Thread.currentThread().getName() + "_SyncClassMethod_Start: "
                    + new SimpleDateFormat("HH:mm::ss").format(new Date()));
            Thread.sleep(1000);
            log.info(Thread.currentThread().getName() + "_SyncClassMethod_End: "
                    + new SimpleDateFormat("HH:mm::ss").format(new Date()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
