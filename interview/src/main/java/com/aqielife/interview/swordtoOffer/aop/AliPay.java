package com.aqielife.interview.swordtoOffer.aop;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AliPay implements Payment{
    private Payment payment;

    public AliPay(Payment payment) {
        this.payment = payment;
    }

    public void beforePay(){
        log.info("从银行取款");
    }
    @Override
    public void pay() {
        beforePay();
        payment.pay();
        afterPay();
    }

    public void afterPay(){
        log.info("付钱给淘宝");
    }
}
