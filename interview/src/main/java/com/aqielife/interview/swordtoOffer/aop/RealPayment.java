package com.aqielife.interview.swordtoOffer.aop;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RealPayment implements Payment{
    @Override
    public void pay() {
        log.info("用户只关心功能");
    }
}
