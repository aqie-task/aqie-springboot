package com.aqielife.interview.swordtoOffer.ioc.di;

public class Luggage {
    private Bottom bottom;

    public Luggage(Bottom bottom) {
        this.bottom = bottom;
    }
    public void move(){
        System.out.println("move");
    }

    public static void main(String[] args) {
        Tire tire = new Tire();
        Bottom bottom = new Bottom(tire);
        (new Luggage(bottom)).move();
    }
}
