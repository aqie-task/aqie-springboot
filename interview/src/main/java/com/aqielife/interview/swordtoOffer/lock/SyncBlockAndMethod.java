package com.aqielife.interview.swordtoOffer.lock;

public class SyncBlockAndMethod {
    // 可重入
    public void aTask(){
        synchronized (this){
            System.out.println("hello");
            synchronized (this){
                System.out.println("Aqie");
            }
        }
    }

    public synchronized void syncTask(){
        System.out.println("world");
    }
}
