package com.aqielife.interview.swordtoOffer.throwable;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ErrorAndException {
    public static void main(String[] args) {
        throwError();
    }
    private static void throwError(){
        throw new StackOverflowError();
        /*NoClassDefFoundError; 1. 类依赖class 不存在 2.类存在不同域 3.大小写
        OutOfMemoryError;*/
    }

    private static void throwRuntimeException(){
        throw new RuntimeException("run time");
        /*ClassCastException;
        IllegalArgumentException;
        IndexOutOfBoundsException;
        NumberFormatException;*/
    }

    private static void throwCheckedException(){
        try {
            throw new FileNotFoundException();
            /*ClassNotFoundException;
            IOException;*/

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static  void write() throws IOException{
        try(DataOutputStream out = new DataOutputStream(new FileOutputStream("data"))){
            out.writeInt(666);
            out.writeUTF("abd");
        }
    }

}
