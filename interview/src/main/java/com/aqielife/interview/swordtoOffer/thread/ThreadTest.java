package com.aqielife.interview.swordtoOffer.thread;

public class ThreadTest {
    private static void attack() {
        System.out.println("Fight");
        System.out.println("Current Thread is : " + Thread.currentThread().getName());
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(){
            @Override
            public void run() {
                attack();
            }
        };
        System.out.println("current main thread is : " + Thread.currentThread().getName());
        // t.run();        // main Fight main
        t.start();         // main Fight Thread-0
        t.join();          // 主线程等待子线程执行完成
        t.start();         // java.lang.IllegalThreadStateException
    }
}
