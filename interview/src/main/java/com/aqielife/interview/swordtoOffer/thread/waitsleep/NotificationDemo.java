package com.aqielife.interview.swordtoOffer.thread.waitsleep;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class NotificationDemo {
    // 某一个线程改动对其他线程可见
    private volatile boolean go = false;
    private static final Logger log = LoggerFactory.getLogger(NotificationDemo.class);

    public static void main(String[] args) throws InterruptedException{

        final NotificationDemo test = new NotificationDemo();

        // 1. 使线程进入等待状态
        Runnable waitTask = new Runnable() {
            @Override
            public void run() {
                try {
                    test.shouldGo();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info(Thread.currentThread().getName() + " finish");

            }

        };

        Runnable notifyTask = new Runnable() {
            @Override
            public void run() {
                test.go();
                log.info(Thread.currentThread().getName() + " finish");
            }
        };

        Thread t1 = new Thread(waitTask,"WT1");
        Thread t2 = new Thread(waitTask,"WT2");
        Thread t3 = new Thread(waitTask,"WT3");
        Thread t4 = new Thread(notifyTask,"NT3");

        t1.start(); t2.start(); t3.start();

        Thread.sleep(200);
        t4.start();
    }

    private synchronized void shouldGo() throws InterruptedException{
        while (go != true){
            log.info(Thread.currentThread() + " is going to wait on this object");
            wait();
            log.info(Thread.currentThread() + "is woken up");
        }
        go = false;
    }

    private synchronized void go(){
        while (go != true){
            log.info(Thread.currentThread() + " is going to notify all or one thread waiting on this object");
            go = true;
            // notify();      // 只唤醒其中一个
            notifyAll();
        }
    }
}
