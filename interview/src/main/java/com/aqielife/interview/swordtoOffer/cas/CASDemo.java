package com.aqielife.interview.swordtoOffer.cas;

/**
 * JUC atomic
 * JAVA9+ : 用Variable Handle 替代 Unsafe
 */
public class CASDemo {
    public volatile int value;
    public void add(){
        value++;
    }
}
