package com.aqielife.interview.swordtoOffer.thread.threadreturn;

public class CycleWait implements Runnable{
    private String value;
    @Override
    public void run() {
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        value = "we have data now";
    }

    public static void main(String[] args) throws InterruptedException {
        CycleWait cw = new CycleWait();
        Thread t = new Thread(cw);
        t.start();
        // 法1
        /*while (cw.value == null){
            Thread.currentThread().sleep(100);
        }*/
        // 法 2
        // t.join();
        System.out.println(cw.value);
    }
}
