package com.aqielife.interview.swordtoOffer.lock;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 查看线程获取锁概率
 */
@Slf4j
public class ReentrantLockDemo implements Runnable{
    // 公平锁：倾向将锁赋予等待时间最久线程; 获取锁顺序按先后调用lock顺序(慎用 排队打饭)
    private ReentrantLock fairLock = new ReentrantLock(true);


    public static void main(String[] args) {
        ReentrantLockDemo rtld = new ReentrantLockDemo();
        // 两个线程争夺同一个锁
        Thread t1 = new Thread(rtld);
        Thread t2 = new Thread(rtld);
        t1.start(); t2.start();
    }

    @Override
    public void run() {
        while (true){

            try {
                // 尝试获取锁对象
                fairLock.lock();
                log.info(Thread.currentThread().getName() + " get lock");
                Thread.sleep(1000);
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                fairLock.unlock();
            }
        }
    }
}
