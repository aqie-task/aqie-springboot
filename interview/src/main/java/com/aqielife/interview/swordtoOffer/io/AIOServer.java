package com.aqielife.interview.swordtoOffer.io;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class AIOServer {
    class A{

    }
    public void server(int port) throws IOException {
        log.info("Listen on port: " + port);
        final AsynchronousServerSocketChannel serverChannel = AsynchronousServerSocketChannel.open();
        InetSocketAddress address = new InetSocketAddress(port);
        // 将ServerSocket 绑定到指定端口
        serverChannel.bind(address);
        final CountDownLatch latch = new CountDownLatch(1);
        // 开始接受新客户端请求, 一旦一个客户端请求被接受 CompletionHandler
        serverChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Object>() {

            @Override
            public void completed(AsynchronousSocketChannel channel, Object attachment) {
                // 一旦完成处理 再次接收新的客户端请求
                serverChannel.accept(null, this);
                ByteBuffer buffer = ByteBuffer.allocate(100);
                // 在channel 里植入一个读操作EchoCompletionHandler;
                // 一旦buffer 有数据 EchoCompletionHandler 被唤醒
                channel.read(buffer, buffer, new EchoCompletionHandler(channel));
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                try {
                    serverChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    latch.countDown();
                }
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    }

    private final class EchoCompletionHandler implements CompletionHandler<Integer, ByteBuffer>{
        private final AsynchronousSocketChannel channel;

        private EchoCompletionHandler(AsynchronousSocketChannel channel) {
            this.channel = channel;
        }

        @Override
        public void completed(Integer result, ByteBuffer buffer) {
            buffer.flip();
            // 在channel 里植入一个读操作CompletionHandler;
            channel.write(buffer, buffer, new CompletionHandler<Integer, ByteBuffer>() {
                @Override
                public void completed(Integer result, ByteBuffer buffer) {
                    if (buffer.hasRemaining()){
                        // 如果buffer 还有内容 再次触发写操作 将buffer内容写入channel
                        channel.write(buffer, buffer, this);
                    }else {
                        // 清除写入buffer内容
                        buffer.compact();
                        // 如果channel 里还有内容需要读入buffer 再次触发写操作将channel内容读入buffer
                        channel.read(buffer, buffer, EchoCompletionHandler.this);
                    }
                }

                @Override
                public void failed(Throwable exc, ByteBuffer attachment) {
                    try{
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            try{
                channel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
