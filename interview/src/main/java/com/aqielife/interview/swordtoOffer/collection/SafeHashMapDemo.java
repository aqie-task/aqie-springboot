package com.aqielife.interview.swordtoOffer.collection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * HashMap 变线程安全
 */
public class SafeHashMapDemo {
    public static void main(String[] args) {
        Map hashMap = new HashMap();
        Map safeMap = Collections.synchronizedMap(hashMap);
        safeMap.put("a","1");
        safeMap.put("b","2");
        System.out.println(safeMap.get("a"));
    }
}
