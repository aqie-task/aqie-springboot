package com.aqielife.interview.swordtoOffer.io;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class BIOServer {
    public void server(int port) throws IOException {
        final ServerSocket socket = new ServerSocket(port);
        while (true){
            // 1.阻塞直到收到新的客户端连接
            final Socket clientSocket = socket.accept();
            log.info("Accept connection: " + clientSocket);
            // 2. 创建子线程处理客户端请求
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try(BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))){
                        PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
                        // 3.从客户端读取数据并原封不动回写回去
                        while (true){
                            writer.println(reader.readLine());
                            writer.flush();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    // 引入线程池
    public void mprovedServer(int port) throws IOException {
        final ServerSocket socket = new ServerSocket(port);
        // 1. 创建线程池
        ExecutorService executorService = Executors.newFixedThreadPool(6);
        while (true){
            // 1.阻塞直到收到新的客户端连接
            final Socket clientSocket = socket.accept();
            log.info("Accept connection: " + clientSocket);
            // 2. 创建子线程处理客户端请求
            executorService.execute(() -> {
                try(BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))){
                    PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
                    // 3.从客户端读取数据并原封不动回写回去
                    while (true){
                        writer.println(reader.readLine());
                        writer.flush();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
