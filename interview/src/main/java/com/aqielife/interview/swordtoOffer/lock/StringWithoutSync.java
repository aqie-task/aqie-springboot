package com.aqielife.interview.swordtoOffer.lock;

/**
 * sb 只会在append 方法中使用,不可能被其他线程竞争，sb属于不可竞争资源,JVM 会自动消除内部锁
 */
public class StringWithoutSync {
    public void add(String str1, String str2){
        // 线程安全
        StringBuffer sb = new StringBuffer();
        sb.append(str1).append(str2);
    }
    public static void main(String[] args) {
        StringWithoutSync withoutSync = new StringWithoutSync();
        for (int i = 0; i < 1000; i++){
            withoutSync.add("a","b");
        }
    }
}
