package com.aqielife.interview.swordtoOffer.juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    public static void main(String[] args) {
        // 1.线程池
        ExecutorService exec = Executors.newCachedThreadPool();
        // 2. 只能5个线程同时访问
        final Semaphore semp = new Semaphore(5);
        for (int i = 0; i < 20 ;i++){
            final int number = i;
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    try {
                        // 3.获取许可
                        semp.acquire();
                        System.out.println("Accessing " + number);
                        Thread.sleep((long) (Math.random() * 10000));
                        // 4.访问完后释放
                        semp.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            exec.execute(run);
        }
        // 退出线程池
        exec.shutdown();
    }
}
