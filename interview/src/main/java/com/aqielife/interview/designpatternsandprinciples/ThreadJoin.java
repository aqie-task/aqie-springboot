package com.aqielife.interview.designpatternsandprinciples;

import lombok.extern.slf4j.Slf4j;

/**
 * 1. join
 * 2. CountDownLatch
 * 3. CyclicBarrier
 * 4. ThreadPool
 */
@Slf4j
public class ThreadJoin {
    static class ThreadA extends Thread {
        public ThreadA() {
            super("A");
        }

        @Override
        public void run() {
            super.run();
            String threadName = Thread.currentThread().getName();
            try {
                Thread.sleep(1000);
                log.info(threadName + "end");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    static class ThreadB extends Thread {
        public ThreadB() {
            super("B");
        }

        @Override
        public void run() {
            super.run();
            String threadName = Thread.currentThread().getName();
            try {
                Thread.sleep(2000);
                log.info(threadName + "end");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadA A = new ThreadA();
        ThreadB B = new ThreadB();
        B.start();
        B.join();
        A.start();

    }
}
