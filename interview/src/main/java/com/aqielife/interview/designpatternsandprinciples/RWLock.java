package com.aqielife.interview.designpatternsandprinciples;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

public class RWLock extends ReadWrite {
    class RLock implements Sync{
        public void acquire() throws InterruptedException{
            beforeRead();
        }

        public void release(){
            afterRead();
        }

        public boolean attempt(long msecs) throws InterruptedException{
            return beforeRead(msecs);
        }
    }

    class WLock implements Sync {
        public void acquire() throws InterruptedException{
            beforeWrite();
        }

        public void release(){
            afterWrite();
        }

        public boolean attempt(long msecs) throws InterruptedException{
            return beforeWrite(msecs);
        }
    }

    interface Sync{

    }

    protected final RLock rLock = new RLock();

    protected final WLock wLock = new WLock();

    @Override
    protected void doRead() {

    }

    @Override
    protected void doWrite() {

    }


    public Sync readLock() {
        return rLock;
    }


    public Sync writeLock() {
        return wLock;
    }

    public boolean beforeRead(long msecs) throws InterruptedException{
     return false;
    }
    public boolean beforeWrite(long msecs) throws InterruptedException{
        return false;
    }
}
