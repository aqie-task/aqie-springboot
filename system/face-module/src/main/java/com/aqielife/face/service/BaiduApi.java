package com.aqielife.face.service;

import org.json.JSONObject;

/**
 * @Author aqie
 * @Date 2022/4/18 17:04
 * @desc
 */
public interface BaiduApi {
    JSONObject detect(String image, String imageType);

    JSONObject search(String image, String imageType, String groupIdList);
}
