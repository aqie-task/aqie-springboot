package com.aqielife.face.controller;

import com.aqielife.common.R;
import com.aqielife.face.dto.SearchRequest;
import com.aqielife.face.service.BaiduApi;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author aqie
 * @Date 2022/4/18 17:15
 * @desc
 */
@RestController
@RequestMapping("baidu")
public class BaiduController {
    @Autowired
    private BaiduApi baiduApi;

    @PostMapping("search")
    public R search(@RequestBody SearchRequest request){
        JSONObject search = baiduApi.search(request.getImage(), request.getImageType(), request.getGroupIdList());
        return R.data(search.toString());
    }
}
