package com.aqielife.face.config;


import com.baidu.aip.face.AipFace;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author aqie
 * @Date 2022/4/18 16:52
 * @desc
 */
@Configuration
public class BaiduConfig {

    @Value("${baidu.appID}")
    private String APP_ID ;

    @Value("${baidu.apiKey}")
    private String API_KEY ;

    @Value("${baidu.secretKey}")
    private String SECRET_KEY ;

    @Bean
    public AipFace apiFace(){
        // 初始化一个AipFace
        AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        return client;
    }
}
