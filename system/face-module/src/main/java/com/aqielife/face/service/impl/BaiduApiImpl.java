package com.aqielife.face.service.impl;

import org.json.JSONObject;
import com.aqielife.face.service.BaiduApi;
import com.baidu.aip.face.AipFace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @Author aqie
 * @Date 2022/4/18 17:04
 * @desc
 */
@Service
public class BaiduApiImpl implements BaiduApi {
    @Autowired
    private AipFace aipFace;

    @Override
    public JSONObject detect(String image, String imageType) {
        HashMap<String, Object> options = new HashMap<String, Object>();
        options.put("face_field", "age");
        options.put("max_face_num", "1");
        options.put("face_type", "LIVE");
        options.put("liveness_control", "LOW");
        return aipFace.detect(image, imageType, options);
    }

    @Override
    public JSONObject search(String image, String imageType, String groupIdList) {
        HashMap<String, Object> options = new HashMap<String, Object>();
        options.put("match_threshold", "70");
        options.put("quality_control", "NORMAL");
        options.put("liveness_control", "LOW");
        options.put("max_user_num", "1");
        return aipFace.search(image, imageType, groupIdList, options);
    }
}
