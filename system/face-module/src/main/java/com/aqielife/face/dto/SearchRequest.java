package com.aqielife.face.dto;

import lombok.Data;

/**
 * @Author aqie
 * @Date 2022/4/18 17:45
 * @desc
 */
@Data
public class SearchRequest {
    private String image;
    private String imageType;
    private String groupIdList;
}
