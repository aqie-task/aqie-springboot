package com.aqielife.leetcode.dependencyInjection.v2;

/**
 * @author aqie
 * @date 2020-10-17 15:53
 * @function
 */
public class SmsSender implements MessageSender {
    @Override
    public void send(String cellphone, String message) {
        //....
    }
}
