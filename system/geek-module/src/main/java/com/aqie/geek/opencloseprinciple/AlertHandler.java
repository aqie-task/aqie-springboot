package com.aqielife.leetcode.opencloseprinciple;

/**
 * @author aqie
 * @date 2020-10-17 14:55
 * @function
 */
public abstract class AlertHandler {
    protected AlertRule rule;
    protected Notification notification;
    public AlertHandler(AlertRule rule, Notification notification) {
        this.rule = rule;
        this.notification = notification;
    }
    public abstract void check(ApiStatInfo apiStatInfo);
}
