package com.aqielife.leetcode.service;

/**
 * @author aqie
 * @date 2020-10-17 11:37
 * @function
 */
public interface EggLayable {
    void layEgg();
}
