package com.aqielife.leetcode.opencloseprinciple;

/**
 * @author aqie
 * @date 2020-10-17 15:03
 * @function
 */
public class Main {
    public static void main(String[] args) {
        ApiStatInfo apiStatInfo = new ApiStatInfo();
        // ... 省略设置 apiStatInfo 数据值的代码
        apiStatInfo.setTimeoutCount(289);
        ApplicationContext.getInstance().getAlert().check(apiStatInfo);
    }
}
