package com.aqielife.leetcode.opencloseprinciple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aqie
 * @date 2020-10-17 14:41
 * @function
 */
public class Alert {
    private List<AlertHandler> alertHandlers = new ArrayList<>();
    public void addAlertHandler(AlertHandler alertHandler) {
        this.alertHandlers.add(alertHandler);
    }


    public void check(ApiStatInfo apiStatInfo) {
        for (AlertHandler handler : alertHandlers) {
            handler.check(apiStatInfo);
        }
    }

}
