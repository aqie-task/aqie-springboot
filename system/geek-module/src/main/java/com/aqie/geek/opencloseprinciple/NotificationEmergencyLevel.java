package com.aqielife.leetcode.opencloseprinciple;

/**
 * @author aqie
 * @date 2020-10-17 14:43
 * @function
 */
public enum  NotificationEmergencyLevel {
    SEVERE("严重"),URGENCY("紧急"),NORMAL("普通"),TRIVIAL("无关紧要");
    private String name;

    NotificationEmergencyLevel(String name) {
        this.name = name;
    }}
