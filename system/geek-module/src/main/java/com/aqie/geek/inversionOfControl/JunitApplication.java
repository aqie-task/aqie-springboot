package com.aqielife.leetcode.inversionOfControl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aqie
 * @date 2020-10-17 15:44
 * @function 控制反转
 */
public class JunitApplication {
    private static final List<TestCase> testCases = new ArrayList<>();

    public static void register(TestCase testCase) {
        testCases.add(testCase);
    }

    public static final void main(String[] args) {
        JunitApplication.register(new UserServiceTest());
        for (TestCase cases:testCases){
            cases.run();
        }
    }
}
