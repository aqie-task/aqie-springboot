package com.aqielife.leetcode.interfaceSegregationPrinciple.user;

/**
 * @author aqie
 * @date 2020-10-17 15:19
 * @function
 */
public interface UserService {
    boolean register(String cellphone, String password);
    boolean login(String cellphone, String password);
    UserInfo getUserInfoById(long id);
    UserInfo getUserInfoByCellphone(String cellphone);
}
