package com.aqielife.leetcode.retry;

/**
 * @author aqie
 * @date 2020-10-22 11:00
 * @function
 */
public class Base {
    private static final int MAX_RETRIES = 100; // ms
    private static final long MAX_WAIT_INTERVAL = 500; // ms

    public static long getWaitTimeExp(int retryCount) {
        long waitTime = ((long) Math.pow(2, retryCount) );
        return waitTime;
    }

    public static void doOperationAndWaitForResult() throws InterruptedException {
// Do some asynchronous operation.
        long token = asyncOperation();
        int retries = 0;
        boolean retry = false;
        do {
// Get the result of the asynchronous operation.
            Results result = getAsyncOperationResult(token);
            if (Results.SUCCESS == result) {
                retry = false;
            } else if ( (Results.NOT_READY == result) ||
                    (Results.TOO_BUSY == result) ||
                    (Results.NO_RESOURCE == result) ||
                    (Results.SERVER_ERROR == result) ) {
                retry = true;
            } else {
                retry = false;
            }
            if (retry) {
                long waitTime = Math.min(getWaitTimeExp(retries), MAX_WAIT_INTERVAL);
// Wait for the next Retry.
                Thread.sleep(waitTime);
            }
        } while (retry && (retries++ < MAX_RETRIES));
    }

    private static Results getAsyncOperationResult(long token) {
        return null;
    }

    private static long asyncOperation() {
        return 0;
    }
}


