package com.aqielife.leetcode.entity;

import com.aqielife.leetcode.service.EggLayable;
import com.aqielife.leetcode.service.Flyable;
import com.aqielife.leetcode.service.impl.EggLayAbility;
import com.aqielife.leetcode.service.impl.FlyAbility;

/**
 * @author aqie
 * @date 2020-10-17 11:40
 * @function
 */
public class Duck implements Flyable, EggLayable {
    private FlyAbility flyAbility = new FlyAbility(); // 组合
    private EggLayAbility eggLayAbility = new EggLayAbility(); // 组合
    @Override
    public void layEgg() {
        eggLayAbility.layEgg(); // 委托
    }

    @Override
    public void fly() {
        flyAbility.fly();
    }
}
