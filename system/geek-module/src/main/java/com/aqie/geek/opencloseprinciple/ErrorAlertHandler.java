package com.aqielife.leetcode.opencloseprinciple;

/**
 * @author aqie
 * @date 2020-10-17 15:00
 * @function
 */
public class ErrorAlertHandler extends AlertHandler {
    public ErrorAlertHandler(AlertRule rule, Notification notification){
        super(rule, notification);
    }
    @Override
    public void check(ApiStatInfo apiStatInfo) {
        if (apiStatInfo.getErrorCount() > rule.getMatchedRule(apiStatInfo.getApi())){
            notification.notify(NotificationEmergencyLevel.SEVERE, "...");
        }
    }

}
