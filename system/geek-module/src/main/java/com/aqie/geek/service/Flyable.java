package com.aqielife.leetcode.service;

/**
 * @author aqie
 * @date 2020-10-17 11:36
 * @function
 */
public interface Flyable {
    void fly();
}
