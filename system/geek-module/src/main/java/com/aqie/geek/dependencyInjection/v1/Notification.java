package com.aqielife.leetcode.dependencyInjection.v1;

/**
 * @author aqie
 * @date 2020-10-17 15:49
 * @function
 * Notification 类负责消息推送，依赖注入
 * MessageSender 类实现推送商品促销、验证码等消息给用户
 */
public class Notification {
    private MessageSender messageSender;

    // 通过构造函数将 messageSender 传递进来
    public Notification(MessageSender messageSender) {
        this.messageSender = messageSender;
    }
    public void sendMessage(String cellphone, String message) {
        //... 省略校验逻辑等...
        this.messageSender.send(cellphone, message);
    }

    public static void main(String[] args) {
        // 使用 Notification
        MessageSender messageSender = new MessageSender();
        Notification notification = new Notification(messageSender);
        notification.sendMessage("aa", "bb");
    }
}
