package com.aqielife.leetcode.opencloseprinciple;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aqie
 * @date 2020-10-17 14:55
 * @function
 */
@Getter
@Setter
public class ApiStatInfo {
    private long timeoutCount;
    private String api;
    private long requestCount;
    private long errorCount;
    private long durationOfSeconds;
}
