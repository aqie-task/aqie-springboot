package com.aqielife.leetcode.service.impl;

import com.aqielife.leetcode.service.EggLayable;

/**
 * @author aqie
 * @date 2020-10-17 11:38
 * @function
 */
public class EggLayAbility implements EggLayable {
    @Override
    public void layEgg() {
        System.out.println("lay egg");
    }
}
