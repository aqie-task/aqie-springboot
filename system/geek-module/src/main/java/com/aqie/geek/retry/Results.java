package com.aqielife.leetcode.retry;

/**
 * @author aqie
 * @date 2020-10-22 11:00
 * @function
 */
public enum Results {
    SUCCESS,
    NOT_READY,
    TOO_BUSY,
    NO_RESOURCE,
    SERVER_ERROR
}
