package com.aqielife.leetcode.interfaceSegregationPrinciple.user;

/**
 * @author aqie
 * @date 2020-10-17 15:20
 * @function
 */
public interface RestrictedUserService {
    boolean deleteUserByCellphone(String cellphone);
    boolean deleteUserById(long id);
}
