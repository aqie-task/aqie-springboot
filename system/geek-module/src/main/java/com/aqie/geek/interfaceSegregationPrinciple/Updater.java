package com.aqielife.leetcode.interfaceSegregationPrinciple;

/**
 * @author aqie
 * @date 2020-10-17 15:25
 * @function
 * 1. 希望支持 Redis 和 Kafka 配置信息的热更新,配置中心中更改了配置信息，我们希望在不用重启系统的情况下，能将最新的配置信息加载到内存中
 */
public interface Updater {
    void update();
}
