package com.aqielife.leetcode.dependencyInjection.v2;


/**
 * @author aqie
 * @date 2020-10-17 15:52
 * @function
 */
public class Notification {
    private MessageSender messageSender;
    public Notification(MessageSender messageSender) {
        this.messageSender = messageSender;
    }
    public void sendMessage(String cellphone, String message) {
        this.messageSender.send(cellphone, message);
    }

    public static void main(String[] args) {
        // 使用 Notification
        MessageSender messageSender = new SmsSender();
        Notification notification = new Notification(messageSender);
    }
}
