package com.aqielife.leetcode.statistics;

import java.util.List;
import java.util.Map;

/**
 * 一次性取太长时间区间的数据，可能会导致拉取太多的数据到内存
 */
public interface MetricsStorage {
    void saveRequestInfo(RequestInfo requestInfo);
    List<RequestInfo> getRequestInfosByDuration(String apiName, long startTimestamp, long endTimestamp);
    Map<String, List<RequestInfo>> getAllRequestInfosByDuration(long startTimestamp, long endTimestamp);
}