package com.aqielife.leetcode.interfaceSegregationPrinciple.user;

/**
 * @author aqie
 * @date 2020-10-17 15:21
 * @function
 */
public class UserServiceImpl implements UserService, RestrictedUserService{
    @Override
    public boolean register(String cellphone, String password) {
        return false;
    }

    @Override
    public boolean login(String cellphone, String password) {
        return false;
    }

    @Override
    public UserInfo getUserInfoById(long id) {
        return null;
    }

    @Override
    public UserInfo getUserInfoByCellphone(String cellphone) {
        return null;
    }

    @Override
    public boolean deleteUserByCellphone(String cellphone) {
        return false;
    }

    @Override
    public boolean deleteUserById(long id) {
        return false;
    }
}
