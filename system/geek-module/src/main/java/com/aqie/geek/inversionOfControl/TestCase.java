package com.aqielife.leetcode.inversionOfControl;

/**
 * @author aqie
 * @date 2020-10-17 15:43
 * @function
 */
public abstract class TestCase {
    public void run() {
        if (doTest()) {
            System.out.println("Test succeed.");
        } else {
            System.out.println("Test failed.");
        }
    }
    public abstract boolean doTest();
}
