package com.aqielife.leetcode.performanceOptimization;

/**
 * @author aqie
 * @date 2020-12-22 16:55
 * @function
 */
public class StringTest {
  public static void main(String[] args) {
    // 字符串常量池
    String str1= "abc";
    String str4= "abc";
    String str2= new String("abc");
    String str3= str2.intern();
    System.out.println(str1==str2); //  false
    System.out.println(str1==str4); //  true
    System.out.println(str2==str3); //  false
    System.out.println(str1==str3); //  true str1在常量池中已经建立了"abc"，这个时候str3是从常量池里取出来的

    String a =new String("abc").intern();
    String b = new String("abc").intern();
    if(a==b) {
      System.out.print("a==b");
    }
  }
}
