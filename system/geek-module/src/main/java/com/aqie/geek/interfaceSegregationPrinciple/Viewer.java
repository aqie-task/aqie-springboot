package com.aqielife.leetcode.interfaceSegregationPrinciple;

import java.util.Map;

/**
 * @author aqie
 * @date 2020-10-17 15:32
 * @function
 * 新的监控功能需求,开发一个内嵌的 SimpleHttpServer，输出项目的配置信息到一个固定的HTTP 地址
 */
public interface Viewer {
    String outputInPlainText();
    Map<String, String> output();
}
