package com.aqielife.leetcode.opencloseprinciple;

/**
 * @author aqie
 * @date 2020-10-17 15:08
 * @function
 */
public class TimeoutAlertHandler extends AlertHandler {
    public TimeoutAlertHandler(AlertRule rule, Notification notification) {
        super(rule, notification);
    }

    @Override
    public void check(ApiStatInfo apiStatInfo) {

    }// 省略代码...}

    public class ApplicationContext {
        private AlertRule alertRule;
        private Notification notification;
        private Alert alert;

        public void initializeBeans() {
            alertRule = new AlertRule(/*. 省略参数.*/); // 省略一些初始化代码
            notification = new Notification(/*. 省略参数.*/); // 省略一些初始化代码
            alert = new Alert();
            alert.addAlertHandler(new TpsAlertHandler(alertRule, notification));
            alert.addAlertHandler(new ErrorAlertHandler(alertRule, notification));
// 改动三：注册 handler
            alert.addAlertHandler(new TimeoutAlertHandler(alertRule, notification));
        }
//... 省略其他未改动代码...
    }
}
