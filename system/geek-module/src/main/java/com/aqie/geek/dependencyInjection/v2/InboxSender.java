package com.aqielife.leetcode.dependencyInjection.v2;

/**
 * @author aqie
 * @date 2020-10-17 15:53
 * @function 站内信发送
 */
public class InboxSender implements MessageSender {
    @Override
    public void send(String cellphone, String message) {
        //....
    }
}
