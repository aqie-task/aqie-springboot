package com.aqielife.leetcode.service.impl;

import com.aqielife.leetcode.service.Flyable;

/**
 * @author aqie
 * @date 2020-10-17 11:39
 * @function
 */
public class FlyAbility implements Flyable {
    @Override
    public void fly() {
        System.out.println("fly");
    }
}
