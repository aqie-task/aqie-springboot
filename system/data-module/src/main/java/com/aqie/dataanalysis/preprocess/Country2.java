package com.aqie.dataanalysis.preprocess;

import lombok.Data;

import java.util.Scanner;

/**
 * @author: aqie
 * @create: 2020-07-31 16:27
 **/
@Data
class Country2 implements Comparable<Country2>{
    protected String name;
    protected int area;


    /*  Constructs a new Country object from the next line being scanned.
        If there are no more lines, the new object's fields are left null.
    */
    public Country2(Scanner in) {
        if (in.hasNextLine()) {
            this.name = in.next();
            this.area = in.nextInt();
        }
    }


    public boolean isNull() {
        return this.area <= 0;
    }

    @Override
    public String toString() {
        return name + "   " + area;
    }

    @Override
    public int compareTo(Country2 o) {
        return this.area - o.area; // 按地区升序
    }
}
