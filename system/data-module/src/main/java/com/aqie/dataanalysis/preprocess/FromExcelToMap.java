package com.aqie.dataanalysis.preprocess;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author: aqie
 * @create: 2020-07-31 15:43
 **/
public class FromExcelToMap {
    public static void main(String[] args) {
        Map<String,Integer> map = loadXL("data/Countries.xlsx", "Countries Worksheet");
        print(map);
    }

    public static void print(Map<String,Integer> map) {
        Set<String> countries = map.keySet();
        for (Object country : countries) {
            Object population = map.get(country);
            System.out.printf("%-10s%,12d%n", country, population);
        }
    }

    /** Returns a Map object containing the data from the specified
     worksheet in the specified Excel file.
     */
    public static Map<String,Integer> loadXL(String fileSpec, String sheetName) {
        Map<String,Integer> map = new TreeMap<>();
        try {
            FileInputStream stream = new FileInputStream(fileSpec);
            HSSFWorkbook workbook = new HSSFWorkbook(stream);
            HSSFSheet worksheet = workbook.getSheet(sheetName);
            DataFormatter formatter = new DataFormatter();
            for (Row row : worksheet) {
                HSSFRow hssfRow = (HSSFRow)row;
                HSSFCell cell = hssfRow.getCell(0);
                String country = cell.getStringCellValue();
                cell = hssfRow.getCell(1);
                String str = formatter.formatCellValue(cell);
                Integer population = Integer.valueOf(str);
                map.put(country, population);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        }
        return map;
    }
}
