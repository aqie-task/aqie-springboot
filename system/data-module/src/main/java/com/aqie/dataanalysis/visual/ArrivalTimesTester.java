package com.aqie.dataanalysis.visual;

import java.util.Random;

/**
 * @author: aqie
 * @create: 2020-08-01 08:26
 **/
public class ArrivalTimesTester {
    static final Random random = new Random();
    static final double LAMBDA = 0.25;

    static double time() {
        double p = random.nextDouble();
        return -Math.log(1 - p)/LAMBDA;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 8; i++) {
            System.out.println(time());
        }
    }
}
