package com.aqie.dataanalysis.preprocess;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author: aqie
 * @create: 2020-07-31 14:32
 **/
@Slf4j
public class HashMapExample {
    private static final String DEFAULT_FILE_PATH = "data/Countries.dat";
    private static final String DEFAULT_DELIMITER = "\\p{javaWhitespace}+";
    private static final String FILE_PATH = "data/Countries.csv";
    private static final String DELIMITER = ",|\\s";
    public static void main(String[] args) {
        readCountryData(DEFAULT_FILE_PATH,DEFAULT_DELIMITER);
        readCountryData(FILE_PATH,DELIMITER);
    }

    public static void readCountryData(String filepath,String delimiter) {
        File dataFile = new File(filepath);
        HashMap<String, Integer> dataset = new HashMap<>(10);
        try {
            Scanner input = new Scanner(dataFile);
            input.useDelimiter(delimiter);
            String column1 = input.next();
            String column2 = input.next();
            System.out.printf("%-10s%12s%n", column1, column2);
            while (input.hasNext()) {
                String country = input.next();
                int population = input.nextInt();
                dataset.put(country, population);
                System.out.printf("%-10s%12s%n", country, population);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        System.out.printf("dataset.size(): %d%n", dataset.size());
        System.out.printf("dataset.get(\"Peru\"): %,d%n", dataset.get("Peru"));
        dataset.put("Peru", 31000000);
        System.out.printf("dataset.size(): %d%n", dataset.size());
        System.out.printf("dataset.get(\"Peru\"): %,d%n", dataset.get("Peru"));
    }
}
