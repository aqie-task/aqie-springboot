package com.aqie.dataanalysis.preprocess;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * @author: aqie
 * @create: 2020-07-31 16:30
 **/
public class SortingData {
    public static void main(String[] args) {
        File file = new File("data/Countries.dat");
        TreeMap<Integer,String> dataset = new TreeMap();
        try {
            Scanner input = new Scanner(file);
            String column1 = input.next();
            String column2 = input.next();
            while (input.hasNext()) {
                String x = input.next();
                int y = input.nextInt();
                dataset.put(y, x);
            }
            input.close();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        print(dataset);
    }

    public static void print(TreeMap<Integer,String> map) {
        for (Integer key : map.keySet()) {
            System.out.printf("%,12d  %-16s%n", key, map.get(key));
        }
    }
}
