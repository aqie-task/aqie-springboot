package com.aqie.dataanalysis.preprocess;

import lombok.Data;

import java.util.Scanner;

/**
 * @author: aqie
 * @create: 2020-07-31 16:27
 **/
@Data
class Country{
    protected String name;
    protected int population;
    protected int area;
    protected boolean landlocked;


    /*  Constructs a new Country object from the next line being scanned.
        If there are no more lines, the new object's fields are left null.
    */
    public Country(Scanner in) {
        if (in.hasNextLine()) {
            this.name = in.next();
            this.population = in.nextInt();
            this.area = in.nextInt();
            this.landlocked = in.nextBoolean();
        }
    }


}
