package com.aqielife.seckill.task;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * todo
 * @Author aqie
 * @Date 2022/4/21 14:19
 * @desc org.redisson.client.RedisException: java.lang.InterruptedException
 */
@Component
@Slf4j
@AllArgsConstructor
public class KillTask implements Runnable {
    private final StringRedisTemplate stringRedisTemplate;

    @Override
    public void run() {
        synchronized (stringRedisTemplate) {
            int num = Integer.parseInt(Objects.requireNonNull(stringRedisTemplate.opsForValue().get("kill_num")));
            if (num > 0) {
                List<String> watchList = new ArrayList<>(Arrays.asList("kill_num", "kill_user"));

                stringRedisTemplate.setEnableTransactionSupport(true);
                try {
                    stringRedisTemplate.watch(watchList);
                    stringRedisTemplate.multi();
                    stringRedisTemplate.opsForValue().decrement("kill_num");
                    stringRedisTemplate.opsForList().rightPush("kill_user",
                            String.valueOf(new Random().nextInt(10000)));
                    stringRedisTemplate.exec();
                } catch (Exception e) {
                    log.error("seckill error {}", e.getMessage());
                    stringRedisTemplate.discard();
                }

            }
        }
    }
}
