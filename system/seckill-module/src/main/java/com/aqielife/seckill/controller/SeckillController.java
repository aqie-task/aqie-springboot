package com.aqielife.seckill.controller;

import com.aqielife.common.R;
import com.aqielife.seckill.task.KillTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author aqie
 * @Date 2022/4/21 14:34
 * @desc
 */
@Slf4j
@RestController
@RequestMapping("seckill")
public class SeckillController {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private KillTask killTask;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("start")
    public R start(){
        stringRedisTemplate.opsForValue().set("kill_num", "50");
        try{
            for(int i = 0; i < 1000; i ++) {
                threadPoolTaskExecutor.execute(killTask);
            }
        } catch(Exception e) {
            log.error("异常：{}", e.getMessage());
        } finally {
            threadPoolTaskExecutor.shutdown();
        }
        return R.data("ok");
    }
}
