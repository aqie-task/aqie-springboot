package com.aqielife.java8.chapter8.demo;

import java.util.concurrent.*;

import static com.aqielife.java8.chapter8.Util.delay;

/**
 * @Author: aqie
 * @Date: 2021-03-13 8:23
 * @Description:
 */
public class FutureDemo {
  public static void main(String[] args) {
    asyncTask();
  }

  private static void asyncTask(){
    ExecutorService executor = Executors.newCachedThreadPool();
    Future<Double> future = executor.submit(new Callable<Double>() {
      @Override
      public Double call() throws Exception {
        return doSomeLongComputation();
      }
    });
    doSomethingElse();

    try {
      Double result = future.get(10, TimeUnit.SECONDS);
      System.out.println("result :" + result);
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (TimeoutException e) {
      e.printStackTrace();
    }
  }

  private static Double doSomeLongComputation() {
    delay();
    System.out.println("doSomeLongComputation");
    return 100.00;
  }

  private static void doSomethingElse() {
    System.out.println("doSomethingElse");
  }
}
