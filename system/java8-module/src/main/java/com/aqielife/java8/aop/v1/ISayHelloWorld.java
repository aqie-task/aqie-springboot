package com.aqielife.java8.aop.v1;

/**
 * @Author aqie
 * @Date 2022/4/22 11:36
 * @desc
 */
public interface ISayHelloWorld {
    public String say();
}

