package com.aqielife.java8.chapter3;

import com.aqielife.java8.bean.Trader;
import com.aqielife.java8.bean.Transaction;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

/**
 * @Author: aqie
 * @Date: 2021-03-10 10:23
 * @Description:
 */
public class StreamDemo3 {
  /**
   * (1) 找出2011年发生的所有交易，并按交易额排序（从低到高）。
   * (2) 交易员都在哪些不同的城市工作过？
   * (3) 查找所有来自于剑桥的交易员，并按姓名排序。
   * (4) 返回所有交易员的姓名字符串，按字母顺序排序。
   * (5) 有没有交易员是在米兰工作的？
   * (6) 打印生活在剑桥的交易员的所有交易额。
   * (7) 所有交易中，最高的交易额是多少？
   * (8) 找到交易额最小的交易。
   */
  public static void main(String[] args) {
    List<Transaction> transactions = initData();

    // 1.
    transactions.stream()
        .filter(t -> t.getYear() == 2011)
        // 正序
        // .sorted(Comparator.comparing(Transaction::getValue))
        // 倒序 大 -> 小
        .sorted((o1, o2) -> o2.getValue() - o1.getValue())
        .forEach(System.out::println);

    //2. 去掉 distinct() ，改用 toSet()
    List<String> cities = transactions.stream()
        .map(transaction -> transaction.getTrader().getCity())
        .distinct()
        .collect(toList());
    System.out.println(cities);

    // 3. 查找所有来自于剑桥的交易员，并按姓名排序
    List<Trader> traders = transactions.stream()
        .map(transaction -> transaction.getTrader())
        .distinct()
        .filter(trader -> "Cambridge".equals(trader.getCity()))
        .sorted(comparing(Trader::getName)).collect(toList());
    System.out.println(traders);

    // 4. 返回所有交易员的姓名字符串，按字母顺序排序
    String traderStr = transactions.stream()
        .map(transaction -> transaction.getTrader().getName())
        .distinct()
        // 倒序
        .sorted(reverseOrder())
         .reduce("", (n1, n2) -> n1 + n2);
    System.out.println(traderStr);

    // 5. 任意匹配 有没有交易员是在米兰工作的
    boolean match = transactions.stream()
        .anyMatch(transaction -> "Milan".equals(transaction.getTrader().getCity()));
    System.out.println(match);

    // 6. 剑桥交易总额
    Integer totalMoneyInCambridge = transactions.stream()
        .filter(transaction -> transaction.getTrader().getCity().equals("Cambridge"))
        .map(transaction -> transaction.getValue())
        .reduce(0, (a, b) -> a + b);
    System.out.println(totalMoneyInCambridge);

    // 7. 最大值
    Integer max = transactions.stream()
        // Transaction::getValue
        .map(transaction -> transaction.getValue())
        // Integer::max
        .reduce(0,(a, b) -> a > b ? a : b);
    System.out.println(max);

    Optional<Integer> max2 = transactions.stream()
        .map(Transaction::getValue)
        .max(Comparator.comparing(Integer::intValue));
    System.out.println(max2.get());


    // 8. 最小交易
    Optional<Transaction> smallestTransaction =
        transactions.stream()
            .reduce((t1, t2) ->
                t1.getValue() < t2.getValue() ? t1 : t2);

    Optional<Transaction> min = transactions.stream()
        .min(comparing(Transaction::getValue));
    System.out.println(min.get());
  }

  private static List<Transaction> initData() {
    Trader raoul = new Trader("Raoul", "Cambridge");
    Trader mario = new Trader("Mario","Milan");
    Trader alan = new Trader("Alan","Cambridge");
    Trader brian = new Trader("Brian","Cambridge");
    List<Transaction> transactions = Arrays.asList(
        new Transaction(brian, 2011, 500),
        new Transaction(brian, 2011, 300),
        new Transaction(raoul, 2012, 1000),
        new Transaction(raoul, 2011, 400),
        new Transaction(mario, 2012, 710),
        new Transaction(mario, 2012, 700),
        new Transaction(alan, 2012, 950)
    );
    return transactions;
  }
}
