package com.aqielife.java8.utils;

import java.util.Optional;

/**
 * @Author: aqie
 * @Date: 2021-03-12 14:24
 * @Description:
 */
public class OptionalUtility {
  /**
   * @param s
   * @return String 转换为 Integer ，并返回一个 Optional 对象
   */
  public static Optional<Integer> stringToInt(String s) {
    try {
      return Optional.of(Integer.parseInt(s));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }
}
