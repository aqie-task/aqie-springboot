package com.aqielife.java8.chapter6;

/**
 * @Author: aqie
 * @Date: 2021-03-11 11:28
 * @Description:
 */
interface Task{
  public void execute();
}
