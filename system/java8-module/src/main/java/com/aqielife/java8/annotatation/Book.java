package com.aqielife.java8.annotatation;

import java.util.Arrays;

/**
 * @Author: aqie
 * @Date: 2021-01-29 15:03
 * @Description:
 */
@Author(name = "Raoul")
@Author(name = "Mario")
@Author(name = "Alan")
public class Book {

  public static void main(String[] args) {
    Author[] authors = Book.class.getAnnotationsByType(Author.class);
    Arrays.stream(authors).forEach(a -> {
      System.out.println(a.name());
    });
  }

}
