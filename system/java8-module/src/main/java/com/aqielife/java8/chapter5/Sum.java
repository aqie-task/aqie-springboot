package com.aqielife.java8.chapter5;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static com.aqielife.java8.utils.NumberUtils.measureSumPerf;

/**
 * @Author: aqie
 * @Date: 2021-03-11 10:10
 * @Description: 接受数字n作为参数，并返回从1到给定参数的所有数字的和
 *  ForkJoinPool, 默认的线程数量是处理器数量
 */
public class Sum {
  public static void main(String[] args) {
    // System.out.println(sequentialSum(100));
    // System.out.println(parallelSum(100));

    // 100ms
    // System.out.println("Sequential sum done in:" + measureSumPerf(Sum::sequentialSum, 10_000_000) + " msecs");
    // 335ms
    // System.out.println("parallel sum done in:" + measureSumPerf(Sum::parallelSum, 10_000_000) + " msecs");

    // 1ms
    System.out.println("Parallel range sum done in:" + measureSumPerf(Sum::parallelRangedSum, 10_000_000) +" msecs");

    // 4ms
    // System.out.println("Iterative sum done in:" + measureSumPerf(Sum::iterativeSum, 10_000_000) + " msecs");

    // 139
    System.out.println("SideEffect parallel sum done in: " +
        measureSumPerf(Sum::sideEffectParallelSum, 10_000_000L) +"msecs" );

    // 44ms
    System.out.println("forkJoin: " + measureSumPerf(ForkJoinSumCalculator::forkJoinSum, 10_000_000L) +"msecs");
  }

  public static long sequentialSum(long n) {
    return Stream.iterate(1L, i -> i + 1)
        .limit(n)
        .reduce(0L, Long::sum);
  }

  /**
   * iterate 生成的是装箱的对象，必须拆箱成数字才能求和；
   * 我们很难把 iterate 分成多个独立块来并行执行
   * @param n
   * @return
   */
  public static long parallelSum(long n) {
    return Stream.iterate(1L, i -> i + 1)
        .limit(n)
        .parallel()
        .reduce(0L, Long::sum);
  }

  /**
   *  LongStream.rangeClosed 直接产生原始类型的 long 数字，没有装箱拆箱的开销
   *   LongStream.rangeClosed 会生成数字范围，很容易拆分为独立的小块
   * @param n
   * @return
   */
  public static long parallelRangedSum(long n) {
    return LongStream.rangeClosed(1, n)
        .parallel()
        .reduce(0L, Long::sum);
  }

  // 更为底层，更重要的是不需要对原始类型做任何装箱或拆箱操作
  public static long iterativeSum(long n) {
    long result = 0;
    for (long i = 1L; i <= n; i++) {
      result += i;
    }
    return result;
  }

  /**
   * 错误使用并行流
   * @param n
   * @return
   */
  public static long sideEffectParallelSum(long n) {
    Accumulator accumulator = new Accumulator();
    LongStream.rangeClosed(1, n)
        .parallel()
        .forEach(accumulator::add);
    return accumulator.total.get();
  }

}

/**
 * 使用原子类优化
 */
class Accumulator {
  public AtomicLong total = new AtomicLong(0);
  public void add(long value) {
    total.addAndGet(value);
  }
}
