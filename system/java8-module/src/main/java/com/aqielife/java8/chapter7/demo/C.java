package com.aqielife.java8.chapter7.demo;

/**
 * @Author: aqie
 * @Date: 2021-03-11 17:19
 * @Description:
 */
public class C implements A,B{
  public static void main(String... args) {
    new C().hello();
    System.out.println(new C().getNumber());
  }

  @Override
  public void hello() {
    B.super.hello();
  }

  @Override
  public Integer getNumber() {
    return B.super.getNumber();
  }
}
