package com.aqielife.java8.chapter8;

import com.aqielife.java8.chapter8.expand.Discount;

import static com.aqielife.java8.chapter8.Util.delay;
import static com.aqielife.java8.chapter8.Util.format;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Shop {

  private final String name;
  private final Random random;

  public Shop(String name) {
    this.name = name;
    random = new Random(name.charAt(0) * name.charAt(1) * name.charAt(2));
  }

  public Double getPrice2(String product) {
    return calculatePrice(product);
  }

  public String getPrice(String product) {
    double price = calculatePrice(product);
    Discount.Code code = Discount.Code.values()[random.nextInt(Discount.Code.values().length)];
    return name + ":" + price + ":" + code;
  }

  /**
   *
   * @param product
   * @return
   */
  public Future<Double> getPriceAsync(String product) {
    CompletableFuture<Double> futurePrice = new CompletableFuture<>();
    new Thread(
            () -> {
              try {
                double price = calculatePrice(product);
                futurePrice.complete(price);
              } catch (Exception ex) {
                futurePrice.completeExceptionally(ex);
              }
            })
        .start();
    return futurePrice;
  }

  public double calculatePrice(String product) {
    delay();
    // throw new RuntimeException("product not available");
    return format(random.nextDouble() * product.charAt(0) + product.charAt(1));
  }

  public String getName() {
    return name;
  }

  public static void main(String[] args) {
    Shop shop = new Shop("BestShop");
    long start = System.nanoTime();
    Future<Double> futurePrice = shop.getPriceAsync("my favorite product");
    long invocationTime = ((System.nanoTime() - start) / 1_000_000);
    System.out.println("Invocation returned after " + invocationTime + " msecs");

    doSomethingElse();
    // 在计算商品价格的同时
    try {
      // 客户要么获得 Future 中封装的值（如果异步任务已经完成），要么发生阻塞
      double price = futurePrice.get();
      System.out.printf("Price is %.2f%n", price);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    long retrievalTime = ((System.nanoTime() - start) / 1_000_000);
    System.out.println("Price returned after " + retrievalTime + " msecs");
  }

  private static void doSomethingElse() {
    System.out.println("执行更多任务，比如查询其他商店");
  }
}
