package com.aqielife.java8.chapter8;

import com.aqielife.java8.aop.CglibProxy;
import com.aqielife.java8.chapter8.expand.Discount;
import com.aqielife.java8.chapter8.expand.Quote;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class BestPriceFinder {

  private final List<Shop> shops =
      Arrays.asList(
          new Shop("BestPrice"),
          new Shop("LetsSaveBig"),
          new Shop("MyFavoriteShop"),
          new Shop("BuyItAll"),
          new Shop("ShopEasy"));

  private final Executor executor =
      Executors.newFixedThreadPool(
          // 定制线程池
          Math.min(shops.size(), 100),
          new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
              Thread t = new Thread(r);
              // 不会阻止程序的关停
              t.setDaemon(true);
              return t;
            }
          });

  /**
   * 查询五家店 需要 5044 msecs
   *
   * @param product
   * @return
   */
  public List<String> findPrices(String product) {
    return shops.stream()
        .map(shop -> String.format("%s price is %.2f", shop.getName(), shop.getPrice2(product)))
        .collect(toList());
  }

  /**
   * 并发执行 2019 msecs
   *
   * @param product
   * @return
   */
  public List<String> findPricesParal(String product) {
    return shops.parallelStream()
        .map(shop -> String.format("%s price is %.2f", shop.getName(), shop.getPrice2(product)))
        .collect(toList());
  }

  /**
   * 1071 msecs
   * 使用 CompletableFuture 实现 findPrices 方法
   * 同时添加自定义线程池
   * @param product
   * @return
   */
  public List<String> findPricesFuture2(String product) {
    List<CompletableFuture<String>> priceFutures =
        shops.stream()
            .map(
                shop ->
                    CompletableFuture.supplyAsync(
                        () -> shop.getName() + " price is " + shop.getPrice(product),executor))
            .collect(Collectors.toList());
    return priceFutures.stream().map(CompletableFuture::join).collect(toList());
  }

  /**
   * 对多个异步任务进行流水线操作
   */


  /**
   * sequential done in 10125 msecs
   * @param product
   * @return
   */
  public List<String> findPricesSequential(String product) {
    return shops.stream()
        .map(shop -> shop.getPrice(product))
        .map(Quote::parse)
        .map(Discount::applyDiscount)
        .collect(toList());
  }

  /**
   * parallel done in 4032 msecs
   * @param product
   * @return
   */
  public List<String> findPricesParallel(String product) {
    return shops.parallelStream()
        .map(shop -> shop.getPrice(product))
        .map(Quote::parse)
        .map(Discount::applyDiscount)
        .collect(toList());
  }


  /**
   * composed CompletableFuture done in 2028 msecs
   * @param product
   * @return
   */
  public List<String> findPricesFuture(String product) {
    List<CompletableFuture<String>> priceFutures =
        findPricesStream(product).collect(Collectors.<CompletableFuture<String>>toList());

    return priceFutures.stream().map(CompletableFuture::join).collect(toList());
  }

  public Stream<CompletableFuture<String>> findPricesStream(String product) {
    return shops.stream()
        .map(shop -> CompletableFuture.supplyAsync(() -> shop.getPrice(product), executor))
        .map(future -> future.thenApply(Quote::parse))
        .map(future -> future.thenCompose(quote ->
                CompletableFuture.supplyAsync(
                () -> Discount.applyDiscount(quote), executor)));
  }

  public void printPricesStream(String product) {
    long start = System.nanoTime();
    CompletableFuture[] futures = findPricesStream(product)
        .map(f -> f.thenAccept(s -> System.out.println(s + " (done in " + ((System.nanoTime() - start) / 1_000_000) + " msecs)")))
        .toArray(size -> new CompletableFuture[size]);
    CompletableFuture.allOf(futures).join();
    System.out.println("All shops have now responded in " + ((System.nanoTime() - start) / 1_000_000) + " msecs");
  }

  public static void main(String[] args) {
    CglibProxy proxy = new CglibProxy();
    // 通过生成子类的方式创建代理类
    BestPriceFinder proxyImp = (BestPriceFinder) proxy.getProxy(BestPriceFinder.class);
    proxyImp.findPrices("myPhone27S");
    proxyImp.findPricesParal("myPhone27S");
    proxyImp.findPricesFuture2("myPhone27S");
  }
}
