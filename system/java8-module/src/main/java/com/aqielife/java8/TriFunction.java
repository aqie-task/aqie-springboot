package com.aqielife.java8;

/**
 * @Author aqie
 * @Date 2022/4/22 11:30
 * @desc
 */
public interface TriFunction<T, U, V, R>{
    R apply(T t, U u, V v);
}

