package com.aqielife.java8.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: aqie
 * @Date: 2021-01-30 15:53
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Color {
  private int red;
  private int green;
  private int yellow;
}
