package com.aqielife.java8.chapter7.api;

/**
 * Created by raoul-gabrielurma on 15/01/2014.
 */
public interface Drawable{
    public void draw();
}
