package com.aqielife.java8.chapter3;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/** @Author: aqie @Date: 2021-03-10 14:35 @Description: 值序列、数组、文件来创建流 由生成函数来创建无限流 */
public class StreamDemo5 {
  public static void main(String[] args) {
    // 由值创建流
    Stream<String> stream = Stream.of("Java 8 ", "Lambdas ", "In ", "Action");
    stream.map(String::toUpperCase).forEach(System.out::println);

    // 数组创建流
    int[] numbers = {2, 3, 5, 7, 11, 13};
    int sum = Arrays.stream(numbers).sum();

    // 文件创建流
    long uniqueWords = 0;
    try (Stream<String> lines = Files.lines(Paths.get("src/main/resources/a.txt"), Charset.defaultCharset())) {
      uniqueWords = lines.flatMap(line -> Arrays.stream(line.split(" "))).distinct().count();
      System.out.println(uniqueWords);
    } catch (IOException e) {
      e.printStackTrace();
    }

    //  Stream.iterate 和 Stream.generate 函数创建流
    Stream.iterate(0, n -> n + 2)
        .limit(10)
        .forEach(System.out::println);

    // 斐波纳契元组序列  iterate 的方法则是纯粹不变的：它没有修改现有状态，
    // 但在每次迭代时会创建新的元组
    Stream.iterate(new int[] {0, 1}, t -> new int[] {t[1], t[0] + t[1]})
        .limit(20)
        .forEach(t -> System.out.println("(" + t[0] + "," + t[1] + ")"));

    Stream.generate(Math::random)
        .limit(5)
        .forEach(System.out::println);

    IntStream ones = IntStream.generate(() -> 1);
    ones.limit(4).forEach(System.out::println);

    IntStream twos = IntStream.generate(new IntSupplier(){
      @Override
      public int getAsInt(){
        return 2;
      }
    });
    twos.limit(2).forEach(System.out::println);

    // 斐波那契数列
    IntSupplier fib = new IntSupplier(){
      private int previous = 0;
      private int current = 1;
      @Override
      public int getAsInt(){
        int oldPrevious = this.previous;
        int nextValue = this.previous + this.current;
        this.previous = this.current;
        this.current = nextValue;
        return oldPrevious;
      }
    };
    IntStream.generate(fib).limit(10).forEach(System.out::println);
  }
}
