package com.aqielife.java8.chapter3;

import com.aqielife.java8.bean.Dish;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * @Author: aqie
 * @Date: 2021-02-04 9:10
 * @Description:
 */
@Slf4j
public class StreamDemo {
  public static void main(String[] args) {
    // 过滤 排序 取名字
    List<Integer> list = Dish.menu.stream().filter(d -> d.getCalories() > 400)
        .sorted(Comparator.comparing(Dish::getCalories))
        .map(Dish::getName)
        .map(String::length)
        .collect(toList());
    log.info(" 返回每道菜名长度 list {}", list);

    Map<Dish.Type, List<Dish>> collect = Dish.menu.stream().filter(d -> d.getCalories() > 400)
        .sorted(Comparator.comparing(Dish::getCalories))
        .collect(groupingBy(Dish::getType));


    // 多核架构并行
    List<String> list2 = Dish.menu.parallelStream().filter(d -> d.getCalories() > 400)
        .sorted(Comparator.comparing(Dish::getCalories))
        .map(Dish::getName)
        .collect(toList());
    log.info("list2 {}", list2);

    // 取偶数去重
    List<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4);
    numbers.stream()
        .filter(i -> i % 2 == 0)
        .distinct()
        // 跳过第一个
        .skip(1)
        .forEach(System.out::println);

    // 打印数组字符长度
    List<String> words = Arrays.asList("Java 8", "Lambdas", "In", "Action");
    words.stream()
        .map(String::length)
        .forEach(System.out::print);

    String[] arrayOfWords = {"Hello", "World"};

    // 错误写法  map 返回的流实际上是 Stream<String[]> 类型
    List<String> words2 = Arrays.asList(arrayOfWords);
    List<String[]> charList = words2.stream()
        .map(word -> word.split(""))
        .distinct()
        .collect(toList());
    log.info("charList {}", charList);

    /**
     * 返回一张列表，列出里面各不相同的字符
     */
    // 接收数组产生字符流 而不是数组流
    Stream<String> stringStream = Arrays.stream(arrayOfWords);
    words2.stream().map(w -> w.split(""))
        .flatMap(Arrays::stream)
        .distinct()
        .forEach(System.out::print);
    System.out.println("---");
    /**
     * 给定两个数字列表，如何返回所有的数对
     * 并成功打印int 数组
     */
    List<Integer> numbers1 = Arrays.asList(1, 2, 3);
    List<Integer> numbers2 = Arrays.asList(3, 4);
    List<int[]> pairs =
        numbers1.stream()
            .flatMap(i -> numbers2.stream()
                .map(j -> new int[]{i, j})
            )
            .collect(toList());
    pairs.stream()
        .forEach(
            i -> {
              System.out.println(Arrays.toString(i));
              // System.out.println(Collections.singletonList(i));
            });
    log.info("pairs {}", pairs);

    int[] a = new int[]{1,2,3};
    log.info("arr {}", a);

    /**
     * 只打印能被三整除的数对
     */
    List<int[]> pairs2 =
        numbers1.stream()
            .flatMap(i -> numbers2.stream().filter( j -> (i + j) % 3 == 0 )
                .map(j -> new int[]{i, j})
            )
            .collect(toList());
    pairs2.stream()
        .forEach(
            i -> {
              System.out.println(Arrays.toString(i));
            });
  }
}
