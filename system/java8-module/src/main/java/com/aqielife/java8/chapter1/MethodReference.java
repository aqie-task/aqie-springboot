package com.aqielife.java8.chapter1;

import com.aqielife.java8.bean.Apple;
import com.aqielife.java8.chapter1.predicate.AppleFancyFormatter;
import com.aqielife.java8.comparator.AppleSizeComparator;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.aqielife.java8.utils.AppleUtils.filterApples;
import static com.aqielife.java8.utils.AppleUtils.prettyPrintApples;

/**
 * @Author: aqie @Date: 2021-01-29 11:40 @Description: https://zhuanlan.zhihu.com/p/151283740
 * 步骤1:引用静态方法 步骤2:引用对象方法 步骤3:引用容器中的对象的方法 步骤4:引用构造器
 */
@Slf4j
public class MethodReference {
  public static void printApple(Apple apple) {
    System.out.println(apple.getName());
  }

  public static void main(String... args) {

    List<Apple> apples =
        Arrays.asList(
            new Apple("a", "red", 100),
            new Apple("b", "green", 200),
            new Apple("c", "yellow", 300));
    // MethodReference::printApple      // 方法引用
    apples.forEach((res) -> printApple(res));
    apples.forEach(System.out::println);

    // 行为参数化 传入不同函数对列表过滤
    List<Apple> redApples = filterApples(apples, Apple::isRedApple);
    log.info("redApples {}", redApples);

    // 行为参数化 传入不同函数对列表过滤
    List<Apple> largeApples = filterApples(apples, (Apple a) -> a.getNumber() > 150);
    log.info("largeApples{}", largeApples);
    apples.forEach(apple -> printApple(apple));

    // 流式处理
    List<Apple> collect =
        apples.stream().filter((Apple a) -> a.getNumber() > 150).collect(Collectors.toList());
    log.info("collect {}", collect);

    // 根据多种方式打印输出
    prettyPrintApples(apples, new AppleFancyFormatter());

    Thread t = new Thread(() -> System.out.println("Hello world"));
    t.start();

    // Comparator
    apples.sort((Apple a, Apple b) -> a.getNumber()- b.getNumber());
    log.info("apples {}", apples);
    apples.sort(new AppleSizeComparator());
    log.info("apples {}", apples);
  }


}
