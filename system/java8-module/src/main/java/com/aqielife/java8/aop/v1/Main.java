package com.aqielife.java8.aop.v1;

import java.lang.reflect.Proxy;

/**
 * @Author aqie
 * @Date 2022/4/22 11:37
 * @desc
 */
public class Main {
    public static void main(String[] args) {
        ManSayHelloWorld sayHelloWorld = new ManSayHelloWorld();
        AOPHandle handle = new AOPHandle(sayHelloWorld);
        ISayHelloWorld i = (ISayHelloWorld) Proxy.newProxyInstance(ManSayHelloWorld.class.getClassLoader(),
                new Class[] { ISayHelloWorld.class }, handle);
        i.say();
    }
}