package com.aqielife.java8.chapter4;

import com.aqielife.java8.bean.Dish;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.aqielife.java8.bean.Dish.menu;
import static com.aqielife.java8.chapter4.Transaction.transactions;
import static com.aqielife.java8.utils.NumberUtils.isPrime;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;

/** @Author: aqie @Date: 2021-03-10 15:04 @Description: */
public class StreamProcessData {
  // Collection 、 Collector 和 collect

  public static void main(String[] args) {
    Map<Transaction.Currency, List<Transaction>> transactionsByCurrencies = new HashMap<>();
    for (Transaction transaction : transactions) {
      Transaction.Currency currency = transaction.getCurrency();
      List<Transaction> transactionsForCurrency = transactionsByCurrencies.get(currency);
      if (transactionsForCurrency == null) {
        transactionsForCurrency = new ArrayList<>();
        transactionsByCurrencies.put(currency, transactionsForCurrency);
      }
      transactionsForCurrency.add(transaction);
    }

    System.out.println(transactionsByCurrencies);



    Map<Transaction.Currency, List<Transaction>> transactionsByCurrencies2 =
        transactions.stream().collect(groupingBy(Transaction::getCurrency));
    System.out.println(transactionsByCurrencies2);


    long howManyDishes = menu.stream().collect(Collectors.counting());


    Comparator<Dish> dishCaloriesComparator =
        comparingInt(Dish::getCalories);

    // 热量最高的菜
    Optional<Dish> mostCalorieDish2 =
        menu.stream().collect(reducing(
            (d1, d2) -> d1.getCalories() > d2.getCalories() ? d1 : d2));
    Optional<Dish> mostCalorieDish =
        menu.stream()
            .collect(maxBy(dishCaloriesComparator));
    System.out.println(mostCalorieDish);

    // 计算菜单总热量
    int totalCalories = menu.stream().map(Dish::getCalories).reduce(0, (i, j) -> i + j);
    int totalCalories1 = menu.stream().collect(reducing(0, Dish::getCalories, (i, j) -> i + j));
    int totalCalories4 = menu.stream().collect(reducing(0, Dish::getCalories, Integer::sum));

    int totalCalories2 = menu.stream().collect(summingInt(Dish::getCalories));
    int totalCalories3 = menu.stream().mapToInt(Dish::getCalories).sum();
    double avgCalories =
        menu.stream().collect(averagingInt(Dish::getCalories));
    System.out.println(totalCalories1 + " " + avgCalories);

    // 连接字符串
    String shortMenu = menu.stream().map(Dish::getName).collect(joining(", "));
    System.out.println(shortMenu);

    String shortMenu2 = menu.stream().map(Dish::getName)
        .collect( reducing ( (s1, s2) -> s1 + s2 + ", " ) ).get();
    System.out.println(shortMenu2);

    String shortMenu3 = menu.stream()
        .collect( reducing( "",Dish::getName, (s1, s2) -> s1 + s2 + ", " ) );
    System.out.println(shortMenu3);


    // 分组
    Map<Dish.Type, List<Dish>> dishesByType =
        menu.stream().collect(groupingBy(Dish::getType));
    System.out.println(dishesByType);


    Map<CaloricLevel, List<Dish>> dishesByCaloricLevel = menu.stream().collect(
        groupingBy(dish -> {
          if (dish.getCalories() <= 400) {
            return CaloricLevel.DIET;
          } else if (dish.getCalories() <= 700) {
            return
                CaloricLevel.NORMAL;
          } else {
            return CaloricLevel.FAT;
          }
        } ));

    System.out.println(dishesByCaloricLevel);

    System.out.println(groupDishedByTypeAndCaloricLevel());

    // 统计每个菜单多少个菜
    Map<Dish.Type, Long> typesCount = menu.stream().collect(
        groupingBy(Dish::getType, counting()));
    System.out.println(typesCount);

    // 统计每种菜热量最高
    Map<Dish.Type, Optional<Dish>> mostCaloricByType =
        menu.stream()
            .collect(groupingBy(Dish::getType,
                maxBy(comparingInt(Dish::getCalories))));
    System.out.println(mostCaloricByType);

    //  去掉optional
    Map<Dish.Type, Dish> mostCaloricByType2 =
        menu.stream()
            .collect(groupingBy(Dish::getType,
                collectingAndThen(
                    maxBy(comparingInt(Dish::getCalories)),
                    Optional::get)));
    System.out.println(mostCaloricByType2);

    // 根据分类计算 总热量
    Map<Dish.Type, Integer> totalCaloriesByType =
        menu.stream().collect(groupingBy(Dish::getType,
            summingInt(Dish::getCalories)));
    System.out.println(totalCaloriesByType);

    Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType =
        menu.stream().collect(
            groupingBy(Dish::getType, mapping(
                dish -> { if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                else return CaloricLevel.FAT; },
                toSet() )));

    System.out.println(caloricLevelsByType);


    Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType2 =
        menu.stream().collect(
            groupingBy(Dish::getType, mapping(
                dish -> { if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                else return CaloricLevel.FAT; },
                toCollection(HashSet::new) )));
    System.out.println(caloricLevelsByType2);

    // 分区
    Map<Boolean, List<Dish>> partitionedMenu =
        menu.stream().collect(partitioningBy(Dish::isVegetarian));
    List<Dish> vegetarianDishes = partitionedMenu.get(true);
    System.out.println(partitionedMenu);

    List<Dish> vegetarianDishes2 =
        menu.stream().filter(Dish::isVegetarian).collect(toList());

    // 分区后分组
    Map<Boolean, Map<Dish.Type, List<Dish>>> vegetarianDishesByType =
        menu.stream().collect(
            partitioningBy(Dish::isVegetarian,
                groupingBy(Dish::getType)));


    // 找到素食和非素食中热量最高的菜
    Map<Boolean, Dish> mostCaloricPartitionedByVegetarian =
        menu.stream().collect(
            partitioningBy(Dish::isVegetarian,
                collectingAndThen(
                    maxBy(comparingInt(Dish::getCalories)),
                    Optional::get)));

    System.out.println(partitionPrimes(100));
  }

  public enum CaloricLevel { DIET, NORMAL, FAT }

  // 多几分组
  private static Map<Dish.Type, Map<CaloricLevel, List<Dish>>> groupDishedByTypeAndCaloricLevel() {
    return menu.stream().collect(
        groupingBy(Dish::getType,
            groupingBy((Dish dish) -> {
              if (dish.getCalories() <= 400) {
                return CaloricLevel.DIET;
              } else if (dish.getCalories() <= 700) {
                return CaloricLevel.NORMAL;
              } else {
                return CaloricLevel.FAT;
              }
            } )
        )
    );
  }

  // 前n个数字区分是否是素数(质数)
  public static Map<Boolean, List<Integer>> partitionPrimes(int n) {
    return IntStream.rangeClosed(2, n).boxed()
        .collect(
            partitioningBy(candidate -> isPrime(candidate)));
  }

}


