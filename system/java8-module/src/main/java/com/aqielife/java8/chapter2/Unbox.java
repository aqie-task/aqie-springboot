package com.aqielife.java8.chapter2;

import com.aqielife.java8.TriFunction;
import com.aqielife.java8.bean.Apple;
import com.aqielife.java8.bean.Color;
import com.aqielife.java8.bean.Fruit;
import com.aqielife.java8.bean.Orange;

import java.util.*;
import java.util.function.*;


/**
 * @Author: aqie
 * @Date: 2021-01-30 10:20
 * @Description:
 */
public class Unbox {
  public static void main(String[] args) {
    // 不会装箱
    IntPredicate evenNumbers = (int i) -> i % 2 == 0;
    evenNumbers.test(1000);

    Thread t = new Thread(() -> {
      System.out.println("Hello world");
    });
    t.start();

    final int a = 100;
    Runnable r = () -> {System.out.println("Tricky example" + a); };
    Thread t1 = new Thread(r);
    t1.start();


    List<String> list = new ArrayList<>();
    // Predicate返回了一个boolean T->boolean   T->void
    Predicate<String> p = s -> list.add(s);
    // Consumer返回了一个void     T->void
    Consumer<String> b = s -> list.add(s);

    // 方法引用
    Function<String, Integer> stringToInteger = (String s) -> Integer.parseInt(s);
    // list.forEach(stringToInteger);
    list.forEach((String s) -> Integer.parseInt(s));

    BiPredicate<List<String>, String> contains = (list2, element) -> list2.contains(element);
    BiPredicate<List<String>, String> contains2 = List::contains;

    // 构造函数引用
    Supplier<Apple> c1 = Apple::new;
    Apple a1 = c1.get();

    // 将构造函数引用传递给 map 方法
    List<Integer> numbers = Arrays.asList(7, 3, 4, 10);
    // 创建不同数量苹果
    List<Apple> apples = map(numbers, Apple::new);

    Apple apple = (Apple)giveMeFruit("apple", 10);
    System.out.println(apple.getNumber() + " apple");

    // 自定义函数式接口
    TriFunction<Integer, Integer, Integer, Color> colorFactory = Color::new;
    Color apply = colorFactory.apply(1, 0, 0);
    System.out.println("color" + apply);
  }

  public static List<Apple> map(List<Integer> list,
                                Function<Integer, Apple> f){
    List<Apple> result = new ArrayList<>();
    for(Integer e: list){
      result.add(f.apply(e));
    }
    return result;
  }

  static Map<String, Function<Integer, Fruit>> map = new HashMap<>();
  static {
    map.put("apple", Apple::new);
    map.put("orange", Orange::new);
  }

  // 传入字符串 返回对应类
  public static Fruit giveMeFruit(String fruit, Integer number){
    return map.get(fruit.toLowerCase())
        .apply(number);
  }
}
