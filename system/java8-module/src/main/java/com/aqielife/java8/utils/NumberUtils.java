package com.aqielife.java8.utils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 * @Author: aqie
 * @Date: 2021-03-10 9:49
 * @Description:
 */
public class NumberUtils {
  public static final List<Integer> numbers = Arrays.asList(1,2,3,4,5);

  public static boolean isPrime(int candidate) {
    int candidateRoot = (int) Math.sqrt((double) candidate);
    return IntStream.rangeClosed(2, candidateRoot)
        .noneMatch(i -> candidate % i == 0);
  }

  public static boolean isPrime(List<Integer> primes, int candidate){
    int candidateRoot = (int) Math.sqrt((double) candidate);
    return takeWhile(primes, i -> i <= candidateRoot)
        .stream()
        .noneMatch(p -> candidate % p == 0);
  }
  public static <A> List<A> takeWhile(List<A> list, Predicate<A> p) {
    int i = 0;
    for (A item : list) {
      if (!p.test(item)) {
        return list.subList(0, i);
      }
      i++;
    }
    return list;
  }

  /**
   * @param adder
   * @param n
   * @return long
   * @Function 测量对前n个自然数求和的函数的性能 它会对传给方法的 long 应用函数10次，记录 每次执行的时间（以毫秒为单位），并返回最短的一次执行时间
   */
  public static long measureSumPerf(Function<Long, Long> adder, long n) {
    long fastest = Long.MAX_VALUE;
    for (int i = 0; i < 10; i++) {
      long start = System.nanoTime();
      long sum = adder.apply(n);
      long duration = (System.nanoTime() - start) / 1_000_000;
      System.out.println("Result: " + sum);
      if (duration < fastest) fastest = duration;
    }
    return fastest;
  }
}
