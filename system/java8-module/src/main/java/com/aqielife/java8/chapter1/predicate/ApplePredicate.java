package com.aqielife.java8.chapter1.predicate;

/**
 * @Author: aqie
 * @Date: 2021-01-29 16:15
 * @Description:
 */
public interface ApplePredicate<T> {
  String accept(T t);
}
