package com.aqielife.java8.chapter8.expand;


import static com.aqielife.java8.chapter8.Util.*;

/**
 * 以枚举类型定义的折扣代码
 */
public class Discount {

    public enum Code {
        NONE(0), SILVER(5), GOLD(10), PLATINUM(15), DIAMOND(20);

        private final int percentage;

        Code(int percentage) {
            this.percentage = percentage;
        }
    }

    public static String applyDiscount(Quote quote) {
        return quote.getShopName() + " price is " + Discount.apply(quote.getPrice(), quote.getDiscountCode());
    }

    /**
     * 计算折扣
     * @param price
     * @param code
     * @return
     */
    private static double apply(double price, Code code) {
        randomDelay();
        return format(price * (100 - code.percentage) / 100);
    }
}
