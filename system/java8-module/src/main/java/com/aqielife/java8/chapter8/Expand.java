package com.aqielife.java8.chapter8;

import java.util.*;

/**
 * @Author: aqie
 * @Date: 2021-03-11 15:46
 * @Description:
 */
public class Expand {

  private Map<String, String> cache = new HashMap<>();
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
    numbers.replaceAll(x -> x * 2);
    System.out.println(numbers);

    numbers = new ArrayList<>(numbers);
    numbers.removeIf(i -> i > 3);
    System.out.println(numbers);
  }

  public String getData(String url){
    String data = cache.get(url);
    if(data == null){
      data = getData(url);
      cache.put(url, data);
    }
    return data;
  }

  public String getData2(String url){
    return cache.computeIfAbsent(url, this::getData2);
  }
}
