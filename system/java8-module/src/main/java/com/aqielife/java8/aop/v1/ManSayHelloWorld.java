package com.aqielife.java8.aop.v1;

/**
 * @Author aqie
 * @Date 2022/4/22 11:36
 * @desc
 */
public class ManSayHelloWorld implements ISayHelloWorld{
    @Override
    public String say() {
        System.out.println("hello world");
        return "hello world";
    }
}
