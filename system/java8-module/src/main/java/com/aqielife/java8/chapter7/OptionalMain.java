package com.aqielife.java8.chapter7;

import java.util.*;

import static java.util.stream.Collectors.toSet;

public class OptionalMain {
  public static void main(String[] args) {
    // 依据一个非空值创建 Optional
    // Optional<Car> optCar = Optional.of(car);

    // 依据一个非空值创建 Optional
    // Optional<Car> optCar2 = Optional.ofNullable(car);

    // 依赖注入 + 控制反转
    Insurance insurance = new Insurance("CambridgeInsurance");
    Car car = new Car(Optional.of(insurance));
    Person person = new Person(Optional.of(car), 50);
    System.out.println(getCarInsuranceName(Optional.of(person)));
    System.out.println(getCarInsuranceName(Optional.of(person), 30));

    Optional<Insurance> optInsurance = Optional.ofNullable(insurance);
    // 判断是否为null 再取值
    Optional<String> name = optInsurance.map(Insurance::getName);
    // System.out.println(name.isPresent() ? name.get() : null);
    System.out.println(name.orElse(null));

    // 进行过滤
    optInsurance.filter(insurance1 ->
          "CambridgeInsurance".equals(insurance1.getName()))
          .ifPresent(x -> System.out.println("ok"));
  }

    public static String getCarInsuranceName(Optional<Person> person) {
        return person.flatMap(Person::getCar)
                     .flatMap(Car::getInsurance)
                     .map(Insurance::getName)
                     .orElse("Unknown");
    }

    public Optional<Insurance> nullSafeFindCheapestInsurance(
        Optional<Person> person, Optional<Car> car) {
        return person.flatMap(p -> car.map(c -> findCheapestInsurance(p, c)));
    }

    public Insurance findCheapestInsurance(Person person, Car car) {
        // 不同的保险公司提供的查询服务
        // 对比所有数据
        Insurance cheapestCompany = null;
        return cheapestCompany;
    }

    public static Set<String> getCarInsuranceNames(List<Person> persons) {
        return persons.stream()
                      .map(Person::getCar)
                      .map(optCar -> optCar.flatMap(Car::getInsurance))
                      .map(optInsurance -> optInsurance.map(Insurance::getName))
                      .flatMap(Optional::stream)
                      .collect(toSet());
    }

    public static String getCarInsuranceName(Optional<Person> person, int minAge) {
        return person.filter(p -> p.getAge() >= minAge)
            .flatMap(Person::getCar)
            .flatMap(Car::getInsurance)
            .map(Insurance::getName)
            .orElse("Unknown");
    }
}
