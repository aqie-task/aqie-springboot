package com.aqielife.java8.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author: aqie
 * @Date: 2021-01-29 10:17
 * @Description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Apple extends Fruit implements Comparable<Apple>{
    private BigDecimal weight;

    private int number;

    private int size;

    private String name;

    private String color;

    public Apple(int number) {
        this.number = number;
    }

    public Apple(String name) {
        this.name = name;
    }

    public Apple(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Apple(String name, String color, int number) {
        this.name = name;
        this.color = color;
        this.number = number;
    }


    public Apple(BigDecimal weight) {
        this.weight = weight;
    }

    public Apple(BigDecimal weight, int number) {
        this.weight = weight;
        this.number = number;
    }

    public Apple(BigDecimal weight, int number, int size) {
        this.weight = weight;
        this.number = number;
        this.size = size;
    }
    public Apple(String name, BigDecimal weight, int number, int size) {
        this(weight, number, size);
        this.name = name;
    }



    /**
     * -1 a < b
     * 0 a == b
     * 1 a > b
     * @param o
     * @return
     */
    @Override
    public int compareTo(Apple o) {
        // return this.weight.compareTo(o.getWeight());  // 正序
        // return this.number - o.number;                   // 正序
        return 0;
    }

    public boolean isRedApple(){
        return "red".equals(color);
    }


}
