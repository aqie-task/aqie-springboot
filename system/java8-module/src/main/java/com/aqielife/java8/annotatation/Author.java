package com.aqielife.java8.annotatation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @Author: aqie
 * @Date: 2021-01-29 15:02
 * @Description:
 */
@Repeatable(Authors.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Author {

  String name();

}
