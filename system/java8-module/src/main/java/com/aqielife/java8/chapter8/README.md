
#### CompletableFuture vs Future
1. 为你的客户提供异步API
2. 使用了同步API的代码变为非阻塞代码
3. 以响应式的方式处理异步操作的完成事件

#### 高级功能
1. 将两个异步计算合并为一个,这两个异步计算之间相互独立，同时第二个又依赖于第一个的结果。
2. 等待 Future 集合中的所有任务都完成。
3. 仅等待 Future 集合中最快结束的任务完成（有可能因为它们试图通过不同的方式计算同一个值），并返回它的结果。
4. 通过编程方式完成一个 Future 任务的执行（即以手工设定异步操作结果的方式）。
5. 应对 Future 的完成事件（即当 Future 的完成事件发生时会收到通知，并能使用 Future 计算的结果进行下一步的操作，不只是简单地阻塞等待操作的结果）

#### 模拟业务场景
- 查询多个在线商店，依据给定的产品或服务找出最低的价格

#### 最佳实践
- 计算密集型的操作，并且没有I/O，那么推荐使用 Stream 接口
- 并行的工作单元还涉及等待I/O的操作（包括网络连接等待），那么使用CompletableFuture 灵活性更好