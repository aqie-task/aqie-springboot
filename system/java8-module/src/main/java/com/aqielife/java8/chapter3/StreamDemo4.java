package com.aqielife.java8.chapter3;

import com.aqielife.java8.bean.Dish;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.aqielife.java8.bean.Dish.menu;

/**
 * @Author: aqie
 * @Date: 2021-03-10 14:10
 * @Description: 数值流
 */
public class StreamDemo4 {
  public static void main(String[] args) {

    // 装箱成本
    Integer totalCalories = menu.stream()
        // Dish::getCalories
        .map(d -> d.getCalories())
        // Integer::sum
        .reduce(0, (a, b) -> a + b);

    System.out.println(totalCalories);

    // 原始类型流特化  mapToInt 、 mapToDouble 和 mapToLong
    int calories = menu.stream()
        .mapToInt(Dish::getCalories)
        .sum();

    // 数值流和原始流转换
    IntStream intStream = menu.stream().mapToInt(Dish::getCalories);
    Stream<Integer> stream = intStream.boxed();

    // 数值流范围
    IntStream.rangeClosed(1, 100).filter(n -> n % 2 == 0).forEach(System.out::println);

    // 数值流应用
    Stream<int[]> pythagoreanTriples =
        IntStream.rangeClosed(1, 100).boxed()
            .flatMap(a ->
                IntStream.rangeClosed(a, 100)
                    .filter(b -> Math.sqrt(a*a + b*b) % 1 == 0)
                    .mapToObj(b ->
                        new int[]{a, b, (int)Math.sqrt(a * a + b * b)})
            );
    pythagoreanTriples.limit(5)
        .forEach(t ->
            System.out.println(t[0] + ", " + t[1] + ", " + t[2]));

    Stream<double[]> pythagoreanTriples2 =
        IntStream.rangeClosed(1, 100).boxed()
            .flatMap(a ->
                IntStream.rangeClosed(a, 100)
                    .mapToObj(
                        b -> new double[]{a, b, Math.sqrt(a*a + b*b)})
                    .filter(t -> t[2] % 1 == 0));
  }
}
