package com.aqielife.java8.utils;

import com.aqielife.java8.bean.Apple;
import com.aqielife.java8.chapter1.predicate.ApplePredicate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @Author: aqie
 * @Date: 2021-02-03 16:27
 * @Description:
 */
public class AppleUtils {
  public static List<Apple> apples(){
    return Arrays.asList(
        new Apple(new BigDecimal("1.00"), 1, 30),
        new Apple(new BigDecimal("2.00"), 3, 20),
        new Apple(new BigDecimal("2.00"), 4, 20),
        new Apple(new BigDecimal("3.90"), 2, 10));
  }

  public static List<Apple> colorApples(){
    return Arrays.asList(
        new Apple("a", "red", 100),
        new Apple("b", "green", 200),
        new Apple("c", "yellow", 300),
        new Apple("d", "red", 400));
  }

  /**
   * Function<Apple,Boolean>
   * 添加泛型
   * @param list
   * @param p 谓词（predicate） 接受函数 返回 bool
   * @return
   */
  public static <T> List<T> filterApples(List<T> list, Predicate<T> p) {
    List<T> result = new ArrayList<>();
    for (T e : list) {
      if (p.test(e)) {
        result.add(e);
      }
    }
    return result;
  }

  public static <T> void prettyPrintApples(List<T> list, ApplePredicate<T> p) {
    for (T e : list) {
      String accept = p.accept(e);
      System.out.println("a: " + accept);
    }
  }
}
