package com.aqielife.java8.chapter3;

import com.aqielife.java8.bean.Dish;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.aqielife.java8.bean.Dish.menu;
import static com.aqielife.java8.utils.NumberUtils.numbers;

/**
 * @Author: aqie
 * @Date: 2021-03-09 17:21
 * @Description:
 */
public class StreamDemo2 {
  // 查找匹配  allMatch 、 anyMatch 、 noneMatch 、 findFirst 和 findAny
  public static void main(String[] args){
    findVegetarian();

    firstSquareDivisibleByThree();

    sum();

    // 返回最大值
    Optional<Integer> max = numbers.stream().reduce((a, b) -> a > b ? a : b);
    System.out.println(max);

    // 返回最小值 Integer::min
    Optional<Integer> min = numbers.stream().reduce((a, b) -> a < b ? a : b);
    System.out.println(min);

    // 计算流中多少道菜
    Integer totalDish = menu.stream().map(d -> 1).reduce(0, Integer::sum);
    System.out.println(totalDish);
    long totalDish2 = menu.stream().count();
    long totalDish3 = menu.size();
  }

  private static void sum() {
    int sum = numbers.stream().reduce(0, (a, b) -> a + b);
    System.out.println(sum);
  }

  private static void firstSquareDivisibleByThree() {
    List<Integer> someNumbers = Arrays.asList(1, 2, 3, 4, 5);
    Optional<Integer> firstSquareDivisibleByThree =
        someNumbers.stream()
            .map(x -> x * x)
            .filter(x -> x % 3 == 0)
            .findFirst(); // 9
    boolean present = firstSquareDivisibleByThree.isPresent();
    System.out.println(present);
  }

  private static void findVegetarian() {
    Optional<Dish> dish =
        menu.stream()
            .filter(Dish::isVegetarian)
            .findAny();
    dish.ifPresent(d -> System.out.println(d.getName()));
  }

}
