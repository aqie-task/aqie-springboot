package com.aqielife.java8.chapter1.predicate;

import com.aqielife.java8.bean.Apple;

/**
 * @Author: aqie
 * @Date: 2021-01-29 16:21
 * @Description:
 */
public class AppleFancyFormatter implements ApplePredicate<Apple>{
  @Override
  public String accept(Apple apple) {
    return (apple.getNumber() > 150 ? "heavy" : "light") + " " + apple.getColor();
  }

}
