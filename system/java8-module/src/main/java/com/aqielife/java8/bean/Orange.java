package com.aqielife.java8.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: aqie
 * @Date: 2021-01-30 15:43
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Orange extends Fruit{
  private int number;

  private String name;

  public Orange(int number) {
    this.number = number;
  }
}
