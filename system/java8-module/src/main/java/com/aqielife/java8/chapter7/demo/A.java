package com.aqielife.java8.chapter7.demo;

/**
 * @Author: aqie
 * @Date: 2021-03-11 17:18
 * @Description:
 */
public interface A {
  default void hello() {
    System.out.println("Hello from A");
  }

  default Number getNumber(){
    return 10;
  }
}
