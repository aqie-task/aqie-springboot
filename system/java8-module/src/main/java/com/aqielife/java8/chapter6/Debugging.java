package com.aqielife.java8.chapter6;


import java.util.*;

import static java.util.stream.Collectors.toList;

public class Debugging{
    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(12, 2), null);
        points.stream().map(p -> p.getX()).forEach(System.out::println);
    }


    private static class Point{
        private int x;
        private int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public static List<Point> moveAllPointsRightBy(List<Point> points, int x){
            return points.stream()
                .map(p -> new Point(p.getX() + x, p.getY()))
                .collect(toList());
        }
    }
}
