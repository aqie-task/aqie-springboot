package com.aqielife.java8.chapter6;

/**
 * @Author: aqie
 * @Date: 2021-03-11 11:29
 * @Description:
 */
public class Demo {

  public static void main(String[] args) {
    doSomething((Task) () -> System.out.println("runnable task 都会匹配"));


  }

  public static void doSomething(Runnable r){ r.run(); }
  public static void doSomething(Task a){ a.execute(); }
}
