package com.aqielife.java8.chapter1;

import com.aqielife.java8.bean.Apple;
import com.aqielife.java8.utils.AppleUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

/** @Author: aqie @Date: 2021-01-29 10:16 @Description: 比较器使用 */
public class CompareDemo {
  public static void main(String[] args) {
    File[] hiddenFiles = new File(".").listFiles(File::isHidden);
    Apple[] apples =
        new Apple[] {
          new Apple(new BigDecimal("1.00")), new Apple(new BigDecimal("2.00")),
        };

    List<Apple> inventory = AppleUtils.apples();
    // Comparable
    // inventory.sort(reverseOrder());
    // inventory.sort(naturalOrder());

    // Comparator
    // inventory.sort(comparing(Apple::getWeight));                      // 正序
    inventory.sort((o1, o2) -> o2.getWeight().compareTo(o1.getWeight())); // 倒序
    // inventory.sort(new AppleComparator());
    System.out.println(inventory);
  }
}
