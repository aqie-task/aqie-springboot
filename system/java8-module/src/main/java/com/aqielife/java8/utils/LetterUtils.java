package com.aqielife.java8.utils;

import java.util.function.DoubleFunction;
import java.util.function.Function;

/**
 * @Author: aqie
 * @Date: 2021-02-03 17:08
 * @Description:
 */
public class LetterUtils {
  public static String addHeader(String text){
    return "From Raoul, Mario and Alan: " + text;
  }
  public static String addFooter(String text){
    return text + " Kind regards";
  }
  public static String checkSpelling(String text){
    return text.replaceAll("labda", "lambda");
  }

  public static <T,R> R handleLetter(T t, Function<T,R> f){
    return f.apply(t);
  }

  // f(3,7)f(x)d(x)
  public static double integrate(DoubleFunction<Double> f, double a, double b) {
    return (f.apply(a) + f.apply(b)) * (b-a) / 2.0;
  }
}