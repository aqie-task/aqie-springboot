package com.aqielife.java8.chapter7;

import java.util.*;

public class Person {

    private Optional<Car> car;

    private int age;

    public Person(Optional<Car> car) {
        this.car = car;
    }

    public Person(Optional<Car> car, int age) {
        this.car = car;
        this.age = age;
    }

    public Optional<Car> getCar() {
        return car;
    }

    public int getAge() {
        return age;
    }
}
