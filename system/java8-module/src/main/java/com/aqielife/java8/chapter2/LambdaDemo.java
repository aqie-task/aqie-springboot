package com.aqielife.java8.chapter2;

import com.aqielife.java8.bean.Apple;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.ToIntBiFunction;

/**
 * @Author: aqie
 * @Date: 2021-01-30 8:58
 * @Description:
 */
public class LambdaDemo {

  private static String FILE_PATH = "lambdasinaction/chap3/data.txt";
  public static void main(String[] args) throws IOException {
    // () -> "Raoul"
    // (Integer i) -> {return "Alan" + i};
    // 布尔表达式
    // (List<String> list) -> list.isEmpty()
    // 创建对象
    // () -> new Apple();
    // 消费一个对象
    // (Apple a) -> { System.out.println(a.getWeight()); }
    // 从一个对象中选择/抽取
    // (String s) -> s.length()
    // 组合两个值
    // (int a, int b) -> a * b
    // 比较两个对象
    // (Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight())

    // 常用函数式接口
    // Comparator Runnable Callable PrivilegedAction


    /*

    String twoLines = processFile((BufferedReader b) -> b.readLine() + b.readLine());
    System.out.println(twoLines);*/
    readFile();
    System.out.println("---");
    // 1. 行为参数化
    String oneLine = processFile((BufferedReader b) -> b.readLine());
    System.out.println(oneLine);
    System.out.println("---");
    String twoLines = processFile((BufferedReader b) -> b.readLine() + b.readLine());
    System.out.println(twoLines);



    // 同一个lambda 可用于多个函数式接口  参数类型一致
    // 不带类型推断
    Comparator<Apple> c1 =
        (Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight());
    ToIntBiFunction<Apple, Apple> c2 =
        (Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight());
    BiFunction<Apple, Apple, Integer> c3 =
        (Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight());


  }

  public static void readFile() throws IOException {
    final Resource resource = new ClassPathResource(FILE_PATH);
    try (BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
      String data;
      while ((data = br.readLine()) != null) {
        System.out.println(data);
      }
    }
  }


  /**
   * 3. 执行行为
   * 抽象接口 分别对应入参 出参
   * 接口作为函数参数
   */
  public static String processFile(BufferedReaderProcessor p) throws IOException {
    final Resource resource = new ClassPathResource(FILE_PATH);
    try(BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()))){
      return p.process(br);
    }
  }

  /**
   * 2. 函数式接口传递行为
   */
  public interface BufferedReaderProcessor{
    String process(BufferedReader b) throws IOException;
  }
}
