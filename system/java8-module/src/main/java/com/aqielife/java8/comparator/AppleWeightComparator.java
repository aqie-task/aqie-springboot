package com.aqielife.java8.comparator;

import com.aqielife.java8.bean.Apple;

import java.util.Comparator;

/**
 * @Author: aqie
 * @Date: 2021-01-29 11:17
 * @Description:
 */
public class AppleWeightComparator implements Comparator<Apple> {

  @Override
  public int compare(Apple o1, Apple o2) {
    // 正序
    return o1.getWeight().compareTo(o2.getWeight());
  }
}
