package com.aqielife.java8.chapter2;

import com.aqielife.java8.bean.Apple;
import com.aqielife.java8.utils.LetterUtils;
import com.aqielife.java8.utils.AppleUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.aqielife.java8.utils.AppleUtils.filterApples;

/**
 * @Author: aqie
 * @Date: 2021-02-03 16:24
 * @Description:
 */
@Slf4j
public class LambdaComplex {

  public static void main(String[] args) {
    // 1. 比较器复合
    List<Apple> apples = AppleUtils.apples();
    // 逆序排列
    apples.sort(Comparator.comparing(Apple::getSize).reversed());
    log.info("apples {}", apples);
    apples.sort(Comparator.comparing(Apple::getSize)
        .reversed()
        .thenComparing(Apple::getNumber));

    // 2. 谓词复合 negate 、 and 和 or
    List<Apple> colorApples = AppleUtils.colorApples();

    Predicate<Apple> redApple = (a -> "red".equals(a.getColor()));
    Predicate<Apple> redAndMuchAppleOrGreenPredicate = redApple.and(a -> a.getNumber() > 150)
        .or(a -> "green".equals(a.getColor()));
    List<Apple> redAndMuchAppleOrGreen = filterApples(colorApples, redAndMuchAppleOrGreenPredicate);
    log.info("redAndMuchAppleOrGreen {}", redAndMuchAppleOrGreen);


    // 3. 函数复合 Function  andThen 和 compose
    Function<Integer, Integer> f = x -> x * 2;
    Function<Integer, Integer> g = x -> x + 1;
    // g(f(x))
    Function<Integer, Integer> h = f.andThen(g);
    int result = h.apply(1);
    log.info(""+result);
    Function<Integer, Integer> h2 = f.compose(g);
    log.info(""+h2.apply(1));

    // 4. 实战 流处理字符串
    Function<String, String> addHeader = LetterUtils::addHeader;
    Function<String, String> transformationPipeline
        = addHeader.andThen(LetterUtils::checkSpelling)
        .andThen(LetterUtils::addFooter);

    String str = "hello labda";
    String s = LetterUtils.handleLetter(str, transformationPipeline);
    log.info(""+s);
  }





}
