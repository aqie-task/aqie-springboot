package com.aqielife.java8.comparator;

import com.aqielife.java8.bean.Apple;

import java.util.Comparator;

/**
 * @Author: aqie
 * @Date: 2021-01-29 11:07
 * @Description:
 */
public class AppleSizeComparator implements Comparator<Apple> {
  // a-b 默认正序
  @Override
  public int compare(Apple o1, Apple o2) {
    return (o1.getSize() - o2.getSize());
  }
}
