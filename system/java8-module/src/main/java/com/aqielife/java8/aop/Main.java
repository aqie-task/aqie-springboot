package com.aqielife.java8.aop;

/**
 * @Author aqie
 * @Date 2022/4/22 11:35
 * @desc
 */
public class Main {
    public static void main(String[] args) {
        CglibProxy proxy = new CglibProxy();
        //通过生成子类的方式创建代理类
        SayHello proxyImp = (SayHello)proxy.getProxy(SayHello.class);
        proxyImp.say();
    }
}