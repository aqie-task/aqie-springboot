package com.aqielife.java8.chapter7.demo;

/**
 * @Author: aqie
 * @Date: 2021-03-11 17:18
 * @Description:
 */
public interface B{
  default void hello() {
    System.out.println("Hello from B");
  }

  default Integer getNumber(){
    return 42;
  }
}
